<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest');

Auth::routes();

Route::get('user/profile', 'HomeController@profile')->name('user.profile');

Route::patch('user/update', 'HomeController@update')->name('user.update');

Route::get('register/bank', 'Auth\BankRegisterController@showBankRegistration')->name('register.bank');

Route::post('register/bank', 'Auth\BankRegisterController@register')->name('register.bank');

Route::get('register/agent', 'Auth\AgentRegisterController@showRegistrationForm')->name('register.agent')->middleware('hasInvitation');

Route::post('register/agent', 'Auth\AgentRegisterController@register')->name('register.agent');

Route::get('register/advisor', 'Auth\AdvisorRegisterController@showRegistrationForm')->name('register.advisor')->middleware('hasInvitation');

Route::post('register/advisor', 'Auth\AdvisorRegisterController@register')->name('register.advisor');


Route::get('register/user', 'Auth\UserRegisterController@showRegistrationForm')->name('register.user')->middleware('hasInvitation');

Route::post('register/user', 'Auth\UserRegisterController@register')->name('register.user');


Route::get('/home', 'HomeController@index')->name('home');

Route::resource('loan_application', 'LoanApplicationController');



Route::get('bank', 'BankController@index')->name('bank.index');

Route::group(['middleware' => ['role:bank', 'permission:create packages|edit packages|delete packages']], function() {

    Route::get('packages', 'PackagesController@index')->name('packages.index');

    Route::post('packages', 'PackagesController@store')->name('packages.store');

    Route::get('packages/{packages}/edit', 'PackagesController@edit')->name('packages.edit');

    Route::patch('packages/{packages}', 'PackagesController@update')->name('packages.update');

    Route::delete('packages/{packages}', 'PackagesController@destroy')->name('packages.destroy');

});





Route::post('bank/{loan_application}/{package}', 'LoanApplicationController@accept')->name('bank.accept');

Route::get('loan_applications', 'UsersApplicationController@show')->name('loan_applications');

Route::get('loan_applications/logs', 'UsersApplicationController@logs')->name('user.logs');

Route::get('loan_applications/offers/{loan_application}', 'UsersApplicationController@show_offers')->name('show_loan_offers');

Route::get('bank/user_applications', 'BankController@user_applications')->name('user_applications.fetch');

Route::get('bank/logs', 'BankController@logs')->name('bank.logs');

Route::get('bank/agents', 'AgentController@index')->name('bank.agents');

Route::post('loan_applications/offers/declined/{bank_verdict}', 'UsersApplicationController@destroy')->name('offer_rejected');


Route::group(['middleware' => ['role:admin']], function() {

  Route::get('admin', 'AdminController@index')->name('admin.index');

  Route::get('admin/banks', 'AdminController@bank')->name('admin.banks');

  Route::patch('admin/banks/approve/{user}', 'AdminController@bank_approve')->name('admin.bank.approve');

  Route::patch('admin/banks/disable/{user}', 'AdminController@bank_disable')->name('admin.bank.disable');

  Route::get('admin/users', 'AdminController@users')->name('admin.users');
});

Route::group(['middleware' => ['permission:send messages|video chat']], function() {
  Route::get('chat/book', 'ChatController@book')->name('chat.book');
  Route::get('/chat/home', 'ChatController@index')->name('chat.home');
  Route::get('/chat/{id}', 'ChatController@chat')->name('chat');
  Route::get('/group/chat/{id}', 'ChatController@groupChat')->name('group.chat');

  Route::post('/chat/message/send', 'ChatController@send')->name('chat.send');
  Route::post('/chat/message/send/file', 'ChatController@sendFilesInConversation')->name('chat.send.file');
  Route::post('/group/chat/message/send', 'ChatController@groupSend')->name('group.send');
  Route::post('/group/chat/message/send/file', 'ChatController@sendFilesInGroupConversation')->name('group.send.file');

  Route::get('/accept/message/request/{id}' , function ($id){
      Chat::acceptMessageRequest($id);
      return redirect()->back();
  })->name('accept.message');

  Route::post('/trigger/{id}' , function (\Illuminate\Http\Request $request , $id) {
      Chat::startVideoCall($id , $request->all());
  });

  Route::post('/group/chat/leave/{id}' , function ($id) {
      Chat::leaveFromGroupConversation($id);
  });

});

Route::group(['middleware' => ['role:bank', 'permission:check agent activity']], function() {
  Route::resource('bank/agent', 'AgentController');
});

Route::get('bank/link', 'AgentController@sendInvitation')->name('send.invitation');

Route::get('bank/advisor/link', 'AdvisorController@sendInvitation')->name('send.advisor.invitation');

Route::group(['middleware' => ['auth', 'role:bank'], 'prefix' => 'invitations'], function() {

  Route::get('/' , 'InvitationsController@index')->name('showInvitations');

  Route::post('/agent', 'InvitationsController@store')->name('storeInvitation');

  Route::get('/advisor' , 'AdvisorController@invites')->name('showAdvisorInvitations');

  Route::post('/advisor', 'AdvisorController@store')->name('storeAdvisorInvitation');

});

Route::group(['middleware' => ['auth', 'role:bank']], function() {
  Route::post('/agent/disable/{user}' , 'AgentController@agent_disable')->name('disable_agent');

  Route::post('/agent/enable/{user}' , 'AgentController@agent_enable')->name('enable_agent');

  Route::get('/advisors', 'AdvisorController@index')->name('advisors.index');
});

Route::post('/loan/application', 'HomePageController@store')->name('store.loan');
