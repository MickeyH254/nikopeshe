<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoanStatus extends Model
{
    protected $fillable = [
      'user_id', 'bank_verdict_id', 'status'
    ];

    public $rejected = "Rejected";

    public $accepted = "Accepted";
}
