<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserLoanStatus;

class BankVerdict extends Model
{
    protected $fillable = [
      'application_id', 'package_id', 'bank_id', 'status'
    ];
    public $declined = "Declined";

    public $accepted = "Accepted";

    public function applications()
    {
      return $this->hasMany(LoanApplication::class, 'application_id');
    }

    public function users() {
      return $this->hasOne(User::class, 'id', 'bank_id');

    }

    public function package() {
      return $this->hasOne(Package::class, 'id', 'package_id');
    }

    public function user_status() {
      return $this->hasOne(UserLoanStatus::class);
    }

}
