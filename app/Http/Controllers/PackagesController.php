<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PackageStoreRequest;
use Illuminate\Support\Facades\Storage;
use App\Package;
use Session;

class PackagesController extends Controller
{
    public function store(PackageStoreRequest $request) {

      $package = new Package;

      $uploadedFile = $request->file('file');

      //Storage::disk('local')->put($uploadedFile, 'Contents');

      $package->bank_id = \Auth::user()->id;
      $package->title = $request->input('title');
      $package->interest = $request->input('interest');
      $package->duration = $request->input('duration');
      $package->terms_and_conditions = $uploadedFile;

      $package->save();

        // $package = request()->validate([
        //   'bank_id' => ['required'],
        //   'title' => ['required', 'min:3', 'max:255'],
        //   'interest' => ['required'],
        //   'duration' => ['required', 'min:1'],
        //   'terms_and_conditions' => ['required']
        // ]);
        //Package::create($package);
        //P
        Session::flash('package_created', 'Package created successfully');

        return redirect()->back();

    }


    public function index() {
      return view('bank.create_package');
    }

    public function edit($package) {
      $my_package = Package::find($package);

      return view('bank.edit_package', compact('my_package'));
    }

    public function update(Package $package) {
      $package->update(['title', 'interest', 'duration', 'terms_and_conditions']);

      Session::flash('package_update', 'Package created successfully');

      return redirect()->back();
    }

    public function destroy(Package $package) {
      $package->delete();

      Session::flash('package_destroy', 'Package created successfully');

      return redirect()->back();
    }
}
