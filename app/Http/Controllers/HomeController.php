<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $login_user = \Auth::user();

        if ($login_user->hasRole('user')) {
          return view ('welcome');
        }

        elseif ($login_user->hasRole('bank')) {
          return redirect()->route('bank.index');
        }

        elseif ($login_user->hasRole('admin')) {
          return view ('admin.index');
        }

        elseif ($login_user->hasRole('agent')) {
          return view ('bank.index');
        }

    }

    public function profile() {
      return view('users.profile');
    }

    public function update() {
      $user_update = new User;

      $user = request()->validate([
        'name' => ['required', 'min:3', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255'],
        'phone_number' => ['required', 'min:10'],
        'id_number' => ['required', 'min:7']
      ]);
      $user_update->update($user);

      return redirect()->back();
    }
}
