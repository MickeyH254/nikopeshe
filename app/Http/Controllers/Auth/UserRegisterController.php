<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserInvitation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class UserRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'id_number' => ['required', 'min:7', 'unique:users'],
          'phone_number' => ['required', "min:10", 'max:10', 'unique:users']
        ]);
    }
    public function showRegistrationForm(Request $request) {
      $invitation_token =$request->get('invitation_token');
      $invitation = UsersInvitation::where('invitation_token', $invitation_token)->firstOrFail();

      $user = User::where('id', 'like', $invitation->user_id)->first();

      $email = $user->email;
      $name = $user->name;
      $phone_number = $user->phone_number;
      $id_number = $user->id_number;

      return view('auth.register_users', compact('email', 'name', 'phone_number', 'id_number'));
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      return User::update([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => Hash::make($data['password']),
          'id_number' => $data['id_number'],
          'phone_number' => $data['phone_number']
      ])->assignRole('user');

    }
}
