<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoanApplication;
use App\UsersInvitation;
use App\User;
use App\Mail\SendInvitations;
use Session;

class HomePageController extends Controller
{
    public function store(Request $request) {
      $user = User::where('email', 'like', strtolower(request('email')))->first();
      if (!$user == null) {
        $loan = new LoanApplication;

        $loan->user_id = $user->id;
        $loan->loan_value = request('loan_value');
        $loan->duration = request('duration');

        $loan->save();

        Session::flash('loan_success', 'Loan Applied successfully');

        return redirect()->route('home');
      }

      elseif($user == null) {
        $first = request('firstName');
        $last = request('LastName');

        $user_name = $first . ' ' . $last;

        $create_user = User::create([
            'name' => $user_name,
            'email' => request('email'),
            'id_number' => request('huduma'),
            'phone_number' => request('phone_number')
        ])->assignRole('user');

        $loan = new LoanApplication;

        $loan->user_id = $create_user->id;
        $loan->loan_value = request('loan_value');
        $loan->duration = request('duration');

        $loan->save();

        $invitation = new UsersInvitation($request->all());
        $invitation->user_id = $create_user->id;;
        $invitation->generateInvitationToken();
        $invitation->save();

        \Mail::to($invitation->email)->send(
          new SendInvitations($invitation)
        );


        Session::flash('loan_success', 'Loan Applied successfully. A Link has been sent please click on it to login');

        return redirect()->route('login');
      }
    }
}
