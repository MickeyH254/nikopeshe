<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\Http\Requests\StoreInvitationRequest;
use App\Mail\SendAgentsInvitation;

class InvitationsController extends Controller
{
    public function index() {
      $invitations = Invitation::where('registered_at', null)->orderBy('created_at', 'desc')->get();
      return view('bank.agent.invitations', compact('invitations'));
    }

    public function store(StoreInvitationRequest $request) {
      $invitation = new Invitation($request->all());
      $invitation->bank_id = \Auth::user()->id;
      $invitation->generateInvitationToken();
      $invitation->save();


      \Mail::to($invitation->email)->send(
        new SendAgentsInvitation($invitation)
      );

      return redirect()->route('send.invitation')
              ->with('success-agent', 'Invitation to register successfully sent to agent.');
    }
}
