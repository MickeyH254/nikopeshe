<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\LoanApplication;
use App\BankVerdict;
use App\Invitation;
use App\Package;
use App\Agent;
use Session;

class LoanApplicationController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * User role
     */

    public function store()
    {
      $loan = new LoanApplication;

      $loan->user_id = \Auth::user()->id;
      $loan->loan_value = request('loan_value');
      $loan->duration = request('duration');

      $loan->save();

      Session::flash('loan_success', 'Loan Applied successfully');

      return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * Bank/Agent Role
     */
    public function show(LoanApplication $loan_application)
    {
      $login_user = \Auth::user();

      if ($login_user->hasRole('bank')) {
        $packages = Package::where('bank_id', 'like', \Auth::user()->id)->get();

        return view('bank.package', compact('loan_application', 'packages'));
      }

      else if ($login_user->hasRole('agent')) {

        $invitation = Invitation::whereHas('invite_for', function(Builder $query) {
          $query->where('user_id', 'like', \Auth::user()->id);
        })->get('bank_id');




        $packages = Package::where('bank_id', 'like', $invitation)->get();

        return view('bank.package', compact('loan_application', 'packages'));
      }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanApplication $loan_application)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LoanApplication $loan_application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanApplication $loan_application)
    {
        $bank_verdict = new BankVerdict;
        $bank_verdict->application_id = $loan_application->id;
        if (\Auth::user()->hasRole('bank')) {
          $bank_verdict->bank_id = \Auth::user()->id;
        }
        elseif (\Auth::user()->hasRole('bank')) {
          $invitation = Invitation::whereHas('invite_for', function(Builder $query) {
            $query->where('user_id', 'like', \Auth::user()->id);
          })->get('bank_id');

          $bank_verdict->bank_id = \Auth::user()->id;
        }

        $bank_verdict->status = $bank_verdict->declined;


        $bank_verdict->save();

        Session::flash('loan_destroy', 'Loan Application declined');

        return redirect()->route('bank.index');

    }

    public function accept(LoanApplication $loan_application, Package $package) {

          $bank_verdict = new BankVerdict;
          $bank_verdict->application_id = $loan_application->id;
          if (\Auth::user()->hasRole('bank')) {
            $bank_verdict->bank_id = \Auth::user()->id;
          }
          elseif (\Auth::user()->hasRole('bank')) {
            $invitation = Invitation::whereHas('invite_for', function(Builder $query) {
              $query->where('user_id', 'like', \Auth::user()->id);
            })->get('bank_id');

            $bank_verdict->bank_id = \Auth::user()->id;
          }
          $bank_verdict->status = $bank_verdict->accepted;
          $bank_verdict->package_id = $package->id;


          $bank_verdict->save();

          Session::flash('loan_accept', 'Loan Application accepted successfully');

          return redirect()->route('bank.index');
    }
}
