<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoanApplication;
use App\BankVerdict;
use App\UserLoanStatus;

class UsersApplicationController extends Controller
{
  public function show()
  {
      $loans = LoanApplication::where('user_id', \Auth::user()->id)->get();
      return view('users.applications', compact('loans'));
  }

  public function show_offers(LoanApplication $loan_application) {
    $status = new BankVerdict;
    $offers = BankVerdict::where('application_id', $loan_application->id)
                         ->where('status', $status->accepted )->get();
    return view ('users.offers', compact('offers'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(BankVerdict $bank_verdict)
  {

      $loan_status = new UserLoanStatus;
      $loan_status->user_id = \Auth::user()->id;
      $loan_status->bank_verdict_id = $bank_verdict->id;
      $loan_status->status = $loan_status->rejected;

      $loan_status->save();

      Session::flash('application_destroy', 'Bank Offer Declined');

      return redirect()->back();

  }

  public function accepted(BankVerdict $bank_verdict)
  {

      $loan_status = new UserLoanStatus;
      $loan_status->user_id = \Auth::user()->id;
      $loan_status->bank_verdict_id = $bank_verdict->id;
      $loan_status->status = $loan_status->accepted;

      $loan_status->save();

      Session::flash('application_approve', 'Bank offer accepted');

      return redirect()->back();

  }

  public function logs() {
    return view('users.logs');
  }
}
