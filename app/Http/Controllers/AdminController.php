<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function index() {
      return view('admin.index');
    }

    public function bank() {
      $banks = User::role('bank')->withTrashed()->get();

      return view('admin.banks', compact('banks'));
    }

    public function bank_approve($user) {
      User::withTrashed()
        ->where('id', $user)
        ->restore();

        Session::flash('bank_approve', 'Bank approved successfully');

      return redirect()->route('admin.banks');
    }

    public function bank_disable($user) {
      User::find($user)->delete();

      Session::flash('bank_disable', 'Bank disabled successfully');

      return redirect()->route('admin.banks');
    }

    public function users() {
      $users = User::role('user')->withTrashed()->get();

      return view('admin.users', compact('users'));
    }
}
