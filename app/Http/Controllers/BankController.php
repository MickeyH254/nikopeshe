<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\LoanApplication;
use App\User;
use App\BankVerdict;
use App\Invitation;
use App\Agent;
use DB;

class BankController extends Controller
{
  public function index()
  {
      // $loans =DB::table('loan_application')->latest()->take(5)->get();
      return view('bank.index');
  }

  public function user_applications() {
    $login_user = \Auth::user();

    if ($login_user->hasRole('bank')) {
      $user_loans = LoanApplication::whereDoesntHave('bank_verdict', function (Builder $query) {
          $query->where('bank_id', 'like', \Auth::user()->id);
      })->get();
    }

    elseif ($login_user->hasRole('agent')) {
      // $agent = Agent::where('user_id', 'like' ,\Auth::user()->id);
      //
      // $invitation = Invitation::where('id', 'like', $agent->invite_id)->first();

      // $agent = Agent::with('invite_by')->where('user_id', \Auth::user()->id)->first();
      // $invitation = $agent->invite_by->toArray();
      //



      $user_loans = LoanApplication::whereDoesntHave('bank_verdict', function (Builder $query) {
          $query->where('bank_id', 'like', Invitation::whereHas('invite_for', function(Builder $query) {
            $query->where('user_id', 'like', \Auth::user()->id);
          })->get('bank_id'));
      })->get();
    }


    return view('bank.loan_applications', compact('user_loans'));
  }

  public function logs() {
    $logs = LoanApplication::whereHas('bank_verdict', function(Builder $query) {
      $query->where('bank_id', 'like', \Auth::user()->id);
    })->get();

    return view('bank.logs', compact('logs'));
  }

}
