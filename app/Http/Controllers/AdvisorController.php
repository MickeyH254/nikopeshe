<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdvisorsInvitation;
use App\User;
use App\Http\Requests\StoreInvitationRequest;
use App\Mail\SendAdvisorInvitation;

class AdvisorController extends Controller
{
    public function index()
    {
      $advisors = User::role('advisor')
                    ->withTrashed()
                    ->get();

      return view('bank.advisors.advisors', compact('advisors'));
    }

    public function sendInvitation() {
      return view('bank.advisors.link');
    }

    public function invites() {
      $invitations = AdvisorsInvitation::where('registered_at', null)->orderBy('created_at', 'desc')->get();
      return view('bank.advisors.invitations', compact('invitations'));
    }

    public function store(StoreInvitationRequest $request) {
      $invitation = new AdvisorsInvitation($request->all());
      $invitation->bank_id = \Auth::user()->id;
      $invitation->generateInvitationToken();
      $invitation->save();

      \Mail::to($invitation->email)->send(
        new SendAdvisorInvitation($invitation)
      );

      return redirect()->route('showInvitations')
              ->with('success', 'Invitation to register successfully sent to advisor.');
    }
}
