<?php

namespace App\Http\Middleware;

use Closure;
use App\Invitation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HasInvitation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('get')) {
          /**
           * No Token = Goodbye;
           */
          if (!$request->has('invitation_token')) {
            return redirect()->back();
          }

          $invitation_token = $request->get('invitation_token');

          /**
           * Lets try to find invitation by its Token
           * if failed -> return to request page with error
           */

          try {
            $invitation = Invitation::where('invitation_token', $invitation_token)->firstOrFail();
          } catch (ModelNotFoundException $e) {
            return redirect()->back()
                  ->with('error', 'Wrong Token! Please check your URL.');
          }

          /**
           * Check if the users are already registered
           * if yes -> redirect to login with error.
           */

          if (!is_null($invitation->registered_at)) {
            return redirect(route('login'))->with('error', 'The invitation link has already been used');
          }
        }
        return $next($request);
    }
}
