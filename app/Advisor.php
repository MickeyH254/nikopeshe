<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
  protected $fillable = [
    'user_id', 'invite_id', 'created_at', 'updated_at'
  ];

}
