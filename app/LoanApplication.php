<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class LoanApplication extends Model
{
    protected $fillable = [
      'user_id', 'loan_value' , 'duration'
    ];

    protected $table = 'loan_application';

    public function bank_verdict() {
        return $this->hasMany(BankVerdict::class, 'application_id');
    }

    public function users() {
      return $this->belongsTo(User::class, 'user_id');
    }

    // public function agents() {
    //   $this->belongsTo(Agent::class, $foreignKey, $ownerKey, $relation])
    // }

    public function invite_by() {
      return $this->hasOne(Invitation::class, 'invite_id');
    }

}
