<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $fillable =[
      'bank_id', 'email', 'invitation_token', 'registered_at',
    ];

    public function generateInvitationToken() {
      $this->invitation_token = substr(md5(rand(0, 9). $this->email . time()), 0, 32);
    }

    public function getLink() {
      return urldecode(route('register.agent') . '?invitation_token=' . $this->invitation_token);
    }

    public function invite_for() {
      return $this->hasOne(Agent::class, 'invite_id');
    }
}
