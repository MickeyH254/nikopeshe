<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
  protected $fillable = [
    'title', 'bank_id', 'interest', 'duration', 'terms_and_conditions', 'description'
  ];

  protected $hidden = [
    'bank_id'
  ];
 //  protected $casts = [
 //     'title' => 'array',
 //     'interest' => 'array',
 //     'duration' => 'array',
 //     'terms_and_conditions' => 'array',
 //
 // ];
 //

}
