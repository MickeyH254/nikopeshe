! function(t, e) {
    typeof module != 'undefined' ? module.exports = e() : typeof define == 'function' && typeof define.amd == 'object' ? define(e) : this[t] = e()
}('domready', function() {
    var o = [],
        e, t = document,
        i = t.documentElement.doScroll,
        d = 'DOMContentLoaded',
        n = (i ? /^loaded|^c/ : /^loaded|^i|^c/).test(t.readyState);
    return n || t.addEventListener(d, e = function() {
            t.removeEventListener(d, e), n = 1;
            while (e = o.shift()) e()
        }),
        function(e) {
            n ? setTimeout(e, 0) : o.push(e)
        }
});
/*! jQuery v3.2.1 | (c) JS Foundation and other contributors | jquery.org/license */
! function(a, b) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a)
    } : b(a)
}("undefined" != typeof window ? window : this, function(a, b) {
    "use strict";
    var c = [],
        d = a.document,
        e = Object.getPrototypeOf,
        f = c.slice,
        g = c.concat,
        h = c.push,
        i = c.indexOf,
        j = {},
        k = j.toString,
        l = j.hasOwnProperty,
        m = l.toString,
        n = m.call(Object),
        o = {};

    function p(a, b) {
        b = b || d;
        var c = b.createElement("script");
        c.text = a, b.head.appendChild(c).parentNode.removeChild(c)
    }
    var q = "3.2.1",
        r = function(a, b) {
            return new r.fn.init(a, b)
        },
        s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        t = /^-ms-/,
        u = /-([a-z])/g,
        v = function(a, b) {
            return b.toUpperCase()
        };
    r.fn = r.prototype = {
        jquery: q,
        constructor: r,
        length: 0,
        toArray: function() {
            return f.call(this)
        },
        get: function(a) {
            return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a]
        },
        pushStack: function(a) {
            var b = r.merge(this.constructor(), a);
            return b.prevObject = this, b
        },
        each: function(a) {
            return r.each(this, a)
        },
        map: function(a) {
            return this.pushStack(r.map(this, function(b, c) {
                return a.call(b, c, b)
            }))
        },
        slice: function() {
            return this.pushStack(f.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(a) {
            var b = this.length,
                c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [this[c]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: h,
        sort: c.sort,
        splice: c.splice
    }, r.extend = r.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {},
            h = 1,
            i = arguments.length,
            j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || r.isFunction(g) || (g = {}), h === i && (g = this, h--); h < i; h++)
            if (null != (a = arguments[h]))
                for (b in a) c = g[b], d = a[b], g !== d && (j && d && (r.isPlainObject(d) || (e = Array.isArray(d))) ? (e ? (e = !1, f = c && Array.isArray(c) ? c : []) : f = c && r.isPlainObject(c) ? c : {}, g[b] = r.extend(j, f, d)) : void 0 !== d && (g[b] = d));
        return g
    }, r.extend({
        expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(a) {
            throw new Error(a)
        },
        noop: function() {},
        isFunction: function(a) {
            return "function" === r.type(a)
        },
        isWindow: function(a) {
            return null != a && a === a.window
        },
        isNumeric: function(a) {
            var b = r.type(a);
            return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a))
        },
        isPlainObject: function(a) {
            var b, c;
            return !(!a || "[object Object]" !== k.call(a)) && (!(b = e(a)) || (c = l.call(b, "constructor") && b.constructor, "function" == typeof c && m.call(c) === n))
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) return !1;
            return !0
        },
        type: function(a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? j[k.call(a)] || "object" : typeof a
        },
        globalEval: function(a) {
            p(a)
        },
        camelCase: function(a) {
            return a.replace(t, "ms-").replace(u, v)
        },
        each: function(a, b) {
            var c, d = 0;
            if (w(a)) {
                for (c = a.length; d < c; d++)
                    if (b.call(a[d], d, a[d]) === !1) break
            } else
                for (d in a)
                    if (b.call(a[d], d, a[d]) === !1) break;
            return a
        },
        trim: function(a) {
            return null == a ? "" : (a + "").replace(s, "")
        },
        makeArray: function(a, b) {
            var c = b || [];
            return null != a && (w(Object(a)) ? r.merge(c, "string" == typeof a ? [a] : a) : h.call(c, a)), c
        },
        inArray: function(a, b, c) {
            return null == b ? -1 : i.call(b, a, c)
        },
        merge: function(a, b) {
            for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
            return a.length = e, a
        },
        grep: function(a, b, c) {
            for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
            return e
        },
        map: function(a, b, c) {
            var d, e, f = 0,
                h = [];
            if (w(a))
                for (d = a.length; f < d; f++) e = b(a[f], f, c), null != e && h.push(e);
            else
                for (f in a) e = b(a[f], f, c), null != e && h.push(e);
            return g.apply([], h)
        },
        guid: 1,
        proxy: function(a, b) {
            var c, d, e;
            if ("string" == typeof b && (c = a[b], b = a, a = c), r.isFunction(a)) return d = f.call(arguments, 2), e = function() {
                return a.apply(b || this, d.concat(f.call(arguments)))
            }, e.guid = a.guid = a.guid || r.guid++, e
        },
        now: Date.now,
        support: o
    }), "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]), r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(a, b) {
        j["[object " + b + "]"] = b.toLowerCase()
    });

    function w(a) {
        var b = !!a && "length" in a && a.length,
            c = r.type(a);
        return "function" !== c && !r.isWindow(a) && ("array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a)
    }
    var x = function(a) {
        var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date,
            v = a.document,
            w = 0,
            x = 0,
            y = ha(),
            z = ha(),
            A = ha(),
            B = function(a, b) {
                return a === b && (l = !0), 0
            },
            C = {}.hasOwnProperty,
            D = [],
            E = D.pop,
            F = D.push,
            G = D.push,
            H = D.slice,
            I = function(a, b) {
                for (var c = 0, d = a.length; c < d; c++)
                    if (a[c] === b) return c;
                return -1
            },
            J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            K = "[\\x20\\t\\r\\n\\f]",
            L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            M = "\\[" + K + "*(" + L + ")(?:" + K + "*([*^$|!~]?=)" + K + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + K + "*\\]",
            N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
            O = new RegExp(K + "+", "g"),
            P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
            Q = new RegExp("^" + K + "*," + K + "*"),
            R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
            S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"),
            T = new RegExp(N),
            U = new RegExp("^" + L + "$"),
            V = {
                ID: new RegExp("^#(" + L + ")"),
                CLASS: new RegExp("^\\.(" + L + ")"),
                TAG: new RegExp("^(" + L + "|[*])"),
                ATTR: new RegExp("^" + M),
                PSEUDO: new RegExp("^" + N),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + K + "*(even|odd|(([+-]|)(\\d*)n|)" + K + "*(?:([+-]|)" + K + "*(\\d+)|))" + K + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + J + ")$", "i"),
                needsContext: new RegExp("^" + K + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + K + "*((?:-\\d)?\\d*)" + K + "*\\)|)(?=[^-]|$)", "i")
            },
            W = /^(?:input|select|textarea|button)$/i,
            X = /^h\d$/i,
            Y = /^[^{]+\{\s*\[native \w/,
            Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            $ = /[+~]/,
            _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"),
            aa = function(a, b, c) {
                var d = "0x" + b - 65536;
                return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
            },
            ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            ca = function(a, b) {
                return b ? "\0" === a ? "\ufffd" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " " : "\\" + a
            },
            da = function() {
                m()
            },
            ea = ta(function(a) {
                return a.disabled === !0 && ("form" in a || "label" in a)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            G.apply(D = H.call(v.childNodes), v.childNodes), D[v.childNodes.length].nodeType
        } catch (fa) {
            G = {
                apply: D.length ? function(a, b) {
                    F.apply(a, H.call(b))
                } : function(a, b) {
                    var c = a.length,
                        d = 0;
                    while (a[c++] = b[d++]);
                    a.length = c - 1
                }
            }
        }

        function ga(a, b, d, e) {
            var f, h, j, k, l, o, r, s = b && b.ownerDocument,
                w = b ? b.nodeType : 9;
            if (d = d || [], "string" != typeof a || !a || 1 !== w && 9 !== w && 11 !== w) return d;
            if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
                if (11 !== w && (l = Z.exec(a)))
                    if (f = l[1]) {
                        if (9 === w) {
                            if (!(j = b.getElementById(f))) return d;
                            if (j.id === f) return d.push(j), d
                        } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d
                    } else {
                        if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;
                        if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d
                    }
                if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
                    if (1 !== w) s = b, r = a;
                    else if ("object" !== b.nodeName.toLowerCase()) {
                        (k = b.getAttribute("id")) ? k = k.replace(ba, ca): b.setAttribute("id", k = u), o = g(a), h = o.length;
                        while (h--) o[h] = "#" + k + " " + sa(o[h]);
                        r = o.join(","), s = $.test(a) && qa(b.parentNode) || b
                    }
                    if (r) try {
                        return G.apply(d, s.querySelectorAll(r)), d
                    } catch (x) {} finally {
                        k === u && b.removeAttribute("id")
                    }
                }
            }
            return i(a.replace(P, "$1"), b, d, e)
        }

        function ha() {
            var a = [];

            function b(c, e) {
                return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e
            }
            return b
        }

        function ia(a) {
            return a[u] = !0, a
        }

        function ja(a) {
            var b = n.createElement("fieldset");
            try {
                return !!a(b)
            } catch (c) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null
            }
        }

        function ka(a, b) {
            var c = a.split("|"),
                e = c.length;
            while (e--) d.attrHandle[c[e]] = b
        }

        function la(a, b) {
            var c = b && a,
                d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
            if (d) return d;
            if (c)
                while (c = c.nextSibling)
                    if (c === b) return -1;
            return a ? 1 : -1
        }

        function ma(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a
            }
        }

        function na(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function oa(a) {
            return function(b) {
                return "form" in b ? b.parentNode && b.disabled === !1 ? "label" in b ? "label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a : b.isDisabled === a || b.isDisabled !== !a && ea(b) === a : b.disabled === a : "label" in b && b.disabled === a
            }
        }

        function pa(a) {
            return ia(function(b) {
                return b = +b, ia(function(c, d) {
                    var e, f = a([], c.length, b),
                        g = f.length;
                    while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                })
            })
        }

        function qa(a) {
            return a && "undefined" != typeof a.getElementsByTagName && a
        }
        c = ga.support = {}, f = ga.isXML = function(a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return !!b && "HTML" !== b.nodeName
        }, m = ga.setDocument = function(a) {
            var b, e, g = a ? a.ownerDocument || a : v;
            return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ja(function(a) {
                return a.className = "i", !a.getAttribute("className")
            }), c.getElementsByTagName = ja(function(a) {
                return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length
            }), c.getElementsByClassName = Y.test(n.getElementsByClassName), c.getById = ja(function(a) {
                return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length
            }), c.getById ? (d.filter.ID = function(a) {
                var b = a.replace(_, aa);
                return function(a) {
                    return a.getAttribute("id") === b
                }
            }, d.find.ID = function(a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c = b.getElementById(a);
                    return c ? [c] : []
                }
            }) : (d.filter.ID = function(a) {
                var b = a.replace(_, aa);
                return function(a) {
                    var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
                    return c && c.value === b
                }
            }, d.find.ID = function(a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c, d, e, f = b.getElementById(a);
                    if (f) {
                        if (c = f.getAttributeNode("id"), c && c.value === a) return [f];
                        e = b.getElementsByName(a), d = 0;
                        while (f = e[d++])
                            if (c = f.getAttributeNode("id"), c && c.value === a) return [f]
                    }
                    return []
                }
            }), d.find.TAG = c.getElementsByTagName ? function(a, b) {
                return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0
            } : function(a, b) {
                var c, d = [],
                    e = 0,
                    f = b.getElementsByTagName(a);
                if ("*" === a) {
                    while (c = f[e++]) 1 === c.nodeType && d.push(c);
                    return d
                }
                return f
            }, d.find.CLASS = c.getElementsByClassName && function(a, b) {
                if ("undefined" != typeof b.getElementsByClassName && p) return b.getElementsByClassName(a)
            }, r = [], q = [], (c.qsa = Y.test(n.querySelectorAll)) && (ja(function(a) {
                o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + K + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + K + "*(?:value|" + J + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]")
            }), ja(function(a) {
                a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var b = n.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + K + "*[*^$|!~]?="), 2 !== a.querySelectorAll(":enabled").length && q.push(":enabled", ":disabled"), o.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(":disabled").length && q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:")
            })), (c.matchesSelector = Y.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function(a) {
                c.disconnectedMatch = s.call(a, "*"), s.call(a, "[s!='']:x"), r.push("!=", N)
            }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Y.test(o.compareDocumentPosition), t = b || Y.test(o.contains) ? function(a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a,
                    d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
            } : function(a, b) {
                if (b)
                    while (b = b.parentNode)
                        if (b === a) return !0;
                return !1
            }, B = b ? function(a, b) {
                if (a === b) return l = !0, 0;
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? I(k, a) - I(k, b) : 0 : 4 & d ? -1 : 1)
            } : function(a, b) {
                if (a === b) return l = !0, 0;
                var c, d = 0,
                    e = a.parentNode,
                    f = b.parentNode,
                    g = [a],
                    h = [b];
                if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;
                if (e === f) return la(a, b);
                c = a;
                while (c = c.parentNode) g.unshift(c);
                c = b;
                while (c = c.parentNode) h.unshift(c);
                while (g[d] === h[d]) d++;
                return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0
            }, n) : n
        }, ga.matches = function(a, b) {
            return ga(a, null, null, b)
        }, ga.matchesSelector = function(a, b) {
            if ((a.ownerDocument || a) !== n && m(a), b = b.replace(S, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
                var d = s.call(a, b);
                if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
            } catch (e) {}
            return ga(b, n, null, [a]).length > 0
        }, ga.contains = function(a, b) {
            return (a.ownerDocument || a) !== n && m(a), t(a, b)
        }, ga.attr = function(a, b) {
            (a.ownerDocument || a) !== n && m(a);
            var e = d.attrHandle[b.toLowerCase()],
                f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
            return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null
        }, ga.escape = function(a) {
            return (a + "").replace(ba, ca)
        }, ga.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a)
        }, ga.uniqueSort = function(a) {
            var b, d = [],
                e = 0,
                f = 0;
            if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
                while (b = a[f++]) b === a[f] && (e = d.push(f));
                while (e--) a.splice(d[e], 1)
            }
            return k = null, a
        }, e = ga.getText = function(a) {
            var b, c = "",
                d = 0,
                f = a.nodeType;
            if (f) {
                if (1 === f || 9 === f || 11 === f) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
                } else if (3 === f || 4 === f) return a.nodeValue
            } else
                while (b = a[d++]) c += e(b);
            return c
        }, d = ga.selectors = {
            cacheLength: 50,
            createPseudo: ia,
            match: V,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    return a[1] = a[1].replace(_, aa), a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                },
                CHILD: function(a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && ga.error(a[0]), a
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    return V.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                }
            },
            filter: {
                TAG: function(a) {
                    var b = a.replace(_, aa).toLowerCase();
                    return "*" === a ? function() {
                        return !0
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                },
                CLASS: function(a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) && y(a, function(a) {
                        return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "")
                    })
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = ga.attr(d, a);
                        return null == e ? "!=" === b : !b || (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1 : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"))
                    }
                },
                CHILD: function(a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3),
                        g = "last" !== a.slice(-4),
                        h = "of-type" === b;
                    return 1 === d && 0 === e ? function(a) {
                        return !!a.parentNode
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
                            q = b.parentNode,
                            r = h && b.nodeName.toLowerCase(),
                            s = !i && !h,
                            t = !1;
                        if (q) {
                            if (f) {
                                while (p) {
                                    m = b;
                                    while (m = m[p])
                                        if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling"
                                }
                                return !0
                            }
                            if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
                                    if (1 === m.nodeType && ++t && m === b) {
                                        k[a] = [w, n, t];
                                        break
                                    }
                            } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1)
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
                                    if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
                            return t -= e, t === d || t % d === 0 && t / d >= 0
                        }
                    }
                },
                PSEUDO: function(a, b) {
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
                    return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function(a, c) {
                        var d, f = e(a, b),
                            g = f.length;
                        while (g--) d = I(a, f[g]), a[d] = !(c[d] = f[g])
                    }) : function(a) {
                        return e(a, 0, c)
                    }) : e
                }
            },
            pseudos: {
                not: ia(function(a) {
                    var b = [],
                        c = [],
                        d = h(a.replace(P, "$1"));
                    return d[u] ? ia(function(a, b, c, e) {
                        var f, g = d(a, null, e, []),
                            h = a.length;
                        while (h--)(f = g[h]) && (a[h] = !(b[h] = f))
                    }) : function(a, e, f) {
                        return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()
                    }
                }),
                has: ia(function(a) {
                    return function(b) {
                        return ga(a, b).length > 0
                    }
                }),
                contains: ia(function(a) {
                    return a = a.replace(_, aa),
                        function(b) {
                            return (b.textContent || b.innerText || e(b)).indexOf(a) > -1
                        }
                }),
                lang: ia(function(a) {
                    return U.test(a || "") || ga.error("unsupported lang: " + a), a = a.replace(_, aa).toLowerCase(),
                        function(b) {
                            var c;
                            do
                                if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
                            return !1
                        }
                }),
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id
                },
                root: function(a) {
                    return a === o
                },
                focus: function(a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                },
                enabled: oa(!1),
                disabled: oa(!0),
                checked: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                },
                selected: function(a) {
                    return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
                },
                empty: function(a) {
                    for (a = a.firstChild; a; a = a.nextSibling)
                        if (a.nodeType < 6) return !1;
                    return !0
                },
                parent: function(a) {
                    return !d.pseudos.empty(a)
                },
                header: function(a) {
                    return X.test(a.nodeName)
                },
                input: function(a) {
                    return W.test(a.nodeName)
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                },
                text: function(a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                },
                first: pa(function() {
                    return [0]
                }),
                last: pa(function(a, b) {
                    return [b - 1]
                }),
                eq: pa(function(a, b, c) {
                    return [c < 0 ? c + b : c]
                }),
                even: pa(function(a, b) {
                    for (var c = 0; c < b; c += 2) a.push(c);
                    return a
                }),
                odd: pa(function(a, b) {
                    for (var c = 1; c < b; c += 2) a.push(c);
                    return a
                }),
                lt: pa(function(a, b, c) {
                    for (var d = c < 0 ? c + b : c; --d >= 0;) a.push(d);
                    return a
                }),
                gt: pa(function(a, b, c) {
                    for (var d = c < 0 ? c + b : c; ++d < b;) a.push(d);
                    return a
                })
            }
        }, d.pseudos.nth = d.pseudos.eq;
        for (b in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) d.pseudos[b] = ma(b);
        for (b in {
                submit: !0,
                reset: !0
            }) d.pseudos[b] = na(b);

        function ra() {}
        ra.prototype = d.filters = d.pseudos, d.setFilters = new ra, g = ga.tokenize = function(a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) return b ? 0 : k.slice(0);
            h = a, i = [], j = d.preFilter;
            while (h) {
                c && !(e = Q.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = R.exec(h)) && (c = e.shift(), f.push({
                    value: c,
                    type: e[0].replace(P, " ")
                }), h = h.slice(c.length));
                for (g in d.filter) !(e = V[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
                    value: c,
                    type: g,
                    matches: e
                }), h = h.slice(c.length));
                if (!c) break
            }
            return b ? h.length : h ? ga.error(a) : z(a, i).slice(0)
        };

        function sa(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d
        }

        function ta(a, b, c) {
            var d = b.dir,
                e = b.next,
                f = e || d,
                g = c && "parentNode" === f,
                h = x++;
            return b.first ? function(b, c, e) {
                while (b = b[d])
                    if (1 === b.nodeType || g) return a(b, c, e);
                return !1
            } : function(b, c, i) {
                var j, k, l, m = [w, h];
                if (i) {
                    while (b = b[d])
                        if ((1 === b.nodeType || g) && a(b, c, i)) return !0
                } else
                    while (b = b[d])
                        if (1 === b.nodeType || g)
                            if (l = b[u] || (b[u] = {}), k = l[b.uniqueID] || (l[b.uniqueID] = {}), e && e === b.nodeName.toLowerCase()) b = b[d] || b;
                            else {
                                if ((j = k[f]) && j[0] === w && j[1] === h) return m[2] = j[2];
                                if (k[f] = m, m[2] = a(b, c, i)) return !0
                            } return !1
            }
        }

        function ua(a) {
            return a.length > 1 ? function(b, c, d) {
                var e = a.length;
                while (e--)
                    if (!a[e](b, c, d)) return !1;
                return !0
            } : a[0]
        }

        function va(a, b, c) {
            for (var d = 0, e = b.length; d < e; d++) ga(a, b[d], c);
            return c
        }

        function wa(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
            return g
        }

        function xa(a, b, c, d, e, f) {
            return d && !d[u] && (d = xa(d)), e && !e[u] && (e = xa(e, f)), ia(function(f, g, h, i) {
                var j, k, l, m = [],
                    n = [],
                    o = g.length,
                    p = f || va(b || "*", h.nodeType ? [h] : h, []),
                    q = !a || !f && b ? p : wa(p, m, a, h, i),
                    r = c ? e || (f ? a : o || d) ? [] : g : q;
                if (c && c(q, r, h, i), d) {
                    j = wa(r, n), d(j, [], h, i), k = j.length;
                    while (k--)(l = j[k]) && (r[n[k]] = !(q[n[k]] = l))
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            j = [], k = r.length;
                            while (k--)(l = r[k]) && j.push(q[k] = l);
                            e(null, r = [], j, i)
                        }
                        k = r.length;
                        while (k--)(l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))
                    }
                } else r = wa(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : G.apply(g, r)
            })
        }

        function ya(a) {
            for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ta(function(a) {
                    return a === b
                }, h, !0), l = ta(function(a) {
                    return I(b, a) > -1
                }, h, !0), m = [function(a, c, d) {
                    var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                    return b = null, e
                }]; i < f; i++)
                if (c = d.relative[a[i].type]) m = [ta(ua(m), c)];
                else {
                    if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
                        for (e = ++i; e < f; e++)
                            if (d.relative[a[e].type]) break;
                        return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({
                            value: " " === a[i - 2].type ? "*" : ""
                        })).replace(P, "$1"), c, i < e && ya(a.slice(i, e)), e < f && ya(a = a.slice(e)), e < f && sa(a))
                    }
                    m.push(c)
                }
            return ua(m)
        }

        function za(a, b) {
            var c = b.length > 0,
                e = a.length > 0,
                f = function(f, g, h, i, k) {
                    var l, o, q, r = 0,
                        s = "0",
                        t = f && [],
                        u = [],
                        v = j,
                        x = f || e && d.find.TAG("*", k),
                        y = w += null == v ? 1 : Math.random() || .1,
                        z = x.length;
                    for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
                        if (e && l) {
                            o = 0, g || l.ownerDocument === n || (m(l), h = !p);
                            while (q = a[o++])
                                if (q(l, g || n, h)) {
                                    i.push(l);
                                    break
                                }
                            k && (w = y)
                        }
                        c && ((l = !q && l) && r--, f && t.push(l))
                    }
                    if (r += s, c && s !== r) {
                        o = 0;
                        while (q = b[o++]) q(t, u, g, h);
                        if (f) {
                            if (r > 0)
                                while (s--) t[s] || u[s] || (u[s] = E.call(i));
                            u = wa(u)
                        }
                        G.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i)
                    }
                    return k && (w = y, j = v), t
                };
            return c ? ia(f) : f
        }
        return h = ga.compile = function(a, b) {
            var c, d = [],
                e = [],
                f = A[a + " "];
            if (!f) {
                b || (b = g(a)), c = b.length;
                while (c--) f = ya(b[c]), f[u] ? d.push(f) : e.push(f);
                f = A(a, za(e, d)), f.selector = a
            }
            return f
        }, i = ga.select = function(a, b, c, e) {
            var f, i, j, k, l, m = "function" == typeof a && a,
                n = !e && g(a = m.selector || a);
            if (c = c || [], 1 === n.length) {
                if (i = n[0] = n[0].slice(0), i.length > 2 && "ID" === (j = i[0]).type && 9 === b.nodeType && p && d.relative[i[1].type]) {
                    if (b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0], !b) return c;
                    m && (b = b.parentNode), a = a.slice(i.shift().value.length)
                }
                f = V.needsContext.test(a) ? 0 : i.length;
                while (f--) {
                    if (j = i[f], d.relative[k = j.type]) break;
                    if ((l = d.find[k]) && (e = l(j.matches[0].replace(_, aa), $.test(i[0].type) && qa(b.parentNode) || b))) {
                        if (i.splice(f, 1), a = e.length && sa(i), !a) return G.apply(c, e), c;
                        break
                    }
                }
            }
            return (m || h(a, n))(e, b, !p, c, !b || $.test(a) && qa(b.parentNode) || b), c
        }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja(function(a) {
            return 1 & a.compareDocumentPosition(n.createElement("fieldset"))
        }), ja(function(a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
        }) || ka("type|href|height|width", function(a, b, c) {
            if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        }), c.attributes && ja(function(a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
        }) || ka("value", function(a, b, c) {
            if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue
        }), ja(function(a) {
            return null == a.getAttribute("disabled")
        }) || ka(J, function(a, b, c) {
            var d;
            if (!c) return a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }), ga
    }(a);
    r.find = x, r.expr = x.selectors, r.expr[":"] = r.expr.pseudos, r.uniqueSort = r.unique = x.uniqueSort, r.text = x.getText, r.isXMLDoc = x.isXML, r.contains = x.contains, r.escapeSelector = x.escape;
    var y = function(a, b, c) {
            var d = [],
                e = void 0 !== c;
            while ((a = a[b]) && 9 !== a.nodeType)
                if (1 === a.nodeType) {
                    if (e && r(a).is(c)) break;
                    d.push(a)
                }
            return d
        },
        z = function(a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c
        },
        A = r.expr.match.needsContext;

    function B(a, b) {
        return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
    }
    var C = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        D = /^.[^:#\[\.,]*$/;

    function E(a, b, c) {
        return r.isFunction(b) ? r.grep(a, function(a, d) {
            return !!b.call(a, d, a) !== c
        }) : b.nodeType ? r.grep(a, function(a) {
            return a === b !== c
        }) : "string" != typeof b ? r.grep(a, function(a) {
            return i.call(b, a) > -1 !== c
        }) : D.test(b) ? r.filter(b, a, c) : (b = r.filter(b, a), r.grep(a, function(a) {
            return i.call(b, a) > -1 !== c && 1 === a.nodeType
        }))
    }
    r.filter = function(a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? r.find.matchesSelector(d, a) ? [d] : [] : r.find.matches(a, r.grep(b, function(a) {
            return 1 === a.nodeType
        }))
    }, r.fn.extend({
        find: function(a) {
            var b, c, d = this.length,
                e = this;
            if ("string" != typeof a) return this.pushStack(r(a).filter(function() {
                for (b = 0; b < d; b++)
                    if (r.contains(e[b], this)) return !0
            }));
            for (c = this.pushStack([]), b = 0; b < d; b++) r.find(a, e[b], c);
            return d > 1 ? r.uniqueSort(c) : c
        },
        filter: function(a) {
            return this.pushStack(E(this, a || [], !1))
        },
        not: function(a) {
            return this.pushStack(E(this, a || [], !0))
        },
        is: function(a) {
            return !!E(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1).length
        }
    });
    var F, G = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
        H = r.fn.init = function(a, b, c) {
            var e, f;
            if (!a) return this;
            if (c = c || F, "string" == typeof a) {
                if (e = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : G.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
                if (e[1]) {
                    if (b = b instanceof r ? b[0] : b, r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), C.test(e[1]) && r.isPlainObject(b))
                        for (e in b) r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
                    return this
                }
                return f = d.getElementById(e[2]), f && (this[0] = f, this.length = 1), this
            }
            return a.nodeType ? (this[0] = a, this.length = 1, this) : r.isFunction(a) ? void 0 !== c.ready ? c.ready(a) : a(r) : r.makeArray(a, this)
        };
    H.prototype = r.fn, F = r(d);
    var I = /^(?:parents|prev(?:Until|All))/,
        J = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    r.fn.extend({
        has: function(a) {
            var b = r(a, this),
                c = b.length;
            return this.filter(function() {
                for (var a = 0; a < c; a++)
                    if (r.contains(this, b[a])) return !0
            })
        },
        closest: function(a, b) {
            var c, d = 0,
                e = this.length,
                f = [],
                g = "string" != typeof a && r(a);
            if (!A.test(a))
                for (; d < e; d++)
                    for (c = this[d]; c && c !== b; c = c.parentNode)
                        if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {
                            f.push(c);
                            break
                        }
            return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f)
        },
        index: function(a) {
            return a ? "string" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(a, b) {
            return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))))
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    });

    function K(a, b) {
        while ((a = a[b]) && 1 !== a.nodeType);
        return a
    }
    r.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        },
        parents: function(a) {
            return y(a, "parentNode")
        },
        parentsUntil: function(a, b, c) {
            return y(a, "parentNode", c)
        },
        next: function(a) {
            return K(a, "nextSibling")
        },
        prev: function(a) {
            return K(a, "previousSibling")
        },
        nextAll: function(a) {
            return y(a, "nextSibling")
        },
        prevAll: function(a) {
            return y(a, "previousSibling")
        },
        nextUntil: function(a, b, c) {
            return y(a, "nextSibling", c)
        },
        prevUntil: function(a, b, c) {
            return y(a, "previousSibling", c)
        },
        siblings: function(a) {
            return z((a.parentNode || {}).firstChild, a)
        },
        children: function(a) {
            return z(a.firstChild)
        },
        contents: function(a) {
            return B(a, "iframe") ? a.contentDocument : (B(a, "template") && (a = a.content || a), r.merge([], a.childNodes))
        }
    }, function(a, b) {
        r.fn[a] = function(c, d) {
            var e = r.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = r.filter(d, e)), this.length > 1 && (J[a] || r.uniqueSort(e), I.test(a) && e.reverse()), this.pushStack(e)
        }
    });
    var L = /[^\x20\t\r\n\f]+/g;

    function M(a) {
        var b = {};
        return r.each(a.match(L) || [], function(a, c) {
            b[c] = !0
        }), b
    }
    r.Callbacks = function(a) {
        a = "string" == typeof a ? M(a) : r.extend({}, a);
        var b, c, d, e, f = [],
            g = [],
            h = -1,
            i = function() {
                for (e = e || a.once, d = b = !0; g.length; h = -1) {
                    c = g.shift();
                    while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1)
                }
                a.memory || (c = !1), b = !1, e && (f = c ? [] : "")
            },
            j = {
                add: function() {
                    return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
                        r.each(b, function(b, c) {
                            r.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== r.type(c) && d(c)
                        })
                    }(arguments), c && !b && i()), this
                },
                remove: function() {
                    return r.each(arguments, function(a, b) {
                        var c;
                        while ((c = r.inArray(b, f, c)) > -1) f.splice(c, 1), c <= h && h--
                    }), this
                },
                has: function(a) {
                    return a ? r.inArray(a, f) > -1 : f.length > 0
                },
                empty: function() {
                    return f && (f = []), this
                },
                disable: function() {
                    return e = g = [], f = c = "", this
                },
                disabled: function() {
                    return !f
                },
                lock: function() {
                    return e = g = [], c || b || (f = c = ""), this
                },
                locked: function() {
                    return !!e
                },
                fireWith: function(a, c) {
                    return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this
                },
                fire: function() {
                    return j.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!d
                }
            };
        return j
    };

    function N(a) {
        return a
    }

    function O(a) {
        throw a
    }

    function P(a, b, c, d) {
        var e;
        try {
            a && r.isFunction(e = a.promise) ? e.call(a).done(b).fail(c) : a && r.isFunction(e = a.then) ? e.call(a, b, c) : b.apply(void 0, [a].slice(d))
        } catch (a) {
            c.apply(void 0, [a])
        }
    }
    r.extend({
        Deferred: function(b) {
            var c = [
                    ["notify", "progress", r.Callbacks("memory"), r.Callbacks("memory"), 2],
                    ["resolve", "done", r.Callbacks("once memory"), r.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", r.Callbacks("once memory"), r.Callbacks("once memory"), 1, "rejected"]
                ],
                d = "pending",
                e = {
                    state: function() {
                        return d
                    },
                    always: function() {
                        return f.done(arguments).fail(arguments), this
                    },
                    "catch": function(a) {
                        return e.then(null, a)
                    },
                    pipe: function() {
                        var a = arguments;
                        return r.Deferred(function(b) {
                            r.each(c, function(c, d) {
                                var e = r.isFunction(a[d[4]]) && a[d[4]];
                                f[d[1]](function() {
                                    var a = e && e.apply(this, arguments);
                                    a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments)
                                })
                            }), a = null
                        }).promise()
                    },
                    then: function(b, d, e) {
                        var f = 0;

                        function g(b, c, d, e) {
                            return function() {
                                var h = this,
                                    i = arguments,
                                    j = function() {
                                        var a, j;
                                        if (!(b < f)) {
                                            if (a = d.apply(h, i), a === c.promise()) throw new TypeError("Thenable self-resolution");
                                            j = a && ("object" == typeof a || "function" == typeof a) && a.then, r.isFunction(j) ? e ? j.call(a, g(f, c, N, e), g(f, c, O, e)) : (f++, j.call(a, g(f, c, N, e), g(f, c, O, e), g(f, c, N, c.notifyWith))) : (d !== N && (h = void 0, i = [a]), (e || c.resolveWith)(h, i))
                                        }
                                    },
                                    k = e ? j : function() {
                                        try {
                                            j()
                                        } catch (a) {
                                            r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== O && (h = void 0, i = [a]), c.rejectWith(h, i))
                                        }
                                    };
                                b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k))
                            }
                        }
                        return r.Deferred(function(a) {
                            c[0][3].add(g(0, a, r.isFunction(e) ? e : N, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : N)), c[2][3].add(g(0, a, r.isFunction(d) ? d : O))
                        }).promise()
                    },
                    promise: function(a) {
                        return null != a ? r.extend(a, e) : e
                    }
                },
                f = {};
            return r.each(c, function(a, b) {
                var g = b[2],
                    h = b[5];
                e[b[1]] = g.add, h && g.add(function() {
                    d = h
                }, c[3 - a][2].disable, c[0][2].lock), g.add(b[3].fire), f[b[0]] = function() {
                    return f[b[0] + "With"](this === f ? void 0 : this, arguments), this
                }, f[b[0] + "With"] = g.fireWith
            }), e.promise(f), b && b.call(f, f), f
        },
        when: function(a) {
            var b = arguments.length,
                c = b,
                d = Array(c),
                e = f.call(arguments),
                g = r.Deferred(),
                h = function(a) {
                    return function(c) {
                        d[a] = this, e[a] = arguments.length > 1 ? f.call(arguments) : c, --b || g.resolveWith(d, e)
                    }
                };
            if (b <= 1 && (P(a, g.done(h(c)).resolve, g.reject, !b), "pending" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();
            while (c--) P(e[c], h(c), g.reject);
            return g.promise()
        }
    });
    var Q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    r.Deferred.exceptionHook = function(b, c) {
        a.console && a.console.warn && b && Q.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c)
    }, r.readyException = function(b) {
        a.setTimeout(function() {
            throw b
        })
    };
    var R = r.Deferred();
    r.fn.ready = function(a) {
        return R.then(a)["catch"](function(a) {
            r.readyException(a)
        }), this
    }, r.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(a) {
            (a === !0 ? --r.readyWait : r.isReady) || (r.isReady = !0, a !== !0 && --r.readyWait > 0 || R.resolveWith(d, [r]))
        }
    }), r.ready.then = R.then;

    function S() {
        d.removeEventListener("DOMContentLoaded", S),
            a.removeEventListener("load", S), r.ready()
    }
    "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll ? a.setTimeout(r.ready) : (d.addEventListener("DOMContentLoaded", S), a.addEventListener("load", S));
    var T = function(a, b, c, d, e, f, g) {
            var h = 0,
                i = a.length,
                j = null == c;
            if ("object" === r.type(c)) {
                e = !0;
                for (h in c) T(a, b, h, c[h], !0, f, g)
            } else if (void 0 !== d && (e = !0, r.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function(a, b, c) {
                    return j.call(r(a), c)
                })), b))
                for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
            return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
        },
        U = function(a) {
            return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType
        };

    function V() {
        this.expando = r.expando + V.uid++
    }
    V.uid = 1, V.prototype = {
        cache: function(a) {
            var b = a[this.expando];
            return b || (b = {}, U(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {
                value: b,
                configurable: !0
            }))), b
        },
        set: function(a, b, c) {
            var d, e = this.cache(a);
            if ("string" == typeof b) e[r.camelCase(b)] = c;
            else
                for (d in b) e[r.camelCase(d)] = b[d];
            return e
        },
        get: function(a, b) {
            return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)]
        },
        access: function(a, b, c) {
            return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b)
        },
        remove: function(a, b) {
            var c, d = a[this.expando];
            if (void 0 !== d) {
                if (void 0 !== b) {
                    Array.isArray(b) ? b = b.map(r.camelCase) : (b = r.camelCase(b), b = b in d ? [b] : b.match(L) || []), c = b.length;
                    while (c--) delete d[b[c]]
                }(void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando])
            }
        },
        hasData: function(a) {
            var b = a[this.expando];
            return void 0 !== b && !r.isEmptyObject(b)
        }
    };
    var W = new V,
        X = new V,
        Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Z = /[A-Z]/g;

    function $(a) {
        return "true" === a || "false" !== a && ("null" === a ? null : a === +a + "" ? +a : Y.test(a) ? JSON.parse(a) : a)
    }

    function _(a, b, c) {
        var d;
        if (void 0 === c && 1 === a.nodeType)
            if (d = "data-" + b.replace(Z, "-$&").toLowerCase(), c = a.getAttribute(d), "string" == typeof c) {
                try {
                    c = $(c)
                } catch (e) {}
                X.set(a, b, c)
            } else c = void 0;
        return c
    }
    r.extend({
        hasData: function(a) {
            return X.hasData(a) || W.hasData(a)
        },
        data: function(a, b, c) {
            return X.access(a, b, c)
        },
        removeData: function(a, b) {
            X.remove(a, b)
        },
        _data: function(a, b, c) {
            return W.access(a, b, c)
        },
        _removeData: function(a, b) {
            W.remove(a, b)
        }
    }), r.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0],
                g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = X.get(f), 1 === f.nodeType && !W.get(f, "hasDataAttrs"))) {
                    c = g.length;
                    while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = r.camelCase(d.slice(5)), _(f, d, e[d])));
                    W.set(f, "hasDataAttrs", !0)
                }
                return e
            }
            return "object" == typeof a ? this.each(function() {
                X.set(this, a)
            }) : T(this, function(b) {
                var c;
                if (f && void 0 === b) {
                    if (c = X.get(f, a), void 0 !== c) return c;
                    if (c = _(f, a), void 0 !== c) return c
                } else this.each(function() {
                    X.set(this, a, b)
                })
            }, null, b, arguments.length > 1, null, !0)
        },
        removeData: function(a) {
            return this.each(function() {
                X.remove(this, a)
            })
        }
    }), r.extend({
        queue: function(a, b, c) {
            var d;
            if (a) return b = (b || "fx") + "queue", d = W.get(a, b), c && (!d || Array.isArray(c) ? d = W.access(a, b, r.makeArray(c)) : d.push(c)), d || []
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = r.queue(a, b),
                d = c.length,
                e = c.shift(),
                f = r._queueHooks(a, b),
                g = function() {
                    r.dequeue(a, b)
                };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return W.get(a, c) || W.access(a, c, {
                empty: r.Callbacks("once memory").add(function() {
                    W.remove(a, [b + "queue", c])
                })
            })
        }
    }), r.fn.extend({
        queue: function(a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? r.queue(this[0], a) : void 0 === b ? this : this.each(function() {
                var c = r.queue(this, a, b);
                r._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a)
            })
        },
        dequeue: function(a) {
            return this.each(function() {
                r.dequeue(this, a)
            })
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", [])
        },
        promise: function(a, b) {
            var c, d = 1,
                e = r.Deferred(),
                f = this,
                g = this.length,
                h = function() {
                    --d || e.resolveWith(f, [f])
                };
            "string" != typeof a && (b = a, a = void 0), a = a || "fx";
            while (g--) c = W.get(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b)
        }
    });
    var aa = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        ba = new RegExp("^(?:([+-])=|)(" + aa + ")([a-z%]*)$", "i"),
        ca = ["Top", "Right", "Bottom", "Left"],
        da = function(a, b) {
            return a = b || a, "none" === a.style.display || "" === a.style.display && r.contains(a.ownerDocument, a) && "none" === r.css(a, "display")
        },
        ea = function(a, b, c, d) {
            var e, f, g = {};
            for (f in b) g[f] = a.style[f], a.style[f] = b[f];
            e = c.apply(a, d || []);
            for (f in b) a.style[f] = g[f];
            return e
        };

    function fa(a, b, c, d) {
        var e, f = 1,
            g = 20,
            h = d ? function() {
                return d.cur()
            } : function() {
                return r.css(a, b, "")
            },
            i = h(),
            j = c && c[3] || (r.cssNumber[b] ? "" : "px"),
            k = (r.cssNumber[b] || "px" !== j && +i) && ba.exec(r.css(a, b));
        if (k && k[3] !== j) {
            j = j || k[3], c = c || [], k = +i || 1;
            do f = f || ".5", k /= f, r.style(a, b, k + j); while (f !== (f = h() / i) && 1 !== f && --g)
        }
        return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e
    }
    var ga = {};

    function ha(a) {
        var b, c = a.ownerDocument,
            d = a.nodeName,
            e = ga[d];
        return e ? e : (b = c.body.appendChild(c.createElement(d)), e = r.css(b, "display"), b.parentNode.removeChild(b), "none" === e && (e = "block"), ga[d] = e, e)
    }

    function ia(a, b) {
        for (var c, d, e = [], f = 0, g = a.length; f < g; f++) d = a[f], d.style && (c = d.style.display, b ? ("none" === c && (e[f] = W.get(d, "display") || null, e[f] || (d.style.display = "")), "" === d.style.display && da(d) && (e[f] = ha(d))) : "none" !== c && (e[f] = "none", W.set(d, "display", c)));
        for (f = 0; f < g; f++) null != e[f] && (a[f].style.display = e[f]);
        return a
    }
    r.fn.extend({
        show: function() {
            return ia(this, !0)
        },
        hide: function() {
            return ia(this)
        },
        toggle: function(a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
                da(this) ? r(this).show() : r(this).hide()
            })
        }
    });
    var ja = /^(?:checkbox|radio)$/i,
        ka = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        la = /^$|\/(?:java|ecma)script/i,
        ma = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    ma.optgroup = ma.option, ma.tbody = ma.tfoot = ma.colgroup = ma.caption = ma.thead, ma.th = ma.td;

    function na(a, b) {
        var c;
        return c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : [], void 0 === b || b && B(a, b) ? r.merge([a], c) : c
    }

    function oa(a, b) {
        for (var c = 0, d = a.length; c < d; c++) W.set(a[c], "globalEval", !b || W.get(b[c], "globalEval"))
    }
    var pa = /<|&#?\w+;/;

    function qa(a, b, c, d, e) {
        for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++)
            if (f = a[n], f || 0 === f)
                if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);
                else if (pa.test(f)) {
            g = g || l.appendChild(b.createElement("div")), h = (ka.exec(f) || ["", ""])[1].toLowerCase(), i = ma[h] || ma._default, g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2], k = i[0];
            while (k--) g = g.lastChild;
            r.merge(m, g.childNodes), g = l.firstChild, g.textContent = ""
        } else m.push(b.createTextNode(f));
        l.textContent = "", n = 0;
        while (f = m[n++])
            if (d && r.inArray(f, d) > -1) e && e.push(f);
            else if (j = r.contains(f.ownerDocument, f), g = na(l.appendChild(f), "script"), j && oa(g), c) {
            k = 0;
            while (f = g[k++]) la.test(f.type || "") && c.push(f)
        }
        return l
    }! function() {
        var a = d.createDocumentFragment(),
            b = a.appendChild(d.createElement("div")),
            c = d.createElement("input");
        c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue
    }();
    var ra = d.documentElement,
        sa = /^key/,
        ta = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        ua = /^([^.]*)(?:\.(.+)|)/;

    function va() {
        return !0
    }

    function wa() {
        return !1
    }

    function xa() {
        try {
            return d.activeElement
        } catch (a) {}
    }

    function ya(a, b, c, d, e, f) {
        var g, h;
        if ("object" == typeof b) {
            "string" != typeof c && (d = d || c, c = void 0);
            for (h in b) ya(a, h, c, d, b[h], f);
            return a
        }
        if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = wa;
        else if (!e) return a;
        return 1 === f && (g = e, e = function(a) {
            return r().off(a), g.apply(this, arguments)
        }, e.guid = g.guid || (g.guid = r.guid++)), a.each(function() {
            r.event.add(this, b, e, d, c)
        })
    }
    r.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = W.get(a);
            if (q) {
                c.handler && (f = c, c = f.handler, e = f.selector), e && r.find.matchesSelector(ra, e), c.guid || (c.guid = r.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function(b) {
                    return "undefined" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0
                }), b = (b || "").match(L) || [""], j = b.length;
                while (j--) h = ua.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n && (l = r.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = r.event.special[n] || {}, k = r.extend({
                    type: n,
                    origType: p,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && r.expr.match.needsContext.test(e),
                    namespace: o.join(".")
                }, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), r.event.global[n] = !0)
            }
        },
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = W.hasData(a) && W.get(a);
            if (q && (i = q.events)) {
                b = (b || "").match(L) || [""], j = b.length;
                while (j--)
                    if (h = ua.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
                        l = r.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length;
                        while (f--) k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
                        g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || r.removeEvent(a, n, q.handle), delete i[n])
                    } else
                        for (n in i) r.event.remove(a, n + b[j], c, d, !0);
                r.isEmptyObject(i) && W.remove(a, "handle events")
            }
        },
        dispatch: function(a) {
            var b = r.event.fix(a),
                c, d, e, f, g, h, i = new Array(arguments.length),
                j = (W.get(this, "events") || {})[b.type] || [],
                k = r.event.special[b.type] || {};
            for (i[0] = b, c = 1; c < arguments.length; c++) i[c] = arguments[c];
            if (b.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, b) !== !1) {
                h = r.event.handlers.call(this, b, j), c = 0;
                while ((f = h[c++]) && !b.isPropagationStopped()) {
                    b.currentTarget = f.elem, d = 0;
                    while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped()) b.rnamespace && !b.rnamespace.test(g.namespace) || (b.handleObj = g, b.data = g.data, e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()))
                }
                return k.postDispatch && k.postDispatch.call(this, b), b.result
            }
        },
        handlers: function(a, b) {
            var c, d, e, f, g, h = [],
                i = b.delegateCount,
                j = a.target;
            if (i && j.nodeType && !("click" === a.type && a.button >= 1))
                for (; j !== this; j = j.parentNode || this)
                    if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
                        for (f = [], g = {}, c = 0; c < i; c++) d = b[c], e = d.selector + " ", void 0 === g[e] && (g[e] = d.needsContext ? r(e, this).index(j) > -1 : r.find(e, this, null, [j]).length), g[e] && f.push(d);
                        f.length && h.push({
                            elem: j,
                            handlers: f
                        })
                    }
            return j = this, i < b.length && h.push({
                elem: j,
                handlers: b.slice(i)
            }), h
        },
        addProp: function(a, b) {
            Object.defineProperty(r.Event.prototype, a, {
                enumerable: !0,
                configurable: !0,
                get: r.isFunction(b) ? function() {
                    if (this.originalEvent) return b(this.originalEvent)
                } : function() {
                    if (this.originalEvent) return this.originalEvent[a]
                },
                set: function(b) {
                    Object.defineProperty(this, a, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: b
                    })
                }
            })
        },
        fix: function(a) {
            return a[r.expando] ? a : new r.Event(a)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== xa() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === xa() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && B(this, "input")) return this.click(), !1
                },
                _default: function(a) {
                    return B(a.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        }
    }, r.removeEvent = function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c)
    }, r.Event = function(a, b) {
        return this instanceof r.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? va : wa, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, b && r.extend(this, b), this.timeStamp = a && a.timeStamp || r.now(), void(this[r.expando] = !0)) : new r.Event(a, b)
    }, r.Event.prototype = {
        constructor: r.Event,
        isDefaultPrevented: wa,
        isPropagationStopped: wa,
        isImmediatePropagationStopped: wa,
        isSimulated: !1,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = va, a && !this.isSimulated && a.preventDefault()
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = va, a && !this.isSimulated && a.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = va, a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation()
        }
    }, r.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(a) {
            var b = a.button;
            return null == a.which && sa.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && ta.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which
        }
    }, r.event.addProp), r.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        r.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this,
                    e = a.relatedTarget,
                    f = a.handleObj;
                return e && (e === d || r.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), r.fn.extend({
        on: function(a, b, c, d) {
            return ya(this, a, b, c, d)
        },
        one: function(a, b, c, d) {
            return ya(this, a, b, c, d, 1)
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, r(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
            if ("object" == typeof a) {
                for (e in a) this.off(e, b, a[e]);
                return this
            }
            return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = wa), this.each(function() {
                r.event.remove(this, a, c, b)
            })
        }
    });
    var za = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Aa = /<script|<style|<link/i,
        Ba = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Ca = /^true\/(.*)/,
        Da = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Ea(a, b) {
        return B(a, "table") && B(11 !== b.nodeType ? b : b.firstChild, "tr") ? r(">tbody", a)[0] || a : a
    }

    function Fa(a) {
        return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a
    }

    function Ga(a) {
        var b = Ca.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function Ha(a, b) {
        var c, d, e, f, g, h, i, j;
        if (1 === b.nodeType) {
            if (W.hasData(a) && (f = W.access(a), g = W.set(b, f), j = f.events)) {
                delete g.handle, g.events = {};
                for (e in j)
                    for (c = 0, d = j[e].length; c < d; c++) r.event.add(b, e, j[e][c])
            }
            X.hasData(a) && (h = X.access(a), i = r.extend({}, h), X.set(b, i))
        }
    }

    function Ia(a, b) {
        var c = b.nodeName.toLowerCase();
        "input" === c && ja.test(a.type) ? b.checked = a.checked : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
    }

    function Ja(a, b, c, d) {
        b = g.apply([], b);
        var e, f, h, i, j, k, l = 0,
            m = a.length,
            n = m - 1,
            q = b[0],
            s = r.isFunction(q);
        if (s || m > 1 && "string" == typeof q && !o.checkClone && Ba.test(q)) return a.each(function(e) {
            var f = a.eq(e);
            s && (b[0] = q.call(this, e, f.html())), Ja(f, b, c, d)
        });
        if (m && (e = qa(b, a[0].ownerDocument, !1, a, d), f = e.firstChild, 1 === e.childNodes.length && (e = f), f || d)) {
            for (h = r.map(na(e, "script"), Fa), i = h.length; l < m; l++) j = e, l !== n && (j = r.clone(j, !0, !0), i && r.merge(h, na(j, "script"))), c.call(a[l], j, l);
            if (i)
                for (k = h[h.length - 1].ownerDocument, r.map(h, Ga), l = 0; l < i; l++) j = h[l], la.test(j.type || "") && !W.access(j, "globalEval") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Da, ""), k))
        }
        return a
    }

    function Ka(a, b, c) {
        for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || r.cleanData(na(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && oa(na(d, "script")), d.parentNode.removeChild(d));
        return a
    }
    r.extend({
        htmlPrefilter: function(a) {
            return a.replace(za, "<$1></$2>")
        },
        clone: function(a, b, c) {
            var d, e, f, g, h = a.cloneNode(!0),
                i = r.contains(a.ownerDocument, a);
            if (!(o.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || r.isXMLDoc(a)))
                for (g = na(h), f = na(a), d = 0, e = f.length; d < e; d++) Ia(f[d], g[d]);
            if (b)
                if (c)
                    for (f = f || na(a), g = g || na(h), d = 0, e = f.length; d < e; d++) Ha(f[d], g[d]);
                else Ha(a, h);
            return g = na(h, "script"), g.length > 0 && oa(g, !i && na(a, "script")), h
        },
        cleanData: function(a) {
            for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++)
                if (U(c)) {
                    if (b = c[W.expando]) {
                        if (b.events)
                            for (d in b.events) e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
                        c[W.expando] = void 0
                    }
                    c[X.expando] && (c[X.expando] = void 0)
                }
        }
    }), r.fn.extend({
        detach: function(a) {
            return Ka(this, a, !0)
        },
        remove: function(a) {
            return Ka(this, a)
        },
        text: function(a) {
            return T(this, function(a) {
                return void 0 === a ? r.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a)
                })
            }, null, a, arguments.length)
        },
        append: function() {
            return Ja(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Ea(this, a);
                    b.appendChild(a)
                }
            })
        },
        prepend: function() {
            return Ja(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Ea(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        },
        before: function() {
            return Ja(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        },
        after: function() {
            return Ja(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (r.cleanData(na(a, !1)), a.textContent = "");
            return this
        },
        clone: function(a, b) {
            return a = null != a && a, b = null == b ? a : b, this.map(function() {
                return r.clone(this, a, b)
            })
        },
        html: function(a) {
            return T(this, function(a) {
                var b = this[0] || {},
                    c = 0,
                    d = this.length;
                if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
                if ("string" == typeof a && !Aa.test(a) && !ma[(ka.exec(a) || ["", ""])[1].toLowerCase()]) {
                    a = r.htmlPrefilter(a);
                    try {
                        for (; c < d; c++) b = this[c] || {}, 1 === b.nodeType && (r.cleanData(na(b, !1)), b.innerHTML = a);
                        b = 0
                    } catch (e) {}
                }
                b && this.empty().append(a)
            }, null, a, arguments.length)
        },
        replaceWith: function() {
            var a = [];
            return Ja(this, arguments, function(b) {
                var c = this.parentNode;
                r.inArray(this, a) < 0 && (r.cleanData(na(this)), c && c.replaceChild(b, this))
            }, a)
        }
    }), r.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        r.fn[a] = function(a) {
            for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++) c = g === f ? this : this.clone(!0), r(e[g])[b](c), h.apply(d, c.get());
            return this.pushStack(d)
        }
    });
    var La = /^margin/,
        Ma = new RegExp("^(" + aa + ")(?!px)[a-z%]+$", "i"),
        Na = function(b) {
            var c = b.ownerDocument.defaultView;
            return c && c.opener || (c = a), c.getComputedStyle(b)
        };
    ! function() {
        function b() {
            if (i) {
                i.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", i.innerHTML = "", ra.appendChild(h);
                var b = a.getComputedStyle(i);
                c = "1%" !== b.top, g = "2px" === b.marginLeft, e = "4px" === b.width, i.style.marginRight = "50%", f = "4px" === b.marginRight, ra.removeChild(h), i = null
            }
        }
        var c, e, f, g, h = d.createElement("div"),
            i = d.createElement("div");
        i.style && (i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", o.clearCloneStyle = "content-box" === i.style.backgroundClip, h.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", h.appendChild(i), r.extend(o, {
            pixelPosition: function() {
                return b(), c
            },
            boxSizingReliable: function() {
                return b(), e
            },
            pixelMarginRight: function() {
                return b(), f
            },
            reliableMarginLeft: function() {
                return b(), g
            }
        }))
    }();

    function Oa(a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || Na(a), c && (g = c.getPropertyValue(b) || c[b], "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)), !o.pixelMarginRight() && Ma.test(g) && La.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g
    }

    function Pa(a, b) {
        return {
            get: function() {
                return a() ? void delete this.get : (this.get = b).apply(this, arguments)
            }
        }
    }
    var Qa = /^(none|table(?!-c[ea]).+)/,
        Ra = /^--/,
        Sa = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Ta = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        Ua = ["Webkit", "Moz", "ms"],
        Va = d.createElement("div").style;

    function Wa(a) {
        if (a in Va) return a;
        var b = a[0].toUpperCase() + a.slice(1),
            c = Ua.length;
        while (c--)
            if (a = Ua[c] + b, a in Va) return a
    }

    function Xa(a) {
        var b = r.cssProps[a];
        return b || (b = r.cssProps[a] = Wa(a) || a), b
    }

    function Ya(a, b, c) {
        var d = ba.exec(b);
        return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b
    }

    function Za(a, b, c, d, e) {
        var f, g = 0;
        for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2) "margin" === c && (g += r.css(a, c + ca[f], !0, e)), d ? ("content" === c && (g -= r.css(a, "padding" + ca[f], !0, e)), "margin" !== c && (g -= r.css(a, "border" + ca[f] + "Width", !0, e))) : (g += r.css(a, "padding" + ca[f], !0, e), "padding" !== c && (g += r.css(a, "border" + ca[f] + "Width", !0, e)));
        return g
    }

    function $a(a, b, c) {
        var d, e = Na(a),
            f = Oa(a, b, e),
            g = "border-box" === r.css(a, "boxSizing", !1, e);
        return Ma.test(f) ? f : (d = g && (o.boxSizingReliable() || f === a.style[b]), "auto" === f && (f = a["offset" + b[0].toUpperCase() + b.slice(1)]), f = parseFloat(f) || 0, f + Za(a, b, c || (g ? "border" : "content"), d, e) + "px")
    }
    r.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        var c = Oa(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = r.camelCase(b),
                    i = Ra.test(b),
                    j = a.style;
                return i || (b = Xa(h)), g = r.cssHooks[b] || r.cssHooks[h], void 0 === c ? g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : j[b] : (f = typeof c, "string" === f && (e = ba.exec(c)) && e[1] && (c = fa(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (r.cssNumber[h] ? "" : "px")), o.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (j[b] = "inherit"), g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i ? j.setProperty(b, c) : j[b] = c)), void 0)
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = r.camelCase(b),
                i = Ra.test(b);
            return i || (b = Xa(h)), g = r.cssHooks[b] || r.cssHooks[h], g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = Oa(a, b, d)), "normal" === e && b in Ta && (e = Ta[b]), "" === c || c ? (f = parseFloat(e), c === !0 || isFinite(f) ? f || 0 : e) : e
        }
    }), r.each(["height", "width"], function(a, b) {
        r.cssHooks[b] = {
            get: function(a, c, d) {
                if (c) return !Qa.test(r.css(a, "display")) || a.getClientRects().length && a.getBoundingClientRect().width ? $a(a, b, d) : ea(a, Sa, function() {
                    return $a(a, b, d)
                })
            },
            set: function(a, c, d) {
                var e, f = d && Na(a),
                    g = d && Za(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);
                return g && (e = ba.exec(c)) && "px" !== (e[3] || "px") && (a.style[b] = c, c = r.css(a, b)), Ya(a, c, g)
            }
        }
    }), r.cssHooks.marginLeft = Pa(o.reliableMarginLeft, function(a, b) {
        if (b) return (parseFloat(Oa(a, "marginLeft")) || a.getBoundingClientRect().left - ea(a, {
            marginLeft: 0
        }, function() {
            return a.getBoundingClientRect().left
        })) + "px"
    }), r.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        r.cssHooks[a + b] = {
            expand: function(c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) e[a + ca[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, La.test(a) || (r.cssHooks[a + b].set = Ya)
    }), r.fn.extend({
        css: function(a, b) {
            return T(this, function(a, b, c) {
                var d, e, f = {},
                    g = 0;
                if (Array.isArray(b)) {
                    for (d = Na(a), e = b.length; g < e; g++) f[b[g]] = r.css(a, b[g], !1, d);
                    return f
                }
                return void 0 !== c ? r.style(a, b, c) : r.css(a, b)
            }, a, b, arguments.length > 1)
        }
    });

    function _a(a, b, c, d, e) {
        return new _a.prototype.init(a, b, c, d, e)
    }
    r.Tween = _a, _a.prototype = {
        constructor: _a,
        init: function(a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || r.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (r.cssNumber[c] ? "" : "px")
        },
        cur: function() {
            var a = _a.propHooks[this.prop];
            return a && a.get ? a.get(this) : _a.propHooks._default.get(this)
        },
        run: function(a) {
            var b, c = _a.propHooks[this.prop];
            return this.options.duration ? this.pos = b = r.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : _a.propHooks._default.set(this), this
        }
    }, _a.prototype.init.prototype = _a.prototype, _a.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = r.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0)
            },
            set: function(a) {
                r.fx.step[a.prop] ? r.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop] ? a.elem[a.prop] = a.now : r.style(a.elem, a.prop, a.now + a.unit)
            }
        }
    }, _a.propHooks.scrollTop = _a.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, r.easing = {
        linear: function(a) {
            return a
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2
        },
        _default: "swing"
    }, r.fx = _a.prototype.init, r.fx.step = {};
    var ab, bb, cb = /^(?:toggle|show|hide)$/,
        db = /queueHooks$/;

    function eb() {
        bb && (d.hidden === !1 && a.requestAnimationFrame ? a.requestAnimationFrame(eb) : a.setTimeout(eb, r.fx.interval), r.fx.tick())
    }

    function fb() {
        return a.setTimeout(function() {
            ab = void 0
        }), ab = r.now()
    }

    function gb(a, b) {
        var c, d = 0,
            e = {
                height: a
            };
        for (b = b ? 1 : 0; d < 4; d += 2 - b) c = ca[d], e["margin" + c] = e["padding" + c] = a;
        return b && (e.opacity = e.width = a), e
    }

    function hb(a, b, c) {
        for (var d, e = (kb.tweeners[b] || []).concat(kb.tweeners["*"]), f = 0, g = e.length; f < g; f++)
            if (d = e[f].call(c, b, a)) return d
    }

    function ib(a, b, c) {
        var d, e, f, g, h, i, j, k, l = "width" in b || "height" in b,
            m = this,
            n = {},
            o = a.style,
            p = a.nodeType && da(a),
            q = W.get(a, "fxshow");
        c.queue || (g = r._queueHooks(a, "fx"), null == g.unqueued && (g.unqueued = 0, h = g.empty.fire, g.empty.fire = function() {
            g.unqueued || h()
        }), g.unqueued++, m.always(function() {
            m.always(function() {
                g.unqueued--, r.queue(a, "fx").length || g.empty.fire()
            })
        }));
        for (d in b)
            if (e = b[d], cb.test(e)) {
                if (delete b[d], f = f || "toggle" === e, e === (p ? "hide" : "show")) {
                    if ("show" !== e || !q || void 0 === q[d]) continue;
                    p = !0
                }
                n[d] = q && q[d] || r.style(a, d)
            }
        if (i = !r.isEmptyObject(b), i || !r.isEmptyObject(n)) {
            l && 1 === a.nodeType && (c.overflow = [o.overflow, o.overflowX, o.overflowY], j = q && q.display, null == j && (j = W.get(a, "display")), k = r.css(a, "display"), "none" === k && (j ? k = j : (ia([a], !0), j = a.style.display || j, k = r.css(a, "display"), ia([a]))), ("inline" === k || "inline-block" === k && null != j) && "none" === r.css(a, "float") && (i || (m.done(function() {
                o.display = j
            }), null == j && (k = o.display, j = "none" === k ? "" : k)), o.display = "inline-block")), c.overflow && (o.overflow = "hidden", m.always(function() {
                o.overflow = c.overflow[0], o.overflowX = c.overflow[1], o.overflowY = c.overflow[2]
            })), i = !1;
            for (d in n) i || (q ? "hidden" in q && (p = q.hidden) : q = W.access(a, "fxshow", {
                display: j
            }), f && (q.hidden = !p), p && ia([a], !0), m.done(function() {
                p || ia([a]), W.remove(a, "fxshow");
                for (d in n) r.style(a, d, n[d])
            })), i = hb(p ? q[d] : 0, d, m), d in q || (q[d] = i.start, p && (i.end = i.start, i.start = 0))
        }
    }

    function jb(a, b) {
        var c, d, e, f, g;
        for (c in a)
            if (d = r.camelCase(c), e = b[d], f = a[c], Array.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = r.cssHooks[d], g && "expand" in g) {
                f = g.expand(f), delete a[d];
                for (c in f) c in a || (a[c] = f[c], b[c] = e)
            } else b[d] = e
    }

    function kb(a, b, c) {
        var d, e, f = 0,
            g = kb.prefilters.length,
            h = r.Deferred().always(function() {
                delete i.elem
            }),
            i = function() {
                if (e) return !1;
                for (var b = ab || fb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) j.tweens[g].run(f);
                return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (i || h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j]), !1)
            },
            j = h.promise({
                elem: a,
                props: r.extend({}, b),
                opts: r.extend(!0, {
                    specialEasing: {},
                    easing: r.easing._default
                }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: ab || fb(),
                duration: c.duration,
                tweens: [],
                createTween: function(b, c) {
                    var d = r.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                    return j.tweens.push(d), d
                },
                stop: function(b) {
                    var c = 0,
                        d = b ? j.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; c < d; c++) j.tweens[c].run(1);
                    return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this
                }
            }),
            k = j.props;
        for (jb(k, j.opts.specialEasing); f < g; f++)
            if (d = kb.prefilters[f].call(j, a, k, j.opts)) return r.isFunction(d.stop) && (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)), d;
        return r.map(k, hb, j), r.isFunction(j.opts.start) && j.opts.start.call(a, j), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always), r.fx.timer(r.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j
    }
    r.Animation = r.extend(kb, {
            tweeners: {
                "*": [function(a, b) {
                    var c = this.createTween(a, b);
                    return fa(c.elem, a, ba.exec(b), c), c
                }]
            },
            tweener: function(a, b) {
                r.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(L);
                for (var c, d = 0, e = a.length; d < e; d++) c = a[d], kb.tweeners[c] = kb.tweeners[c] || [], kb.tweeners[c].unshift(b)
            },
            prefilters: [ib],
            prefilter: function(a, b) {
                b ? kb.prefilters.unshift(a) : kb.prefilters.push(a)
            }
        }), r.speed = function(a, b, c) {
            var d = a && "object" == typeof a ? r.extend({}, a) : {
                complete: c || !c && b || r.isFunction(a) && a,
                duration: a,
                easing: c && b || b && !r.isFunction(b) && b
            };
            return r.fx.off ? d.duration = 0 : "number" != typeof d.duration && (d.duration in r.fx.speeds ? d.duration = r.fx.speeds[d.duration] : d.duration = r.fx.speeds._default), null != d.queue && d.queue !== !0 || (d.queue = "fx"), d.old = d.complete, d.complete = function() {
                r.isFunction(d.old) && d.old.call(this), d.queue && r.dequeue(this, d.queue)
            }, d
        }, r.fn.extend({
            fadeTo: function(a, b, c, d) {
                return this.filter(da).css("opacity", 0).show().end().animate({
                    opacity: b
                }, a, c, d)
            },
            animate: function(a, b, c, d) {
                var e = r.isEmptyObject(a),
                    f = r.speed(b, c, d),
                    g = function() {
                        var b = kb(this, r.extend({}, a), f);
                        (e || W.get(this, "finish")) && b.stop(!0)
                    };
                return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
            },
            stop: function(a, b, c) {
                var d = function(a) {
                    var b = a.stop;
                    delete a.stop, b(c)
                };
                return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function() {
                    var b = !0,
                        e = null != a && a + "queueHooks",
                        f = r.timers,
                        g = W.get(this);
                    if (e) g[e] && g[e].stop && d(g[e]);
                    else
                        for (e in g) g[e] && g[e].stop && db.test(e) && d(g[e]);
                    for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
                    !b && c || r.dequeue(this, a)
                })
            },
            finish: function(a) {
                return a !== !1 && (a = a || "fx"), this.each(function() {
                    var b, c = W.get(this),
                        d = c[a + "queue"],
                        e = c[a + "queueHooks"],
                        f = r.timers,
                        g = d ? d.length : 0;
                    for (c.finish = !0, r.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                    for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
                    delete c.finish
                })
            }
        }), r.each(["toggle", "show", "hide"], function(a, b) {
            var c = r.fn[b];
            r.fn[b] = function(a, d, e) {
                return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(gb(b, !0), a, d, e)
            }
        }), r.each({
            slideDown: gb("show"),
            slideUp: gb("hide"),
            slideToggle: gb("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(a, b) {
            r.fn[a] = function(a, c, d) {
                return this.animate(b, a, c, d)
            }
        }), r.timers = [], r.fx.tick = function() {
            var a, b = 0,
                c = r.timers;
            for (ab = r.now(); b < c.length; b++) a = c[b], a() || c[b] !== a || c.splice(b--, 1);
            c.length || r.fx.stop(), ab = void 0
        }, r.fx.timer = function(a) {
            r.timers.push(a), r.fx.start()
        }, r.fx.interval = 13, r.fx.start = function() {
            bb || (bb = !0, eb())
        }, r.fx.stop = function() {
            bb = null
        }, r.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, r.fn.delay = function(b, c) {
            return b = r.fx ? r.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function(c, d) {
                var e = a.setTimeout(c, b);
                d.stop = function() {
                    a.clearTimeout(e)
                }
            })
        },
        function() {
            var a = d.createElement("input"),
                b = d.createElement("select"),
                c = b.appendChild(d.createElement("option"));
            a.type = "checkbox", o.checkOn = "" !== a.value, o.optSelected = c.selected, a = d.createElement("input"), a.value = "t", a.type = "radio", o.radioValue = "t" === a.value
        }();
    var lb, mb = r.expr.attrHandle;
    r.fn.extend({
        attr: function(a, b) {
            return T(this, r.attr, a, b, arguments.length > 1)
        },
        removeAttr: function(a) {
            return this.each(function() {
                r.removeAttr(this, a)
            })
        }
    }), r.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? r.prop(a, b, c) : (1 === f && r.isXMLDoc(a) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? lb : void 0)), void 0 !== c ? null === c ? void r.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = r.find.attr(a, b),
                null == d ? void 0 : d))
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!o.radioValue && "radio" === b && B(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        },
        removeAttr: function(a, b) {
            var c, d = 0,
                e = b && b.match(L);
            if (e && 1 === a.nodeType)
                while (c = e[d++]) a.removeAttribute(c)
        }
    }), lb = {
        set: function(a, b, c) {
            return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c
        }
    }, r.each(r.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = mb[b] || r.find.attr;
        mb[b] = function(a, b, d) {
            var e, f, g = b.toLowerCase();
            return d || (f = mb[g], mb[g] = e, e = null != c(a, b, d) ? g : null, mb[g] = f), e
        }
    });
    var nb = /^(?:input|select|textarea|button)$/i,
        ob = /^(?:a|area)$/i;
    r.fn.extend({
        prop: function(a, b) {
            return T(this, r.prop, a, b, arguments.length > 1)
        },
        removeProp: function(a) {
            return this.each(function() {
                delete this[r.propFix[a] || a]
            })
        }
    }), r.extend({
        prop: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return 1 === f && r.isXMLDoc(a) || (b = r.propFix[b] || b, e = r.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    var b = r.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : nb.test(a.nodeName) || ob.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), o.optSelected || (r.propHooks.selected = {
        get: function(a) {
            var b = a.parentNode;
            return b && b.parentNode && b.parentNode.selectedIndex, null
        },
        set: function(a) {
            var b = a.parentNode;
            b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)
        }
    }), r.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        r.propFix[this.toLowerCase()] = this
    });

    function pb(a) {
        var b = a.match(L) || [];
        return b.join(" ")
    }

    function qb(a) {
        return a.getAttribute && a.getAttribute("class") || ""
    }
    r.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (r.isFunction(a)) return this.each(function(b) {
                r(this).addClass(a.call(this, b, qb(this)))
            });
            if ("string" == typeof a && a) {
                b = a.match(L) || [];
                while (c = this[i++])
                    if (e = qb(c), d = 1 === c.nodeType && " " + pb(e) + " ") {
                        g = 0;
                        while (f = b[g++]) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
                        h = pb(d), e !== h && c.setAttribute("class", h)
                    }
            }
            return this
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (r.isFunction(a)) return this.each(function(b) {
                r(this).removeClass(a.call(this, b, qb(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof a && a) {
                b = a.match(L) || [];
                while (c = this[i++])
                    if (e = qb(c), d = 1 === c.nodeType && " " + pb(e) + " ") {
                        g = 0;
                        while (f = b[g++])
                            while (d.indexOf(" " + f + " ") > -1) d = d.replace(" " + f + " ", " ");
                        h = pb(d), e !== h && c.setAttribute("class", h)
                    }
            }
            return this
        },
        toggleClass: function(a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : r.isFunction(a) ? this.each(function(c) {
                r(this).toggleClass(a.call(this, c, qb(this), b), b)
            }) : this.each(function() {
                var b, d, e, f;
                if ("string" === c) {
                    d = 0, e = r(this), f = a.match(L) || [];
                    while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b)
                } else void 0 !== a && "boolean" !== c || (b = qb(this), b && W.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : W.get(this, "__className__") || ""))
            })
        },
        hasClass: function(a) {
            var b, c, d = 0;
            b = " " + a + " ";
            while (c = this[d++])
                if (1 === c.nodeType && (" " + pb(qb(c)) + " ").indexOf(b) > -1) return !0;
            return !1
        }
    });
    var rb = /\r/g;
    r.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0]; {
                if (arguments.length) return d = r.isFunction(a), this.each(function(c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, r(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : Array.isArray(e) && (e = r.map(e, function(a) {
                        return null == a ? "" : a + ""
                    })), b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
                });
                if (e) return b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c)
            }
        }
    }), r.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = r.find.attr(a, "value");
                    return null != b ? b : pb(r.text(a))
                }
            },
            select: {
                get: function(a) {
                    var b, c, d, e = a.options,
                        f = a.selectedIndex,
                        g = "select-one" === a.type,
                        h = g ? null : [],
                        i = g ? f + 1 : e.length;
                    for (d = f < 0 ? i : g ? f : 0; d < i; d++)
                        if (c = e[d], (c.selected || d === f) && !c.disabled && (!c.parentNode.disabled || !B(c.parentNode, "optgroup"))) {
                            if (b = r(c).val(), g) return b;
                            h.push(b)
                        }
                    return h
                },
                set: function(a, b) {
                    var c, d, e = a.options,
                        f = r.makeArray(b),
                        g = e.length;
                    while (g--) d = e[g], (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);
                    return c || (a.selectedIndex = -1), f
                }
            }
        }
    }), r.each(["radio", "checkbox"], function() {
        r.valHooks[this] = {
            set: function(a, b) {
                if (Array.isArray(b)) return a.checked = r.inArray(r(a).val(), b) > -1
            }
        }, o.checkOn || (r.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var sb = /^(?:focusinfocus|focusoutblur)$/;
    r.extend(r.event, {
        trigger: function(b, c, e, f) {
            var g, h, i, j, k, m, n, o = [e || d],
                p = l.call(b, "type") ? b.type : b,
                q = l.call(b, "namespace") ? b.namespace.split(".") : [];
            if (h = i = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !sb.test(p + r.event.triggered) && (p.indexOf(".") > -1 && (q = p.split("."), p = q.shift(), q.sort()), k = p.indexOf(":") < 0 && "on" + p, b = b[r.expando] ? b : new r.Event(p, "object" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = q.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : r.makeArray(c, [b]), n = r.event.special[p] || {}, f || !n.trigger || n.trigger.apply(e, c) !== !1)) {
                if (!f && !n.noBubble && !r.isWindow(e)) {
                    for (j = n.delegateType || p, sb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) o.push(h), i = h;
                    i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a)
                }
                g = 0;
                while ((h = o[g++]) && !b.isPropagationStopped()) b.type = g > 1 ? j : n.bindType || p, m = (W.get(h, "events") || {})[b.type] && W.get(h, "handle"), m && m.apply(h, c), m = k && h[k], m && m.apply && U(h) && (b.result = m.apply(h, c), b.result === !1 && b.preventDefault());
                return b.type = p, f || b.isDefaultPrevented() || n._default && n._default.apply(o.pop(), c) !== !1 || !U(e) || k && r.isFunction(e[p]) && !r.isWindow(e) && (i = e[k], i && (e[k] = null), r.event.triggered = p, e[p](), r.event.triggered = void 0, i && (e[k] = i)), b.result
            }
        },
        simulate: function(a, b, c) {
            var d = r.extend(new r.Event, c, {
                type: a,
                isSimulated: !0
            });
            r.event.trigger(d, null, b)
        }
    }), r.fn.extend({
        trigger: function(a, b) {
            return this.each(function() {
                r.event.trigger(a, b, this)
            })
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            if (c) return r.event.trigger(a, b, c, !0)
        }
    }), r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(a, b) {
        r.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), r.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }
    }), o.focusin = "onfocusin" in a, o.focusin || r.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        var c = function(a) {
            r.event.simulate(b, a.target, r.event.fix(a))
        };
        r.event.special[b] = {
            setup: function() {
                var d = this.ownerDocument || this,
                    e = W.access(d, b);
                e || d.addEventListener(a, c, !0), W.access(d, b, (e || 0) + 1)
            },
            teardown: function() {
                var d = this.ownerDocument || this,
                    e = W.access(d, b) - 1;
                e ? W.access(d, b, e) : (d.removeEventListener(a, c, !0), W.remove(d, b))
            }
        }
    });
    var tb = a.location,
        ub = r.now(),
        vb = /\?/;
    r.parseXML = function(b) {
        var c;
        if (!b || "string" != typeof b) return null;
        try {
            c = (new a.DOMParser).parseFromString(b, "text/xml")
        } catch (d) {
            c = void 0
        }
        return c && !c.getElementsByTagName("parsererror").length || r.error("Invalid XML: " + b), c
    };
    var wb = /\[\]$/,
        xb = /\r?\n/g,
        yb = /^(?:submit|button|image|reset|file)$/i,
        zb = /^(?:input|select|textarea|keygen)/i;

    function Ab(a, b, c, d) {
        var e;
        if (Array.isArray(b)) r.each(b, function(b, e) {
            c || wb.test(a) ? d(a, e) : Ab(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d)
        });
        else if (c || "object" !== r.type(b)) d(a, b);
        else
            for (e in b) Ab(a + "[" + e + "]", b[e], c, d)
    }
    r.param = function(a, b) {
        var c, d = [],
            e = function(a, b) {
                var c = r.isFunction(b) ? b() : b;
                d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c)
            };
        if (Array.isArray(a) || a.jquery && !r.isPlainObject(a)) r.each(a, function() {
            e(this.name, this.value)
        });
        else
            for (c in a) Ab(c, a[c], b, e);
        return d.join("&")
    }, r.fn.extend({
        serialize: function() {
            return r.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var a = r.prop(this, "elements");
                return a ? r.makeArray(a) : this
            }).filter(function() {
                var a = this.type;
                return this.name && !r(this).is(":disabled") && zb.test(this.nodeName) && !yb.test(a) && (this.checked || !ja.test(a))
            }).map(function(a, b) {
                var c = r(this).val();
                return null == c ? null : Array.isArray(c) ? r.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(xb, "\r\n")
                    }
                }) : {
                    name: b.name,
                    value: c.replace(xb, "\r\n")
                }
            }).get()
        }
    });
    var Bb = /%20/g,
        Cb = /#.*$/,
        Db = /([?&])_=[^&]*/,
        Eb = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Fb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Gb = /^(?:GET|HEAD)$/,
        Hb = /^\/\//,
        Ib = {},
        Jb = {},
        Kb = "*/".concat("*"),
        Lb = d.createElement("a");
    Lb.href = tb.href;

    function Mb(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0,
                f = b.toLowerCase().match(L) || [];
            if (r.isFunction(c))
                while (d = f[e++]) "+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function Nb(a, b, c, d) {
        var e = {},
            f = a === Jb;

        function g(h) {
            var i;
            return e[h] = !0, r.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)
            }), i
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*")
    }

    function Ob(a, b) {
        var c, d, e = r.ajaxSettings.flatOptions || {};
        for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
        return d && r.extend(!0, a, d), a
    }

    function Pb(a, b, c) {
        var d, e, f, g, h = a.contents,
            i = a.dataTypes;
        while ("*" === i[0]) i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
        if (d)
            for (e in h)
                if (h[e] && h[e].test(d)) {
                    i.unshift(e);
                    break
                }
        if (i[0] in c) f = i[0];
        else {
            for (e in c) {
                if (!i[0] || a.converters[e + " " + i[0]]) {
                    f = e;
                    break
                }
                g || (g = e)
            }
            f = f || g
        }
        if (f) return f !== i[0] && i.unshift(f), c[f]
    }

    function Qb(a, b, c, d) {
        var e, f, g, h, i, j = {},
            k = a.dataTypes.slice();
        if (k[1])
            for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        f = k.shift();
        while (f)
            if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
                if ("*" === f) f = i;
                else if ("*" !== i && i !== f) {
            if (g = j[i + " " + f] || j["* " + f], !g)
                for (e in j)
                    if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                        g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                        break
                    }
            if (g !== !0)
                if (g && a["throws"]) b = g(b);
                else try {
                    b = g(b)
                } catch (l) {
                    return {
                        state: "parsererror",
                        error: g ? l : "No conversion from " + i + " to " + f
                    }
                }
        }
        return {
            state: "success",
            data: b
        }
    }
    r.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: tb.href,
            type: "GET",
            isLocal: Fb.test(tb.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Kb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": r.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? Ob(Ob(a, r.ajaxSettings), b) : Ob(r.ajaxSettings, a)
        },
        ajaxPrefilter: Mb(Ib),
        ajaxTransport: Mb(Jb),
        ajax: function(b, c) {
            "object" == typeof b && (c = b, b = void 0), c = c || {};
            var e, f, g, h, i, j, k, l, m, n, o = r.ajaxSetup({}, c),
                p = o.context || o,
                q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event,
                s = r.Deferred(),
                t = r.Callbacks("once memory"),
                u = o.statusCode || {},
                v = {},
                w = {},
                x = "canceled",
                y = {
                    readyState: 0,
                    getResponseHeader: function(a) {
                        var b;
                        if (k) {
                            if (!h) {
                                h = {};
                                while (b = Eb.exec(g)) h[b[1].toLowerCase()] = b[2]
                            }
                            b = h[a.toLowerCase()]
                        }
                        return null == b ? null : b
                    },
                    getAllResponseHeaders: function() {
                        return k ? g : null
                    },
                    setRequestHeader: function(a, b) {
                        return null == k && (a = w[a.toLowerCase()] = w[a.toLowerCase()] || a, v[a] = b), this
                    },
                    overrideMimeType: function(a) {
                        return null == k && (o.mimeType = a), this
                    },
                    statusCode: function(a) {
                        var b;
                        if (a)
                            if (k) y.always(a[y.status]);
                            else
                                for (b in a) u[b] = [u[b], a[b]];
                        return this
                    },
                    abort: function(a) {
                        var b = a || x;
                        return e && e.abort(b), A(0, b), this
                    }
                };
            if (s.promise(y), o.url = ((b || o.url || tb.href) + "").replace(Hb, tb.protocol + "//"), o.type = c.method || c.type || o.method || o.type, o.dataTypes = (o.dataType || "*").toLowerCase().match(L) || [""], null == o.crossDomain) {
                j = d.createElement("a");
                try {
                    j.href = o.url, j.href = j.href, o.crossDomain = Lb.protocol + "//" + Lb.host != j.protocol + "//" + j.host
                } catch (z) {
                    o.crossDomain = !0
                }
            }
            if (o.data && o.processData && "string" != typeof o.data && (o.data = r.param(o.data, o.traditional)), Nb(Ib, o, c, y), k) return y;
            l = r.event && o.global, l && 0 === r.active++ && r.event.trigger("ajaxStart"), o.type = o.type.toUpperCase(), o.hasContent = !Gb.test(o.type), f = o.url.replace(Cb, ""), o.hasContent ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(Bb, "+")) : (n = o.url.slice(f.length), o.data && (f += (vb.test(f) ? "&" : "?") + o.data, delete o.data), o.cache === !1 && (f = f.replace(Db, "$1"), n = (vb.test(f) ? "&" : "?") + "_=" + ub++ + n), o.url = f + n), o.ifModified && (r.lastModified[f] && y.setRequestHeader("If-Modified-Since", r.lastModified[f]), r.etag[f] && y.setRequestHeader("If-None-Match", r.etag[f])), (o.data && o.hasContent && o.contentType !== !1 || c.contentType) && y.setRequestHeader("Content-Type", o.contentType), y.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Kb + "; q=0.01" : "") : o.accepts["*"]);
            for (m in o.headers) y.setRequestHeader(m, o.headers[m]);
            if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k)) return y.abort();
            if (x = "abort", t.add(o.complete), y.done(o.success), y.fail(o.error), e = Nb(Jb, o, c, y)) {
                if (y.readyState = 1, l && q.trigger("ajaxSend", [y, o]), k) return y;
                o.async && o.timeout > 0 && (i = a.setTimeout(function() {
                    y.abort("timeout")
                }, o.timeout));
                try {
                    k = !1, e.send(v, A)
                } catch (z) {
                    if (k) throw z;
                    A(-1, z)
                }
            } else A(-1, "No Transport");

            function A(b, c, d, h) {
                var j, m, n, v, w, x = c;
                k || (k = !0, i && a.clearTimeout(i), e = void 0, g = h || "", y.readyState = b > 0 ? 4 : 0, j = b >= 200 && b < 300 || 304 === b, d && (v = Pb(o, y, d)), v = Qb(o, v, y, j), j ? (o.ifModified && (w = y.getResponseHeader("Last-Modified"), w && (r.lastModified[f] = w), w = y.getResponseHeader("etag"), w && (r.etag[f] = w)), 204 === b || "HEAD" === o.type ? x = "nocontent" : 304 === b ? x = "notmodified" : (x = v.state, m = v.data, n = v.error, j = !n)) : (n = x, !b && x || (x = "error", b < 0 && (b = 0))), y.status = b, y.statusText = (c || x) + "", j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]), y.statusCode(u), u = void 0, l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [y, o, j ? m : n]), t.fireWith(p, [y, x]), l && (q.trigger("ajaxComplete", [y, o]), --r.active || r.event.trigger("ajaxStop")))
            }
            return y
        },
        getJSON: function(a, b, c) {
            return r.get(a, b, c, "json")
        },
        getScript: function(a, b) {
            return r.get(a, void 0, b, "script")
        }
    }), r.each(["get", "post"], function(a, b) {
        r[b] = function(a, c, d, e) {
            return r.isFunction(c) && (e = e || d, d = c, c = void 0), r.ajax(r.extend({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            }, r.isPlainObject(a) && a))
        }
    }), r._evalUrl = function(a) {
        return r.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }, r.fn.extend({
        wrapAll: function(a) {
            var b;
            return this[0] && (r.isFunction(a) && (a = a.call(this[0])), b = r(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                var a = this;
                while (a.firstElementChild) a = a.firstElementChild;
                return a
            }).append(this)), this
        },
        wrapInner: function(a) {
            return r.isFunction(a) ? this.each(function(b) {
                r(this).wrapInner(a.call(this, b))
            }) : this.each(function() {
                var b = r(this),
                    c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        },
        wrap: function(a) {
            var b = r.isFunction(a);
            return this.each(function(c) {
                r(this).wrapAll(b ? a.call(this, c) : a)
            })
        },
        unwrap: function(a) {
            return this.parent(a).not("body").each(function() {
                r(this).replaceWith(this.childNodes)
            }), this
        }
    }), r.expr.pseudos.hidden = function(a) {
        return !r.expr.pseudos.visible(a)
    }, r.expr.pseudos.visible = function(a) {
        return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length)
    }, r.ajaxSettings.xhr = function() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {}
    };
    var Rb = {
            0: 200,
            1223: 204
        },
        Sb = r.ajaxSettings.xhr();
    o.cors = !!Sb && "withCredentials" in Sb, o.ajax = Sb = !!Sb, r.ajaxTransport(function(b) {
        var c, d;
        if (o.cors || Sb && !b.crossDomain) return {
            send: function(e, f) {
                var g, h = b.xhr();
                if (h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)
                    for (g in b.xhrFields) h[g] = b.xhrFields[g];
                b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");
                for (g in e) h.setRequestHeader(g, e[g]);
                c = function(a) {
                    return function() {
                        c && (c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null, "abort" === a ? h.abort() : "error" === a ? "number" != typeof h.status ? f(0, "error") : f(h.status, h.statusText) : f(Rb[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? {
                            binary: h.response
                        } : {
                            text: h.responseText
                        }, h.getAllResponseHeaders()))
                    }
                }, h.onload = c(), d = h.onerror = c("error"), void 0 !== h.onabort ? h.onabort = d : h.onreadystatechange = function() {
                    4 === h.readyState && a.setTimeout(function() {
                        c && d()
                    })
                }, c = c("abort");
                try {
                    h.send(b.hasContent && b.data || null)
                } catch (i) {
                    if (c) throw i
                }
            },
            abort: function() {
                c && c()
            }
        }
    }), r.ajaxPrefilter(function(a) {
        a.crossDomain && (a.contents.script = !1)
    }), r.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(a) {
                return r.globalEval(a), a
            }
        }
    }), r.ajaxPrefilter("script", function(a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET")
    }), r.ajaxTransport("script", function(a) {
        if (a.crossDomain) {
            var b, c;
            return {
                send: function(e, f) {
                    b = r("<script>").prop({
                        charset: a.scriptCharset,
                        src: a.url
                    }).on("load error", c = function(a) {
                        b.remove(), c = null, a && f("error" === a.type ? 404 : 200, a.type)
                    }), d.head.appendChild(b[0])
                },
                abort: function() {
                    c && c()
                }
            }
        }
    });
    var Tb = [],
        Ub = /(=)\?(?=&|$)|\?\?/;
    r.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = Tb.pop() || r.expando + "_" + ub++;
            return this[a] = !0, a
        }
    }), r.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = b.jsonp !== !1 && (Ub.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && Ub.test(b.data) && "data");
        if (h || "jsonp" === b.dataTypes[0]) return e = b.jsonpCallback = r.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(Ub, "$1" + e) : b.jsonp !== !1 && (b.url += (vb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function() {
            return g || r.error(e + " was not called"), g[0]
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function() {
            g = arguments
        }, d.always(function() {
            void 0 === f ? r(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, Tb.push(e)), g && r.isFunction(f) && f(g[0]), g = f = void 0
        }), "script"
    }), o.createHTMLDocument = function() {
        var a = d.implementation.createHTMLDocument("").body;
        return a.innerHTML = "<form></form><form></form>", 2 === a.childNodes.length
    }(), r.parseHTML = function(a, b, c) {
        if ("string" != typeof a) return [];
        "boolean" == typeof b && (c = b, b = !1);
        var e, f, g;
        return b || (o.createHTMLDocument ? (b = d.implementation.createHTMLDocument(""), e = b.createElement("base"), e.href = d.location.href, b.head.appendChild(e)) : b = d), f = C.exec(a), g = !c && [], f ? [b.createElement(f[1])] : (f = qa([a], b, g), g && g.length && r(g).remove(), r.merge([], f.childNodes))
    }, r.fn.load = function(a, b, c) {
        var d, e, f, g = this,
            h = a.indexOf(" ");
        return h > -1 && (d = pb(a.slice(h)), a = a.slice(0, h)), r.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && r.ajax({
            url: a,
            type: e || "GET",
            dataType: "html",
            data: b
        }).done(function(a) {
            f = arguments, g.html(d ? r("<div>").append(r.parseHTML(a)).find(d) : a)
        }).always(c && function(a, b) {
            g.each(function() {
                c.apply(this, f || [a.responseText, b, a])
            })
        }), this
    }, r.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(a, b) {
        r.fn[b] = function(a) {
            return this.on(b, a)
        }
    }), r.expr.pseudos.animated = function(a) {
        return r.grep(r.timers, function(b) {
            return a === b.elem
        }).length
    }, r.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = r.css(a, "position"),
                l = r(a),
                m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = r.css(a, "top"), i = r.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
        }
    }, r.fn.extend({
        offset: function(a) {
            if (arguments.length) return void 0 === a ? this : this.each(function(b) {
                r.offset.setOffset(this, a, b)
            });
            var b, c, d, e, f = this[0];
            if (f) return f.getClientRects().length ? (d = f.getBoundingClientRect(), b = f.ownerDocument, c = b.documentElement, e = b.defaultView, {
                top: d.top + e.pageYOffset - c.clientTop,
                left: d.left + e.pageXOffset - c.clientLeft
            }) : {
                top: 0,
                left: 0
            }
        },
        position: function() {
            if (this[0]) {
                var a, b, c = this[0],
                    d = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === r.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), B(a[0], "html") || (d = a.offset()), d = {
                    top: d.top + r.css(a[0], "borderTopWidth", !0),
                    left: d.left + r.css(a[0], "borderLeftWidth", !0)
                }), {
                    top: b.top - d.top - r.css(c, "marginTop", !0),
                    left: b.left - d.left - r.css(c, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var a = this.offsetParent;
                while (a && "static" === r.css(a, "position")) a = a.offsetParent;
                return a || ra
            })
        }
    }), r.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var c = "pageYOffset" === b;
        r.fn[a] = function(d) {
            return T(this, function(a, d, e) {
                var f;
                return r.isWindow(a) ? f = a : 9 === a.nodeType && (f = a.defaultView), void 0 === e ? f ? f[b] : a[d] : void(f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : a[d] = e)
            }, a, d, arguments.length)
        }
    }), r.each(["top", "left"], function(a, b) {
        r.cssHooks[b] = Pa(o.pixelPosition, function(a, c) {
            if (c) return c = Oa(a, b), Ma.test(c) ? r(a).position()[b] + "px" : c
        })
    }), r.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        r.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, d) {
            r.fn[d] = function(e, f) {
                var g = arguments.length && (c || "boolean" != typeof e),
                    h = c || (e === !0 || f === !0 ? "margin" : "border");
                return T(this, function(b, c, e) {
                    var f;
                    return r.isWindow(b) ? 0 === d.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a] : 9 === b.nodeType ? (f = b.documentElement, Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a])) : void 0 === e ? r.css(b, c, h) : r.style(b, c, e, h)
                }, b, g ? e : void 0, g)
            }
        })
    }), r.fn.extend({
        bind: function(a, b, c) {
            return this.on(a, null, b, c)
        },
        unbind: function(a, b) {
            return this.off(a, null, b)
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d)
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    }), r.holdReady = function(a) {
        a ? r.readyWait++ : r.ready(!0)
    }, r.isArray = Array.isArray, r.parseJSON = JSON.parse, r.nodeName = B, "function" == typeof define && define.amd && define("jquery", [], function() {
        return r
    });
    var Vb = a.jQuery,
        Wb = a.$;
    return r.noConflict = function(b) {
        return a.$ === r && (a.$ = Wb), b && a.jQuery === r && (a.jQuery = Vb), r
    }, b || (a.jQuery = a.$ = r), r
});;
(function(e) {
    'use strict';
    if (typeof exports === 'object') {
        e(require('jquery'))
    } else if (typeof define === 'function' && define.amd) {
        define(['jquery'], e)
    } else {
        e(jQuery)
    }
})(function(e) {
    'use strict';
    var n = function(e) {
        e = e || 'once';
        if (typeof e !== 'string') {
            throw new TypeError('The jQuery Once id parameter must be a string')
        };
        return e
    };
    e.fn.once = function(t) {
        var r = 'jquery-once-' + n(t);
        return this.filter(function() {
            return e(this).data(r) !== !0
        }).data(r, !0)
    };
    e.fn.removeOnce = function(e) {
        return this.findOnce(e).removeData('jquery-once-' + n(e))
    };
    e.fn.findOnce = function(t) {
        var r = 'jquery-once-' + n(t);
        return this.filter(function() {
            return e(this).data(r) === !0
        })
    }
});;
(function() {
    var t = document.querySelector('head > script[type="application/json"][data-drupal-selector="drupal-settings-json"], body > script[type="application/json"][data-drupal-selector="drupal-settings-json"]');
    window.drupalSettings = {};
    if (t !== null) {
        window.drupalSettings = JSON.parse(t.textContent)
    }
})();;
window.drupalTranslations = {
    "strings": {
        "": {
            "Image": "Billede",
            "Link": "Link",
            "Unlink": "Fjern link",
            "Edit Link": "Redig\u00e9r link",
            "Edit": "Redig\u00e9r",
            "Extend": "Udvid",
            "(active tab)": "(aktiv fane)",
            "Home": "Lendme",
            "End tour": "Slut tour",
            "Wed": "ons",
            "Divorced": "Skilt",
            "Widowed": "Enke\/Enkemand",
            "Co-habitant": "Samlevende",
            "1 hour\u0003@count hours": "1 time\u0003@count timer",
            "Changed": "\u00c6ndret",
            "1 min\u0003@count min": "1 minut\u0003@count minutter",
            "1 day\u0003@count days": "1 dag\u0003@count dage",
            "Open": "\u00c5ben",
            "Close": "Luk",
            "@action @title configuration options": "Indstillinger for @action @title",
            "Hide": "Hide",
            "Show": "Vis",
            "An AJAX HTTP error occurred.": "Der opstod en AJAX HTTP-fejl.",
            "HTTP Result Code: !status": "HTTP resultatkode: !status",
            "An AJAX HTTP request terminated abnormally.": "En AJAX HTTP-foresp\u00f8rgsel afsluttede p\u00e5 unormal vis.",
            "Debugging information follows.": "Information til fejls\u00f8gning f\u00f8lger.",
            "Path: !uri": "Sti: !uri",
            "StatusText: !statusText": "StatusText: !statusText",
            "ResponseText: !responseText": "ResponseText: !responseText",
            "ReadyState: !readyState": "ReadyState: !readyState",
            "CustomMessage: !customMessage": "CustomMessage: !customMessage",
            "Please wait...": "Vent venligst\u2026",
            "The callback URL is not local and not trusted: !url": "Callback-URLen er ikke lokal og er ikke trov\u00e6rdig: !url",
            "Collapse": "Klap sammen",
            "@label": "@label",
            "Horizontal orientation": "Horisontal placering",
            "Vertical orientation": "Vertikal placering",
            "The toolbar cannot be set to a horizontal orientation when it is locked.": "V\u00e6rkt\u00f8jslinjen kan ikke s\u00e6ttes til horisontal orientering n\u00e5r den er l\u00e5st.",
            "Tray orientation changed to @orientation.": "Bakkeplacering \u00e6ndret til @orientation.",
            "closed": "lukket",
            "opened": "\u00e5bnet",
            "Tray \u0022@tray\u0022 @action.": "Bakke \u0022@tray\u0022 @action.",
            "Tray @action.": "Bakke @action.",
            "!tour_item of !total": "!tour_item af !total",
            "Tabbing is no longer constrained by the Contextual module.": "Tabbing er ikke l\u00e6ngere begr\u00e6nset af Contextual-modulet.",
            "Tabbing is constrained to a set of @contextualsCount and the edit mode toggle.": "Tabbing er begr\u00e6nset af @contextualsCount og redigeringstilstanden.",
            "Press the esc key to exit.": "Tryk p\u00e5 esc for at afslutte.",
            "@count contextual link\u0003@count contextual links": "@count kontekstuelt link\u0003@count kontekstuellle links",
            "Quick edit": "Hurtig redigering",
            "OK": "O.k.",
            "Could not load the form for \u003Cq\u003E@field-label\u003C\/q\u003E, either due to a website problem or a network connection problem.\u003Cbr\u003EPlease try again.": "Kunne ikke indl\u00e6se formularen for \u003Cq\u003E@field-label\u003C\/q\u003E, enten p\u00e5 grund af et problem med websitet eller et netv\u00e6rksproblem.\u003Cbr\u003EFors\u00f8g venligst igen.",
            "Network problem!": "Netv\u00e6rksproblem!",
            "Your changes to \u003Cq\u003E@entity-title\u003C\/q\u003E could not be saved, either due to a website problem or a network connection problem.\u003Cbr\u003EPlease try again.": "Dine \u00e6ndringer til \u003Cq\u003E@entity-title\u003C\/q\u003E kunne ikke gemmes, enten p\u00e5 grund af et problem med websitet eller et netv\u00e6rksproblem.\u003Cbr\u003EFors\u00f8g venligst igen.",
            "You have unsaved changes": "Du har \u00e6ndringer, som ikke er blevet gemt",
            "Discard changes?": "Fortryd \u00e6ndringer?",
            "Save": "Gem",
            "Discard changes": "Fortryd \u00e6ndringer",
            "Saving": "Gemmer",
            "Hide description": "Skjul beskrivelse",
            "Show description": "Vis beskrivelse",
            "Changes made in this table will not be saved until the form is submitted.": "\u00c6ndringer i tabellen bliver ikke gemt, f\u00f8r du indsender formularen.",
            "Show all columns": "Vis alle kolonner",
            "Hide lower priority columns": "Skjul kolonner med lav prioritet",
            "Show table cells that were hidden to make the table fit within a small screen.": "Vis tabelceller som var skjult for at f\u00e5 tabellen til at passe p\u00e5 en lille sk\u00e6rm.",
            "Re-order rows by numerical weight instead of dragging.": "Sort\u00e9r r\u00e6kker med numeriske v\u00e6gte i stedet for at tr\u00e6kke dem.",
            "Show row weights": "Vis r\u00e6kkev\u00e6gte",
            "Hide row weights": "Skjul r\u00e6kkev\u00e6gte",
            "Drag to re-order": "Tr\u00e6k for at (om)sortere",
            "You have unsaved changes.": "Du har \u00e6ndringer, som ikke er blevet gemt.",
            "List additional actions": "Vis yderligere handlinger",
            "Not restricted": "Ikke begr\u00e6nset",
            "Restricted to certain pages": "Begr\u00e6nset til bestemte sider",
            "The block cannot be placed in this region.": "Blokken kan ikke placeres i denne region.",
            "Mon": "man",
            "Monday": "Mandag",
            "Enabled": "Aktiveret",
            "Fri": "fre",
            "Apply": "Udf\u00f8r",
            "Select all rows in this table": "V\u00e6lg alle r\u00e6kker i tabellen",
            "Deselect all rows in this table": "Frav\u00e6lg alle r\u00e6kker i tabellen",
            "New revision": "Ny version",
            "No revision": "Ingen version",
            "By @name on @date": "Af @name, @date",
            "By @name": "Af @name",
            "Authored on @date": "Skrevet den @date",
            "Not promoted": "Ikke forfremmet",
            "Needs to be updated": "Skal opdateres",
            "Does not need to be updated": "Beh\u00f8ver ikke at blive opdateret",
            "Flag other translations as outdated": "Mark\u00e9r andre overs\u00e6ttelser som for\u00e6ldede",
            "Do not flag other translations as outdated": "Mark\u00e9r ikke andre overs\u00e6ttelser som for\u00e6ldede",
            "Done": "F\u00e6rdig",
            "Prev": "Forrige",
            "Next": "N\u00e6ste",
            "Today": "I dag",
            "Jan": "jan",
            "Feb": "feb",
            "Mar": "mar",
            "Apr": "apr",
            "May": "maj",
            "Jun": "jun",
            "Jul": "jul",
            "Aug": "aug",
            "Sep": "sep",
            "Oct": "okt",
            "Nov": "nov",
            "Dec": "dec",
            "Sunday": "S\u00f8ndag",
            "Tuesday": "Tirsdag",
            "Wednesday": "Onsdag",
            "Thursday": "Torsdag",
            "Friday": "Fredag",
            "Saturday": "L\u00f8rdag",
            "Sun": "s\u00f8n",
            "Tue": "tir",
            "Thu": "tor",
            "Sat": "l\u00f8r",
            "Su": "S\u00f8",
            "Mo": "Ma",
            "Tu": "Ti",
            "We": "On",
            "Th": "To",
            "Fr": "Fr",
            "Sa": "L\u00f8",
            "mm\/dd\/yy": "mm\/dd\/yy",
            "Alias: @alias": "Alias: @alias",
            "No alias": "Intet alias",
            "Not in menu": "Ikke i menu",
            "Change text format?": "Skift tekstformat?",
            "Continue": "Forts\u00e6t",
            "Cancel": "Annull\u00e9r",
            "Rich Text Editor, !label field": "Rich Text Editor, !label field",
            "Loading...": "Indl\u00e6ser...",
            "Hide summary": "Skjul resum\u00e9",
            "Edit summary": "Redig\u00e9r resum\u00e9",
            "The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.": "Den valgte fil %filename kan ikke uploades. Kun filer med f\u00f8lgende filendelser er tilladt: %extensions.",
            "No": "Nej",
            "Yes": "Ja",
            "Add": "Tilf\u00f8j",
            "Remove group": "Fjern gruppe",
            "Apply (all displays)": "Anvend (alle displays)",
            "Revert to default": "Gendan til standard",
            "Apply (this display)": "Anvend (dette display)",
            "!modules modules are available in the modified list.": "!modules moduler er tilg\u00e6ngelige i den modificerede liste.",
            "1 month\u0003@count months": "1 m\u00e5ned\u0003@count m\u00e5neder",
            "1 week\u0003@count weeks": "1 uge\u0003@count uger",
            "This permission is inherited from the authenticated user role.": "Denne tilladelse er arvet fra den godkendte brugerrolle.",
            "Separated": "Separeret",
            "- Select -": "- V\u00e6lg -",
            "Insert this token into your form": "Inds\u00e6t dette token i din formular",
            "First click a text field to insert your tokens into.": "Klik p\u00e5 et tekstfelt som tokens skal s\u00e6ttes ind i.",
            "Not published": "Ikke udgivet",
            "0 sec": "0 sek.",
            "Automatic alias": "Automatisk alias",
            "1 sec\u0003@count sec": "1 sek.\u0003@count sekunder",
            "Disabled": "Deaktiveret",
            "Information": "Information",
            "Add group": "Tilf\u00f8j gruppe",
            "1 year\u0003@count years": "1 \u00e5r\u0003@count \u00e5r",
            "button": "knap",
            "New group": "Ny gruppe",
            "Requires a title": "Kr\u00e6ver en titel",
            "Don\u0027t display post information": "Vis ikke information om indl\u00e6gget",
            "No styles configured": "Ingen styles konfigureret",
            "@count styles configured": "@count styles konfigureret",
            "Based on the text editor configuration, these tags have automatically been added: \u003Cstrong\u003E@tag-list\u003C\/strong\u003E.": "Disse tags er blevet tilf\u00f8jet automatisk p\u00e5 basis af indstillingerne for tekst-editoren: \u003Cstrong\u003E@tag-list\u003C\/strong\u003E.",
            "Uploads disabled": "Uploads deaktiveret",
            "Uploads enabled, max size: @size @dimensions": "Uploads er aktiveret, maksimal st\u00f8rrelse: @size @dimensions",
            "Hide group names": "Skjul gruppenavne",
            "Show group names": "Vis gruppenavne",
            "@groupName button group in position @position of @positionCount in row @row of @rowCount.": "Knapgruppe @groupName p\u00e5 position @position af @positionCount p\u00e5 r\u00e6kke @row af @rowCount.",
            "Press the down arrow key to create a new row.": "Tryk pil ned for at oprette en ny r\u00e6kke.",
            "@name @type.": "@name @type.",
            "Press the down arrow key to activate.": "Tryk pil ned for at aktivere.",
            "@name @type in position @position of @positionCount in @groupName button group in row @row of @rowCount.": "@name @type p\u00e5 position @position af @positionCount i knapgruppen @groupName p\u00e5 r\u00e6kke @row af @rowCount.",
            "Press the down arrow key to create a new button group in a new row.": "Tryk pil ned for at oprette en ny knapgruppe i en ny r\u00e6kke.",
            "This is the last group. Move the button forward to create a new group.": "Dette er den sidste gruppe. Flyt knappen frem for at oprette en ny gruppe.",
            "The \u0022@name\u0022 button is currently enabled.": "Knappen @name er i \u00f8jebliket aktiveret.",
            "Use the keyboard arrow keys to change the position of this button.": "Brug piletasterne p\u00e5 tastatures for at \u00e6ndre knappens position.",
            "Press the up arrow key on the top row to disable the button.": "Tryk p\u00e5 pil-op i den \u00f8verste r\u00e6kke for at deaktivere knappen.",
            "The \u0022@name\u0022 button is currently disabled.": "The \u0022@name\u0022 button is currently disabled.",
            "Use the down arrow key to move this button into the active toolbar.": "Brug pil-ned for at flytte denne knap til den aktive v\u00e6rkt\u00f8jslinje.",
            "This @name is currently enabled.": "Dette @name er i \u00f8jebliket deaktiveret.",
            "Use the keyboard arrow keys to change the position of this separator.": "Brug piletasterne p\u00e5 tastaturet for at \u00e6ndre skillelinjens position.",
            "Separators are used to visually split individual buttons.": "Skillelinjer bruger til at adskille individuelle knapper visuelt.",
            "This @name is currently disabled.": "Dette @name er i \u00f8jebliket deaktiveret.",
            "Use the down arrow key to move this separator into the active toolbar.": "Brug pil-ned for at flytte denne skillelinje til den aktive v\u00e6rkt\u00f8jslinje.",
            "You may add multiple separators to each button group.": "Du kan tilf\u00f8je flere skillelinjer til hver knapgruppe.",
            "Please provide a name for the button group.": "Angive venligst et navn p\u00e5 knapgruppen.",
            "Button group name": "Navn p\u00e5 knapgruppe",
            "Editing the name of the new button group in a dialog.": "Redig\u00e9r navnet p\u00e5 den nye knapgruppe i en dialogboks.",
            "Editing the name of the \u0022@groupName\u0022 button group in a dialog.": "Redig\u00e9r navnet p\u00e5 knapgruppen \u0022@groupName\u0022 i en dialogboks.",
            "Place a button to create a new button group.": "Plac\u00e9r en knap for at oprette en ny knapgruppe.",
            "Add a CKEditor button group to the end of this row.": "Tilf\u00f8j en CKEditor knapgruppe i slutningen af denne r\u00e6kke.",
            "Leave preview?": "Forlad forh\u00e5ndsvisning?",
            "Leave preview": "Forlad forh\u00e5ndsvisning",
            "Leaving the preview will cause unsaved changes to be lost. Are you sure you want to leave the preview?": "Hvis du forlader forh\u00e5ndsvisningen, vil \u00e6ndringer, som ikke er gemt, g\u00e5 tabt. Er du sikker p\u00e5, at du vil forlade forh\u00e5ndsvisningen?",
            "Missing fields are marked with red. If there is an error, you can read more by clicking the info-icons.": "Du mangler at udfylde de r\u00f8de felter, eller der er fejl i indtastningen. L\u00e6s mere under info-ikonerne ved hvert felt.",
            "Missing fields": "Fejl i indtastning",
            "Ssn exists": "CPR-nummer er i brug",
            "The SSN already exist in Lendmes system. Please log in to continue your application.": "CPR-nummeret er allerede i brug i en anden l\u00e5neans\u00f8gning.\u003Cbr\u003E\u003Cbr\u003EIndtast dit CPR-nummer p\u00e5 vores \u003Ca href=\u0022https:\/\/lendme.dk\/log-ind\u0022\u003ELog ind side\u003C\/a\u003E, s\u00e5 sender vi adgangen til din ans\u00f8gning til din e-mail adresse.",
            "You are about to accept an offer.": "Du sendes videre",
            "When you accept an offer, your application ends at Lendme. It is important that you are completely certain that you chose the right loan before you procede": "N\u00e5r du accepterer et tilbud, afsluttes din ans\u00f8gning. Du skal derfor v\u00e6re helt sikker, f\u00f8r du g\u00e5r videre.\u003Cbr\u003E\u003Cbr\u003EBem\u00e6rk, at banken i visse tilf\u00e6lde foretager yderligere verificering af dine oplysninger. Det kan i sj\u00e6ldne tilf\u00e6lde f\u00f8re til, at banken tr\u00e6kker tilbuddet tilbage.",
            "Accept": "Accepter",
            "Decline": "Afvis",
            "The password must contain a mix between lower and upper case lettes and digits.": "Adgangskoden skal best\u00e5 af mindst: 8 karakterer, et stort bogstav og et lille bogstav",
            "Spouse": "\u00c6gtef\u00e6lle",
            "Error in SSN": "Fejl i CPR-nummer",
            "You must be at least 18 years old to use Lendme\\\u0027s service.": "Du skal v\u00e6re mindst 18 \u00e5r for at kunne benytte Lendme.",
            "The e-mail address can not be identical to the mail applicant e-mail.": "Medans\u00f8gers e-mail adresse skal v\u00e6re en anden end hovedans\u00f8gers e-mail adresse.",
            "Payment remarks": "Betalingsanm\u00e6rkninger",
            "You can not have payment remarks when applying for a loan through Lendme. \\\n            Lendme validates this information with RKI and Debitor Registret.": "Du kan ikke benytte Lendme, hvis du har betalingsanm\u00e6rkninger.\u003Cbr\u003E\u003Cbr\u003ELendme foretager opslag hos RKI og Debitor Registret for at validere dette.",
            "You must be at least 18 years old to use Lendmes service.": "Du skal v\u00e6re mindst 18 \u00e5r for at kunne benytte Lendme.",
            "You can not have payment remarks when applying for a loan through Lendme. Lendme validates this information with RKI and Debitor Registret.": "Du kan ikke benytte Lendme, hvis du har betalingsanm\u00e6rkninger.\u003Cbr\u003E\u003Cbr\u003ELendme foretager opslag hos RKI og Debitor Registret for at validere dette.",
            "The co ssn can not be identical to the applicant ssn.": "Det CPR-nummer du har indtastet ved din partner\/med-ans\u00f8ger er allerede i brug i en anden ans\u00f8gning.\u003Cbr\u003E\u003Cbr\u003EBem\u00e6rk, at du ikke kan benytte det samme CPR-nummer ved din partner\/med-ans\u00f8ger, som du allerede har indtastet tidligere.\u003Cbr\u003E\u003Cbr\u003EDu skal kontakte os, hvis du vil have nulstillet et CPR-nummer.",
            "Co-applicant": "Medans\u00f8ger",
            "Do you have an co-applicant": "Har du en medans\u00f8ger?",
            "Is your spouse co-applicant?": "Er din \u00e6gtef\u00e6lle medans\u00f8ger?",
            "Is your co-habitant co-applicant?": "Er din samlever medans\u00f8ger?",
            "Did you mean": "Mente du",
            "Type of temporary residence permit": "Type af midlertidig opholdstilladelse",
            "Type of permanent residence permit": "Type af permanent opholdstilladelse",
            "Important": "VIGTIGT",
            "Remember to input the precise amount on your paycheck.": "Du kan skrive din l\u00f8n afrundet til n\u00e6rmeste tusinde.\u003Cbr\u003E\u003Cbr\u003EHvis du for eksempel tjener 27.730 kr. kan du blot skrive 28.000 kr.\u003Cbr\u003E\u003Cbr\u003EDet er vigtigt, at du skriver din l\u00f8n uden eventuelle ekstra till\u00e6g, bonusser og feriepenge, da din l\u00f8n kontrolleres i forhold til data fra SKAT.",
            "It is important that you input all debt you have.": "Skriv din nuv\u00e6rende restg\u00e6ld i nedenst\u00e5ende felter.\u003Cbr\u003E\u003Cbr\u003EDu kan afrunde restg\u00e6lden til n\u00e6rmeste 1.000kr.\u003Cbr\u003E\u003Cbr\u003EHvis du ikke oplyser din restg\u00e6ld pr\u00e6cist, vil bankerne vurdere din ans\u00f8gning p\u00e5 et forkert grundlag, og de l\u00e5netilbud du modtager, kan blive annulleret.",
            "Are you sure you do not have a mortgage loan?": "Du har angivet, at du bor i ejerbolig, men at du ikke har et boligl\u00e5n.\u003Cbr\u003E\u003Cbr\u003EDa bankerne kontrollerer oplysninger om g\u00e6ld, er det vigtigt, at du angiver evt. boligl\u00e5n i dette felt."
        },
        "Long month name": {
            "August": "august",
            "January": "januar",
            "February": "februar",
            "March": "marts",
            "April": "april",
            "May": "maj",
            "June": "juni",
            "July": "juli",
            "September": "september",
            "October": "oktober",
            "November": "november",
            "December": "december"
        }
    },
    "pluralFormula": {
        "1": 0,
        "default": 1
    }
};;;
window.Drupal = {
    behaviors: {},
    locale: {}
};
(function(t, r, e) {
    t.throwError = function(t) {
        setTimeout(function() {
            throw t
        }, 0)
    };
    t.attachBehaviors = function(e, n) {
        e = e || document;
        n = n || r;
        var o = t.behaviors;
        Object.keys(o || {}).forEach(function(r) {
            if (typeof o[r].attach === 'function') {
                try {
                    o[r].attach(e, n)
                } catch (a) {
                    t.throwError(a)
                }
            }
        })
    };
    t.detachBehaviors = function(e, n, o) {
        e = e || document;
        n = n || r;
        o = o || 'unload';
        var a = t.behaviors;
        Object.keys(a || {}).forEach(function(r) {
            if (typeof a[r].detach === 'function') {
                try {
                    a[r].detach(e, n, o)
                } catch (c) {
                    t.throwError(c)
                }
            }
        })
    };
    t.checkPlain = function(t) {
        t = t.toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#39;');
        return t
    };
    t.formatString = function(e, r) {
        var n = {};
        Object.keys(r || {}).forEach(function(e) {
            switch (e.charAt(0)) {
                case '@':
                    n[e] = t.checkPlain(r[e]);
                    break;
                case '!':
                    n[e] = r[e];
                    break;
                default:
                    n[e] = t.theme('placeholder', r[e]);
                    break
            }
        });
        return t.stringReplace(e, n, null)
    };
    t.stringReplace = function(e, n, r) {
        if (e.length === 0) {
            return e
        };
        if (!Array.isArray(r)) {
            r = Object.keys(n || {});
            r.sort(function(t, e) {
                return t.length - e.length
            })
        };
        if (r.length === 0) {
            return e
        };
        var c = r.pop(),
            a = e.split(c);
        if (r.length) {
            for (var o = 0; o < a.length; o++) {
                a[o] = t.stringReplace(a[o], n, r.slice(0))
            }
        };
        return a.join(n[c])
    };
    t.t = function(r, o, n) {
        n = n || {};
        n.context = n.context || '';
        if (typeof e !== 'undefined' && e.strings && e.strings[n.context] && e.strings[n.context][r]) {
            r = e.strings[n.context][r]
        };
        if (o) {
            r = t.formatString(r, o)
        };
        return r
    };
    t.url = function(t) {
        return r.path.baseUrl + r.path.pathPrefix + t
    };
    t.url.toAbsolute = function(t) {
        var r = document.createElement('a');
        try {
            t = decodeURIComponent(t)
        } catch (e) {};
        r.setAttribute('href', t);
        return r.cloneNode(!1).href
    };
    t.url.isLocal = function(e) {
        var n = t.url.toAbsolute(e),
            c = location.protocol;
        if (c === 'http:' && n.indexOf('https:') === 0) {
            c = 'https:'
        };
        var o = c + '//' + location.host + r.path.baseUrl.slice(0, -1);
        try {
            n = decodeURIComponent(n)
        } catch (a) {};
        try {
            o = decodeURIComponent(o)
        } catch (a) {};
        return n === o || n.indexOf(o + '/') === 0
    };
    t.formatPlural = function(n, o, a, c, u) {
        c = c || {};
        c['@count'] = n;
        var l = r.pluralDelimiter,
            f = t.t(o + l + a, c, u).split(l),
            i = 0;
        if (typeof e !== 'undefined' && e.pluralFormula) {
            i = n in e.pluralFormula ? e.pluralFormula[n] : e.pluralFormula.default
        } else if (c['@count'] !== 1) {
            i = 1
        };
        return f[i]
    };
    t.encodePath = function(t) {
        return window.encodeURIComponent(t).replace(/%2F/g, '/')
    };
    t.theme = function(e) {
        if (e in t.theme) {
            var a;
            for (var n = arguments.length, o = Array(n > 1 ? n - 1 : 0), r = 1; r < n; r++) {
                o[r - 1] = arguments[r]
            };
            return (a = t.theme)[e].apply(a, o)
        }
    };
    t.theme.placeholder = function(e) {
        return '<em class="placeholder">' + t.checkPlain(e) + '</em>'
    }
})(Drupal, window.drupalSettings, window.drupalTranslations);;;
if (window.jQuery) {
    jQuery.noConflict()
};
document.documentElement.className += ' js';
(function(n, e, o) {
    n(function() {
        e.attachBehaviors(document, o)
    })
})(domready, Drupal, window.drupalSettings);;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], e) : e(jQuery)
}(function(e) {
    return e.extend(e.expr[':'], {
        data: e.expr.createPseudo ? e.expr.createPseudo(function(n) {
            return function(t) {
                return !!e.data(t, n)
            }
        }) : function(n, r, t) {
            return !!e.data(n, t[3])
        }
    })
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], e) : e(jQuery)
}(function(e) {
    return e.fn.extend({
        disableSelection: function() {
            var e = 'onselectstart' in document.createElement('div') ? 'selectstart' : 'mousedown';
            return function() {
                return this.on(e + '.ui-disableSelection', function(e) {
                    e.preventDefault()
                })
            }
        }(),
        enableSelection: function() {
            return this.off('.ui-disableSelection')
        }
    })
});;
! function(n) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], n) : n(jQuery)
}(function(n) {
    return n.fn.form = function() {
        return 'string' == typeof this[0].form ? this.closest('form') : n(this[0].form)
    }
});;
! function(e) {
    "function" == typeof define && define.amd ? define(["jquery", "./version", "./escape-selector"], e) : e(jQuery)
}(function(e) {
    return e.fn.labels = function() {
        var t, i, n, s, a;
        return this[0].labels && this[0].labels.length ? this.pushStack(this[0].labels) : (s = this.eq(0).parents("label"), n = this.attr("id"), n && (t = this.eq(0).parents().last(), a = t.add(t.length ? t.siblings() : this.siblings()), i = "label[for='" + e.ui.escapeSelector(n) + "']", s = s.add(a.find(i).addBack(i))), this.pushStack(s))
    }
});;
! function(t) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], t) : t(jQuery)
}(function(t) {
    '1.7' === t.fn.jquery.substring(0, 3) && (t.each(['Width', 'Height'], function(i, n) {
        function h(n, i, e, r) {
            return t.each(s, function() {
                i -= parseFloat(t.css(n, 'padding' + this)) || 0, e && (i -= parseFloat(t.css(n, 'border' + this + 'Width')) || 0), r && (i -= parseFloat(t.css(n, 'margin' + this)) || 0)
            }), i
        };
        var s = 'Width' === n ? ['Left', 'Right'] : ['Top', 'Bottom'],
            e = n.toLowerCase(),
            r = {
                innerWidth: t.fn.innerWidth,
                innerHeight: t.fn.innerHeight,
                outerWidth: t.fn.outerWidth,
                outerHeight: t.fn.outerHeight
            };
        t.fn['inner' + n] = function(i) {
            return void 0 === i ? r['inner' + n].call(this) : this.each(function() {
                t(this).css(e, h(this, i) + 'px')
            })
        }, t.fn['outer' + n] = function(i, s) {
            return 'number' != typeof i ? r['outer' + n].call(this, i) : this.each(function() {
                t(this).css(e, h(this, i, !0, s) + 'px')
            })
        }
    }), t.fn.addBack = function(t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
    })
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], e) : e(jQuery)
}(function(e) {
    return e.fn.scrollParent = function(t) {
        var n = this.css('position'),
            s = 'absolute' === n,
            i = t ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
            o = this.parents().filter(function() {
                var t = e(this);
                return (!s || 'static' !== t.css('position')) && i.test(t.css('overflow') + t.css('overflow-y') + t.css('overflow-x'))
            }).eq(0);
        return 'fixed' !== n && o.length ? o : e(this[0].ownerDocument || document)
    }
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version', './focusable'], e) : e(jQuery)
}(function(e) {
    return e.extend(e.expr[':'], {
        tabbable: function(n) {
            var u = e.attr(n, 'tabindex'),
                t = null != u;
            return (!t || u >= 0) && e.ui.focusable(n, t)
        }
    })
});;
! function(i) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], i) : i(jQuery)
}(function(i) {
    return i.fn.extend({
        uniqueId: function() {
            var i = 0;
            return function() {
                return this.each(function() {
                    this.id || (this.id = 'ui-id-' + ++i)
                })
            }
        }(),
        removeUniqueId: function() {
            return this.each(function() {
                /^ui-id-\d+$/.test(this.id) && i(this).removeAttr('id')
            })
        }
    })
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery'], e) : e(jQuery)
}(function(e) {
    return e.ui = e.ui || {}, e.ui.version = '1.12.1'
});;
! function(e) {
    "function" == typeof define && define.amd ? define(["jquery", "./version"], e) : e(jQuery)
}(function(e) {
    function i(e) {
        for (var i = e.css("visibility");
            "inherit" === i;) e = e.parent(), i = e.css("visibility");
        return "hidden" !== i
    };
    return e.ui.focusable = function(t, f) {
        var s, a, r, n, o, u = t.nodeName.toLowerCase();
        return "area" === u ? (s = t.parentNode, a = s.name, !(!t.href || !a || "map" !== s.nodeName.toLowerCase()) && (r = e("img[usemap='#" + a + "']"), r.length > 0 && r.is(":visible"))) : (/^(input|select|textarea|button|object)$/.test(u) ? (n = !t.disabled, n && (o = e(t).closest("fieldset")[0], o && (n = !o.disabled))) : n = "a" === u ? t.href || f : f, n && e(t).is(":visible") && i(e(t)))
    }, e.extend(e.expr[":"], {
        focusable: function(i) {
            return e.ui.focusable(i, null != e.attr(i, "tabindex"))
        }
    }), e.ui.focusable
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], e) : e(jQuery)
}(function(e) {
    return e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase())
});;
! function(E) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], E) : E(jQuery)
}(function(E) {
    return E.ui.keyCode = {
        BACKSPACE: 8,
        COMMA: 188,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    }
});;
! function(n) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], n) : n(jQuery)
}(function(n) {
    return n.ui.plugin = {
        add: function(i, t, u) {
            var e, o = n.ui[i].prototype;
            for (e in u) o.plugins[e] = o.plugins[e] || [], o.plugins[e].push([t, u[e]])
        },
        call: function(n, u, o, t) {
            var e, i = n.plugins[u];
            if (i && (t || n.element[0].parentNode && 11 !== n.element[0].parentNode.nodeType))
                for (e = 0; e < i.length; e++) n.options[i[e][0]] && i[e][1].apply(n.element, o)
        }
    }
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], e) : e(jQuery)
}(function(e) {
    return e.ui.safeActiveElement = function(n) {
        var e;
        try {
            e = n.activeElement
        } catch (t) {
            e = n.body
        };
        return e || (e = n.body), e.nodeName || (e = n.body), e
    }
});;
! function(e) {
    'function' == typeof define && define.amd ? define(['jquery', './version'], e) : e(jQuery)
}(function(e) {
    return e.ui.safeBlur = function(n) {
        n && 'body' !== n.nodeName.toLowerCase() && e(n).trigger('blur')
    }
});;
/*! jQuery UI - v1.12.1 - 2017-03-31
 * http://jqueryui.com
 * Copyright jQuery Foundation and other contributors; Licensed  */
! function(a) {
    "function" == typeof define && define.amd ? define(["jquery", "./version"], a) : a(jQuery)
}(function(a) {
    var b = 0,
        c = Array.prototype.slice;
    return a.cleanData = function(b) {
        return function(c) {
            var d, e, f;
            for (f = 0; null != (e = c[f]); f++) try {
                d = a._data(e, "events"), d && d.remove && a(e).triggerHandler("remove")
            } catch (g) {}
            b(c)
        }
    }(a.cleanData), a.widget = function(b, c, d) {
        var e, f, g, h = {},
            i = b.split(".")[0];
        b = b.split(".")[1];
        var j = i + "-" + b;
        return d || (d = c, c = a.Widget), a.isArray(d) && (d = a.extend.apply(null, [{}].concat(d))), a.expr[":"][j.toLowerCase()] = function(b) {
            return !!a.data(b, j)
        }, a[i] = a[i] || {}, e = a[i][b], f = a[i][b] = function(a, b) {
            return this._createWidget ? void(arguments.length && this._createWidget(a, b)) : new f(a, b)
        }, a.extend(f, e, {
            version: d.version,
            _proto: a.extend({}, d),
            _childConstructors: []
        }), g = new c, g.options = a.widget.extend({}, g.options), a.each(d, function(b, d) {
            return a.isFunction(d) ? void(h[b] = function() {
                function a() {
                    return c.prototype[b].apply(this, arguments)
                }

                function e(a) {
                    return c.prototype[b].apply(this, a)
                }
                return function() {
                    var b, c = this._super,
                        f = this._superApply;
                    return this._super = a, this._superApply = e, b = d.apply(this, arguments), this._super = c, this._superApply = f, b
                }
            }()) : void(h[b] = d)
        }), f.prototype = a.widget.extend(g, {
            widgetEventPrefix: e ? g.widgetEventPrefix || b : b
        }, h, {
            constructor: f,
            namespace: i,
            widgetName: b,
            widgetFullName: j
        }), e ? (a.each(e._childConstructors, function(b, c) {
            var d = c.prototype;
            a.widget(d.namespace + "." + d.widgetName, f, c._proto)
        }), delete e._childConstructors) : c._childConstructors.push(f), a.widget.bridge(b, f), f
    }, a.widget.extend = function(b) {
        for (var d, e, f = c.call(arguments, 1), g = 0, h = f.length; g < h; g++)
            for (d in f[g]) e = f[g][d], f[g].hasOwnProperty(d) && void 0 !== e && (a.isPlainObject(e) ? b[d] = a.isPlainObject(b[d]) ? a.widget.extend({}, b[d], e) : a.widget.extend({}, e) : b[d] = e);
        return b
    }, a.widget.bridge = function(b, d) {
        var e = d.prototype.widgetFullName || b;
        a.fn[b] = function(f) {
            var g = "string" == typeof f,
                h = c.call(arguments, 1),
                i = this;
            return g ? this.length || "instance" !== f ? this.each(function() {
                var c, d = a.data(this, e);
                return "instance" === f ? (i = d, !1) : d ? a.isFunction(d[f]) && "_" !== f.charAt(0) ? (c = d[f].apply(d, h), c !== d && void 0 !== c ? (i = c && c.jquery ? i.pushStack(c.get()) : c, !1) : void 0) : a.error("no such method '" + f + "' for " + b + " widget instance") : a.error("cannot call methods on " + b + " prior to initialization; attempted to call method '" + f + "'")
            }) : i = void 0 : (h.length && (f = a.widget.extend.apply(null, [f].concat(h))), this.each(function() {
                var b = a.data(this, e);
                b ? (b.option(f || {}), b._init && b._init()) : a.data(this, e, new d(f, this))
            })), i
        }
    }, a.Widget = function() {}, a.Widget._childConstructors = [], a.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            classes: {},
            disabled: !1,
            create: null
        },
        _createWidget: function(c, d) {
            d = a(d || this.defaultElement || this)[0], this.element = a(d), this.uuid = b++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = a(), this.hoverable = a(), this.focusable = a(), this.classesElementLookup = {}, d !== this && (a.data(d, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function(a) {
                    a.target === d && this.destroy()
                }
            }), this.document = a(d.style ? d.ownerDocument : d.document || d), this.window = a(this.document[0].defaultView || this.document[0].parentWindow)), this.options = a.widget.extend({}, this.options, this._getCreateOptions(), c), this._create(), this.options.disabled && this._setOptionDisabled(this.options.disabled), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: function() {
            return {}
        },
        _getCreateEventData: a.noop,
        _create: a.noop,
        _init: a.noop,
        destroy: function() {
            var b = this;
            this._destroy(), a.each(this.classesElementLookup, function(a, c) {
                b._removeClass(c, a)
            }), this.element.off(this.eventNamespace).removeData(this.widgetFullName), this.widget().off(this.eventNamespace).removeAttr("aria-disabled"), this.bindings.off(this.eventNamespace)
        },
        _destroy: a.noop,
        widget: function() {
            return this.element
        },
        option: function(b, c) {
            var d, e, f, g = b;
            if (0 === arguments.length) return a.widget.extend({}, this.options);
            if ("string" == typeof b)
                if (g = {}, d = b.split("."), b = d.shift(), d.length) {
                    for (e = g[b] = a.widget.extend({}, this.options[b]), f = 0; f < d.length - 1; f++) e[d[f]] = e[d[f]] || {}, e = e[d[f]];
                    if (b = d.pop(), 1 === arguments.length) return void 0 === e[b] ? null : e[b];
                    e[b] = c
                } else {
                    if (1 === arguments.length) return void 0 === this.options[b] ? null : this.options[b];
                    g[b] = c
                }
            return this._setOptions(g), this
        },
        _setOptions: function(a) {
            var b;
            for (b in a) this._setOption(b, a[b]);
            return this
        },
        _setOption: function(a, b) {
            return "classes" === a && this._setOptionClasses(b), this.options[a] = b, "disabled" === a && this._setOptionDisabled(b), this
        },
        _setOptionClasses: function(b) {
            var c, d, e;
            for (c in b) e = this.classesElementLookup[c], b[c] !== this.options.classes[c] && e && e.length && (d = a(e.get()), this._removeClass(e, c), d.addClass(this._classes({
                element: d,
                keys: c,
                classes: b,
                add: !0
            })))
        },
        _setOptionDisabled: function(a) {
            this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!a), a && (this._removeClass(this.hoverable, null, "ui-state-hover"), this._removeClass(this.focusable, null, "ui-state-focus"))
        },
        enable: function() {
            return this._setOptions({
                disabled: !1
            })
        },
        disable: function() {
            return this._setOptions({
                disabled: !0
            })
        },
        _classes: function(b) {
            function c(c, f) {
                var g, h;
                for (h = 0; h < c.length; h++) g = e.classesElementLookup[c[h]] || a(), g = a(b.add ? a.unique(g.get().concat(b.element.get())) : g.not(b.element).get()), e.classesElementLookup[c[h]] = g, d.push(c[h]), f && b.classes[c[h]] && d.push(b.classes[c[h]])
            }
            var d = [],
                e = this;
            return b = a.extend({
                element: this.element,
                classes: this.options.classes || {}
            }, b), this._on(b.element, {
                remove: "_untrackClassesElement"
            }), b.keys && c(b.keys.match(/\S+/g) || [], !0), b.extra && c(b.extra.match(/\S+/g) || []), d.join(" ")
        },
        _untrackClassesElement: function(b) {
            var c = this;
            a.each(c.classesElementLookup, function(d, e) {
                a.inArray(b.target, e) !== -1 && (c.classesElementLookup[d] = a(e.not(b.target).get()))
            })
        },
        _removeClass: function(a, b, c) {
            return this._toggleClass(a, b, c, !1)
        },
        _addClass: function(a, b, c) {
            return this._toggleClass(a, b, c, !0)
        },
        _toggleClass: function(a, b, c, d) {
            d = "boolean" == typeof d ? d : c;
            var e = "string" == typeof a || null === a,
                f = {
                    extra: e ? b : c,
                    keys: e ? a : b,
                    element: e ? this.element : a,
                    add: d
                };
            return f.element.toggleClass(this._classes(f), d), this
        },
        _on: function(b, c, d) {
            var e, f = this;
            "boolean" != typeof b && (d = c, c = b, b = !1), d ? (c = e = a(c), this.bindings = this.bindings.add(c)) : (d = c, c = this.element, e = this.widget()), a.each(d, function(d, g) {
                function h() {
                    if (b || f.options.disabled !== !0 && !a(this).hasClass("ui-state-disabled")) return ("string" == typeof g ? f[g] : g).apply(f, arguments)
                }
                "string" != typeof g && (h.guid = g.guid = g.guid || h.guid || a.guid++);
                var i = d.match(/^([\w:-]*)\s*(.*)$/),
                    j = i[1] + f.eventNamespace,
                    k = i[2];
                k ? e.on(j, k, h) : c.on(j, h)
            })
        },
        _off: function(b, c) {
            c = (c || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, b.off(c).off(c), this.bindings = a(this.bindings.not(b).get()), this.focusable = a(this.focusable.not(b).get()), this.hoverable = a(this.hoverable.not(b).get())
        },
        _delay: function(a, b) {
            function c() {
                return ("string" == typeof a ? d[a] : a).apply(d, arguments)
            }
            var d = this;
            return setTimeout(c, b || 0)
        },
        _hoverable: function(b) {
            this.hoverable = this.hoverable.add(b), this._on(b, {
                mouseenter: function(b) {
                    this._addClass(a(b.currentTarget), null, "ui-state-hover")
                },
                mouseleave: function(b) {
                    this._removeClass(a(b.currentTarget), null, "ui-state-hover")
                }
            })
        },
        _focusable: function(b) {
            this.focusable = this.focusable.add(b), this._on(b, {
                focusin: function(b) {
                    this._addClass(a(b.currentTarget), null, "ui-state-focus")
                },
                focusout: function(b) {
                    this._removeClass(a(b.currentTarget), null, "ui-state-focus")
                }
            })
        },
        _trigger: function(b, c, d) {
            var e, f, g = this.options[b];
            if (d = d || {}, c = a.Event(c), c.type = (b === this.widgetEventPrefix ? b : this.widgetEventPrefix + b).toLowerCase(), c.target = this.element[0], f = c.originalEvent)
                for (e in f) e in c || (c[e] = f[e]);
            return this.element.trigger(c, d), !(a.isFunction(g) && g.apply(this.element[0], [c].concat(d)) === !1 || c.isDefaultPrevented())
        }
    }, a.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(b, c) {
        a.Widget.prototype["_" + b] = function(d, e, f) {
            "string" == typeof e && (e = {
                effect: e
            });
            var g, h = e ? e === !0 || "number" == typeof e ? c : e.effect || c : b;
            e = e || {}, "number" == typeof e && (e = {
                duration: e
            }), g = !a.isEmptyObject(e), e.complete = f, e.delay && d.delay(e.delay), g && a.effects && a.effects.effect[h] ? d[b](e) : h !== b && d[h] ? d[h](e.duration, e.easing, f) : d.queue(function(c) {
                a(this)[b](), f && f.call(d[0]), c()
            })
        }
    }), a.widget
});;
/*!
 * hoverIntent v1.9.0 // 2017.09.01 // jQuery v1.7.0+
 * http://briancherne.github.io/jquery-hoverIntent/
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007-2017 Brian Cherne
 */
! function(factory) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], factory) : jQuery && !jQuery.fn.hoverIntent && factory(jQuery)
}(function($) {
    "use strict";
    var cX, cY, _cfg = {
            interval: 100,
            sensitivity: 6,
            timeout: 0
        },
        INSTANCE_COUNT = 0,
        track = function(ev) {
            cX = ev.pageX, cY = ev.pageY
        },
        compare = function(ev, $el, s, cfg) {
            if (Math.sqrt((s.pX - cX) * (s.pX - cX) + (s.pY - cY) * (s.pY - cY)) < cfg.sensitivity) return $el.off(s.event, track), delete s.timeoutId, s.isActive = !0, ev.pageX = cX, ev.pageY = cY, delete s.pX, delete s.pY, cfg.over.apply($el[0], [ev]);
            s.pX = cX, s.pY = cY, s.timeoutId = setTimeout(function() {
                compare(ev, $el, s, cfg)
            }, cfg.interval)
        },
        delay = function(ev, $el, s, out) {
            return delete $el.data("hoverIntent")[s.id], out.apply($el[0], [ev])
        };
    $.fn.hoverIntent = function(handlerIn, handlerOut, selector) {
        var instanceId = INSTANCE_COUNT++,
            cfg = $.extend({}, _cfg);
        $.isPlainObject(handlerIn) ? (cfg = $.extend(cfg, handlerIn), $.isFunction(cfg.out) || (cfg.out = cfg.over)) : cfg = $.isFunction(handlerOut) ? $.extend(cfg, {
            over: handlerIn,
            out: handlerOut,
            selector: selector
        }) : $.extend(cfg, {
            over: handlerIn,
            out: handlerIn,
            selector: handlerOut
        });
        var handleHover = function(e) {
            var ev = $.extend({}, e),
                $el = $(this),
                hoverIntentData = $el.data("hoverIntent");
            hoverIntentData || $el.data("hoverIntent", hoverIntentData = {});
            var state = hoverIntentData[instanceId];
            state || (hoverIntentData[instanceId] = state = {
                id: instanceId
            }), state.timeoutId && (state.timeoutId = clearTimeout(state.timeoutId));
            var mousemove = state.event = "mousemove.hoverIntent.hoverIntent" + instanceId;
            if ("mouseenter" === e.type) {
                if (state.isActive) return;
                state.pX = ev.pageX, state.pY = ev.pageY, $el.off(mousemove, track).on(mousemove, track), state.timeoutId = setTimeout(function() {
                    compare(ev, $el, state, cfg)
                }, cfg.interval)
            } else {
                if (!state.isActive) return;
                $el.off(mousemove, track), state.timeoutId = setTimeout(function() {
                    delay(ev, $el, state, cfg.out)
                }, cfg.timeout)
            }
        };
        return this.on({
            "mouseenter.hoverIntent": handleHover,
            "mouseleave.hoverIntent": handleHover
        }, cfg.selector)
    }
});;
(function(s) {
    Drupal.behaviors.formtips = {
        attach: function(o, r) {
            var t = r.formtips,
                i = t.selectors;
            if (s.isArray(i)) {
                i = i.join(', ')
            };
            var e = s('.form-item .description').not(i).not('.formtips-processed').addClass('formtips-processed');
            if (t.max_width.length) {
                e.css('max-width', t.max_width)
            };
            s(document).on('keyup', function(s) {
                if (s.which === 27) {
                    e.removeClass('formtips-show')
                }
            });
            e.each(function() {
                var i = s(this),
                    e = s(this).closest('.form-item'),
                    r = e.find('label,.fieldset-legend').first();
                i.toggleClass('formtips-show', !1);
                e.css('position', 'relative');
                var o = s('<a class="formtip"></a>');
                r.wrap('<div class="formtips-wrapper"></div>').append(o);
                if (t.trigger_action === 'click') {
                    o.on('click', function() {
                        i.toggleClass('formtips-show');
                        return !1
                    });
                    e.on('click', function(t) {
                        var o = s(t.target);
                        if (!o.hasClass('formtip') && !o.hasClass('formtips-processed')) {
                            i.toggleClass('formtips-show', !1)
                        }
                    })
                } else {
                    o.hoverIntent({
                        sensitivity: t.sensitivity,
                        interval: t.interval,
                        over: function() {
                            i.toggleClass('formtips-show', !0)
                        },
                        timeout: t.timeout,
                        out: function() {
                            i.toggleClass('formtips-show', !1)
                        }
                    })
                }
            })
        }
    }
})(jQuery);;;
window.__lc = window.__lc || {};
window.__lc.license = 7760841;
(function() {
    var t = document.createElement('script');
    t.type = 'text/javascript';
    t.async = !0;

    var c = document.getElementsByTagName('script')[0];
    c.parentNode.insertBefore(t, c)
})();;
(function(t, i, n, s) {
    function e(i, s) {
        this.settings = null;
        this.options = t.extend({}, e.Defaults, s);
        this.$element = t(i);
        this._handlers = {};
        this._plugins = {};
        this._supress = {};
        this._current = null;
        this._speed = null;
        this._coordinates = [];
        this._breakpoint = null;
        this._width = null;
        this._items = [];
        this._clones = [];
        this._mergers = [];
        this._widths = [];
        this._invalidated = {};
        this._pipe = [];
        this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {
                start: null,
                current: null
            },
            direction: null
        };
        this._states = {
            current: {},
            tags: {
                'initializing': ['busy'],
                'animating': ['busy'],
                'dragging': ['interacting']
            }
        };
        t.each(['onResize', 'onThrottledResize'], t.proxy(function(e, i) {
            this._handlers[i] = t.proxy(this[i], this)
        }, this));
        t.each(e.Plugins, t.proxy(function(t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
        }, this));
        t.each(e.Workers, t.proxy(function(e, i) {
            this._pipe.push({
                'filter': i.filter,
                'run': t.proxy(i.run, this)
            })
        }, this));
        this.setup();
        this.initialize()
    };
    e.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: i,
        fallbackEasing: 'swing',
        info: !1,
        nestedItemSelector: !1,
        itemElement: 'div',
        stageElement: 'div',
        refreshClass: 'owl-refresh',
        loadedClass: 'owl-loaded',
        loadingClass: 'owl-loading',
        rtlClass: 'owl-rtl',
        responsiveClass: 'owl-responsive',
        dragClass: 'owl-drag',
        itemClass: 'owl-item',
        stageClass: 'owl-stage',
        stageOuterClass: 'owl-stage-outer',
        grabClass: 'owl-grab'
    };
    e.Width = {
        Default: 'default',
        Inner: 'inner',
        Outer: 'outer'
    };
    e.Type = {
        Event: 'event',
        State: 'state'
    };
    e.Plugins = {};
    e.Workers = [{
        filter: ['width', 'settings'],
        run: function() {
            this._width = this.$element.width()
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function(t) {
            t.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ['items', 'settings'],
        run: function() {
            this.$stage.children('.cloned').remove()
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function(t) {
            var e = this.settings.margin || '',
                n = !this.settings.autoWidth,
                i = this.settings.rtl,
                s = {
                    'width': 'auto',
                    'margin-left': i ? e : '',
                    'margin-right': i ? '' : e
                };
            !n && this.$stage.children().css(s);
            t.css = s
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function(t) {
            var s = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                e = null,
                i = this._items.length,
                o = !this.settings.autoWidth,
                n = [];
            t.items = {
                merge: !1,
                width: s
            };
            while (i--) {
                e = this._mergers[i];
                e = this.settings.mergeFit && Math.min(e, this.settings.items) || e;
                t.items.merge = e > 1 || t.items.merge;
                n[i] = !o ? this._items[i].width() : s * e
            };
            this._widths = n
        }
    }, {
        filter: ['items', 'settings'],
        run: function() {
            var e = [],
                i = this._items,
                s = this.settings,
                r = Math.max(s.items * 2, 4),
                h = Math.ceil(i.length / 2) * 2,
                a = s.loop && i.length ? s.rewind ? r : Math.max(r, h) : 0,
                n = '',
                o = '';
            a /= 2;
            while (a--) {
                e.push(this.normalize(e.length / 2, !0));
                n = n + i[e[e.length - 1]][0].outerHTML;
                e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0));
                o = i[e[e.length - 1]][0].outerHTML + o
            };
            this._clones = e;
            t(n).addClass('cloned').appendTo(this.$stage);
            t(o).addClass('cloned').prependTo(this.$stage)
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function() {
            var n = this.settings.rtl ? 1 : -1,
                o = this._clones.length + this._items.length,
                t = -1,
                i = 0,
                s = 0,
                e = [];
            while (++t < o) {
                i = e[t - 1] || 0;
                s = this._widths[this.relative(t)] + this.settings.margin;
                e.push(i + s * n)
            };
            this._coordinates = e
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function() {
            var t = this.settings.stagePadding,
                e = this._coordinates,
                i = {
                    'width': Math.ceil(Math.abs(e[e.length - 1])) + t * 2,
                    'padding-left': t || '',
                    'padding-right': t || ''
                };
            this.$stage.css(i)
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function(t) {
            var e = this._coordinates.length,
                i = !this.settings.autoWidth,
                s = this.$stage.children();
            if (i && t.items.merge) {
                while (e--) {
                    t.css.width = this._widths[this.relative(e)];
                    s.eq(e).css(t.css)
                }
            } else if (i) {
                t.css.width = t.items.width;
                s.css(t.css)
            }
        }
    }, {
        filter: ['items'],
        run: function() {
            this._coordinates.length < 1 && this.$stage.removeAttr('style')
        }
    }, {
        filter: ['width', 'items', 'settings'],
        run: function(t) {
            t.current = t.current ? this.$stage.children().index(t.current) : 0;
            t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current));
            this.reset(t.current)
        }
    }, {
        filter: ['position'],
        run: function() {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ['width', 'position', 'items', 'settings'],
        run: function() {
            var n = this.settings.rtl ? 1 : -1,
                o = this.settings.stagePadding * 2,
                e = this.coordinates(this.current()) + o,
                r = e + this.width() * n,
                i, s, a = [],
                t, h;
            for (t = 0, h = this._coordinates.length; t < h; t++) {
                i = this._coordinates[t - 1] || 0;
                s = Math.abs(this._coordinates[t]) + o * n;
                if ((this.op(i, '<=', e) && (this.op(i, '>', r))) || (this.op(s, '<', e) && this.op(s, '>', r))) {
                    a.push(t)
                }
            };
            this.$stage.children('.active').removeClass('active');
            this.$stage.children(':eq(' + a.join('), :eq(') + ')').addClass('active');
            if (this.settings.center) {
                this.$stage.children('.center').removeClass('center');
                this.$stage.children().eq(this.current()).addClass('center')
            }
        }
    }];
    e.prototype.initialize = function() {
        this.enter('initializing');
        this.trigger('initialize');
        this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);
        if (this.settings.autoWidth && !this.is('pre-loading')) {
            var e, i, n;
            e = this.$element.find('img');
            i = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : s;
            n = this.$element.children(i).width();
            if (e.length && n <= 0) {
                this.preloadAutoWidthImages(e)
            }
        };
        this.$element.addClass(this.options.loadingClass);
        this.$stage = t('<' + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>');
        this.$element.append(this.$stage.parent());
        this.replace(this.$element.children().not(this.$stage.parent()));
        if (this.$element.is(':visible')) {
            this.refresh()
        } else {
            this.invalidate('width')
        };
        this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass);
        this.registerEventHandlers();
        this.leave('initializing');
        this.trigger('initialized')
    };
    e.prototype.setup = function() {
        var n = this.viewport(),
            s = this.options.responsive,
            i = -1,
            e = null;
        if (!s) {
            e = t.extend({}, this.options)
        } else {
            t.each(s, function(t) {
                if (t <= n && t > i) {
                    i = Number(t)
                }
            });
            e = t.extend({}, this.options, s[i]);
            if (typeof e.stagePadding === 'function') {
                e.stagePadding = e.stagePadding()
            };
            delete e.responsive;
            if (e.responsiveClass) {
                this.$element.attr('class', this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + i))
            }
        };
        this.trigger('change', {
            property: {
                name: 'settings',
                value: e
            }
        });
        this._breakpoint = i;
        this.settings = e;
        this.invalidate('settings');
        this.trigger('changed', {
            property: {
                name: 'settings',
                value: this.settings
            }
        })
    };
    e.prototype.optionsLogic = function() {
        if (this.settings.autoWidth) {
            this.settings.stagePadding = !1;
            this.settings.merge = !1
        }
    };
    e.prototype.prepare = function(e) {
        var i = this.trigger('prepare', {
            content: e
        });
        if (!i.data) {
            i.data = t('<' + this.settings.itemElement + '/>').addClass(this.options.itemClass).append(e)
        };
        this.trigger('prepared', {
            content: i.data
        });
        return i.data
    };
    e.prototype.update = function() {
        var e = 0,
            i = this._pipe.length,
            s = t.proxy(function(t) {
                return this[t]
            }, this._invalidated),
            n = {};
        while (e < i) {
            if (this._invalidated.all || t.grep(this._pipe[e].filter, s).length > 0) {
                this._pipe[e].run(n)
            };
            e++
        };
        this._invalidated = {};
        !this.is('valid') && this.enter('valid')
    };
    e.prototype.width = function(t) {
        t = t || e.Width.Default;
        switch (t) {
            case e.Width.Inner:
            case e.Width.Outer:
                return this._width;
            default:
                return this._width - this.settings.stagePadding * 2 + this.settings.margin
        }
    };
    e.prototype.refresh = function() {
        this.enter('refreshing');
        this.trigger('refresh');
        this.setup();
        this.optionsLogic();
        this.$element.addClass(this.options.refreshClass);
        this.update();
        this.$element.removeClass(this.options.refreshClass);
        this.leave('refreshing');
        this.trigger('refreshed')
    };
    e.prototype.onThrottledResize = function() {
        i.clearTimeout(this.resizeTimer);
        this.resizeTimer = i.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    };
    e.prototype.onResize = function() {
        if (!this._items.length) {
            return !1
        };
        if (this._width === this.$element.width()) {
            return !1
        };
        if (!this.$element.is(':visible')) {
            return !1
        };
        this.enter('resizing');
        if (this.trigger('resize').isDefaultPrevented()) {
            this.leave('resizing');
            return !1
        };
        this.invalidate('width');
        this.refresh();
        this.leave('resizing');
        this.trigger('resized')
    };
    e.prototype.registerEventHandlers = function() {
        if (t.support.transition) {
            this.$stage.on(t.support.transition.end + '.owl.core', t.proxy(this.onTransitionEnd, this))
        };
        if (this.settings.responsive !== !1) {
            this.on(i, 'resize', this._handlers.onThrottledResize)
        };
        if (this.settings.mouseDrag) {
            this.$element.addClass(this.options.dragClass);
            this.$stage.on('mousedown.owl.core', t.proxy(this.onDragStart, this));
            this.$stage.on('dragstart.owl.core selectstart.owl.core', function() {
                return !1
            })
        };
        if (this.settings.touchDrag) {
            this.$stage.on('touchstart.owl.core', t.proxy(this.onDragStart, this));
            this.$stage.on('touchcancel.owl.core', t.proxy(this.onDragEnd, this))
        }
    };
    e.prototype.onDragStart = function(e) {
        var i = null;
        if (e.which === 3) {
            return
        };
        if (t.support.transform) {
            i = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
            i = {
                x: i[i.length === 16 ? 12 : 4],
                y: i[i.length === 16 ? 13 : 5]
            }
        } else {
            i = this.$stage.position();
            i = {
                x: this.settings.rtl ? i.left + this.$stage.width() - this.width() + this.settings.margin : i.left,
                y: i.top
            }
        };
        if (this.is('animating')) {
            t.support.transform ? this.animate(i.x) : this.$stage.stop();
            this.invalidate('position')
        };
        this.$element.toggleClass(this.options.grabClass, e.type === 'mousedown');
        this.speed(0);
        this._drag.time = new Date().getTime();
        this._drag.target = t(e.target);
        this._drag.stage.start = i;
        this._drag.stage.current = i;
        this._drag.pointer = this.pointer(e);
        t(n).on('mouseup.owl.core touchend.owl.core', t.proxy(this.onDragEnd, this));
        t(n).one('mousemove.owl.core touchmove.owl.core', t.proxy(function(e) {
            var i = this.difference(this._drag.pointer, this.pointer(e));
            t(n).on('mousemove.owl.core touchmove.owl.core', t.proxy(this.onDragMove, this));
            if (Math.abs(i.x) < Math.abs(i.y) && this.is('valid')) {
                return
            };
            e.preventDefault();
            this.enter('dragging');
            this.trigger('drag')
        }, this))
    };
    e.prototype.onDragMove = function(t) {
        var e = null,
            i = null,
            n = null,
            o = this.difference(this._drag.pointer, this.pointer(t)),
            s = this.difference(this._drag.stage.start, o);
        if (!this.is('dragging')) {
            return
        };
        t.preventDefault();
        if (this.settings.loop) {
            e = this.coordinates(this.minimum());
            i = this.coordinates(this.maximum() + 1) - e;
            s.x = (((s.x - e) % i + i) % i) + e
        } else {
            e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
            i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
            n = this.settings.pullDrag ? -1 * o.x / 5 : 0;
            s.x = Math.max(Math.min(s.x, e + n), i + n)
        };
        this._drag.stage.current = s;
        this.animate(s.x)
    };
    e.prototype.onDragEnd = function(e) {
        var i = this.difference(this._drag.pointer, this.pointer(e)),
            o = this._drag.stage.current,
            s = i.x > 0 ^ this.settings.rtl ? 'left' : 'right';
        t(n).off('.owl.core');
        this.$element.removeClass(this.options.grabClass);
        if (i.x !== 0 && this.is('dragging') || !this.is('valid')) {
            this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
            this.current(this.closest(o.x, i.x !== 0 ? s : this._drag.direction));
            this.invalidate('position');
            this.update();
            this._drag.direction = s;
            if (Math.abs(i.x) > 3 || new Date().getTime() - this._drag.time > 300) {
                this._drag.target.one('click.owl.core', function() {
                    return !1
                })
            }
        };
        if (!this.is('dragging')) {
            return
        };
        this.leave('dragging');
        this.trigger('dragged')
    };
    e.prototype.closest = function(e, i) {
        var s = -1,
            n = 30,
            r = this.width(),
            o = this.coordinates();
        if (!this.settings.freeDrag) {
            t.each(o, t.proxy(function(t, a) {
                if (i === 'left' && e > a - n && e < a + n) {
                    s = t
                } else if (i === 'right' && e > a - r - n && e < a - r + n) {
                    s = t + 1
                } else if (this.op(e, '<', a) && this.op(e, '>', o[t + 1] || a - r)) {
                    s = i === 'left' ? t + 1 : t
                };
                return s === -1
            }, this))
        };
        if (!this.settings.loop) {
            if (this.op(e, '>', o[this.minimum()])) {
                s = e = this.minimum()
            } else if (this.op(e, '<', o[this.maximum()])) {
                s = e = this.maximum()
            }
        };
        return s
    };
    e.prototype.animate = function(e) {
        var i = this.speed() > 0;
        this.is('animating') && this.onTransitionEnd();
        if (i) {
            this.enter('animating');
            this.trigger('translate')
        };
        if (t.support.transform3d && t.support.transition) {
            this.$stage.css({
                transform: 'translate3d(' + e + 'px,0px,0px)',
                transition: (this.speed() / 1000) + 's'
            })
        } else if (i) {
            this.$stage.animate({
                left: e + 'px'
            }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this))
        } else {
            this.$stage.css({
                left: e + 'px'
            })
        }
    };
    e.prototype.is = function(t) {
        return this._states.current[t] && this._states.current[t] > 0
    };
    e.prototype.current = function(t) {
        if (t === s) {
            return this._current
        };
        if (this._items.length === 0) {
            return s
        };
        t = this.normalize(t);
        if (this._current !== t) {
            var e = this.trigger('change', {
                property: {
                    name: 'position',
                    value: t
                }
            });
            if (e.data !== s) {
                t = this.normalize(e.data)
            };
            this._current = t;
            this.invalidate('position');
            this.trigger('changed', {
                property: {
                    name: 'position',
                    value: this._current
                }
            })
        };
        return this._current
    };
    e.prototype.invalidate = function(e) {
        if (t.type(e) === 'string') {
            this._invalidated[e] = !0;
            this.is('valid') && this.leave('valid')
        };
        return t.map(this._invalidated, function(t, e) {
            return e
        })
    };
    e.prototype.reset = function(t) {
        t = this.normalize(t);
        if (t === s) {
            return
        };
        this._speed = 0;
        this._current = t;
        this.suppress(['translate', 'translated']);
        this.animate(this.coordinates(t));
        this.release(['translate', 'translated'])
    };
    e.prototype.normalize = function(t, e) {
        var i = this._items.length,
            n = e ? 0 : this._clones.length;
        if (!this.isNumeric(t) || i < 1) {
            t = s
        } else if (t < 0 || t >= i + n) {
            t = ((t - n / 2) % i + i) % i + n / 2
        };
        return t
    };
    e.prototype.relative = function(t) {
        t -= this._clones.length / 2;
        return this.normalize(t, !0)
    };
    e.prototype.maximum = function(t) {
        var i = this.settings,
            e = this._coordinates.length,
            s, n, o;
        if (i.loop) {
            e = this._clones.length / 2 + this._items.length - 1
        } else if (i.autoWidth || i.merge) {
            s = this._items.length;
            n = this._items[--s].width();
            o = this.$element.width();
            while (s--) {
                n += this._items[s].width() + this.settings.margin;
                if (n > o) {
                    break
                }
            };
            e = s + 1
        } else if (i.center) {
            e = this._items.length - 1
        } else {
            e = this._items.length - i.items
        };
        if (t) {
            e -= this._clones.length / 2
        };
        return Math.max(e, 0)
    };
    e.prototype.minimum = function(t) {
        return t ? 0 : this._clones.length / 2
    };
    e.prototype.items = function(t) {
        if (t === s) {
            return this._items.slice()
        };
        t = this.normalize(t, !0);
        return this._items[t]
    };
    e.prototype.mergers = function(t) {
        if (t === s) {
            return this._mergers.slice()
        };
        t = this.normalize(t, !0);
        return this._mergers[t]
    };
    e.prototype.clones = function(e) {
        var i = this._clones.length / 2,
            o = i + this._items.length,
            n = function(t) {
                return t % 2 === 0 ? o + t / 2 : i - (t + 1) / 2
            };
        if (e === s) {
            return t.map(this._clones, function(t, e) {
                return n(e)
            })
        };
        return t.map(this._clones, function(t, i) {
            return t === e ? n(i) : null
        })
    };
    e.prototype.speed = function(t) {
        if (t !== s) {
            this._speed = t
        };
        return this._speed
    };
    e.prototype.coordinates = function(e) {
        var o = 1,
            n = e - 1,
            i;
        if (e === s) {
            return t.map(this._coordinates, t.proxy(function(t, e) {
                return this.coordinates(e)
            }, this))
        };
        if (this.settings.center) {
            if (this.settings.rtl) {
                o = -1;
                n = e + 1
            };
            i = this._coordinates[e];
            i += (this.width() - i + (this._coordinates[n] || 0)) / 2 * o
        } else {
            i = this._coordinates[n] || 0
        };
        i = Math.ceil(i);
        return i
    };
    e.prototype.duration = function(t, e, i) {
        if (i === 0) {
            return 0
        };
        return Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs((i || this.settings.smartSpeed))
    };
    e.prototype.to = function(t, e) {
        var o = this.current(),
            s = null,
            i = t - this.relative(o),
            h = (i > 0) - (i < 0),
            r = this._items.length,
            a = this.minimum(),
            n = this.maximum();
        if (this.settings.loop) {
            if (!this.settings.rewind && Math.abs(i) > r / 2) {
                i += h * -1 * r
            };
            t = o + i;
            s = ((t - a) % r + r) % r + a;
            if (s !== t && s - i <= n && s - i > 0) {
                o = s - i;
                t = s;
                this.reset(o)
            }
        } else if (this.settings.rewind) {
            n += 1;
            t = (t % n + n) % n
        } else {
            t = Math.max(a, Math.min(n, t))
        };
        this.speed(this.duration(o, t, e));
        this.current(t);
        if (this.$element.is(':visible')) {
            this.update()
        }
    };
    e.prototype.next = function(t) {
        t = t || !1;
        this.to(this.relative(this.current()) + 1, t)
    };
    e.prototype.prev = function(t) {
        t = t || !1;
        this.to(this.relative(this.current()) - 1, t)
    };
    e.prototype.onTransitionEnd = function(t) {
        if (t !== s) {
            t.stopPropagation();
            if ((t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0)) {
                return !1
            }
        };
        this.leave('animating');
        this.trigger('translated')
    };
    e.prototype.viewport = function() {
        var e;
        if (this.options.responsiveBaseElement !== i) {
            e = t(this.options.responsiveBaseElement).width()
        } else if (i.innerWidth) {
            e = i.innerWidth
        } else if (n.documentElement && n.documentElement.clientWidth) {
            e = n.documentElement.clientWidth
        } else {
            console.warn('Can not detect viewport width.')
        };
        return e
    };
    e.prototype.replace = function(e) {
        this.$stage.empty();
        this._items = [];
        if (e) {
            e = (e instanceof jQuery) ? e : t(e)
        };
        if (this.settings.nestedItemSelector) {
            e = e.find('.' + this.settings.nestedItemSelector)
        };
        e.filter(function() {
            return this.nodeType === 1
        }).each(t.proxy(function(t, e) {
            e = this.prepare(e);
            this.$stage.append(e);
            this._items.push(e);
            this._mergers.push(e.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1)
        }, this));
        this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);
        this.invalidate('items')
    };
    e.prototype.add = function(e, i) {
        var n = this.relative(this._current);
        i = i === s ? this._items.length : this.normalize(i, !0);
        e = e instanceof jQuery ? e : t(e);
        this.trigger('add', {
            content: e,
            position: i
        });
        e = this.prepare(e);
        if (this._items.length === 0 || i === this._items.length) {
            this._items.length === 0 && this.$stage.append(e);
            this._items.length !== 0 && this._items[i - 1].after(e);
            this._items.push(e);
            this._mergers.push(e.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1)
        } else {
            this._items[i].before(e);
            this._items.splice(i, 0, e);
            this._mergers.splice(i, 0, e.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1)
        };
        this._items[n] && this.reset(this._items[n].index());
        this.invalidate('items');
        this.trigger('added', {
            content: e,
            position: i
        })
    };
    e.prototype.remove = function(t) {
        t = this.normalize(t, !0);
        if (t === s) {
            return
        };
        this.trigger('remove', {
            content: this._items[t],
            position: t
        });
        this._items[t].remove();
        this._items.splice(t, 1);
        this._mergers.splice(t, 1);
        this.invalidate('items');
        this.trigger('removed', {
            content: null,
            position: t
        })
    };
    e.prototype.preloadAutoWidthImages = function(e) {
        e.each(t.proxy(function(e, i) {
            this.enter('pre-loading');
            i = t(i);
            t(new Image()).one('load', t.proxy(function(t) {
                i.attr('src', t.target.src);
                i.css('opacity', 1);
                this.leave('pre-loading');
                !this.is('pre-loading') && !this.is('initializing') && this.refresh()
            }, this)).attr('src', i.attr('src') || i.attr('data-src') || i.attr('data-src-retina'))
        }, this))
    };
    e.prototype.destroy = function() {
        this.$element.off('.owl.core');
        this.$stage.off('.owl.core');
        t(n).off('.owl.core');
        if (this.settings.responsive !== !1) {
            i.clearTimeout(this.resizeTimer);
            this.off(i, 'resize', this._handlers.onThrottledResize)
        };
        for (var e in this._plugins) {
            this._plugins[e].destroy()
        };
        this.$stage.children('.cloned').remove();
        this.$stage.unwrap();
        this.$stage.children().contents().unwrap();
        this.$stage.children().unwrap();
        this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), '')).removeData('owl.carousel')
    };
    e.prototype.op = function(t, e, i) {
        var s = this.settings.rtl;
        switch (e) {
            case '<':
                return s ? t > i : t < i;
            case '>':
                return s ? t < i : t > i;
            case '>=':
                return s ? t <= i : t >= i;
            case '<=':
                return s ? t >= i : t <= i;
            default:
                break
        }
    };
    e.prototype.on = function(t, e, i, s) {
        if (t.addEventListener) {
            t.addEventListener(e, i, s)
        } else if (t.attachEvent) {
            t.attachEvent('on' + e, i)
        }
    };
    e.prototype.off = function(t, e, i, s) {
        if (t.removeEventListener) {
            t.removeEventListener(e, i, s)
        } else if (t.detachEvent) {
            t.detachEvent('on' + e, i)
        }
    };
    e.prototype.trigger = function(i, s, n, o, l) {
        var h = {
                item: {
                    count: this._items.length,
                    index: this.current()
                }
            },
            a = t.camelCase(t.grep(['on', i, n], function(t) {
                return t
            }).join('-').toLowerCase()),
            r = t.Event([i, 'owl', n || 'carousel'].join('.').toLowerCase(), t.extend({
                relatedTarget: this
            }, h, s));
        if (!this._supress[i]) {
            t.each(this._plugins, function(t, e) {
                if (e.onTrigger) {
                    e.onTrigger(r)
                }
            });
            this.register({
                type: e.Type.Event,
                name: i
            });
            this.$element.trigger(r);
            if (this.settings && typeof this.settings[a] === 'function') {
                this.settings[a].call(this, r)
            }
        };
        return r
    };
    e.prototype.enter = function(e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
            if (this._states.current[e] === s) {
                this._states.current[e] = 0
            };
            this._states.current[e]++
        }, this))
    };
    e.prototype.leave = function(e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
            this._states.current[e]--
        }, this))
    };
    e.prototype.register = function(i) {
        if (i.type === e.Type.Event) {
            if (!t.event.special[i.name]) {
                t.event.special[i.name] = {}
            };
            if (!t.event.special[i.name].owl) {
                var s = t.event.special[i.name]._default;
                t.event.special[i.name]._default = function(t) {
                    if (s && s.apply && (!t.namespace || t.namespace.indexOf('owl') === -1)) {
                        return s.apply(this, arguments)
                    };
                    return t.namespace && t.namespace.indexOf('owl') > -1
                };
                t.event.special[i.name].owl = !0
            }
        } else if (i.type === e.Type.State) {
            if (!this._states.tags[i.name]) {
                this._states.tags[i.name] = i.tags
            } else {
                this._states.tags[i.name] = this._states.tags[i.name].concat(i.tags)
            };
            this._states.tags[i.name] = t.grep(this._states.tags[i.name], t.proxy(function(e, s) {
                return t.inArray(e, this._states.tags[i.name]) === s
            }, this))
        }
    };
    e.prototype.suppress = function(e) {
        t.each(e, t.proxy(function(t, e) {
            this._supress[e] = !0
        }, this))
    };
    e.prototype.release = function(e) {
        t.each(e, t.proxy(function(t, e) {
            delete this._supress[e]
        }, this))
    };
    e.prototype.pointer = function(t) {
        var e = {
            x: null,
            y: null
        };
        t = t.originalEvent || t || i.event;
        t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t;
        if (t.pageX) {
            e.x = t.pageX;
            e.y = t.pageY
        } else {
            e.x = t.clientX;
            e.y = t.clientY
        };
        return e
    };
    e.prototype.isNumeric = function(t) {
        return !isNaN(parseFloat(t))
    };
    e.prototype.difference = function(t, e) {
        return {
            x: t.x - e.x,
            y: t.y - e.y
        }
    };
    t.fn.owlCarousel = function(i) {
        var s = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var o = t(this),
                n = o.data('owl.carousel');
            if (!n) {
                n = new e(this, typeof i == 'object' && i);
                o.data('owl.carousel', n);
                t.each(['next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'], function(i, s) {
                    n.register({
                        type: e.Type.Event,
                        name: s
                    });
                    n.$element.on(s + '.owl.carousel.core', t.proxy(function(t) {
                        if (t.namespace && t.relatedTarget !== this) {
                            this.suppress([s]);
                            n[s].apply(this, [].slice.call(arguments, 1));
                            this.release([s])
                        }
                    }, n))
                })
            };
            if (typeof i == 'string' && i.charAt(0) !== '_') {
                n[i].apply(n, s)
            }
        })
    };
    t.fn.owlCarousel.Constructor = e
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._interval = null;
        this._visible = null;
        this._handlers = {
            'initialized.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoRefresh) {
                    this.watch()
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        autoRefresh: !0,
        autoRefreshInterval: 500
    };
    n.prototype.watch = function() {
        if (this._interval) {
            return
        };
        this._visible = this._core.$element.is(':visible');
        this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval)
    };
    n.prototype.refresh = function() {
        if (this._core.$element.is(':visible') === this._visible) {
            return
        };
        this._visible = !this._visible;
        this._core.$element.toggleClass('owl-hidden', !this._visible);
        this._visible && (this._core.invalidate('width') && this._core.refresh())
    };
    n.prototype.destroy = function() {
        var t, i;
        e.clearInterval(this._interval);
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        };
        for (i in Object.getOwnPropertyNames(this)) {
            typeof this[i] != 'function' && (this[i] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._loaded = [];
        this._handlers = {
            'initialized.owl.carousel change.owl.carousel resized.owl.carousel': t.proxy(function(e) {
                if (!e.namespace) {
                    return
                };
                if (!this._core.settings || !this._core.settings.lazyLoad) {
                    return
                };
                if ((e.property && e.property.name == 'position') || e.type == 'initialized') {
                    var i = this._core.settings,
                        o = (i.center && Math.ceil(i.items / 2) || i.items),
                        r = ((i.center && o * -1) || 0),
                        n = (e.property && e.property.value !== s ? e.property.value : this._core.current()) + r,
                        a = this._core.clones().length,
                        h = t.proxy(function(t, e) {
                            this.load(e)
                        }, this);
                    while (r++ < o) {
                        this.load(a / 2 + this._core.relative(n));
                        a && t.each(this._core.clones(this._core.relative(n)), h);
                        n++
                    }
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        lazyLoad: !1
    };
    n.prototype.load = function(i) {
        var s = this._core.$stage.children().eq(i),
            n = s && s.find('.owl-lazy');
        if (!n || t.inArray(s.get(0), this._loaded) > -1) {
            return
        };
        n.each(t.proxy(function(i, s) {
            var n = t(s),
                r, o = (e.devicePixelRatio > 1 && n.attr('data-src-retina')) || n.attr('data-src');
            this._core.trigger('load', {
                element: n,
                url: o
            }, 'lazy');
            if (n.is('img')) {
                n.one('load.owl.lazy', t.proxy(function() {
                    n.css('opacity', 1);
                    this._core.trigger('loaded', {
                        element: n,
                        url: o
                    }, 'lazy')
                }, this)).attr('src', o)
            } else {
                r = new Image();
                r.onload = t.proxy(function() {
                    n.css({
                        'background-image': 'url("' + o + '")',
                        'opacity': '1'
                    });
                    this._core.trigger('loaded', {
                        element: n,
                        url: o
                    }, 'lazy')
                }, this);
                r.src = o
            }
        }, this));
        this._loaded.push(s.get(0))
    };
    n.prototype.destroy = function() {
        var t, e;
        for (t in this.handlers) {
            this._core.$element.off(t, this.handlers[t])
        };
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != 'function' && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Lazy = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._handlers = {
            'initialized.owl.carousel refreshed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoHeight) {
                    this.update()
                }
            }, this),
            'changed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoHeight && t.property.name == 'position') {
                    this.update()
                }
            }, this),
            'loaded.owl.lazy': t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoHeight && t.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
                    this.update()
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        autoHeight: !1,
        autoHeightClass: 'owl-height'
    };
    n.prototype.update = function() {
        var e = this._core._current,
            n = e + this._core.settings.items,
            o = this._core.$stage.children().toArray().slice(e, n),
            i = [],
            s = 0;
        t.each(o, function(e, s) {
            i.push(t(s).height())
        });
        s = Math.max.apply(null, i);
        this._core.$stage.parent().height(s).addClass(this._core.settings.autoHeightClass)
    };
    n.prototype.destroy = function() {
        var t, e;
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        };
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != 'function' && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.AutoHeight = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._videos = {};
        this._playing = null;
        this._handlers = {
            'initialized.owl.carousel': t.proxy(function(t) {
                if (t.namespace) {
                    this._core.register({
                        type: 'state',
                        name: 'playing',
                        tags: ['interacting']
                    })
                }
            }, this),
            'resize.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.video && this.isInFullScreen()) {
                    t.preventDefault()
                }
            }, this),
            'refreshed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.is('resizing')) {
                    this._core.$stage.find('.cloned .owl-video-frame').remove()
                }
            }, this),
            'changed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && t.property.name === 'position' && this._playing) {
                    this.stop()
                }
            }, this),
            'prepared.owl.carousel': t.proxy(function(e) {
                if (!e.namespace) {
                    return
                };
                var i = t(e.content).find('.owl-video');
                if (i.length) {
                    i.css('display', 'none');
                    this.fetch(i, t(e.content))
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers);
        this._core.$element.on('click.owl.video', '.owl-video-play-icon', t.proxy(function(t) {
            this.play(t)
        }, this))
    };
    n.Defaults = {
        video: !1,
        videoHeight: !1,
        videoWidth: !1
    };
    n.prototype.fetch = function(t, e) {
        var n = (function() {
                if (t.attr('data-vimeo-id')) {
                    return 'vimeo'
                } else if (t.attr('data-vzaar-id')) {
                    return 'vzaar'
                } else {
                    return 'youtube'
                }
            })(),
            i = t.attr('data-vimeo-id') || t.attr('data-youtube-id') || t.attr('data-vzaar-id'),
            o = t.attr('data-width') || this._core.settings.videoWidth,
            r = t.attr('data-height') || this._core.settings.videoHeight,
            s = t.attr('href');
        if (s) {
            i = s.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
            if (i[3].indexOf('youtu') > -1) {
                n = 'youtube'
            } else if (i[3].indexOf('vimeo') > -1) {
                n = 'vimeo'
            } else if (i[3].indexOf('vzaar') > -1) {
                n = 'vzaar'
            } else {
                throw new Error('Video URL not supported.')
            };
            i = i[6]
        } else {
            throw new Error('Missing video URL.')
        };
        this._videos[s] = {
            type: n,
            id: i,
            width: o,
            height: r
        };
        e.attr('data-video', s);
        this.thumbnail(t, this._videos[s])
    };
    n.prototype.thumbnail = function(e, i) {
        var o, h, s, c = i.width && i.height ? 'style="width:' + i.width + 'px;height:' + i.height + 'px;"' : '',
            r = e.find('img'),
            a = 'src',
            l = '',
            p = this._core.settings,
            n = function(t) {
                h = '<div class="owl-video-play-icon"></div>';
                if (p.lazyLoad) {
                    o = '<div class="owl-video-tn ' + l + '" ' + a + '="' + t + '"></div>'
                } else {
                    o = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>'
                };
                e.after(o);
                e.after(h)
            };
        e.wrap('<div class="owl-video-wrapper"' + c + '></div>');
        if (this._core.settings.lazyLoad) {
            a = 'data-src';
            l = 'owl-lazy'
        };
        if (r.length) {
            n(r.attr(a));
            r.remove();
            return !1
        };
        if (i.type === 'youtube') {
            s = '//img.youtube.com/vi/' + i.id + '/hqdefault.jpg';
            n(s)
        } else if (i.type === 'vimeo') {
            t.ajax({
                type: 'GET',
                url: '//vimeo.com/api/v2/video/' + i.id + '.json',
                jsonp: 'callback',
                dataType: 'jsonp',
                success: function(t) {
                    s = t[0].thumbnail_large;
                    n(s)
                }
            })
        } else if (i.type === 'vzaar') {
            t.ajax({
                type: 'GET',
                url: '//vzaar.com/api/videos/' + i.id + '.json',
                jsonp: 'callback',
                dataType: 'jsonp',
                success: function(t) {
                    s = t.framegrab_url;
                    n(s)
                }
            })
        }
    };
    n.prototype.stop = function() {
        this._core.trigger('stop', null, 'video');
        this._playing.find('.owl-video-frame').remove();
        this._playing.removeClass('owl-video-playing');
        this._playing = null;
        this._core.leave('playing');
        this._core.trigger('stopped', null, 'video')
    };
    n.prototype.play = function(e) {
        var a = t(e.target),
            s = a.closest('.' + this._core.settings.itemClass),
            i = this._videos[s.attr('data-video')],
            o = i.width || '100%',
            r = i.height || this._core.$stage.height(),
            n;
        if (this._playing) {
            return
        };
        this._core.enter('playing');
        this._core.trigger('play', null, 'video');
        s = this._core.items(this._core.relative(s.index()));
        this._core.reset(s.index());
        if (i.type === 'youtube') {
            n = '<iframe width="' + o + '" height="' + r + '" src="//www.youtube.com/embed/' + i.id + '?autoplay=1&rel=0&v=' + i.id + '" frameborder="0" allowfullscreen></iframe>'
        } else if (i.type === 'vimeo') {
            n = '<iframe src="//player.vimeo.com/video/' + i.id + '?autoplay=1" width="' + o + '" height="' + r + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        } else if (i.type === 'vzaar') {
            n = '<iframe frameborder="0"height="' + r + '"width="' + o + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + i.id + '/player?autoplay=true"></iframe>'
        };
        t('<div class="owl-video-frame">' + n + '</div>').insertAfter(s.find('.owl-video'));
        this._playing = s.addClass('owl-video-playing')
    };
    n.prototype.isInFullScreen = function() {
        var e = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
        return e && t(e).parent().hasClass('owl-video-frame')
    };
    n.prototype.destroy = function() {
        var t, e;
        this._core.$element.off('click.owl.video');
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        };
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != 'function' && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Video = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this.core = e;
        this.core.options = t.extend({}, n.Defaults, this.core.options);
        this.swapping = !0;
        this.previous = s;
        this.next = s;
        this.handlers = {
            'change.owl.carousel': t.proxy(function(t) {
                if (t.namespace && t.property.name == 'position') {
                    this.previous = this.core.current();
                    this.next = t.property.value
                }
            }, this),
            'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': t.proxy(function(t) {
                if (t.namespace) {
                    this.swapping = t.type == 'translated'
                }
            }, this),
            'translate.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
                    this.swap()
                }
            }, this)
        };
        this.core.$element.on(this.handlers)
    };
    n.Defaults = {
        animateOut: !1,
        animateIn: !1
    };
    n.prototype.swap = function() {
        if (this.core.settings.items !== 1) {
            return
        };
        if (!t.support.animation || !t.support.transition) {
            return
        };
        this.core.speed(0);
        var e, i = t.proxy(this.clear, this),
            o = this.core.$stage.children().eq(this.previous),
            r = this.core.$stage.children().eq(this.next),
            s = this.core.settings.animateIn,
            n = this.core.settings.animateOut;
        if (this.core.current() === this.previous) {
            return
        };
        if (n) {
            e = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
            o.one(t.support.animation.end, i).css({
                'left': e + 'px'
            }).addClass('animated owl-animated-out').addClass(n)
        };
        if (s) {
            r.one(t.support.animation.end, i).addClass('animated owl-animated-in').addClass(s)
        }
    };
    n.prototype.clear = function(e) {
        t(e.target).css({
            'left': ''
        }).removeClass('animated owl-animated-out owl-animated-in').removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut);
        this.core.onTransitionEnd()
    };
    n.prototype.destroy = function() {
        var t, e;
        for (t in this.handlers) {
            this.core.$element.off(t, this.handlers[t])
        };
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != 'function' && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Animate = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._timeout = null;
        this._paused = !1;
        this._handlers = {
            'changed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && t.property.name === 'settings') {
                    if (this._core.settings.autoplay) {
                        this.play()
                    } else {
                        this.stop()
                    }
                } else if (t.namespace && t.property.name === 'position') {
                    if (this._core.settings.autoplay) {
                        this._setAutoPlayInterval()
                    }
                }
            }, this),
            'initialized.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoplay) {
                    this.play()
                }
            }, this),
            'play.owl.autoplay': t.proxy(function(t, e, i) {
                if (t.namespace) {
                    this.play(e, i)
                }
            }, this),
            'stop.owl.autoplay': t.proxy(function(t) {
                if (t.namespace) {
                    this.stop()
                }
            }, this),
            'mouseover.owl.autoplay': t.proxy(function() {
                if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
                    this.pause()
                }
            }, this),
            'mouseleave.owl.autoplay': t.proxy(function() {
                if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
                    this.play()
                }
            }, this),
            'touchstart.owl.core': t.proxy(function() {
                if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
                    this.pause()
                }
            }, this),
            'touchend.owl.core': t.proxy(function() {
                if (this._core.settings.autoplayHoverPause) {
                    this.play()
                }
            }, this)
        };
        this._core.$element.on(this._handlers);
        this._core.options = t.extend({}, n.Defaults, this._core.options)
    };
    n.Defaults = {
        autoplay: !1,
        autoplayTimeout: 5000,
        autoplayHoverPause: !1,
        autoplaySpeed: !1
    };
    n.prototype.play = function(t, e) {
        this._paused = !1;
        if (this._core.is('rotating')) {
            return
        };
        this._core.enter('rotating');
        this._setAutoPlayInterval()
    };
    n.prototype._getNextTimeout = function(s, n) {
        if (this._timeout) {
            e.clearTimeout(this._timeout)
        };
        return e.setTimeout(t.proxy(function() {
            if (this._paused || this._core.is('busy') || this._core.is('interacting') || i.hidden) {
                return
            };
            this._core.next(n || this._core.settings.autoplaySpeed)
        }, this), s || this._core.settings.autoplayTimeout)
    };
    n.prototype._setAutoPlayInterval = function() {
        this._timeout = this._getNextTimeout()
    };
    n.prototype.stop = function() {
        if (!this._core.is('rotating')) {
            return
        };
        e.clearTimeout(this._timeout);
        this._core.leave('rotating')
    };
    n.prototype.pause = function() {
        if (!this._core.is('rotating')) {
            return
        };
        this._paused = !0
    };
    n.prototype.destroy = function() {
        var t, e;
        this.stop();
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        };
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != 'function' && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.autoplay = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, n) {
    'use strict';
    var s = function(e) {
        this._core = e;
        this._initialized = !1;
        this._pages = [];
        this._controls = {};
        this._templates = [];
        this.$element = this._core.$element;
        this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        };
        this._handlers = {
            'prepared.owl.carousel': t.proxy(function(e) {
                if (e.namespace && this._core.settings.dotsData) {
                    this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>')
                }
            }, this),
            'added.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.dotsData) {
                    this._templates.splice(t.position, 0, this._templates.pop())
                }
            }, this),
            'remove.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._core.settings.dotsData) {
                    this._templates.splice(t.position, 1)
                }
            }, this),
            'changed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && t.property.name == 'position') {
                    this.draw()
                }
            }, this),
            'initialized.owl.carousel': t.proxy(function(t) {
                if (t.namespace && !this._initialized) {
                    this._core.trigger('initialize', null, 'navigation');
                    this.initialize();
                    this.update();
                    this.draw();
                    this._initialized = !0;
                    this._core.trigger('initialized', null, 'navigation')
                }
            }, this),
            'refreshed.owl.carousel': t.proxy(function(t) {
                if (t.namespace && this._initialized) {
                    this._core.trigger('refresh', null, 'navigation');
                    this.update();
                    this.draw();
                    this._core.trigger('refreshed', null, 'navigation')
                }
            }, this)
        };
        this._core.options = t.extend({}, s.Defaults, this._core.options);
        this.$element.on(this._handlers)
    };
    s.Defaults = {
        nav: !1,
        navText: ['prev', 'next'],
        navSpeed: !1,
        navElement: 'div',
        navContainer: !1,
        navContainerClass: 'owl-nav',
        navClass: ['owl-prev', 'owl-next'],
        slideBy: 1,
        dotClass: 'owl-dot',
        dotsClass: 'owl-dots',
        dots: !0,
        dotsEach: !1,
        dotsData: !1,
        dotsSpeed: !1,
        dotsContainer: !1
    };
    s.prototype.initialize = function() {
        var i, e = this._core.settings;
        this._controls.$relative = (e.navContainer ? t(e.navContainer) : t('<div>').addClass(e.navContainerClass).appendTo(this.$element)).addClass('disabled');
        this._controls.$previous = t('<' + e.navElement + '>').addClass(e.navClass[0]).html(e.navText[0]).prependTo(this._controls.$relative).on('click', t.proxy(function(t) {
            this.prev(e.navSpeed)
        }, this));
        this._controls.$next = t('<' + e.navElement + '>').addClass(e.navClass[1]).html(e.navText[1]).appendTo(this._controls.$relative).on('click', t.proxy(function(t) {
            this.next(e.navSpeed)
        }, this));
        if (!e.dotsData) {
            this._templates = [t('<div>').addClass(e.dotClass).append(t('<span>')).prop('outerHTML')]
        };
        this._controls.$absolute = (e.dotsContainer ? t(e.dotsContainer) : t('<div>').addClass(e.dotsClass).appendTo(this.$element)).addClass('disabled');
        this._controls.$absolute.on('click', 'div', t.proxy(function(i) {
            var s = t(i.target).parent().is(this._controls.$absolute) ? t(i.target).index() : t(i.target).parent().index();
            i.preventDefault();
            this.to(s, e.dotsSpeed)
        }, this));
        for (i in this._overrides) {
            this._core[i] = t.proxy(this[i], this)
        }
    };
    s.prototype.destroy = function() {
        var t, s, e, i;
        for (t in this._handlers) {
            this.$element.off(t, this._handlers[t])
        };
        for (s in this._controls) {
            this._controls[s].remove()
        };
        for (i in this.overides) {
            this._core[i] = this._overrides[i]
        };
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != 'function' && (this[e] = null)
        }
    };
    s.prototype.update = function() {
        var e, i, o, s = this._core.clones().length / 2,
            a = s + this._core.items().length,
            n = this._core.maximum(!0),
            t = this._core.settings,
            r = t.center || t.autoWidth || t.dotsData ? 1 : t.dotsEach || t.items;
        if (t.slideBy !== 'page') {
            t.slideBy = Math.min(t.slideBy, t.items)
        };
        if (t.dots || t.slideBy == 'page') {
            this._pages = [];
            for (e = s, i = 0, o = 0; e < a; e++) {
                if (i >= r || i === 0) {
                    this._pages.push({
                        start: Math.min(n, e - s),
                        end: e - s + r - 1
                    });
                    if (Math.min(n, e - s) === n) {
                        break
                    };
                    i = 0, ++o
                };
                i += this._core.mergers(this._core.relative(e))
            }
        }
    };
    s.prototype.draw = function() {
        var i, e = this._core.settings,
            s = this._core.items().length <= e.items,
            n = this._core.relative(this._core.current()),
            o = e.loop || e.rewind;
        this._controls.$relative.toggleClass('disabled', !e.nav || s);
        if (e.nav) {
            this._controls.$previous.toggleClass('disabled', !o && n <= this._core.minimum(!0));
            this._controls.$next.toggleClass('disabled', !o && n >= this._core.maximum(!0))
        };
        this._controls.$absolute.toggleClass('disabled', !e.dots || s);
        if (e.dots) {
            i = this._pages.length - this._controls.$absolute.children().length;
            if (e.dotsData && i !== 0) {
                this._controls.$absolute.html(this._templates.join(''))
            } else if (i > 0) {
                this._controls.$absolute.append(new Array(i + 1).join(this._templates[0]))
            } else if (i < 0) {
                this._controls.$absolute.children().slice(i).remove()
            };
            this._controls.$absolute.find('.active').removeClass('active');
            this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass('active')
        }
    };
    s.prototype.onTrigger = function(e) {
        var i = this._core.settings;
        e.page = {
            index: t.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: i && (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items)
        }
    };
    s.prototype.current = function() {
        var e = this._core.relative(this._core.current());
        return t.grep(this._pages, t.proxy(function(t, i) {
            return t.start <= e && t.end >= e
        }, this)).pop()
    };
    s.prototype.getPosition = function(e) {
        var i, s, n = this._core.settings;
        if (n.slideBy == 'page') {
            i = t.inArray(this.current(), this._pages);
            s = this._pages.length;
            e ? ++i : --i;
            i = this._pages[((i % s) + s) % s].start
        } else {
            i = this._core.relative(this._core.current());
            s = this._core.items().length;
            e ? i += n.slideBy : i -= n.slideBy
        };
        return i
    };
    s.prototype.next = function(e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
    };
    s.prototype.prev = function(e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
    };
    s.prototype.to = function(e, i, s) {
        var n;
        if (!s && this._pages.length) {
            n = this._pages.length;
            t.proxy(this._overrides.to, this._core)(this._pages[((e % n) + n) % n].start, i)
        } else {
            t.proxy(this._overrides.to, this._core)(e, i)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Navigation = s
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    'use strict';
    var n = function(i) {
        this._core = i;
        this._hashes = {};
        this.$element = this._core.$element;
        this._handlers = {
            'initialized.owl.carousel': t.proxy(function(i) {
                if (i.namespace && this._core.settings.startPosition === 'URLHash') {
                    t(e).trigger('hashchange.owl.navigation')
                }
            }, this),
            'prepared.owl.carousel': t.proxy(function(e) {
                if (e.namespace) {
                    var i = t(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');
                    if (!i) {
                        return
                    };
                    this._hashes[i] = e.content
                }
            }, this),
            'changed.owl.carousel': t.proxy(function(i) {
                if (i.namespace && i.property.name === 'position') {
                    var n = this._core.items(this._core.relative(this._core.current())),
                        s = t.map(this._hashes, function(t, e) {
                            return t === n ? e : null
                        }).join();
                    if (!s || e.location.hash.slice(1) === s) {
                        return
                    };
                    e.location.hash = s
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this.$element.on(this._handlers);
        t(e).on('hashchange.owl.navigation', t.proxy(function(t) {
            var n = e.location.hash.substring(1),
                o = this._core.$stage.children(),
                i = this._hashes[n] && o.index(this._hashes[n]);
            if (i === s || i === this._core.current()) {
                return
            };
            this._core.to(this._core.relative(i), !1, !0)
        }, this))
    };
    n.Defaults = {
        URLhashListener: !1
    };
    n.prototype.destroy = function() {
        var i, s;
        t(e).off('hashchange.owl.navigation');
        for (i in this._handlers) {
            this._core.$element.off(i, this._handlers[i])
        };
        for (s in Object.getOwnPropertyNames(this)) {
            typeof this[s] != 'function' && (this[s] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Hash = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var h = t('<support>').get(0).style,
        l = 'Webkit Moz O ms'.split(' '),
        a = {
            transition: {
                end: {
                    WebkitTransition: 'webkitTransitionEnd',
                    MozTransition: 'transitionend',
                    OTransition: 'oTransitionEnd',
                    transition: 'transitionend'
                }
            },
            animation: {
                end: {
                    WebkitAnimation: 'webkitAnimationEnd',
                    MozAnimation: 'animationend',
                    OAnimation: 'oAnimationEnd',
                    animation: 'animationend'
                }
            }
        },
        o = {
            csstransforms: function() {
                return !!n('transform')
            },
            csstransforms3d: function() {
                return !!n('perspective')
            },
            csstransitions: function() {
                return !!n('transition')
            },
            cssanimations: function() {
                return !!n('animation')
            }
        };

    function n(e, i) {
        var n = !1,
            o = e.charAt(0).toUpperCase() + e.slice(1);
        t.each((e + ' ' + l.join(o + ' ') + o).split(' '), function(t, e) {
            if (h[e] !== s) {
                n = i ? e : !0;
                return !1
            }
        });
        return n
    };

    function r(t) {
        return n(t, !0)
    };
    if (o.csstransitions()) {
        t.support.transition = new String(r('transition'));
        t.support.transition.end = a.transition.end[t.support.transition]
    };
    if (o.cssanimations()) {
        t.support.animation = new String(r('animation'));
        t.support.animation.end = a.animation.end[t.support.animation]
    };
    if (o.csstransforms()) {
        t.support.transform = new String(r('transform'));
        t.support.transform3d = o.csstransforms3d()
    }
})(window.Zepto || window.jQuery, window, document);;
(function(t, e) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define('jquery-bridget/jquery-bridget', ['jquery'], function(i) {
            e(t, i)
        })
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e(t, require('jquery'))
    } else {
        t.jQueryBridget = e(t, t.jQuery)
    }
}(window, function(t, e) {
    'use strict';
    var s = Array.prototype.slice,
        i = t.console,
        n = typeof i == 'undefined' ? function() {} : function(t) {
            i.error(t)
        };

    function o(i, o, a) {
        a = a || e || t.jQuery;
        if (!a) {
            return
        };
        if (!o.prototype.option) {
            o.prototype.option = function(t) {
                if (!a.isPlainObject(t)) {
                    return
                };
                this.options = a.extend(!0, this.options, t)
            }
        };
        a.fn[i] = function(t) {
            if (typeof t == 'string') {
                var e = s.call(arguments, 1);
                return u(this, t, e)
            };
            h(this, t);
            return this
        };

        function u(t, e, o) {
            var r, s = '$().' + i + '("' + e + '")';
            t.each(function(t, u) {
                var h = a.data(u, i);
                if (!h) {
                    n(i + ' not initialized. Cannot call methods, i.e. ' + s);
                    return
                };
                var f = h[e];
                if (!f || e.charAt(0) == '_') {
                    n(s + ' is not a valid method');
                    return
                };
                var d = f.apply(h, o);
                r = r === undefined ? d : r
            });
            return r !== undefined ? r : t
        };

        function h(t, e) {
            t.each(function(t, n) {
                var r = a.data(n, i);
                if (r) {
                    r.option(e);
                    r._init()
                } else {
                    r = new o(n, e);
                    a.data(n, i, r)
                }
            })
        };
        r(a)
    };

    function r(t) {
        if (!t || (t && t.bridget)) {
            return
        };
        t.bridget = o
    };
    r(e || t.jQuery);
    return o
}));
(function(t, e) {
    if (typeof define == 'function' && define.amd) {
        define('ev-emitter/ev-emitter', e)
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e()
    } else {
        t.EvEmitter = e()
    }
}(this, function() {
    function e() {};
    var t = e.prototype;
    t.on = function(t, e) {
        if (!t || !e) {
            return
        };
        var n = this._events = this._events || {};
        var i = n[t] = n[t] || [];
        if (i.indexOf(e) == -1) {
            i.push(e)
        };
        return this
    };
    t.once = function(t, e) {
        if (!t || !e) {
            return
        };
        this.on(t, e);
        var i = this._onceEvents = this._onceEvents || {};
        var n = i[t] = i[t] || [];
        n[e] = !0;
        return this
    };
    t.off = function(t, e) {
        var i = this._events && this._events[t];
        if (!i || !i.length) {
            return
        };
        var n = i.indexOf(e);
        if (n != -1) {
            i.splice(n, 1)
        };
        return this
    };
    t.emitEvent = function(t, e) {
        var n = this._events && this._events[t];
        if (!n || !n.length) {
            return
        };
        var r = 0,
            i = n[r];
        e = e || [];
        var o = this._onceEvents && this._onceEvents[t];
        while (i) {
            var s = o && o[i];
            if (s) {
                this.off(t, i);
                delete o[i]
            };
            i.apply(this, e);
            r += s ? 0 : 1;
            i = n[r]
        };
        return this
    };
    return e
}));
(function(t, e) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define('get-size/get-size', [], function() {
            return e()
        })
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e()
    } else {
        t.getSize = e()
    }
})(window, function() {
    'use strict';

    function t(t) {
        var e = parseFloat(t),
            i = t.indexOf('%') == -1 && !isNaN(e);
        return i && e
    };

    function u() {};
    var a = typeof console == 'undefined' ? u : function(t) {
            console.error(t)
        },
        e = ['paddingLeft', 'paddingRight', 'paddingTop', 'paddingBottom', 'marginLeft', 'marginRight', 'marginTop', 'marginBottom', 'borderLeftWidth', 'borderRightWidth', 'borderTopWidth', 'borderBottomWidth'],
        o = e.length;

    function h() {
        var i = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0
        };
        for (var t = 0; t < o; t++) {
            var n = e[t];
            i[n] = 0
        };
        return i
    };

    function r(t) {
        var e = getComputedStyle(t);
        if (!e) {
            a('Style returned ' + e + '. Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1')
        };
        return e
    };
    var i = !1,
        n;

    function f() {
        if (i) {
            return
        };
        i = !0;
        var e = document.createElement('div');
        e.style.width = '200px';
        e.style.padding = '1px 2px 3px 4px';
        e.style.borderStyle = 'solid';
        e.style.borderWidth = '1px 2px 3px 4px';
        e.style.boxSizing = 'border-box';
        var o = document.body || document.documentElement;
        o.appendChild(e);
        var a = r(e);
        s.isBoxSizeOuter = n = t(a.width) == 200;
        o.removeChild(e)
    };

    function s(i) {
        f();
        if (typeof i == 'string') {
            i = document.querySelector(i)
        };
        if (!i || typeof i != 'object' || !i.nodeType) {
            return
        };
        var a = r(i);
        if (a.display == 'none') {
            return h()
        };
        var s = {};
        s.width = i.offsetWidth;
        s.height = i.offsetHeight;
        var I = s.isBorderBox = a.boxSizing == 'border-box';
        for (var u = 0; u < o; u++) {
            var g = e[u],
                x = a[g],
                y = parseFloat(x);
            s[g] = !isNaN(y) ? y : 0
        };
        var v = s.paddingLeft + s.paddingRight,
            d = s.paddingTop + s.paddingBottom,
            z = s.marginLeft + s.marginRight,
            b = s.marginTop + s.marginBottom,
            l = s.borderLeftWidth + s.borderRightWidth,
            c = s.borderTopWidth + s.borderBottomWidth,
            E = I && n,
            p = t(a.width);
        if (p !== !1) {
            s.width = p + (E ? 0 : v + l)
        };
        var m = t(a.height);
        if (m !== !1) {
            s.height = m + (E ? 0 : d + c)
        };
        s.innerWidth = s.width - (v + l);
        s.innerHeight = s.height - (d + c);
        s.outerWidth = s.width + z;
        s.outerHeight = s.height + b;
        return s
    };
    return s
});
(function(t, e) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define('matches-selector/matches-selector', e)
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e()
    } else {
        t.matchesSelector = e()
    }
}(window, function() {
    'use strict';
    var t = (function() {
        var e = Element.prototype;
        if (e.matches) {
            return 'matches'
        };
        if (e.matchesSelector) {
            return 'matchesSelector'
        };
        var n = ['webkit', 'moz', 'ms', 'o'];
        for (var t = 0; t < n.length; t++) {
            var o = n[t],
                i = o + 'MatchesSelector';
            if (e[i]) {
                return i
            }
        }
    })();
    return function(e, i) {
        return e[t](i)
    }
}));
(function(t, e) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define('fizzy-ui-utils/utils', ['matches-selector/matches-selector'], function(i) {
            return e(t, i)
        })
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e(t, require('desandro-matches-selector'))
    } else {
        t.fizzyUIUtils = e(t, t.matchesSelector)
    }
}(window, function(t, e) {
    var i = {};
    i.extend = function(t, e) {
        for (var i in e) {
            t[i] = e[i]
        };
        return t
    };
    i.modulo = function(t, e) {
        return ((t % e) + e) % e
    };
    i.makeArray = function(t) {
        var e = [];
        if (Array.isArray(t)) {
            e = t
        } else if (t && typeof t.length == 'number') {
            for (var i = 0; i < t.length; i++) {
                e.push(t[i])
            }
        } else {
            e.push(t)
        };
        return e
    };
    i.removeFrom = function(t, e) {
        var i = t.indexOf(e);
        if (i != -1) {
            t.splice(i, 1)
        }
    };
    i.getParent = function(t, i) {
        while (t != document.body) {
            t = t.parentNode;
            if (e(t, i)) {
                return t
            }
        }
    };
    i.getQueryElement = function(t) {
        if (typeof t == 'string') {
            return document.querySelector(t)
        };
        return t
    };
    i.handleEvent = function(t) {
        var e = 'on' + t.type;
        if (this[e]) {
            this[e](t)
        }
    };
    i.filterFindElements = function(t, n) {
        t = i.makeArray(t);
        var o = [];
        t.forEach(function(t) {
            if (!(t instanceof HTMLElement)) {
                return
            };
            if (!n) {
                o.push(t);
                return
            };
            if (e(t, n)) {
                o.push(t)
            };
            var r = t.querySelectorAll(n);
            for (var i = 0; i < r.length; i++) {
                o.push(r[i])
            }
        });
        return o
    };
    i.debounceMethod = function(t, e, i) {
        var o = t.prototype[e],
            n = e + 'Timeout';
        t.prototype[e] = function() {
            var t = this[n];
            if (t) {
                clearTimeout(t)
            };
            var r = arguments,
                e = this;
            this[n] = setTimeout(function() {
                o.apply(e, r);
                delete e[n]
            }, i || 100)
        }
    };
    i.docReady = function(t) {
        if (document.readyState == 'complete') {
            t()
        } else {
            document.addEventListener('DOMContentLoaded', t)
        }
    };
    i.toDashed = function(t) {
        return t.replace(/(.)([A-Z])/g, function(t, e, i) {
            return e + '-' + i
        }).toLowerCase()
    };
    var n = t.console;
    i.htmlInit = function(e, o) {
        i.docReady(function() {
            var s = i.toDashed(o),
                r = 'data-' + s,
                u = document.querySelectorAll('[' + r + ']'),
                h = document.querySelectorAll('.js-' + s),
                f = i.makeArray(u).concat(i.makeArray(h)),
                d = r + '-options',
                a = t.jQuery;
            f.forEach(function(t) {
                var i = t.getAttribute(r) || t.getAttribute(d),
                    s;
                try {
                    s = i && JSON.parse(i)
                } catch (h) {
                    if (n) {
                        n.t$('Error parsing ' + r + ' on ' + t.className + ': ' + h)
                    };
                    return
                };
                var u = new e(t, s);
                if (a) {
                    a.data(t, o, u)
                }
            })
        })
    };
    return i
}));
(function(t, e) {
    if (typeof define == 'function' && define.amd) {
        define('outlayer/item', ['ev-emitter/ev-emitter', 'get-size/get-size'], function(i, n) {
            return e(t, i, n)
        })
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e(t, require('ev-emitter'), require('get-size'))
    } else {
        t.Outlayer = {};
        t.Outlayer.Item = e(t, t.EvEmitter, t.getSize)
    }
}(window, function(t, e, n) {
    'use strict';

    function c(t) {
        for (var e in t) {
            return !1
        };
        e = null;
        return !0
    };
    var a = document.documentElement.style,
        o = typeof a.transition == 'string' ? 'transition' : 'WebkitTransition',
        l = typeof a.transform == 'string' ? 'transform' : 'WebkitTransform',
        u = {
            WebkitTransition: 'webkitTransitionEnd',
            transition: 'transitionend'
        }[o];
    var s = [l, o, o + 'Duration', o + 'Property'];

    function r(t, e) {
        if (!t) {
            return
        };
        this.element = t;
        this.layout = e;
        this.position = {
            x: 0,
            y: 0
        };
        this._create()
    };
    var i = r.prototype = Object.create(e.prototype);
    i.constructor = r;
    i._create = function() {
        this._transn = {
            ingProperties: {},
            clean: {},
            onEnd: {}
        };
        this.css({
            position: 'absolute'
        })
    };
    i.handleEvent = function(t) {
        var e = 'on' + t.type;
        if (this[e]) {
            this[e](t)
        }
    };
    i.getSize = function() {
        this.size = n(this.element)
    };
    i.css = function(t) {
        var n = this.element.style;
        for (var e in t) {
            var i = s[e] || e;
            n[i] = t[e]
        }
    };
    i.getPosition = function() {
        var r = getComputedStyle(this.element),
            s = this.layout._getOption('originLeft'),
            a = this.layout._getOption('originTop'),
            n = r[s ? 'left' : 'right'],
            o = r[a ? 'top' : 'bottom'],
            t = this.layout.size,
            e = n.indexOf('%') != -1 ? (parseFloat(n) / 100) * t.width : parseInt(n, 10),
            i = o.indexOf('%') != -1 ? (parseFloat(o) / 100) * t.height : parseInt(o, 10);
        e = isNaN(e) ? 0 : e;
        i = isNaN(i) ? 0 : i;
        e -= s ? t.paddingLeft : t.paddingRight;
        i -= a ? t.paddingTop : t.paddingBottom;
        this.position.x = e;
        this.position.y = i
    };
    i.layoutPosition = function() {
        var n = this.layout.size,
            t = {};
        var e = this.layout._getOption('originLeft'),
            i = this.layout._getOption('originTop'),
            u = e ? 'paddingLeft' : 'paddingRight',
            h = e ? 'left' : 'right',
            f = e ? 'right' : 'left',
            d = this.position.x + n[u];
        t[h] = this.getXValue(d);
        t[f] = '';
        var o = i ? 'paddingTop' : 'paddingBottom',
            r = i ? 'top' : 'bottom',
            s = i ? 'bottom' : 'top',
            a = this.position.y + n[o];
        t[r] = this.getYValue(a);
        t[s] = '';
        this.css(t);
        this.emitEvent('layout', [this])
    };
    i.getXValue = function(t) {
        var e = this.layout._getOption('horizontal');
        return this.layout.options.percentPosition && !e ? ((t / this.layout.size.width) * 100) + '%' : t + 'px'
    };
    i.getYValue = function(t) {
        var e = this.layout._getOption('horizontal');
        return this.layout.options.percentPosition && e ? ((t / this.layout.size.height) * 100) + '%' : t + 'px'
    };
    i._transitionTo = function(t, e) {
        this.getPosition();
        var r = this.position.x,
            s = this.position.y,
            a = parseInt(t, 10),
            u = parseInt(e, 10),
            h = a === this.position.x && u === this.position.y;
        this.setPosition(t, e);
        if (h && !this.isTransitioning) {
            this.layoutPosition();
            return
        };
        var n = t - r,
            o = e - s,
            i = {};
        i.transform = this.getTranslate(n, o);
        this.transition({
            to: i,
            onTransitionEnd: {
                transform: this.layoutPosition
            },
            isCleaning: !0
        })
    };
    i.getTranslate = function(t, e) {
        var i = this.layout._getOption('originLeft'),
            n = this.layout._getOption('originTop');
        t = i ? t : -t;
        e = n ? e : -e;
        return 'translate3d(' + t + 'px, ' + e + 'px, 0)'
    };
    i.goTo = function(t, e) {
        this.setPosition(t, e);
        this.layoutPosition()
    };
    i.moveTo = i._transitionTo;
    i.setPosition = function(t, e) {
        this.position.x = parseInt(t, 10);
        this.position.y = parseInt(e, 10)
    };
    i._nonTransition = function(t) {
        this.css(t.to);
        if (t.isCleaning) {
            this._removeStyles(t.to)
        };
        for (var e in t.onTransitionEnd) {
            t.onTransitionEnd[e].call(this)
        }
    };
    i._transition = function(t) {
        if (!parseFloat(this.layout.options.transitionDuration)) {
            this._nonTransition(t);
            return
        };
        var i = this._transn;
        for (var e in t.onTransitionEnd) {
            i.onEnd[e] = t.onTransitionEnd[e]
        };
        for (e in t.to) {
            i.ingProperties[e] = !0;
            if (t.isCleaning) {
                i.clean[e] = !0
            }
        };
        if (t.from) {
            this.css(t.from);
            var n = this.element.offsetHeight;
            n = null
        };
        this.enableTransition(t.to);
        this.css(t.to);
        this.isTransitioning = !0
    };

    function m(t) {
        return t.replace(/([A-Z])/g, function(t) {
            return '-' + t.toLowerCase()
        })
    };
    var d = 'opacity,' + m(s.transform || 'transform');
    i.enableTransition = function() {
        if (this.isTransitioning) {
            return
        };
        this.css({
            transitionProperty: d,
            transitionDuration: this.layout.options.transitionDuration
        });
        this.element.addEventListener(u, this, !1)
    };
    i.transition = r.prototype[o ? '_transition' : '_nonTransition'];
    i.onwebkitTransitionEnd = function(t) {
        this.ontransitionend(t)
    };
    i.onotransitionend = function(t) {
        this.ontransitionend(t)
    };
    var f = {
        '-webkit-transform': 'transform'
    };
    i.ontransitionend = function(t) {
        if (t.target !== this.element) {
            return
        };
        var e = this._transn,
            i = f[t.propertyName] || t.propertyName;
        delete e.ingProperties[i];
        if (c(e.ingProperties)) {
            this.disableTransition()
        };
        if (i in e.clean) {
            this.element.style[t.propertyName] = '';
            delete e.clean[i]
        };
        if (i in e.onEnd) {
            var n = e.onEnd[i];
            n.call(this);
            delete e.onEnd[i]
        };
        this.emitEvent('transitionEnd', [this])
    };
    i.disableTransition = function() {
        this.removeTransitionStyles();
        this.element.removeEventListener(u, this, !1);
        this.isTransitioning = !1
    };
    i._removeStyles = function(t) {
        var e = {};
        for (var i in t) {
            e[i] = ''
        };
        this.css(e)
    };
    var h = {
        transitionProperty: '',
        transitionDuration: ''
    };
    i.removeTransitionStyles = function() {
        this.css(h)
    };
    i.removeElem = function() {
        this.element.parentNode.removeChild(this.element);
        this.css({
            display: ''
        });
        this.emitEvent('remove', [this])
    };
    i.remove = function() {
        if (!o || !parseFloat(this.layout.options.transitionDuration)) {
            this.removeElem();
            return
        };
        this.once('transitionEnd', function() {
            this.removeElem()
        });
        this.hide()
    };
    i.reveal = function() {
        delete this.isHidden;
        this.css({
            display: ''
        });
        var e = this.layout.options,
            t = {};
        var i = this.getHideRevealTransitionEndProperty('visibleStyle');
        t[i] = this.onRevealTransitionEnd;
        this.transition({
            from: e.hiddenStyle,
            to: e.visibleStyle,
            isCleaning: !0,
            onTransitionEnd: t
        })
    };
    i.onRevealTransitionEnd = function() {
        if (!this.isHidden) {
            this.emitEvent('reveal')
        }
    };
    i.getHideRevealTransitionEndProperty = function(t) {
        var e = this.layout.options[t];
        if (e.opacity) {
            return 'opacity'
        };
        for (var i in e) {
            return i
        }
    };
    i.hide = function() {
        this.isHidden = !0;
        this.css({
            display: ''
        });
        var e = this.layout.options,
            t = {};
        var i = this.getHideRevealTransitionEndProperty('hiddenStyle');
        t[i] = this.onHideTransitionEnd;
        this.transition({
            from: e.visibleStyle,
            to: e.hiddenStyle,
            isCleaning: !0,
            onTransitionEnd: t
        })
    };
    i.onHideTransitionEnd = function() {
        if (this.isHidden) {
            this.css({
                display: 'none'
            });
            this.emitEvent('hide')
        }
    };
    i.destroy = function() {
        this.css({
            position: '',
            left: '',
            right: '',
            top: '',
            bottom: '',
            transition: '',
            transform: ''
        })
    };
    return r
}));
(function(t, e) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define('outlayer/outlayer', ['ev-emitter/ev-emitter', 'get-size/get-size', 'fizzy-ui-utils/utils', './item'], function(i, n, o, r) {
            return e(t, i, n, o, r)
        })
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e(t, require('ev-emitter'), require('get-size'), require('fizzy-ui-utils'), require('./item'))
    } else {
        t.Outlayer = e(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
    }
}(window, function(t, i, r, n, a) {
    'use strict';
    var h = t.console,
        s = t.jQuery,
        f = function() {},
        l = 0,
        u = {};

    function o(t, e) {
        var i = n.getQueryElement(t);
        if (!i) {
            if (h) {
                h.error('Bad element for ' + this.constructor.namespace + ': ' + (i || t))
            };
            return
        };
        this.element = i;
        if (s) {
            this.$element = s(this.element)
        };
        this.options = n.extend({}, this.constructor.defaults);
        this.option(e);
        var o = ++l;
        this.element.outlayerGUID = o;
        u[o] = this;
        this._create();
        var r = this._getOption('initLayout');
        if (r) {
            this.layout()
        }
    };
    o.namespace = 'outlayer';
    o.Item = a;
    o.defaults = {
        containerStyle: {
            position: 'relative'
        },
        initLayout: !0,
        originLeft: !0,
        originTop: !0,
        resize: !0,
        resizeContainer: !0,
        transitionDuration: '0.4s',
        hiddenStyle: {
            opacity: 0,
            transform: 'scale(0.001)'
        },
        visibleStyle: {
            opacity: 1,
            transform: 'scale(1)'
        }
    };
    var e = o.prototype;
    n.extend(e, i.prototype);
    e.option = function(t) {
        n.extend(this.options, t)
    };
    e._getOption = function(t) {
        var e = this.constructor.compatOptions[t];
        return e && this.options[e] !== undefined ? this.options[e] : this.options[t]
    };
    o.compatOptions = {
        initLayout: 'isInitLayout',
        horizontal: 'isHorizontal',
        layoutInstant: 'isLayoutInstant',
        originLeft: 'isOriginLeft',
        originTop: 'isOriginTop',
        resize: 'isResizeBound',
        resizeContainer: 'isResizingContainer'
    };
    e._create = function() {
        this.reloadItems();
        this.stamps = [];
        this.stamp(this.options.stamp);
        n.extend(this.element.style, this.options.containerStyle);
        var t = this._getOption('resize');
        if (t) {
            this.bindResize()
        }
    };
    e.reloadItems = function() {
        this.items = this._itemize(this.element.children)
    };
    e._itemize = function(t) {
        var i = this._filterFindItemElements(t),
            s = this.constructor.Item,
            n = [];
        for (var e = 0; e < i.length; e++) {
            var o = i[e],
                r = new s(o, this);
            n.push(r)
        };
        return n
    };
    e._filterFindItemElements = function(t) {
        return n.filterFindElements(t, this.options.itemSelector)
    };
    e.getItemElements = function() {
        return this.items.map(function(t) {
            return t.element
        })
    };
    e.layout = function() {
        this._resetLayout();
        this._manageStamps();
        var t = this._getOption('layoutInstant'),
            e = t !== undefined ? t : !this._isLayoutInited;
        this.layoutItems(this.items, e);
        this._isLayoutInited = !0
    };
    e._init = e.layout;
    e._resetLayout = function() {
        this.getSize()
    };
    e.getSize = function() {
        this.size = r(this.element)
    };
    e._getMeasurement = function(t, e) {
        var i = this.options[t],
            n;
        if (!i) {
            this[t] = 0
        } else {
            if (typeof i == 'string') {
                n = this.element.querySelector(i)
            } else if (i instanceof HTMLElement) {
                n = i
            };
            this[t] = n ? r(n)[e] : i
        }
    };
    e.layoutItems = function(t, e) {
        t = this._getItemsForLayout(t);
        this._layoutItems(t, e);
        this._postLayout()
    };
    e._getItemsForLayout = function(t) {
        return t.filter(function(t) {
            return !t.isIgnored
        })
    };
    e._layoutItems = function(t, e) {
        this._emitCompleteOnItems('layout', t);
        if (!t || !t.length) {
            return
        };
        var i = [];
        t.forEach(function(t) {
            var n = this._getItemLayoutPosition(t);
            n.item = t;
            n.isInstant = e || t.isLayoutInstant;
            i.push(n)
        }, this);
        this._processLayoutQueue(i)
    };
    e._getItemLayoutPosition = function() {
        return {
            x: 0,
            y: 0
        }
    };
    e._processLayoutQueue = function(t) {
        t.forEach(function(t) {
            this._positionItem(t.item, t.x, t.y, t.isInstant)
        }, this)
    };
    e._positionItem = function(t, e, i, n) {
        if (n) {
            t.goTo(e, i)
        } else {
            t.moveTo(e, i)
        }
    };
    e._postLayout = function() {
        this.resizeContainer()
    };
    e.resizeContainer = function() {
        var e = this._getOption('resizeContainer');
        if (!e) {
            return
        };
        var t = this._getContainerSize();
        if (t) {
            this._setContainerMeasure(t.width, !0);
            this._setContainerMeasure(t.height, !1)
        }
    };
    e._getContainerSize = f;
    e._setContainerMeasure = function(t, e) {
        if (t === undefined) {
            return
        };
        var i = this.size;
        if (i.isBorderBox) {
            t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth
        };
        t = Math.max(t, 0);
        this.element.style[e ? 'width' : 'height'] = t + 'px'
    };
    e._emitCompleteOnItems = function(t, e) {
        var r = this;

        function o() {
            r.dispatchEvent(t + 'Complete', null, [e])
        };
        var n = e.length;
        if (!e || !n) {
            o();
            return
        };
        var i = 0;

        function s() {
            i++;
            if (i == n) {
                o()
            }
        };
        e.forEach(function(e) {
            e.once(t, s)
        })
    };
    e.dispatchEvent = function(t, e, i) {
        var o = e ? [e].concat(i) : i;
        this.emitEvent(t, o);
        if (s) {
            this.$element = this.$element || s(this.element);
            if (e) {
                var n = s.Event(e);
                n.type = t;
                this.$element.trigger(n, i)
            } else {
                this.$element.trigger(t, i)
            }
        }
    };
    e.ignore = function(t) {
        var e = this.getItem(t);
        if (e) {
            e.isIgnored = !0
        }
    };
    e.unignore = function(t) {
        var e = this.getItem(t);
        if (e) {
            delete e.isIgnored
        }
    };
    e.stamp = function(t) {
        t = this._find(t);
        if (!t) {
            return
        };
        this.stamps = this.stamps.concat(t);
        t.forEach(this.ignore, this)
    };
    e.unstamp = function(t) {
        t = this._find(t);
        if (!t) {
            return
        };
        t.forEach(function(t) {
            n.removeFrom(this.stamps, t);
            this.unignore(t)
        }, this)
    };
    e._find = function(t) {
        if (!t) {
            return
        };
        if (typeof t == 'string') {
            t = this.element.querySelectorAll(t)
        };
        t = n.makeArray(t);
        return t
    };
    e._manageStamps = function() {
        if (!this.stamps || !this.stamps.length) {
            return
        };
        this._getBoundingRect();
        this.stamps.forEach(this._manageStamp, this)
    };
    e._getBoundingRect = function() {
        var e = this.element.getBoundingClientRect(),
            t = this.size;
        this._boundingRect = {
            left: e.left + t.paddingLeft + t.borderLeftWidth,
            top: e.top + t.paddingTop + t.borderTopWidth,
            right: e.right - (t.paddingRight + t.borderRightWidth),
            bottom: e.bottom - (t.paddingBottom + t.borderBottomWidth)
        }
    };
    e._manageStamp = f;
    e._getElementOffset = function(t) {
        var e = t.getBoundingClientRect(),
            i = this._boundingRect,
            n = r(t),
            o = {
                left: e.left - i.left - n.marginLeft,
                top: e.top - i.top - n.marginTop,
                right: i.right - e.right - n.marginRight,
                bottom: i.bottom - e.bottom - n.marginBottom
            };
        return o
    };
    e.handleEvent = n.handleEvent;
    e.bindResize = function() {
        t.addEventListener('resize', this);
        this.isResizeBound = !0
    };
    e.unbindResize = function() {
        t.removeEventListener('resize', this);
        this.isResizeBound = !1
    };
    e.onresize = function() {
        this.resize()
    };
    n.debounceMethod(o, 'onresize', 100);
    e.resize = function() {
        if (!this.isResizeBound || !this.needsResizeLayout()) {
            return
        };
        this.layout()
    };
    e.needsResizeLayout = function() {
        var t = r(this.element),
            e = this.size && t;
        return e && t.innerWidth !== this.size.innerWidth
    };
    e.addItems = function(t) {
        var e = this._itemize(t);
        if (e.length) {
            this.items = this.items.concat(e)
        };
        return e
    };
    e.appended = function(t) {
        var e = this.addItems(t);
        if (!e.length) {
            return
        };
        this.layoutItems(e, !0);
        this.reveal(e)
    };
    e.prepended = function(t) {
        var e = this._itemize(t);
        if (!e.length) {
            return
        };
        var i = this.items.slice(0);
        this.items = e.concat(i);
        this._resetLayout();
        this._manageStamps();
        this.layoutItems(e, !0);
        this.reveal(e);
        this.layoutItems(i)
    };
    e.reveal = function(t) {
        this._emitCompleteOnItems('reveal', t);
        if (!t || !t.length) {
            return
        };
        t.forEach(function(t) {
            t.reveal()
        })
    };
    e.hide = function(t) {
        this._emitCompleteOnItems('hide', t);
        if (!t || !t.length) {
            return
        };
        t.forEach(function(t) {
            t.hide()
        })
    };
    e.revealItemElements = function(t) {
        var e = this.getItems(t);
        this.reveal(e)
    };
    e.hideItemElements = function(t) {
        var e = this.getItems(t);
        this.hide(e)
    };
    e.getItem = function(t) {
        for (var e = 0; e < this.items.length; e++) {
            var i = this.items[e];
            if (i.element == t) {
                return i
            }
        }
    };
    e.getItems = function(t) {
        t = n.makeArray(t);
        var e = [];
        t.forEach(function(t) {
            var i = this.getItem(t);
            if (i) {
                e.push(i)
            }
        }, this);
        return e
    };
    e.remove = function(t) {
        var e = this.getItems(t);
        this._emitCompleteOnItems('remove', e);
        if (!e || !e.length) {
            return
        };
        e.forEach(function(t) {
            t.remove();
            n.removeFrom(this.items, t)
        }, this)
    };
    e.destroy = function() {
        var t = this.element.style;
        t.height = '';
        t.position = '';
        t.width = '';
        this.items.forEach(function(t) {
            t.destroy()
        });
        this.unbindResize();
        var e = this.element.outlayerGUID;
        delete u[e];
        delete this.element.outlayerGUID;
        if (s) {
            s.removeData(this.element, this.constructor.namespace)
        }
    };
    o.data = function(t) {
        t = n.getQueryElement(t);
        var e = t && t.outlayerGUID;
        return e && u[e]
    };
    o.create = function(t, e) {
        var i = d(o);
        i.defaults = n.extend({}, o.defaults);
        n.extend(i.defaults, e);
        i.compatOptions = n.extend({}, o.compatOptions);
        i.namespace = t;
        i.data = o.data;
        i.Item = d(a);
        n.htmlInit(i, t);
        if (s && s.bridget) {
            s.bridget(t, i)
        };
        return i
    };

    function d(t) {
        function e() {
            t.apply(this, arguments)
        };
        e.prototype = Object.create(t.prototype);
        e.prototype.constructor = e;
        return e
    };
    o.Item = a;
    return o
}));
(function(t, e) {
    if (typeof define == 'function' && define.amd) {
        define(['outlayer/outlayer', 'get-size/get-size'], e)
    } else if (typeof module == 'object' && module.exports) {
        module.exports = e(require('outlayer'), require('get-size'))
    } else {
        t.Masonry = e(t.Outlayer, t.getSize)
    }
}(window, function(t, e) {
    var i = t.create('masonry');
    i.compatOptions.fitWidth = 'isFitWidth';
    i.prototype._resetLayout = function() {
        this.getSize();
        this._getMeasurement('columnWidth', 'outerWidth');
        this._getMeasurement('gutter', 'outerWidth');
        this.measureColumns();
        this.colYs = [];
        for (var t = 0; t < this.cols; t++) {
            this.colYs.push(0)
        };
        this.maxY = 0
    };
    i.prototype.measureColumns = function() {
        this.getContainerWidth();
        if (!this.columnWidth) {
            var r = this.items[0],
                s = r && r.element;
            this.columnWidth = s && e(s).outerWidth || this.containerWidth
        };
        var t = this.columnWidth += this.gutter,
            n = this.containerWidth + this.gutter,
            i = n / t,
            o = t - n % t,
            a = o && o < 1 ? 'round' : 'floor';
        i = Math[a](i);
        this.cols = Math.max(i, 1)
    };
    i.prototype.getContainerWidth = function() {
        var i = this._getOption('fitWidth'),
            n = i ? this.element.parentNode : this.element,
            t = e(n);
        this.containerWidth = t && t.innerWidth
    };
    i.prototype._getItemLayoutPosition = function(t) {
        t.getSize();
        var s = t.size.outerWidth % this.columnWidth,
            f = s && s < 1 ? 'round' : 'ceil',
            o = Math[f](t.size.outerWidth / this.columnWidth);
        o = Math.min(o, this.cols);
        var i = this._getColGroup(o),
            n = Math.min.apply(Math, i),
            r = i.indexOf(n),
            h = {
                x: this.columnWidth * r,
                y: n
            };
        var a = n + t.size.outerHeight,
            u = this.cols + 1 - i.length;
        for (var e = 0; e < u; e++) {
            this.colYs[r + e] = a
        };
        return h
    };
    i.prototype._getColGroup = function(t) {
        if (t < 2) {
            return this.colYs
        };
        var i = [],
            o = this.cols + 1 - t;
        for (var e = 0; e < o; e++) {
            var n = this.colYs.slice(e, e + t);
            i[e] = Math.max.apply(Math, n)
        };
        return i
    };
    i.prototype._manageStamp = function(t) {
        var s = e(t),
            o = this._getElementOffset(t),
            d = this._getOption('originLeft'),
            a = d ? o.left : o.right,
            u = a + s.outerWidth,
            r = Math.floor(a / this.columnWidth);
        r = Math.max(0, r);
        var n = Math.floor(u / this.columnWidth);
        n -= u % this.columnWidth ? 0 : 1;
        n = Math.min(this.cols - 1, n);
        var h = this._getOption('originTop'),
            f = (h ? o.top : o.bottom) + s.outerHeight;
        for (var i = r; i <= n; i++) {
            this.colYs[i] = Math.max(f, this.colYs[i])
        }
    };
    i.prototype._getContainerSize = function() {
        this.maxY = Math.max.apply(Math, this.colYs);
        var t = {
            height: this.maxY
        };
        if (this._getOption('fitWidth')) {
            t.width = this._getContainerFitWidth()
        };
        return t
    };
    i.prototype._getContainerFitWidth = function() {
        var t = 0,
            e = this.cols;
        while (--e) {
            if (this.colYs[e] !== 0) {
                break
            };
            t++
        };
        return (this.cols - t) * this.columnWidth - this.gutter
    };
    i.prototype.needsResizeLayout = function() {
        var t = this.containerWidth;
        this.getContainerWidth();
        return t != this.containerWidth
    };
    return i
}));;
(function(e, t) {
    'use strict';
    if (typeof define == 'function' && define.amd) {
        define(['ev-emitter/ev-emitter'], function(i) {
            return t(e, i)
        })
    } else if (typeof module == 'object' && module.exports) {
        module.exports = t(e, require('ev-emitter'))
    } else {
        e.imagesLoaded = t(e, e.EvEmitter)
    }
})(window, function(e, i) {
    'use strict';
    var n = e.jQuery,
        s = e.console;

    function h(e, t) {
        for (var i in t) {
            e[i] = t[i]
        };
        return e
    };

    function m(e) {
        var t = [];
        if (Array.isArray(e)) {
            t = e
        } else if (typeof e.length == 'number') {
            for (var i = 0; i < e.length; i++) {
                t.push(e[i])
            }
        } else {
            t.push(e)
        };
        return t
    };

    function t(e, i, r) {
        if (!(this instanceof t)) {
            return new t(e, i, r)
        };
        if (typeof e == 'string') {
            e = document.querySelectorAll(e)
        };
        this.elements = m(e);
        this.options = h({}, this.options);
        if (typeof i == 'function') {
            r = i
        } else {
            h(this.options, i)
        };
        if (r) {
            this.on('always', r)
        };
        this.getImages();
        if (n) {
            this.jqDeferred = new n.Deferred()
        };
        setTimeout(function() {
            this.check()
        }.bind(this))
    };
    t.prototype = Object.create(i.prototype);
    t.prototype.options = {};
    t.prototype.getImages = function() {
        this.images = [];
        this.elements.forEach(this.addElementImages, this)
    };
    t.prototype.addElementImages = function(e) {
        if (e.nodeName == 'IMG') {
            this.addImage(e)
        };
        if (this.options.background === !0) {
            this.addElementBackgroundImages(e)
        };
        var n = e.nodeType;
        if (!n || !a[n]) {
            return
        };
        var r = e.querySelectorAll('img');
        for (var t = 0; t < r.length; t++) {
            var s = r[t];
            this.addImage(s)
        };
        if (typeof this.options.background == 'string') {
            var i = e.querySelectorAll(this.options.background);
            for (t = 0; t < i.length; t++) {
                var o = i[t];
                this.addElementBackgroundImages(o)
            }
        }
    };
    var a = {
        1: !0,
        9: !0,
        11: !0
    };
    t.prototype.addElementBackgroundImages = function(e) {
        var i = getComputedStyle(e);
        if (!i) {
            return
        };
        var n = /url\((['"])?(.*?)\1\)/gi,
            t = n.exec(i.backgroundImage);
        while (t !== null) {
            var r = t && t[2];
            if (r) {
                this.addBackground(r, e)
            };
            t = n.exec(i.backgroundImage)
        }
    };
    t.prototype.addImage = function(e) {
        var t = new r(e);
        this.images.push(t)
    };
    t.prototype.addBackground = function(e, t) {
        var i = new o(e, t);
        this.images.push(i)
    };
    t.prototype.check = function() {
        var e = this;
        this.progressedCount = 0;
        this.hasAnyBroken = !1;
        if (!this.images.length) {
            this.complete();
            return
        };

        function t(t, i, r) {
            setTimeout(function() {
                e.progress(t, i, r)
            })
        };
        this.images.forEach(function(e) {
            e.once('progress', t);
            e.check()
        })
    };
    t.prototype.progress = function(e, t, i) {
        this.progressedCount++;
        this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded;
        this.emitEvent('progress', [this, e, t]);
        if (this.jqDeferred && this.jqDeferred.notify) {
            this.jqDeferred.notify(this, e)
        };
        if (this.progressedCount == this.images.length) {
            this.complete()
        };
        if (this.options.debug && s) {
            s.log('progress: ' + i, e, t)
        }
    };
    t.prototype.complete = function() {
        var t = this.hasAnyBroken ? 'fail' : 'done';
        this.isComplete = !0;
        this.emitEvent(t, [this]);
        this.emitEvent('always', [this]);
        if (this.jqDeferred) {
            var e = this.hasAnyBroken ? 'reject' : 'resolve';
            this.jqDeferred[e](this)
        }
    };

    function r(e) {
        this.img = e
    };
    r.prototype = Object.create(i.prototype);
    r.prototype.check = function() {
        var e = this.getIsImageComplete();
        if (e) {
            this.confirm(this.img.naturalWidth !== 0, 'naturalWidth');
            return
        };
        this.proxyImage = new Image();
        this.proxyImage.addEventListener('load', this);
        this.proxyImage.addEventListener('error', this);
        this.img.addEventListener('load', this);
        this.img.addEventListener('error', this);
        this.proxyImage.src = this.img.src
    };
    r.prototype.getIsImageComplete = function() {
        return this.img.complete && this.img.naturalWidth !== undefined
    };
    r.prototype.confirm = function(e, t) {
        this.isLoaded = e;
        this.emitEvent('progress', [this, this.img, t])
    };
    r.prototype.handleEvent = function(e) {
        var t = 'on' + e.type;
        if (this[t]) {
            this[t](e)
        }
    };
    r.prototype.onload = function() {
        this.confirm(!0, 'onload');
        this.unbindEvents()
    };
    r.prototype.onerror = function() {
        this.confirm(!1, 'onerror');
        this.unbindEvents()
    };
    r.prototype.unbindEvents = function() {
        this.proxyImage.removeEventListener('load', this);
        this.proxyImage.removeEventListener('error', this);
        this.img.removeEventListener('load', this);
        this.img.removeEventListener('error', this)
    };

    function o(e, t) {
        this.url = e;
        this.element = t;
        this.img = new Image()
    };
    o.prototype = Object.create(r.prototype);
    o.prototype.check = function() {
        this.img.addEventListener('load', this);
        this.img.addEventListener('error', this);
        this.img.src = this.url;
        var e = this.getIsImageComplete();
        if (e) {
            this.confirm(this.img.naturalWidth !== 0, 'naturalWidth');
            this.unbindEvents()
        }
    };
    o.prototype.unbindEvents = function() {
        this.img.removeEventListener('load', this);
        this.img.removeEventListener('error', this)
    };
    o.prototype.confirm = function(e, t) {
        this.isLoaded = e;
        this.emitEvent('progress', [this, this.element, t])
    };
    t.makeJQueryPlugin = function(i) {
        i = i || e.jQuery;
        if (!i) {
            return
        };
        n = i;
        n.fn.imagesLoaded = function(e, i) {
            var r = new t(this, e, i);
            return r.jqDeferred.promise(n(this))
        }
    };
    t.makeJQueryPlugin();
    return t
});;
(function(t) {
    t.fn.centerImage = function(i, e) {
        e = e || function() {};
        var o = this,
            r = t(this).length;
        i = i == 'inside';
        var n = function(e) {
                var n = t(e),
                    o = n.parent();
                o.css({
                    overflow: 'hidden',
                    position: o.css('position') == 'absolute' ? 'absolute' : 'relative'
                });
                n.css({
                    'position': 'static',
                    'width': 'auto',
                    'height': 'auto',
                    'max-width': '100%',
                    'max-height': '100%'
                });
                var r = {
                    w: o.width(),
                    h: o.height(),
                    r: o.width() / o.height()
                };
                var e = {
                    w: n.width(),
                    h: n.height(),
                    r: n.width() / n.height()
                };
                n.css({
                    'max-width': 'none',
                    'max-height': 'none',
                    'width': Math.round((r.r > e.r) ^ i ? '100%' : r.h / e.h * e.w),
                    'height': Math.round((r.r < e.r) ^ i ? '100%' : r.w / e.w * e.h)
                });
                var r = {
                    w: o.width(),
                    h: o.height()
                };
                var e = {
                    w: n.width(),
                    h: n.height()
                };
                n.css({
                    'position': 'absolute',
                    'left': Math.round((r.w - e.w) / 2),
                    'top': Math.round((r.h - e.h) / 3)
                });
                h(e)
            },
            h = function(t) {
                r--;
                e.apply(o, [t, r])
            };
        return o.each(function(i) {
            if (this.complete || this.readyState === 'complete') {
                (function(t) {
                    setTimeout(function() {
                        n(t)
                    }, 1)
                })(this)
            } else {
                (function(i) {
                    t(i).one('load', function() {
                        setTimeout(function() {
                            n(i)
                        }, 1)
                    }).one('error', function() {
                        h(i)
                    }).end();
                    if (navigator.userAgent.indexOf('Trident/5') >= 0 || navigator.userAgent.indexOf('Trident/6')) {
                        i.src = i.src
                    }
                })(this)
            }
        })
    };
    t.fn.imageCenterResize = function(i) {
        return t(this).centerImage('inside', i)
    };
    t.fn.imageCropFill = function(i) {
        return t(this).centerImage('outside', i)
    }
})(jQuery);;
(function(e) {
    e.fn.labelauty = function(i) {
        var n = e.extend({
            development: !1,
            class: 'labelauty',
            label: !0,
            separator: '|',
            checked_label: 'Checked',
            unchecked_label: 'Unchecked',
            force_random_id: !1,
            minimum_width: !1,
            same_width: !0
        }, i);
        return this.each(function() {
            var i = e(this),
                p = i.is(':checked'),
                k = i.attr('type'),
                o = !0,
                h, r, s, m = i.attr('aria-label');
            i.attr('aria-hidden', !0);
            if (i.is(':checkbox') === !1 && i.is(':radio') === !1) return this;
            i.addClass(n.class);
            h = i.attr('data-labelauty');
            o = n.label;
            if (o === !0) {
                if (h == null || h.length === 0) {
                    r = [];
                    r[0] = n.unchecked_label;
                    r[1] = n.checked_label
                } else {
                    r = h.split(n.separator);
                    if (r.length > 2) {
                        o = !1;
                        a(n.development, 'There\'s more than two labels. LABELAUTY will not use labels.')
                    } else {
                        if (r.length === 1) a(n.development, 'There\'s just one label. LABELAUTY will use this one for both cases.')
                    }
                }
            };
            i.css({
                display: 'none'
            });
            i.removeAttr('data-labelauty');
            s = i.attr('id');
            if (n.force_random_id || s == null || s.trim() === '') {
                var u = 1 + Math.floor(Math.random() * 1024000);
                s = 'labelauty-' + u;
                while (e(s).length !== 0) {
                    u++;
                    s = 'labelauty-' + u;
                    a(n.development, 'Holy crap, between 1024 thousand numbers, one raised a conflict. Trying again.')
                };
                i.attr('id', s)
            };
            var c = jQuery(t(s, m, p, k, r, o, i));
            c.click(function(a) {
                var l = e(c).width() / 2 >= a.pageX - e(this).offset().left;
                if ((l != i.is(':checked') && i.hasClass('checkbox-switch')) || !i.hasClass('checkbox-switch')) {
                    if (i.is(':checked')) {
                        e(c).attr('aria-checked', !1)
                    } else {
                        e(c).attr('aria-checked', !0)
                    }
                } else {
                    a.preventDefault()
                }
            });
            c.keypress(function(a) {
                a.preventDefault();
                if (a.keyCode === 32 || a.keyCode === 13) {
                    if (i.is(':checked')) {
                        i.prop('checked', !1);
                        e(c).attr('aria-checked', !1)
                    } else {
                        i.prop('checked', !0);
                        e(c).attr('aria-checked', !0)
                    }
                }
            });
            i.after(c);
            if (n.minimum_width !== !1) i.next('label[for=' + s + ']').css({
                'min-width': n.minimum_width
            });
            if (n.same_width != !1 && n.label == !0) {
                var d = i.next('label[for=' + s + ']'),
                    f = l(d.find('span.labelauty-unchecked')),
                    b = l(d.find('span.labelauty-checked'));
                if (f > b) d.find('span.labelauty-checked').width(f);
                else d.find('span.labelauty-unchecked').width(b)
            }
        })
    };

    function l(e) {
        var l = 0,
            a = e,
            t = 'position: absolute !important; top: -1000 !important; ';
        a = a.clone().attr('style', t).appendTo('body');
        l = a.width(!0);
        a.remove();
        return l
    };

    function a(e, a) {
        if (e && window.console && window.console.log) window.console.log('jQuery-LABELAUTY: ' + a)
    };

    function t(e, i, d, o, a, h, r) {
        var c, l, t, n = '';
        if (a == null) l = t = '';
        else {
            l = a[0];
            if (a[1] == null) t = l;
            else t = a[1]
        };
        var u = r.attr('tabindex'),
            s = '';
        if (u) {
            s = 'tabindex="' + r.attr('tabindex') + '"'
        };
        if (i == null) n = '';
        else n = ' role="' + o + '" aria-checked="' + d + '" aria-label="' + i + '"';
        if (h == !0) {
            c = '<label for="' + e + '" ' + n + s + '><span class="labelauty-unchecked-image"></span><span class="labelauty-unchecked">' + l + '</span><span class="labelauty-checked-image"></span><span class="labelauty-checked">' + t + '</span></label>'
        } else {
            c = '<label for="' + e + '" ' + n + s + '><span class="labelauty-unchecked-image"></span><span class="labelauty-checked-image"></span></label>'
        };
        return c
    }
}(jQuery));;
(function() {
    var t, r, n, i, o, e = function(t, e) {
            return function() {
                return t.apply(e, arguments)
            }
        },
        s = [].indexOf || function(t) {
            for (var e = 0, n = this.length; e < n; e++) {
                if (e in this && this[e] === t) return e
            };
            return -1
        };
    r = (function() {
        function t() {};
        t.prototype.extend = function(t, e) {
            var n, i;
            for (n in e) {
                i = e[n];
                if (t[n] == null) {
                    t[n] = i
                }
            };
            return t
        };
        t.prototype.isMobile = function(t) {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(t)
        };
        t.prototype.createEvent = function(t, e, n, o) {
            var i;
            if (e == null) {
                e = !1
            };
            if (n == null) {
                n = !1
            };
            if (o == null) {
                o = null
            };
            if (document.createEvent != null) {
                i = document.createEvent('CustomEvent');
                i.initCustomEvent(t, e, n, o)
            } else if (document.createEventObject != null) {
                i = document.createEventObject();
                i.eventType = t
            } else {
                i.eventName = t
            };
            return i
        };
        t.prototype.emitEvent = function(t, e) {
            if (t.dispatchEvent != null) {
                return t.dispatchEvent(e)
            } else if (e in (t != null)) {
                return t[e]()
            } else if (('on' + e) in (t != null)) {
                return t['on' + e]()
            }
        };
        t.prototype.addEvent = function(t, e, n) {
            if (t.addEventListener != null) {
                return t.addEventListener(e, n, !1)
            } else if (t.attachEvent != null) {
                return t.attachEvent('on' + e, n)
            } else {
                return t[e] = n
            }
        };
        t.prototype.removeEvent = function(t, e, n) {
            if (t.removeEventListener != null) {
                return t.removeEventListener(e, n, !1)
            } else if (t.detachEvent != null) {
                return t.detachEvent('on' + e, n)
            } else {
                return delete t[e]
            }
        };
        t.prototype.innerHeight = function() {
            if ('innerHeight' in window) {
                return window.innerHeight
            } else {
                return document.documentElement.clientHeight
            }
        };
        return t
    })();
    n = this.WeakMap || this.MozWeakMap || (n = (function() {
        function t() {
            this.keys = [];
            this.values = []
        };
        t.prototype.get = function(t) {
            var e, o, n, r, i;
            i = this.keys;
            for (e = n = 0, r = i.length; n < r; e = ++n) {
                o = i[e];
                if (o === t) {
                    return this.values[e]
                }
            }
        };
        t.prototype.set = function(t, e) {
            var n, r, i, s, o;
            o = this.keys;
            for (n = i = 0, s = o.length; i < s; n = ++i) {
                r = o[n];
                if (r === t) {
                    this.values[n] = e;
                    return
                }
            };
            this.keys.push(t);
            return this.values.push(e)
        };
        return t
    })());
    t = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (t = (function() {
        function t() {
            if (typeof console !== 'undefined' && console !== null) {
                console.warn('MutationObserver is not supported by your browser.')
            };
            if (typeof console !== 'undefined' && console !== null) {
                console.warn('WOW.js cannot detect dom mutations, please call .sync() after loading new content.')
            }
        };
        t.notSupported = !0;
        t.prototype.observe = function() {};
        return t
    })());
    i = this.getComputedStyle || function(t, e) {
        this.getPropertyValue = function(e) {
            var n;
            if (e === 'float') {
                e = 'styleFloat'
            };
            if (o.test(e)) {
                e.replace(o, function(t, e) {
                    return e.toUpperCase()
                })
            };
            return ((n = t.currentStyle) != null ? n[e] : void 0) || null
        };
        return this
    };
    o = /(\-([a-z]){1})/g;
    this.WOW = (function() {
        o.prototype.defaults = {
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null,
            scrollContainer: null
        };

        function o(t) {
            if (t == null) {
                t = {}
            };
            this.scrollCallback = e(this.scrollCallback, this);
            this.scrollHandler = e(this.scrollHandler, this);
            this.resetAnimation = e(this.resetAnimation, this);
            this.start = e(this.start, this);
            this.scrolled = !0;
            this.config = this.util().extend(t, this.defaults);
            if (t.scrollContainer != null) {
                this.config.scrollContainer = document.querySelector(t.scrollContainer)
            };
            this.animationNameCache = new n();
            this.wowEvent = this.util().createEvent(this.config.boxClass)
        };
        o.prototype.init = function() {
            var t;
            this.element = window.document.documentElement;
            if ((t = document.readyState) === 'interactive' || t === 'complete') {
                this.start()
            } else {
                this.util().addEvent(document, 'DOMContentLoaded', this.start)
            };
            return this.finished = []
        };
        o.prototype.start = function() {
            var e, n, o, i;
            this.stopped = !1;
            this.boxes = (function() {
                var t, o, n, i;
                n = this.element.querySelectorAll('.' + this.config.boxClass);
                i = [];
                for (t = 0, o = n.length; t < o; t++) {
                    e = n[t];
                    i.push(e)
                };
                return i
            }).call(this);
            this.all = (function() {
                var t, o, n, i;
                n = this.boxes;
                i = [];
                for (t = 0, o = n.length; t < o; t++) {
                    e = n[t];
                    i.push(e)
                };
                return i
            }).call(this);
            if (this.boxes.length) {
                if (this.disabled()) {
                    this.resetStyle()
                } else {
                    i = this.boxes;
                    for (n = 0, o = i.length; n < o; n++) {
                        e = i[n];
                        this.applyStyle(e, !0)
                    }
                }
            };
            if (!this.disabled()) {
                this.util().addEvent(this.config.scrollContainer || window, 'scroll', this.scrollHandler);
                this.util().addEvent(window, 'resize', this.scrollHandler);
                this.interval = setInterval(this.scrollCallback, 50)
            };
            if (this.config.live) {
                return new t((function(t) {
                    return function(e) {
                        var n, o, r, s, i;
                        i = [];
                        for (n = 0, o = e.length; n < o; n++) {
                            s = e[n];
                            i.push((function() {
                                var t, i, e, n;
                                e = s.addedNodes || [];
                                n = [];
                                for (t = 0, i = e.length; t < i; t++) {
                                    r = e[t];
                                    n.push(this.doSync(r))
                                };
                                return n
                            }).call(t))
                        };
                        return i
                    }
                })(this)).observe(document.body, {
                    childList: !0,
                    subtree: !0
                })
            }
        };
        o.prototype.stop = function() {
            this.stopped = !0;
            this.util().removeEvent(this.config.scrollContainer || window, 'scroll', this.scrollHandler);
            this.util().removeEvent(window, 'resize', this.scrollHandler);
            if (this.interval != null) {
                return clearInterval(this.interval)
            }
        };
        o.prototype.sync = function(e) {
            if (t.notSupported) {
                return this.doSync(this.element)
            }
        };
        o.prototype.doSync = function(t) {
            var e, n, r, o, i;
            if (t == null) {
                t = this.element
            };
            if (t.nodeType !== 1) {
                return
            };
            t = t.parentNode || t;
            o = t.querySelectorAll('.' + this.config.boxClass);
            i = [];
            for (n = 0, r = o.length; n < r; n++) {
                e = o[n];
                if (s.call(this.all, e) < 0) {
                    this.boxes.push(e);
                    this.all.push(e);
                    if (this.stopped || this.disabled()) {
                        this.resetStyle()
                    } else {
                        this.applyStyle(e, !0)
                    };
                    i.push(this.scrolled = !0)
                } else {
                    i.push(void 0)
                }
            };
            return i
        };
        o.prototype.show = function(t) {
            this.applyStyle(t);
            t.className = t.className + ' ' + this.config.animateClass;
            if (this.config.callback != null) {
                this.config.callback(t)
            };
            this.util().emitEvent(t, this.wowEvent);
            this.util().addEvent(t, 'animationend', this.resetAnimation);
            this.util().addEvent(t, 'oanimationend', this.resetAnimation);
            this.util().addEvent(t, 'webkitAnimationEnd', this.resetAnimation);
            this.util().addEvent(t, 'MSAnimationEnd', this.resetAnimation);
            return t
        };
        o.prototype.applyStyle = function(t, e) {
            var n, i, o;
            i = t.getAttribute('data-wow-duration');
            n = t.getAttribute('data-wow-delay');
            o = t.getAttribute('data-wow-iteration');
            return this.animate((function(r) {
                return function() {
                    return r.customStyle(t, e, i, n, o)
                }
            })(this))
        };
        o.prototype.animate = (function() {
            if ('requestAnimationFrame' in window) {
                return function(t) {
                    return window.requestAnimationFrame(t)
                }
            } else {
                return function(t) {
                    return t()
                }
            }
        })();
        o.prototype.resetStyle = function() {
            var i, t, o, e, n;
            e = this.boxes;
            n = [];
            for (t = 0, o = e.length; t < o; t++) {
                i = e[t];
                n.push(i.style.visibility = 'visible')
            };
            return n
        };
        o.prototype.resetAnimation = function(t) {
            var e;
            if (t.type.toLowerCase().indexOf('animationend') >= 0) {
                e = t.target || t.srcElement;
                return e.className = e.className.replace(this.config.animateClass, '').trim()
            }
        };
        o.prototype.customStyle = function(t, e, n, i, o) {
            if (e) {
                this.cacheAnimationName(t)
            };
            t.style.visibility = e ? 'hidden' : 'visible';
            if (n) {
                this.vendorSet(t.style, {
                    animationDuration: n
                })
            };
            if (i) {
                this.vendorSet(t.style, {
                    animationDelay: i
                })
            };
            if (o) {
                this.vendorSet(t.style, {
                    animationIterationCount: o
                })
            };
            this.vendorSet(t.style, {
                animationName: e ? 'none' : this.cachedAnimationName(t)
            });
            return t
        };
        o.prototype.vendors = ['moz', 'webkit'];
        o.prototype.vendorSet = function(t, e) {
            var n, i, o, r;
            i = [];
            for (n in e) {
                o = e[n];
                t['' + n] = o;
                i.push((function() {
                    var e, l, i, s;
                    i = this.vendors;
                    s = [];
                    for (e = 0, l = i.length; e < l; e++) {
                        r = i[e];
                        s.push(t['' + r + (n.charAt(0).toUpperCase()) + (n.substr(1))] = o)
                    };
                    return s
                }).call(this))
            };
            return i
        };
        o.prototype.vendorCSS = function(t, e) {
            var n, l, r, o, s, a;
            s = i(t);
            o = s.getPropertyCSSValue(e);
            r = this.vendors;
            for (n = 0, l = r.length; n < l; n++) {
                a = r[n];
                o = o || s.getPropertyCSSValue('-' + a + '-' + e)
            };
            return o
        };
        o.prototype.animationName = function(t) {
            var e, n;
            try {
                e = this.vendorCSS(t, 'animation-name').cssText
            } catch (o) {
                e = i(t).getPropertyValue('animation-name')
            };
            if (e === 'none') {
                return ''
            } else {
                return e
            }
        };
        o.prototype.cacheAnimationName = function(t) {
            return this.animationNameCache.set(t, this.animationName(t))
        };
        o.prototype.cachedAnimationName = function(t) {
            return this.animationNameCache.get(t)
        };
        o.prototype.scrollHandler = function() {
            return this.scrolled = !0
        };
        o.prototype.scrollCallback = function() {
            var t;
            if (this.scrolled) {
                this.scrolled = !1;
                this.boxes = (function() {
                    var e, o, n, i;
                    n = this.boxes;
                    i = [];
                    for (e = 0, o = n.length; e < o; e++) {
                        t = n[e];
                        if (!(t)) {
                            continue
                        };
                        if (this.isVisible(t)) {
                            this.show(t);
                            continue
                        };
                        i.push(t)
                    };
                    return i
                }).call(this);
                if (!(this.boxes.length || this.config.live)) {
                    return this.stop()
                }
            }
        };
        o.prototype.offsetTop = function(t) {
            var e;
            while (t.offsetTop === void 0) {
                t = t.parentNode
            };
            e = t.offsetTop;
            while (t = t.offsetParent) {
                e += t.offsetTop
            };
            return e
        };
        o.prototype.isVisible = function(t) {
            var i, o, e, r, n;
            o = t.getAttribute('data-wow-offset') || this.config.offset;
            n = (this.config.scrollContainer && this.config.scrollContainer.scrollTop) || window.pageYOffset;
            r = n + Math.min(this.element.clientHeight, this.util().innerHeight()) - o;
            e = this.offsetTop(t);
            i = e + t.clientHeight;
            return e <= r && i >= n
        };
        o.prototype.util = function() {
            return this._util != null ? this._util : this._util = new r()
        };
        o.prototype.disabled = function() {
            return !this.config.mobile && this.util().isMobile(navigator.userAgent)
        };
        return o
    })()
}).call(this);;
(function(e) {
    var l = navigator.platform,
        t = {
            tabPause: 800,
            focusChange: null,
            iOS: (l === 'iPad' || l === 'iPhone' || l === 'iPod'),
            firefox: (typeof InstallTrigger !== 'undefined'),
            ie11: !(window.ActiveXObject) && 'ActiveXObject' in window
        };
    var a = function(t, a) {
            if (a === null || typeof a === 'undefined') {
                return
            };
            for (var r in a) {
                e(t).data('autotab-' + r, a[r])
            }
        },
        r = function(t) {
            var r = {
                arrowKey: !1,
                format: 'all',
                loaded: !1,
                disabled: !1,
                pattern: null,
                uppercase: !1,
                lowercase: !1,
                nospace: !1,
                maxlength: 2147483647,
                target: null,
                previous: null,
                trigger: null,
                originalValue: '',
                changed: !1,
                editable: (t.type === 'text' || t.type === 'password' || t.type === 'textarea' || t.type === 'tel' || t.type === 'number' || t.type === 'email' || t.type === 'search' || t.type === 'url'),
                filterable: (t.type === 'text' || t.type === 'password' || t.type === 'textarea'),
                tabOnSelect: !1
            };
            if (e.autotab.selectFilterByClass === !0 && typeof e(t).data('autotab-format') === 'undefined') {
                var n = ['all', 'text', 'alpha', 'number', 'numeric', 'alphanumeric', 'hex', 'hexadecimal', 'custom'];
                for (var i in n) {
                    if (e(t).hasClass(n[i])) {
                        r.format = n[i];
                        break
                    }
                }
            };
            for (var i in r) {
                if (typeof e(t).data('autotab-' + i) !== 'undefined') {
                    r[i] = e(t).data('autotab-' + i)
                }
            };
            if (!r.loaded) {
                if (r.trigger !== null && typeof r.trigger === 'string') {
                    r.trigger = r.trigger.toString()
                };
                a(t, r)
            };
            return r
        },
        i = function(e) {
            return (typeof e !== 'undefined' && (typeof e === 'string' || !(e instanceof jQuery)))
        },
        s = function(e) {
            var t = 0,
                a = 0,
                r = 0;
            if (e.type === 'text' || e.type === 'password' || e.type === 'textarea') {
                if (typeof e.selectionStart === 'number' && typeof e.selectionEnd === 'number') {
                    t = e.selectionStart;
                    a = e.selectionEnd;
                    r = 1
                } else if (document.selection && document.selection.createRange) {
                    var i = document.selection.createRange(),
                        n = e.createTextRange(),
                        o = e.createTextRange(),
                        l = i.getBookmark();
                    n.moveToBookmark(l);
                    o.setEndPoint('EndToStart', n);
                    t = o.text.length;
                    a = t + i.text.length;
                    r = 2
                }
            };
            return {
                start: t,
                end: a,
                selectionType: r
            }
        };
    e.autotab = function(t) {
        if (typeof t !== 'object') {
            t = {}
        };
        e(':input').autotab(t)
    };
    e.autotab.selectFilterByClass = !1;
    e.autotab.next = function() {
        var t = e(document.activeElement);
        if (t.length) {
            t.trigger('autotab-next')
        }
    };
    e.autotab.previous = function() {
        var t = e(document.activeElement);
        if (t.length) {
            t.trigger('autotab-previous')
        }
    };
    e.autotab.remove = function(t) {
        i(t) ? e(t).autotab('remove') : e(':input').autotab('remove')
    };
    e.autotab.restore = function(t) {
        i(t) ? e(t).autotab('restore') : e(':input').autotab('restore')
    };
    e.autotab.refresh = function(t) {
        i(t) ? e(t).autotab('refresh') : e(':input').autotab('refresh')
    };
    e.fn.autotab = function(o, u) {
        if (!this.length) {
            return this
        };
        var s = e.grep(this, function(e, t) {
            return e.type != 'hidden'
        });
        if (o == 'filter') {
            if (typeof u === 'string' || typeof u === 'function') {
                u = {
                    format: u
                }
            };
            for (var l = 0, g = s.length; l < g; l++) {
                var t = r(s[l]),
                    f = u;
                f.target = t.target;
                f.previous = t.previous;
                e.extend(t, f);
                if (!t.loaded) {
                    t.disabled = !0;
                    n(s[l], f)
                } else {
                    a(s[l], t)
                }
            }
        } else if (o == 'remove' || o == 'destroy' || o == 'disable') {
            for (var l = 0, g = s.length; l < g; l++) {
                var t = r(s[l]);
                t.disabled = !0;
                a(s[l], t)
            }
        } else if (o == 'restore' || o == 'enable') {
            for (var l = 0, g = s.length; l < g; l++) {
                var t = r(s[l]);
                t.disabled = !1;
                a(s[l], t)
            }
        } else if (o == 'refresh') {
            for (var l = 0, g = s.length; l < g; l++) {
                var t = r(s[l]),
                    h = l + 1,
                    c = l - 1,
                    p = function() {
                        if (l > 0 && h < g) {
                            t.target = s[h]
                        } else if (l > 0) {
                            t.target = null
                        } else {
                            t.target = s[h]
                        }
                    },
                    v = function() {
                        if (l > 0 && h < g) {
                            t.previous = s[c]
                        } else if (l > 0) {
                            t.previous = s[c]
                        } else {
                            t.previous = null
                        }
                    };
                if (t.target === null || t.target.selector === '') {
                    p()
                } else if (typeof t.target === 'string' || t.target.selector) {
                    t.target = e(typeof t.target === 'string' ? t.target : t.target.selector);
                    if (t.target.length === 0) {
                        p()
                    }
                };
                if (t.previous === null || t.previous.selector === '') {
                    v()
                } else if (typeof t.previous === 'string' || t.previous.selector) {
                    t.previous = e(typeof t.previous === 'string' ? t.previous : t.previous.selector);
                    if (t.previous.length === 0) {
                        v()
                    }
                };
                if (!t.loaded) {
                    n(s[l], t)
                } else {
                    if (i(t.target)) {
                        t.target = e(t.target)
                    };
                    if (i(t.previous)) {
                        t.previous = e(t.previous)
                    };
                    a(s[l], t)
                }
            }
        } else {
            if (o === null || typeof o === 'undefined') {
                u = {}
            } else if (typeof o === 'string' || typeof o === 'function') {
                u = {
                    format: o
                }
            } else if (typeof o === 'object') {
                u = o
            };
            if (s.length > 1) {
                for (var l = 0, g = s.length; l < g; l++) {
                    var h = l + 1,
                        c = l - 1,
                        f = u;
                    if (l > 0 && h < g) {
                        f.target = s[h];
                        f.previous = s[c]
                    } else if (l > 0) {
                        f.target = null;
                        f.previous = s[c]
                    } else {
                        f.target = s[h];
                        f.previous = null
                    };
                    n(s[l], f)
                }
            } else {
                n(s[0], u)
            }
        };
        return this
    };
    var o = function(e, t, r) {
            if (typeof r.format === 'function') {
                return r.format(t, e)
            };
            var a = null;
            switch (r.format) {
                case 'text':
                    a = new RegExp('[0-9]+', 'g');
                    break;
                case 'alpha':
                    a = new RegExp('[^a-zA-Z]+', 'g');
                    break;
                case 'number':
                case 'numeric':
                    a = new RegExp('[^0-9]+', 'g');
                    break;
                case 'alphanumeric':
                    a = new RegExp('[^0-9a-zA-Z]+', 'g');
                    break;
                case 'hex':
                case 'hexadecimal':
                    a = new RegExp('[^0-9A-Fa-f]+', 'g');
                    break;
                case 'custom':
                    a = new RegExp(r.pattern, 'g');
                    break;
                case 'all':
                default:
                    break
            };
            if (a !== null) {
                t = t.replace(a, '')
            };
            if (r.nospace) {
                a = new RegExp('[ ]+', 'g');
                t = t.replace(a, '')
            };
            if (r.uppercase) {
                t = t.toUpperCase()
            };
            if (r.lowercase) {
                t = t.toLowerCase()
            };
            return t
        },
        n = function(n, u) {
            var l = r(n);
            if (l.disabled) {
                l.disabled = !1;
                l.target = null;
                l.previous = null
            };
            e.extend(l, u);
            if (i(l.target)) {
                l.target = e(l.target)
            };
            if (i(l.previous)) {
                l.previous = e(l.previous)
            };
            var f = n.maxLength;
            if (typeof n.maxLength === 'undefined' && n.type == 'textarea') {
                f = n.maxLength = n.getAttribute('maxlength')
            };
            if (l.maxlength == 2147483647 && f != 2147483647 && f != -1) {
                l.maxlength = f
            } else if (l.maxlength > 0) {
                n.maxLength = l.maxlength
            } else {
                l.target = null
            };
            if (!l.loaded) {
                l.loaded = !0;
                a(n, l)
            } else {
                a(n, l);
                return
            };
            if (n.type == 'select-one') {
                e(n).on('change', function(t) {
                    var a = r(this);
                    if (a.tabOnSelect) {
                        e(this).trigger('autotab-next')
                    }
                })
            };
            e(n).on('autotab-next', function(e, a) {
                var i = this;
                setTimeout(function() {
                    if (!a) {
                        a = r(i)
                    };
                    var e = a.target;
                    if (!a.disabled && e.length) {
                        if (!t.iOS) {
                            if (e.prop('disabled') || e.prop('readonly')) {
                                e.trigger('autotab-next')
                            } else {
                                if (a.arrowKey) {
                                    e.focus()
                                } else {
                                    e.focus().select()
                                }
                            };
                            t.focusChange = new Date()
                        }
                    }
                }, 1)
            }).on('autotab-previous', function(e, i) {
                var n = this;
                setTimeout(function() {
                    if (!i) {
                        i = r(n)
                    };
                    var e = i.previous;
                    if (!i.disabled && e.length) {
                        var o = e.val();
                        if (e.prop('disabled') || e.prop('readonly')) {
                            e.trigger('autotab-previous')
                        } else if (o.length && e.data('autotab-editable') && !i.arrowKey) {
                            if (t.ie11) {
                                e.val(o.substring(0, o.length - 1)).focus()
                            } else {
                                e.focus().val(o.substring(0, o.length - 1))
                            };
                            a(e, {
                                changed: !0
                            })
                        } else {
                            if (i.arrowKey) {
                                a(this, {
                                    arrowKey: !1
                                })
                            };
                            if (t.ie11) {
                                e.val(o).focus()
                            } else {
                                e.focus().val(o)
                            }
                        };
                        t.focusChange = null
                    }
                }, 1)
            }).on('focus', function() {
                a(this, {
                    originalValue: this.value
                })
            }).on('blur', function() {
                var t = r(this);
                if (t.changed && this.value != t.originalValue) {
                    a(this, {
                        changed: !1
                    });
                    e(this).change()
                }
            }).on('keydown.autotab', function(i) {
                var n = r(this);
                if (!n || n.disabled) {
                    return !0
                };
                var l = s(this),
                    o = i.which || i.charCode;
                if (o == 8) {
                    n.arrowKey = !1;
                    if (!n.editable) {
                        e(this).trigger('autotab-previous', n);
                        return !1
                    };
                    a(this, {
                        changed: (this.value !== n.originalValue)
                    });
                    if (this.value.length === 0) {
                        e(this).trigger('autotab-previous', n);
                        return
                    }
                } else if (o == 9 && t.focusChange !== null) {
                    if (i.shiftKey) {
                        t.focusChange = null;
                        return
                    };
                    if ((new Date().getTime() - t.focusChange.getTime()) < t.tabPause) {
                        t.focusChange = null;
                        return !1
                    }
                } else if (this.type !== 'range' && this.type !== 'select-one' && this.type !== 'select-multiple') {
                    if ((this.type !== 'tel' && this.type !== 'number') || ((this.type === 'tel' || this.type === 'number') && this.value.length == 0)) {
                        if (o == 37 && (!n.editable || l.start == 0)) {
                            n.arrowKey = !0;
                            e(this).trigger('autotab-previous', n)
                        } else if (o == 39 && (!n.editable || !n.filterable || l.end == this.value.length || this.value.length == 0)) {
                            n.arrowKey = !0;
                            e(this).trigger('autotab-next', n)
                        }
                    }
                }
            }).on('keypress.autotab', function(i) {
                var n = r(this),
                    g = i.which || i.keyCode;
                if (!n || n.disabled || (t.firefox && i.charCode === 0) || i.ctrlKey || i.altKey || g == 13 || this.disabled) {
                    return !0
                };
                var u = String.fromCharCode(g);
                if (this.type != 'text' && this.type != 'password' && this.type != 'textarea') {
                    if ((this.value.length + 1) >= n.maxlength) {
                        n.arrowKey = !1;
                        e(this).trigger('autotab-next', n)
                    };
                    return !(this.value.length == n.maxlength)
                };
                if (n.trigger !== null && n.trigger.indexOf(u) >= 0) {
                    if (t.focusChange !== null && (new Date().getTime() - t.focusChange.getTime()) < t.tabPause) {
                        t.focusChange = null
                    } else {
                        n.arrowKey = !1;
                        e(this).trigger('autotab-next', n)
                    };
                    return !1
                };
                t.focusChange = null;
                var h = document.selection && document.selection.createRange ? !0 : (g > 0);
                u = o(this, u, n);
                if (h && (u === null || u === '')) {
                    return !1
                };
                if (h && (this.value.length <= this.maxLength)) {
                    var l = s(this);
                    if (l.start === 0 && l.end == this.value.length) {
                        this.value = u;
                        a(this, {
                            changed: (this.value != n.originalValue)
                        })
                    } else {
                        if (this.value.length == this.maxLength && l.start === l.end) {
                            n.arrowKey = !1;
                            e(this).trigger('autotab-next', n);
                            return !1
                        };
                        this.value = this.value.slice(0, l.start) + u + this.value.slice(l.end);
                        a(this, {
                            changed: (this.value != n.originalValue)
                        })
                    };
                    if (this.value.length != n.maxlength) {
                        l.start++;
                        if (l.selectionType == 1) {
                            this.selectionStart = this.selectionEnd = l.start
                        } else if (l.selectionType == 2) {
                            var f = this.createTextRange();
                            f.collapse(!0);
                            f.moveEnd('character', l.start);
                            f.moveStart('character', l.start);
                            f.select()
                        }
                    }
                };
                if (this.value.length == n.maxlength) {
                    n.arrowKey = !1;
                    e(this).trigger('autotab-next', n)
                };
                return !1
            }).on('drop paste', function(a) {
                var i = r(this);
                if (!i) {
                    return !0
                };
                this.maxLength = 2147483647;
                (function(a, n) {
                    setTimeout(function() {
                        var u = -1,
                            l = document.createElement('input');
                        l.type = 'hidden';
                        l.value = a.value.toLowerCase();
                        l.originalValue = a.value;
                        a.value = o(a, a.value, n).substr(0, n.maxlength);
                        var s = function(a, i) {
                            if (!a) {
                                return
                            };
                            var n = r(a);
                            if (e(a).prop('disabled') || e(a).prop('readonly') || !n.editable) {
                                e(a).trigger('autotab-next');
                                if (!t.iOS) {
                                    s(n.target[0], i)
                                };
                                return
                            };
                            for (var g = 0, c = i.length; g < c; g++) {
                                u = l.value.indexOf(i.charAt(g).toLowerCase(), u) + 1
                            };
                            var h = l.originalValue.substr(u),
                                f = o(a, h, n).substr(0, n.maxlength);
                            if (!f) {
                                return
                            };
                            a.value = f;
                            if (f.length == n.maxlength) {
                                n.arrowKey = !1;
                                e(a).trigger('autotab-next', n);
                                if (t.firefox) {
                                    setTimeout(function() {
                                        a.selectionStart = a.value.length
                                    }, 1)
                                };
                                if (!t.iOS) {
                                    s(n.target[0], f)
                                }
                            }
                        };
                        if (a.value.length == n.maxlength) {
                            i.arrowKey = !1;
                            e(a).trigger('autotab-next', i);
                            if (!t.iOS) {
                                s(n.target[0], a.value.toLowerCase())
                            }
                        };
                        a.maxLength = n.maxlength
                    }, 1)
                })(this, i)
            })
        };
    e.fn.autotab_magic = function(t) {
        return e(this).autotab()
    };
    e.fn.autotab_filter = function(t) {
        var a = {};
        if (typeof t === 'string' || typeof t === 'function') {
            a.format = t
        } else {
            e.extend(a, t)
        };
        return e(this).autotab('filter', a)
    }
})(jQuery);;
(function(t) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], t)
    } else if (typeof module !== 'undefined' && module.exports) {
        module.exports = t(require('jquery'))
    } else {
        t(jQuery)
    }
})(function(t) {
    var a = -1,
        n = -1,
        i = function(t) {
            return parseFloat(t) || 0
        },
        s = function(e) {
            var o = 1,
                a = t(e),
                r = null,
                n = [];
            a.each(function() {
                var e = t(this),
                    a = e.offset().top - i(e.css('margin-top')),
                    s = n.length > 0 ? n[n.length - 1] : null;
                if (s === null) {
                    n.push(e)
                } else {
                    if (Math.floor(Math.abs(r - a)) <= o) {
                        n[n.length - 1] = s.add(e)
                    } else {
                        n.push(e)
                    }
                };
                r = a
            });
            return n
        },
        r = function(e) {
            var i = {
                byRow: !0,
                property: 'height',
                target: null,
                remove: !1
            };
            if (typeof e === 'object') {
                return t.extend(i, e)
            };
            if (typeof e === 'boolean') {
                i.byRow = e
            } else if (e === 'remove') {
                i.remove = !0
            };
            return i
        },
        e = t.fn.matchHeight = function(i) {
            var n = r(i);
            if (n.remove) {
                var o = this;
                this.css(n.property, '');
                t.each(e._groups, function(t, e) {
                    e.elements = e.elements.not(o)
                });
                return this
            };
            if (this.length <= 1 && !n.target) {
                return this
            };
            e._groups.push({
                elements: this,
                options: n
            });
            e._apply(this, n);
            return this
        };
    e.version = '0.7.0';
    e._groups = [];
    e._throttle = 80;
    e._maintainScroll = !1;
    e._beforeUpdate = null;
    e._afterUpdate = null;
    e._rows = s;
    e._parse = i;
    e._parseOptions = r;
    e._apply = function(n, l) {
        var o = r(l),
            a = t(n),
            c = [a],
            f = t(window).scrollTop(),
            p = t('html').outerHeight(!0),
            h = a.parents().filter(':hidden');
        h.each(function() {
            var e = t(this);
            e.data('style-cache', e.attr('style'))
        });
        h.css('display', 'block');
        if (o.byRow && !o.target) {
            a.each(function() {
                var i = t(this),
                    e = i.css('display');
                if (e !== 'inline-block' && e !== 'flex' && e !== 'inline-flex') {
                    e = 'block'
                };
                i.data('style-cache', i.attr('style'));
                i.css({
                    'display': e,
                    'padding-top': '0',
                    'padding-bottom': '0',
                    'margin-top': '0',
                    'margin-bottom': '0',
                    'border-top-width': '0',
                    'border-bottom-width': '0',
                    'height': '100px',
                    'overflow': 'hidden'
                })
            });
            c = s(a);
            a.each(function() {
                var e = t(this);
                e.attr('style', e.data('style-cache') || '')
            })
        };
        t.each(c, function(e, n) {
            var r = t(n),
                a = 0;
            if (!o.target) {
                if (o.byRow && r.length <= 1) {
                    r.css(o.property, '');
                    return
                };
                r.each(function() {
                    var e = t(this),
                        r = e.attr('style'),
                        i = e.css('display');
                    if (i !== 'inline-block' && i !== 'flex' && i !== 'inline-flex') {
                        i = 'block'
                    };
                    var n = {
                        'display': i
                    };
                    n[o.property] = '';
                    e.css(n);
                    if (e.outerHeight(!1) > a) {
                        a = e.outerHeight(!1)
                    };
                    if (r) {
                        e.attr('style', r)
                    } else {
                        e.css('display', '')
                    }
                })
            } else {
                a = o.target.outerHeight(!1)
            };
            r.each(function() {
                var e = t(this),
                    n = 0;
                if (o.target && e.is(o.target)) {
                    return
                };
                if (e.css('box-sizing') !== 'border-box') {
                    n += i(e.css('border-top-width')) + i(e.css('border-bottom-width'));
                    n += i(e.css('padding-top')) + i(e.css('padding-bottom'))
                };
                e.css(o.property, (a - n) + 'px')
            })
        });
        h.each(function() {
            var e = t(this);
            e.attr('style', e.data('style-cache') || null)
        });
        if (e._maintainScroll) {
            t(window).scrollTop((f / p) * t('html').outerHeight(!0))
        };
        return this
    };
    e._applyDataApi = function() {
        var e = {};
        t('[data-match-height], [data-mh]').each(function() {
            var i = t(this),
                n = i.attr('data-mh') || i.attr('data-match-height');
            if (n in e) {
                e[n] = e[n].add(i)
            } else {
                e[n] = i
            }
        });
        t.each(e, function() {
            this.matchHeight(!0)
        })
    };
    var o = function(i) {
        if (e._beforeUpdate) {
            e._beforeUpdate(i, e._groups)
        };
        t.each(e._groups, function() {
            e._apply(this.elements, this.options)
        });
        if (e._afterUpdate) {
            e._afterUpdate(i, e._groups)
        }
    };
    e._update = function(i, r) {
        if (r && r.type === 'resize') {
            var s = t(window).width();
            if (s === a) {
                return
            };
            a = s
        };
        if (!i) {
            o(r)
        } else if (n === -1) {
            n = setTimeout(function() {
                o(r);
                n = -1
            }, e._throttle)
        }
    };
    t(e._applyDataApi);
    t(window).bind('load', function(t) {
        e._update(!1, t)
    });
    t(window).bind('resize orientationchange', function(t) {
        e._update(!0, t)
    })
});;
'use strict';
(function(t, e, a) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], t)
    } else if (typeof exports === 'object') {
        module.exports = t(require('jquery'))
    } else {
        t(e || a)
    }
}(function(t) {
    var n = function(e, n, s) {
        var a = {
            invalid: [],
            getCaret: function() {
                try {
                    var t, n = 0,
                        l = e.get(0),
                        r = document.selection,
                        s = l.selectionStart;
                    if (r && navigator.appVersion.indexOf('MSIE 10') === -1) {
                        t = r.createRange();
                        t.moveStart('character', -a.val().length);
                        n = t.text.length
                    } else if (s || s === '0') {
                        n = s
                    };
                    return n
                } catch (i) {}
            },
            setCaret: function(t) {
                try {
                    if (e.is(':focus')) {
                        var a, n = e.get(0);
                        if (n.setSelectionRange) {
                            n.setSelectionRange(t, t)
                        } else {
                            a = n.createTextRange();
                            a.collapse(!0);
                            a.moveEnd('character', t);
                            a.moveStart('character', t);
                            a.select()
                        }
                    }
                } catch (s) {}
            },
            events: function() {
                e.on('keydown.mask', function(t) {
                    e.data('mask-keycode', t.keyCode || t.which);
                    e.data('mask-previus-value', e.val())
                }).on(t.jMaskGlobals.useInput ? 'input.mask' : 'keyup.mask', a.behaviour).on('paste.mask drop.mask', function() {
                    setTimeout(function() {
                        e.keydown().keyup()
                    }, 100)
                }).on('change.mask', function() {
                    e.data('changed', !0)
                }).on('blur.mask', function() {
                    if (i !== a.val() && !e.data('changed')) {
                        e.trigger('change')
                    };
                    e.data('changed', !1)
                }).on('blur.mask', function() {
                    i = a.val()
                }).on('focus.mask', function(e) {
                    if (s.selectOnFocus === !0) {
                        t(e.target).select()
                    }
                }).on('focusout.mask', function() {
                    if (s.clearIfNotMatch && !l.test(a.val())) {
                        a.val('')
                    }
                })
            },
            getRegexMask: function() {
                var s = [],
                    a, i, c, o, t, l;
                for (var e = 0; e < n.length; e++) {
                    a = r.translation[n.charAt(e)];
                    if (a) {
                        i = a.pattern.toString().replace(/.{1}$|^.{1}/g, '');
                        c = a.optional;
                        o = a.recursive;
                        if (o) {
                            s.push(n.charAt(e));
                            t = {
                                digit: n.charAt(e),
                                pattern: i
                            }
                        } else {
                            s.push(!c && !o ? i : (i + '?'))
                        }
                    } else {
                        s.push(n.charAt(e).replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'))
                    }
                };
                l = s.join('');
                if (t) {
                    l = l.replace(new RegExp('(' + t.digit + '(.*' + t.digit + ')?)'), '($1)?').replace(new RegExp(t.digit, 'g'), t.pattern)
                };
                return new RegExp(l)
            },
            destroyEvents: function() {
                e.off(['input', 'keydown', 'keyup', 'paste', 'drop', 'blur', 'focusout', ''].join('.mask '))
            },
            val: function(t) {
                var s = e.is('input'),
                    a = s ? 'val' : 'text',
                    n;
                if (arguments.length > 0) {
                    if (e[a]() !== t) {
                        e[a](t)
                    };
                    n = e
                } else {
                    n = e[a]()
                };
                return n
            },
            calculateCaretPosition: function(t, a) {
                var s = a.length,
                    n = e.data('mask-previus-value'),
                    r = n.length;
                if (e.data('mask-keycode') === 8 && n !== a) {
                    t = t - (a.slice(0, t).length - n.slice(0, t).length)
                } else if (n !== a) {
                    if (t >= r) {
                        t = s
                    } else {
                        t = t + (a.slice(0, t).length - n.slice(0, t).length)
                    }
                };
                return t
            },
            behaviour: function(n) {
                n = n || window.event;
                a.invalid = [];
                var l = e.data('mask-keycode');
                if (t.inArray(l, r.byPassKeys) === -1) {
                    var s = a.getMasked(),
                        i = a.getCaret();
                    setTimeout(function(t, e) {
                        a.setCaret(a.calculateCaretPosition(t, e))
                    }, 10, i, s);
                    a.val(s);
                    a.setCaret(i);
                    return a.callbacks(n)
                }
            },
            getMasked: function(t, l) {
                var f = [],
                    b = l === undefined ? a.val() : l + '',
                    e = 0,
                    v = n.length,
                    o = 0,
                    m = b.length,
                    i = 1,
                    k = 'push',
                    p = -1,
                    h, y;
                if (s.reverse) {
                    k = 'unshift';
                    i = -1;
                    h = 0;
                    e = v - 1;
                    o = m - 1;
                    y = function() {
                        return e > -1 && o > -1
                    }
                } else {
                    h = v - 1;
                    y = function() {
                        return e < v && o < m
                    }
                };
                var g;
                while (y()) {
                    var d = n.charAt(e),
                        u = b.charAt(o),
                        c = r.translation[d];
                    if (c) {
                        if (u.match(c.pattern)) {
                            f[k](u);
                            if (c.recursive) {
                                if (p === -1) {
                                    p = e
                                } else if (e === h) {
                                    e = p - i
                                };
                                if (h === p) {
                                    e -= i
                                }
                            };
                            e += i
                        } else if (u === g) {
                            g = undefined
                        } else if (c.optional) {
                            e += i;
                            o -= i
                        } else if (c.fallback) {
                            f[k](c.fallback);
                            e += i;
                            o -= i
                        } else {
                            a.invalid.push({
                                p: o,
                                v: u,
                                e: c.pattern
                            })
                        };
                        o += i
                    } else {
                        if (!t) {
                            f[k](d)
                        };
                        if (u === d) {
                            o += i
                        } else {
                            g = d
                        };
                        e += i
                    }
                };
                var M = n.charAt(h);
                if (v === m + 1 && !r.translation[M]) {
                    f.push(M)
                };
                return f.join('')
            },
            callbacks: function(t) {
                var r = a.val(),
                    c = r !== i,
                    o = [r, t, e, s],
                    l = function(t, e, a) {
                        if (typeof s[t] === 'function' && e) {
                            s[t].apply(this, a)
                        }
                    };
                l('onChange', c === !0, o);
                l('onKeyPress', c === !0, o);
                l('onComplete', r.length === n.length, o);
                l('onInvalid', a.invalid.length > 0, [r, t, e, a.invalid, s])
            }
        };
        e = t(e);
        var r = this,
            i = a.val(),
            l;
        n = typeof n === 'function' ? n(a.val(), undefined, e, s) : n;
        r.mask = n;
        r.options = s;
        r.remove = function() {
            var t = a.getCaret();
            a.destroyEvents();
            a.val(r.getCleanVal());
            a.setCaret(t);
            return e
        };
        r.getCleanVal = function() {
            return a.getMasked(!0)
        };
        r.getMaskedVal = function(t) {
            return a.getMasked(!1, t)
        };
        r.init = function(i) {
            i = i || !1;
            s = s || {};
            r.clearIfNotMatch = t.jMaskGlobals.clearIfNotMatch;
            r.byPassKeys = t.jMaskGlobals.byPassKeys;
            r.translation = t.extend({}, t.jMaskGlobals.translation, s.translation);
            r = t.extend(!0, {}, r, s);
            l = a.getRegexMask();
            if (i) {
                a.events();
                a.val(a.getMasked())
            } else {
                if (s.placeholder) {
                    e.attr('placeholder', s.placeholder)
                };
                if (e.data('mask')) {
                    e.attr('autocomplete', 'off')
                };
                for (var o = 0, u = !0; o < n.length; o++) {
                    var c = r.translation[n.charAt(o)];
                    if (c && c.recursive) {
                        u = !1;
                        break
                    }
                };
                if (u) {
                    e.attr('maxlength', n.length)
                };
                a.destroyEvents();
                a.events();
                var f = a.getCaret();
                a.val(a.getMasked());
                a.setCaret(f)
            }
        };
        r.init(!e.is('input'))
    };
    t.maskWatchers = {};
    var s = function() {
            var e = t(this),
                s = {},
                r = 'data-mask-',
                i = e.attr('data-mask');
            if (e.attr(r + 'reverse')) {
                s.reverse = !0
            };
            if (e.attr(r + 'clearifnotmatch')) {
                s.clearIfNotMatch = !0
            };
            if (e.attr(r + 'selectonfocus') === 'true') {
                s.selectOnFocus = !0
            };
            if (a(e, i, s)) {
                return e.data('mask', new n(this, i, s))
            }
        },
        a = function(e, a, n) {
            n = n || {};
            var s = t(e).data('mask'),
                r = JSON.stringify,
                l = t(e).val() || t(e).text();
            try {
                if (typeof a === 'function') {
                    a = a(l)
                };
                return typeof s !== 'object' || r(s.options) !== r(n) || s.mask !== a
            } catch (i) {}
        },
        r = function(t) {
            var e = document.createElement('div'),
                a;
            t = 'on' + t;
            a = (t in e);
            if (!a) {
                e.setAttribute(t, 'return;');
                a = typeof e[t] === 'function'
            };
            e = null;
            return a
        };
    t.fn.mask = function(e, s) {
        s = s || {};
        var r = this.selector,
            i = t.jMaskGlobals,
            o = i.watchInterval,
            c = s.watchInputs || i.watchInputs,
            l = function() {
                if (a(this, e, s)) {
                    return t(this).data('mask', new n(this, e, s))
                }
            };
        t(this).each(l);
        if (r && r !== '' && c) {
            clearInterval(t.maskWatchers[r]);
            t.maskWatchers[r] = setInterval(function() {
                t(document).find(r).each(l)
            }, o)
        };
        return this
    };
    t.fn.masked = function(t) {
        return this.data('mask').getMaskedVal(t)
    };
    t.fn.unmask = function() {
        clearInterval(t.maskWatchers[this.selector]);
        delete t.maskWatchers[this.selector];
        return this.each(function() {
            var e = t(this).data('mask');
            if (e) {
                e.remove().removeData('mask')
            }
        })
    };
    t.fn.cleanVal = function() {
        return this.data('mask').getCleanVal()
    };
    t.applyDataMask = function(e) {
        e = e || t.jMaskGlobals.maskElements;
        var a = (e instanceof t) ? e : t(e);
        a.filter(t.jMaskGlobals.dataMaskAttr).each(s)
    };
    var e = {
        maskElements: 'input,td,span,div',
        dataMaskAttr: '*[data-mask]',
        dataMask: !0,
        watchInterval: 300,
        watchInputs: !0,
        useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && r('input'),
        watchDataMask: !1,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            '0': {
                pattern: /\d/
            },
            '9': {
                pattern: /\d/,
                optional: !0
            },
            '#': {
                pattern: /\d/,
                recursive: !0
            },
            'A': {
                pattern: /[a-zA-Z0-9]/
            },
            'S': {
                pattern: /[a-zA-Z]/
            }
        }
    };
    t.jMaskGlobals = t.jMaskGlobals || {};
    e = t.jMaskGlobals = t.extend(!0, {}, e, t.jMaskGlobals);
    if (e.dataMask) {
        t.applyDataMask()
    };
    setInterval(function() {
        if (t.jMaskGlobals.watchDataMask) {
            t.applyDataMask()
        }
    }, e.watchInterval)
}, window.jQuery, window.Zepto));;
(function(e, t, n) {
    'use strict';
    (function o(e, t, n) {
        function r(a, i) {
            if (!t[a]) {
                if (!e[a]) {
                    var c = typeof require == 'function' && require;
                    if (!i && c) return c(a, !0);
                    if (s) return s(a, !0);
                    var u = new Error('Cannot find module \'' + a + '\'');
                    throw u.code = 'MODULE_NOT_FOUND', u
                };
                var l = t[a] = {
                    exports: {}
                };
                e[a][0].call(l.exports, function(t) {
                    var n = e[a][1][t];
                    return r(n ? n : t)
                }, l, l.exports, o, e, t, n)
            };
            return t[a].exports
        };
        var s = typeof require == 'function' && require;
        for (var a = 0; a < n.length; a++) r(n[a]);
        return r
    })({
        1: [function(o, a, s) {
            'use strict';
            var p = function(e) {
                return e && e.__esModule ? e : {
                    'default': e
                }
            };
            Object.defineProperty(s, '__esModule', {
                value: !0
            });
            var r = o('./modules/handle-dom'),
                c = o('./modules/utils'),
                i = o('./modules/handle-swal-dom'),
                g = o('./modules/handle-click'),
                b = o('./modules/handle-key'),
                h = p(b),
                w = o('./modules/default-params'),
                d = p(w),
                v = o('./modules/set-params'),
                y = p(v),
                m, f, l, u;
            s['default'] = l = u = function() {
                var a = arguments[0];
                r.addClass(t.body, 'stop-scrolling');
                i.resetInput();

                function S(e) {
                    var t = a;
                    return t[e] === n ? d['default'][e] : t[e]
                };
                if (a === n) {
                    c.logStr('SweetAlert expects at least 1 attribute!');
                    return !1
                };
                var o = c.extend({}, d['default']);
                switch (typeof a) {
                    case 'string':
                        o.title = a;
                        o.text = arguments[1] || '';
                        o.type = arguments[2] || '';
                        break;
                    case 'object':
                        if (a.title === n) {
                            c.logStr('Missing "title" argument!');
                            return !1
                        };
                        o.title = a.title;
                        for (var C in d['default']) {
                            o[C] = S(C)
                        };
                        o.confirmButtonText = o.showCancelButton ? 'Confirm' : d['default'].confirmButtonText;
                        o.confirmButtonText = S('confirmButtonText');
                        o.doneFunction = arguments[1] || null;
                        break;
                    default:
                        c.logStr('Unexpected type of argument! Expected "string" or "object", got ' + typeof a);
                        return !1
                };
                y['default'](o);
                i.fixVerticalPosition();
                i.openModal(arguments[1]);
                var p = i.getModal(),
                    v = p.querySelectorAll('button'),
                    b = ['onclick', 'onmouseover', 'onmouseout', 'onmousedown', 'onmouseup', 'onfocus'],
                    w = function(e) {
                        return g.handleButton(e, o, p)
                    };
                for (var l = 0; l < v.length; l++) {
                    for (var s = 0; s < b.length; s++) {
                        var k = b[s];
                        v[l][k] = w
                    }
                };
                i.getOverlay().onclick = w;
                m = e.onkeydown;
                var x = function(e) {
                    return h['default'](e, o, p)
                };
                e.onkeydown = x;
                e.onfocus = function() {
                    setTimeout(function() {
                        if (f !== n) {
                            f.focus();
                            f = n
                        }
                    }, 0)
                };
                u.enableButtons()
            };
            l.setDefaults = u.setDefaults = function(e) {
                if (!e) {
                    throw new Error('userParams is required')
                };
                if (typeof e !== 'object') {
                    throw new Error('userParams has to be a object')
                };
                c.extend(d['default'], e)
            };
            l.close = u.close = function() {
                var o = i.getModal();
                r.fadeOut(i.getOverlay(), 5);
                r.fadeOut(o, 5);
                r.removeClass(o, 'showSweetAlert');
                r.addClass(o, 'hideSweetAlert');
                r.removeClass(o, 'visible');
                var s = o.querySelector('.sa-icon.sa-success');
                r.removeClass(s, 'animate');
                r.removeClass(s.querySelector('.sa-tip'), 'animateSuccessTip');
                r.removeClass(s.querySelector('.sa-long'), 'animateSuccessLong');
                var l = o.querySelector('.sa-icon.sa-error');
                r.removeClass(l, 'animateErrorIcon');
                r.removeClass(l.querySelector('.sa-x-mark'), 'animateXMark');
                var a = o.querySelector('.sa-icon.sa-warning');
                r.removeClass(a, 'pulseWarning');
                r.removeClass(a.querySelector('.sa-body'), 'pulseWarningIns');
                r.removeClass(a.querySelector('.sa-dot'), 'pulseWarningIns');
                setTimeout(function() {
                    var e = o.getAttribute('data-custom-class');
                    r.removeClass(o, e)
                }, 300);
                r.removeClass(t.body, 'stop-scrolling');
                e.onkeydown = m;
                if (e.previousActiveElement) {
                    e.previousActiveElement.focus()
                };
                f = n;
                clearTimeout(o.timeout);
                return !0
            };
            l.showInputError = u.showInputError = function(e) {
                var t = i.getModal(),
                    o = t.querySelector('.sa-input-error');
                r.addClass(o, 'show');
                var n = t.querySelector('.sa-error-container');
                r.addClass(n, 'show');
                n.querySelector('p').innerHTML = e;
                setTimeout(function() {
                    l.enableButtons()
                }, 1);
                t.querySelector('input').focus()
            };
            l.resetInputError = u.resetInputError = function(e) {
                if (e && e.keyCode === 13) {
                    return !1
                };
                var t = i.getModal(),
                    o = t.querySelector('.sa-input-error');
                r.removeClass(o, 'show');
                var n = t.querySelector('.sa-error-container');
                r.removeClass(n, 'show')
            };
            l.disableButtons = u.disableButtons = function(e) {
                var t = i.getModal(),
                    n = t.querySelector('button.confirm'),
                    o = t.querySelector('button.cancel');
                n.disabled = !0;
                o.disabled = !0
            };
            l.enableButtons = u.enableButtons = function(e) {
                var t = i.getModal(),
                    n = t.querySelector('button.confirm'),
                    o = t.querySelector('button.cancel');
                n.disabled = !1;
                o.disabled = !1
            };
            if (typeof e !== 'undefined') {
                e.sweetAlert = e.swal = l
            } else {
                c.logStr('SweetAlert is a frontend module!')
            };
            a.exports = s['default']
        }, {
            './modules/default-params': 2,
            './modules/handle-click': 3,
            './modules/handle-dom': 4,
            './modules/handle-key': 5,
            './modules/handle-swal-dom': 6,
            './modules/set-params': 8,
            './modules/utils': 9
        }],
        2: [function(e, t, n) {
            'use strict';
            Object.defineProperty(n, '__esModule', {
                value: !0
            });
            var o = {
                title: '',
                text: '',
                type: null,
                allowOutsideClick: !1,
                showConfirmButton: !0,
                showCancelButton: !1,
                closeOnConfirm: !0,
                closeOnCancel: !0,
                confirmButtonText: 'OK',
                confirmButtonColor: '#8CD4F5',
                cancelButtonText: 'Cancel',
                imageUrl: null,
                imageSize: null,
                timer: null,
                customClass: '',
                html: !1,
                animation: !0,
                allowEscapeKey: !0,
                inputType: 'text',
                inputPlaceholder: '',
                inputValue: '',
                showLoaderOnConfirm: !1
            };
            n['default'] = o;
            t.exports = n['default']
        }, {}],
        3: [function(t, n, o) {
            'use strict';
            Object.defineProperty(o, '__esModule', {
                value: !0
            });
            var r = t('./utils'),
                u = t('./handle-swal-dom'),
                a = t('./handle-dom'),
                l = function(t, n, o) {
                    var f = t || e.event,
                        l = f.target || f.srcElement,
                        c = l.className.indexOf('confirm') !== -1,
                        w = l.className.indexOf('sweet-overlay') !== -1,
                        p = a.hasClass(o, 'visible'),
                        v = n.doneFunction && o.getAttribute('data-has-done-function') === 'true',
                        u, m, y;
                    if (c && n.confirmButtonColor) {
                        u = n.confirmButtonColor;
                        m = r.colorLuminance(u, -0.04);
                        y = r.colorLuminance(u, -0.14)
                    };

                    function d(e) {
                        if (c && n.confirmButtonColor) {
                            l.style.backgroundColor = e
                        }
                    };
                    switch (f.type) {
                        case 'mouseover':
                            d(m);
                            break;
                        case 'mouseout':
                            d(u);
                            break;
                        case 'mousedown':
                            d(y);
                            break;
                        case 'mouseup':
                            d(m);
                            break;
                        case 'focus':
                            var h = o.querySelector('button.confirm');
                            var b = o.querySelector('button.cancel');
                            if (c) {
                                b.style.boxShadow = 'none'
                            } else {
                                h.style.boxShadow = 'none'
                            };
                            break;
                        case 'click':
                            var g = o === l;
                            var C = a.isDescendant(o, l);
                            if (!g && !C && p && !n.allowOutsideClick) {
                                break
                            };
                            if (c && v && p) {
                                s(o, n)
                            } else if (v && p || w) {
                                i(o, n)
                            } else if (a.isDescendant(o, l) && l.tagName === 'BUTTON') {
                                sweetAlert.close()
                            };
                            break
                    }
                },
                s = function(e, t) {
                    var n = !0;
                    if (a.hasClass(e, 'show-input')) {
                        n = e.querySelector('input').value;
                        if (!n) {
                            n = ''
                        }
                    };
                    t.doneFunction(n);
                    if (t.closeOnConfirm) {
                        sweetAlert.close()
                    };
                    if (t.showLoaderOnConfirm) {
                        sweetAlert.disableButtons()
                    }
                },
                i = function(e, t) {
                    var n = String(t.doneFunction).replace(/\s/g, ''),
                        o = n.substring(0, 9) === 'function(' && n.substring(9, 10) !== ')';
                    if (o) {
                        t.doneFunction(!1)
                    };
                    if (t.closeOnCancel) {
                        sweetAlert.close()
                    }
                };
            o['default'] = {
                handleButton: l,
                handleConfirm: s,
                handleCancel: i
            };
            n.exports = o['default']
        }, {
            './handle-dom': 4,
            './handle-swal-dom': 6,
            './utils': 9
        }],
        4: [function(n, a, o) {
            'use strict';
            Object.defineProperty(o, '__esModule', {
                value: !0
            });
            var r = function(e, t) {
                    return new RegExp(' ' + t + ' ').test(' ' + e.className + ' ')
                },
                h = function(e, t) {
                    if (!r(e, t)) {
                        e.className += ' ' + t
                    }
                },
                y = function(e, t) {
                    var n = ' ' + e.className.replace(/[\t\r\n]/g, ' ') + ' ';
                    if (r(e, t)) {
                        while (n.indexOf(' ' + t + ' ') >= 0) {
                            n = n.replace(' ' + t + ' ', ' ')
                        };
                        e.className = n.replace(/^\s+|\s+$/g, '')
                    }
                },
                v = function(e) {
                    var n = t.createElement('div');
                    n.appendChild(t.createTextNode(e));
                    return n.innerHTML
                },
                s = function(e) {
                    e.style.opacity = '';
                    e.style.display = 'block'
                },
                m = function(e) {
                    if (e && !e.length) {
                        return s(e)
                    };
                    for (var t = 0; t < e.length; ++t) {
                        s(e[t])
                    }
                },
                i = function(e) {
                    e.style.opacity = '';
                    e.style.display = 'none'
                },
                b = function(e) {
                    if (e && !e.length) {
                        return i(e)
                    };
                    for (var t = 0; t < e.length; ++t) {
                        i(e[t])
                    }
                },
                p = function(e, t) {
                    var n = t.parentNode;
                    while (n !== null) {
                        if (n === e) {
                            return !0
                        };
                        n = n.parentNode
                    };
                    return !1
                },
                l = function(e) {
                    e.style.left = '-9999px';
                    e.style.display = 'block';
                    var n = e.clientHeight,
                        t;
                    if (typeof getComputedStyle !== 'undefined') {
                        t = parseInt(getComputedStyle(e).getPropertyValue('padding-top'), 10)
                    } else {
                        t = parseInt(e.currentStyle.padding)
                    };
                    e.style.left = '';
                    e.style.display = 'none';
                    return '-' + parseInt((n + t) / 2) + 'px'
                },
                d = function(e, t) {
                    if (+e.style.opacity < 1) {
                        t = t || 16;
                        e.style.opacity = 0;
                        e.style.display = 'block';
                        var n = +new Date(),
                            o = (function(e) {
                                function t() {
                                    return e.apply(this, arguments)
                                };
                                t.toString = function() {
                                    return e.toString()
                                };
                                return t
                            })(function() {
                                e.style.opacity = +e.style.opacity + (new Date() - n) / 100;
                                n = +new Date();
                                if (+e.style.opacity < 1) {
                                    setTimeout(o, t)
                                }
                            });
                        o()
                    };
                    e.style.display = 'block'
                },
                c = function(e, t) {
                    t = t || 16;
                    e.style.opacity = 1;
                    var n = +new Date(),
                        o = (function(e) {
                            function t() {
                                return e.apply(this, arguments)
                            };
                            t.toString = function() {
                                return e.toString()
                            };
                            return t
                        })(function() {
                            e.style.opacity = +e.style.opacity - (new Date() - n) / 100;
                            n = +new Date();
                            if (+e.style.opacity > 0) {
                                setTimeout(o, t)
                            } else {
                                e.style.display = 'none'
                            }
                        });
                    o()
                },
                f = function(n) {
                    if (typeof MouseEvent === 'function') {
                        var a = new MouseEvent('click', {
                            view: e,
                            bubbles: !1,
                            cancelable: !0
                        });
                        n.dispatchEvent(a)
                    } else if (t.createEvent) {
                        var o = t.createEvent('MouseEvents');
                        o.initEvent('click', !1, !1);
                        n.dispatchEvent(o)
                    } else if (t.createEventObject) {
                        n.fireEvent('onclick')
                    } else if (typeof n.onclick === 'function') {
                        n.onclick()
                    }
                },
                u = function(t) {
                    if (typeof t.stopPropagation === 'function') {
                        t.stopPropagation();
                        t.preventDefault()
                    } else if (e.event && e.event.hasOwnProperty('cancelBubble')) {
                        e.event.cancelBubble = !0
                    }
                };
            o.hasClass = r;
            o.addClass = h;
            o.removeClass = y;
            o.escapeHtml = v;
            o._show = s;
            o.show = m;
            o._hide = i;
            o.hide = b;
            o.isDescendant = p;
            o.getTopMargin = l;
            o.fadeIn = d;
            o.fadeOut = c;
            o.fireClick = f;
            o.stopEventPropagation = u
        }, {}],
        5: [function(t, o, a) {
            'use strict';
            Object.defineProperty(a, '__esModule', {
                value: !0
            });
            var r = t('./handle-dom'),
                s = t('./handle-swal-dom'),
                i = function(t, o, a) {
                    var l = t || e.event,
                        f = l.keyCode || l.which,
                        p = a.querySelector('button.confirm'),
                        m = a.querySelector('button.cancel'),
                        c = a.querySelectorAll('button[tabindex]');
                    if ([9, 13, 32, 27].indexOf(f) === -1) {
                        return
                    };
                    var i = l.target || l.srcElement,
                        u = -1;
                    for (var d = 0; d < c.length; d++) {
                        if (i === c[d]) {
                            u = d;
                            break
                        }
                    };
                    if (f === 9) {
                        if (u === -1) {
                            i = p
                        } else {
                            if (u === c.length - 1) {
                                i = c[0]
                            } else {
                                i = c[u + 1]
                            }
                        };
                        r.stopEventPropagation(l);
                        i.focus();
                        if (o.confirmButtonColor) {
                            s.setFocusStyle(i, o.confirmButtonColor)
                        }
                    } else {
                        if (f === 13) {
                            if (i.tagName === 'INPUT') {
                                i = p;
                                p.focus()
                            };
                            if (u === -1) {
                                i = p
                            } else {
                                i = n
                            }
                        } else if (f === 27 && o.allowEscapeKey === !0) {
                            i = m;
                            r.fireClick(i, l)
                        } else {
                            i = n
                        }
                    }
                };
            a['default'] = i;
            o.exports = a['default']
        }, {
            './handle-dom': 4,
            './handle-swal-dom': 6
        }],
        6: [function(n, o, a) {
            'use strict';
            var u = function(e) {
                return e && e.__esModule ? e : {
                    'default': e
                }
            };
            Object.defineProperty(a, '__esModule', {
                value: !0
            });
            var b = n('./utils'),
                r = n('./handle-dom'),
                h = n('./default-params'),
                i = u(h),
                C = n('./injected-html'),
                S = u(C),
                v = '.sweet-alert',
                p = '.sweet-overlay',
                l = function() {
                    var e = t.createElement('div');
                    e.innerHTML = S['default'];
                    while (e.firstChild) {
                        t.body.appendChild(e.firstChild)
                    }
                },
                s = (function(e) {
                    function t() {
                        return e.apply(this, arguments)
                    };
                    t.toString = function() {
                        return e.toString()
                    };
                    return t
                })(function() {
                    var e = t.querySelector(v);
                    if (!e) {
                        l();
                        e = s()
                    };
                    return e
                }),
                d = function() {
                    var e = s();
                    if (e) {
                        return e.querySelector('input')
                    }
                },
                f = function() {
                    return t.querySelector(p)
                },
                w = function(e, t) {
                    var n = b.hexToRgb(t);
                    e.style.boxShadow = '0 0 2px rgba(' + n + ', 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)'
                },
                g = function(n) {
                    var o = s();
                    r.fadeIn(f(), 10);
                    r.show(o);
                    r.addClass(o, 'showSweetAlert');
                    r.removeClass(o, 'hideSweetAlert');
                    e.previousActiveElement = t.activeElement;
                    var l = o.querySelector('button.confirm');
                    l.focus();
                    setTimeout(function() {
                        r.addClass(o, 'visible')
                    }, 500);
                    var a = o.getAttribute('data-timer');
                    if (a !== 'null' && a !== '') {
                        var i = n;
                        o.timeout = setTimeout(function() {
                            var e = (i || null) && o.getAttribute('data-has-done-function') === 'true';
                            if (e) {
                                i(null)
                            } else {
                                sweetAlert.close()
                            }
                        }, a)
                    }
                },
                m = function() {
                    var t = s(),
                        e = d();
                    r.removeClass(t, 'show-input');
                    e.value = i['default'].inputValue;
                    e.setAttribute('type', i['default'].inputType);
                    e.setAttribute('placeholder', i['default'].inputPlaceholder);
                    c()
                },
                c = function(e) {
                    if (e && e.keyCode === 13) {
                        return !1
                    };
                    var t = s(),
                        o = t.querySelector('.sa-input-error');
                    r.removeClass(o, 'show');
                    var n = t.querySelector('.sa-error-container');
                    r.removeClass(n, 'show')
                },
                y = function() {
                    var e = s();
                    e.style.marginTop = r.getTopMargin(s())
                };
            a.sweetAlertInitialize = l;
            a.getModal = s;
            a.getOverlay = f;
            a.getInput = d;
            a.setFocusStyle = w;
            a.openModal = g;
            a.resetInput = m;
            a.resetInputError = c;
            a.fixVerticalPosition = y
        }, {
            './default-params': 2,
            './handle-dom': 4,
            './injected-html': 7,
            './utils': 9
        }],
        7: [function(e, t, n) {
            'use strict';
            Object.defineProperty(n, '__esModule', {
                value: !0
            });
            var o = '<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <div class="sa-confirm-button-container">\n        <button class="confirm" tabIndex="1">OK</button><div class="la-ball-fall">\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>\n    </div></div>';
            n['default'] = o;
            t.exports = n['default']
        }, {}],
        8: [function(e, t, a) {
            'use strict';
            Object.defineProperty(a, '__esModule', {
                value: !0
            });
            var i = e('./utils'),
                r = e('./handle-swal-dom'),
                o = e('./handle-dom'),
                s = ['error', 'warning', 'info', 'success', 'input', 'prompt'],
                l = function(e) {
                    var t = r.getModal(),
                        g = t.querySelector('h2'),
                        y = t.querySelector('p'),
                        u = t.querySelector('button.cancel'),
                        a = t.querySelector('button.confirm');
                    g.innerHTML = e.html ? e.title : o.escapeHtml(e.title).split('\n').join('<br>');
                    y.innerHTML = e.html ? e.text : o.escapeHtml(e.text || '').split('\n').join('<br>');
                    if (e.text) o.show(y);
                    if (e.customClass) {
                        o.addClass(t, e.customClass);
                        t.setAttribute('data-custom-class', e.customClass)
                    } else {
                        var b = t.getAttribute('data-custom-class');
                        o.removeClass(t, b);
                        t.setAttribute('data-custom-class', '')
                    };
                    o.hide(t.querySelectorAll('.sa-icon'));
                    if (e.type && !i.isIE8()) {
                        var v = (function() {
                            var u = !1;
                            for (var l = 0; l < s.length; l++) {
                                if (e.type === s[l]) {
                                    u = !0;
                                    break
                                }
                            };
                            if (!u) {
                                logStr('Unknown alert type: ' + e.type);
                                return {
                                    v: !1
                                }
                            };
                            var c = ['success', 'error', 'warning', 'info'],
                                a = n;
                            if (c.indexOf(e.type) !== -1) {
                                a = t.querySelector('.sa-icon.sa-' + e.type);
                                o.show(a)
                            };
                            var i = r.getInput();
                            switch (e.type) {
                                case 'success':
                                    o.addClass(a, 'animate');
                                    o.addClass(a.querySelector('.sa-tip'), 'animateSuccessTip');
                                    o.addClass(a.querySelector('.sa-long'), 'animateSuccessLong');
                                    break;
                                case 'error':
                                    o.addClass(a, 'animateErrorIcon');
                                    o.addClass(a.querySelector('.sa-x-mark'), 'animateXMark');
                                    break;
                                case 'warning':
                                    o.addClass(a, 'pulseWarning');
                                    o.addClass(a.querySelector('.sa-body'), 'pulseWarningIns');
                                    o.addClass(a.querySelector('.sa-dot'), 'pulseWarningIns');
                                    break;
                                case 'input':
                                case 'prompt':
                                    i.setAttribute('type', e.inputType);
                                    i.value = e.inputValue;
                                    i.setAttribute('placeholder', e.inputPlaceholder);
                                    o.addClass(t, 'show-input');
                                    setTimeout(function() {
                                        i.focus();
                                        i.addEventListener('keyup', swal.resetInputError)
                                    }, 400);
                                    break
                            }
                        })();
                        if (typeof v === 'object') {
                            return v.v
                        }
                    };
                    if (e.imageUrl) {
                        var l = t.querySelector('.sa-icon.sa-custom');
                        l.style.backgroundImage = 'url(' + e.imageUrl + ')';
                        o.show(l);
                        var p = 80,
                            m = 80;
                        if (e.imageSize) {
                            var c = e.imageSize.toString().split('x'),
                                d = c[0],
                                f = c[1];
                            if (!d || !f) {
                                logStr('Parameter imageSize expects value with format WIDTHxHEIGHT, got ' + e.imageSize)
                            } else {
                                p = d;
                                m = f
                            }
                        };
                        l.setAttribute('style', l.getAttribute('style') + 'width:' + p + 'px; height:' + m + 'px')
                    };
                    t.setAttribute('data-has-cancel-button', e.showCancelButton);
                    if (e.showCancelButton) {
                        u.style.display = 'inline-block'
                    } else {
                        o.hide(u)
                    };
                    t.setAttribute('data-has-confirm-button', e.showConfirmButton);
                    if (e.showConfirmButton) {
                        a.style.display = 'inline-block'
                    } else {
                        o.hide(a)
                    };
                    if (e.cancelButtonText) {
                        u.innerHTML = o.escapeHtml(e.cancelButtonText)
                    };
                    if (e.confirmButtonText) {
                        a.innerHTML = o.escapeHtml(e.confirmButtonText)
                    };
                    if (e.confirmButtonColor) {
                        a.style.backgroundColor = e.confirmButtonColor;
                        a.style.borderLeftColor = e.confirmLoadingButtonColor;
                        a.style.borderRightColor = e.confirmLoadingButtonColor;
                        r.setFocusStyle(a, e.confirmButtonColor)
                    };
                    t.setAttribute('data-allow-outside-click', e.allowOutsideClick);
                    var h = e.doneFunction ? !0 : !1;
                    t.setAttribute('data-has-done-function', h);
                    if (!e.animation) {
                        t.setAttribute('data-animation', 'none')
                    } else if (typeof e.animation === 'string') {
                        t.setAttribute('data-animation', e.animation)
                    } else {
                        t.setAttribute('data-animation', 'pop')
                    };
                    t.setAttribute('data-timer', e.timer)
                };
            a['default'] = l;
            t.exports = a['default']
        }, {
            './handle-dom': 4,
            './handle-swal-dom': 6,
            './utils': 9
        }],
        9: [function(t, n, o) {
            'use strict';
            Object.defineProperty(o, '__esModule', {
                value: !0
            });
            var a = function(e, t) {
                    for (var n in t) {
                        if (t.hasOwnProperty(n)) {
                            e[n] = t[n]
                        }
                    };
                    return e
                },
                r = function(e) {
                    var t = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);
                    return t ? parseInt(t[1], 16) + ', ' + parseInt(t[2], 16) + ', ' + parseInt(t[3], 16) : null
                },
                s = function() {
                    return e.attachEvent && !e.addEventListener
                },
                i = function(t) {
                    if (e.console) {
                        e.console.log('SweetAlert: ' + t)
                    }
                },
                l = function(e, t) {
                    e = String(e).replace(/[^0-9a-f]/gi, '');
                    if (e.length < 6) {
                        e = e[0] + e[0] + e[1] + e[1] + e[2] + e[2]
                    };
                    t = t || 0;
                    var a = '#',
                        n, o;
                    for (o = 0; o < 3; o++) {
                        n = parseInt(e.substr(o * 2, 2), 16);
                        n = Math.round(Math.min(Math.max(0, n + n * t), 255)).toString(16);
                        a += ('00' + n).substr(n.length)
                    };
                    return a
                };
            o.extend = a;
            o.hexToRgb = r;
            o.isIE8 = s;
            o.logStr = i;
            o.colorLuminance = l
        }, {}]
    }, {}, [1]);
    if (typeof define === 'function' && define.amd) {
        define(function() {
            return sweetAlert
        })
    } else if (typeof module !== 'undefined' && module.exports) {
        module.exports = sweetAlert
    }
})(window, document);;;
var utmCookie = {
    cookieNamePrefix: '_uc_',
    utmParams: ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content', 'gclid'],
    cookieExpiryDays: 30,
    createCookie: function(e, i, o) {
        var t = '';
        if (o) {
            var r = new Date();
            r.setTime(r.getTime() + (o * 24 * 60 * 60 * 1000));
            t = '; expires=' + r.toGMTString()
        } else {
            t = ''
        };
        document.cookie = this.cookieNamePrefix + e + '=' + i + t + '; path=/'
    },
    deleteCookie: function(e) {
        document.cookie = this.cookieNamePrefix + e + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/'
    },
    readCookie: function(r, o) {
        var t;
        if (o !== undefined) {
            t = r + '='
        } else {
            t = this.cookieNamePrefix + r + '='
        };
        var a = document.cookie.split(';');
        for (var i = 0; i < a.length; i++) {
            var e = a[i];
            while (e.charAt(0) === ' ') {
                e = e.substring(1, e.length)
            };
            if (e.indexOf(t) === 0) {
                return e.substring(t.length, e.length)
            }
        };
        return null
    },
    getParameterByName: function(e) {
        e = e.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
        var t = '[\\?&]' + e + '=([^&#]*)',
            i = new RegExp(t),
            r = i.exec(window.location.search);
        if (r == null) {
            return ''
        } else {
            return decodeURIComponent(r[1].replace(/\+/g, ' '))
        }
    },
    utmPresentInUrl: function() {
        var t = !1;
        for (var e = 0; e < this.utmParams.length; e++) {
            var i = this.utmParams[e],
                r = this.getParameterByName(i);
            if (r !== '' && r !== undefined) {
                t = !0
            }
        };
        return t
    },
    writeUtmCookieFromParams: function() {
        for (var e = 0; e < this.utmParams.length; e++) {
            var r = this.utmParams[e],
                t = this.getParameterByName(r);
            if (t) {
                this.createCookie(r, t, this.cookieExpiryDays)
            }
        }
    },
    resetCookies: function() {
        for (var e = 0; e < this.utmParams.length; e++) {
            var r = this.utmParams[e];
            this.deleteCookie(r)
        }
    },
    writeReferrer: function() {
        if (document.referrer) {
            this.createCookie('referrer', document.referrer, this.cookieExpiryDays)
        }
    }
};
utmCookie.writeReferrer();
if (utmCookie.utmPresentInUrl()) {
    utmCookie.resetCookies();
    utmCookie.writeUtmCookieFromParams()
};;
(function(e) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], e)
    } else if (typeof module === 'object' && module.exports) {
        module.exports = e(require('jquery'))
    } else {
        e(jQuery)
    }
}(function(e) {
    var o = Array.prototype.slice,
        p = Array.prototype.splice,
        s = {
            topSpacing: 0,
            bottomSpacing: 0,
            className: 'is-sticky',
            wrapperClassName: 'sticky-wrapper',
            center: !1,
            getWidthFrom: '',
            widthFromWrapper: !0,
            responsiveWidth: !1,
            zIndex: 'inherit'
        },
        r = e(window),
        l = e(document),
        i = [],
        c = r.height(),
        n = function() {
            var o = r.scrollTop(),
                d = l.height(),
                u = d - c,
                h = (o > u) ? u - o : 0;
            for (var a = 0, y = i.length; a < y; a++) {
                var t = i[a],
                    g = t.stickyWrapper.offset().top,
                    m = g - t.topSpacing - h;
                t.stickyWrapper.css('height', t.stickyElement.outerHeight());
                if (o <= m) {
                    if (t.currentTop !== null) {
                        t.stickyElement.css({
                            'width': '',
                            'position': '',
                            'top': '',
                            'z-index': ''
                        });
                        t.stickyElement.parent().removeClass(t.className);
                        t.stickyElement.trigger('sticky-end', [t]);
                        t.currentTop = null
                    }
                } else {
                    var n = d - t.stickyElement.outerHeight() - t.topSpacing - t.bottomSpacing - o - h;
                    if (n < 0) {
                        n = n + t.topSpacing
                    } else {
                        n = t.topSpacing
                    };
                    if (t.currentTop !== n) {
                        var s;
                        if (t.getWidthFrom) {
                            padding = t.stickyElement.innerWidth() - t.stickyElement.width();
                            s = e(t.getWidthFrom).width() - padding || null
                        } else if (t.widthFromWrapper) {
                            s = t.stickyWrapper.width()
                        };
                        if (s == null) {
                            s = t.stickyElement.width()
                        };
                        t.stickyElement.css('width', s).css('position', 'fixed').css('top', n).css('z-index', t.zIndex);
                        t.stickyElement.parent().addClass(t.className);
                        if (t.currentTop === null) {
                            t.stickyElement.trigger('sticky-start', [t])
                        } else {
                            t.stickyElement.trigger('sticky-update', [t])
                        };
                        if (t.currentTop === t.topSpacing && t.currentTop > n || t.currentTop === null && n < t.topSpacing) {
                            t.stickyElement.trigger('sticky-bottom-reached', [t])
                        } else if (t.currentTop !== null && n === t.topSpacing && t.currentTop < n) {
                            t.stickyElement.trigger('sticky-bottom-unreached', [t])
                        };
                        t.currentTop = n
                    };
                    var p = t.stickyWrapper.parent(),
                        f = (t.stickyElement.offset().top + t.stickyElement.outerHeight() >= p.offset().top + p.outerHeight()) && (t.stickyElement.offset().top <= t.topSpacing);
                    if (f) {
                        t.stickyElement.css('position', 'absolute').css('top', '').css('bottom', 0).css('z-index', '')
                    } else {
                        t.stickyElement.css('position', 'fixed').css('top', n).css('bottom', '').css('z-index', t.zIndex)
                    }
                }
            }
        },
        a = function() {
            c = r.height();
            for (var s = 0, o = i.length; s < o; s++) {
                var t = i[s],
                    n = null;
                if (t.getWidthFrom) {
                    if (t.responsiveWidth) {
                        n = e(t.getWidthFrom).width()
                    }
                } else if (t.widthFromWrapper) {
                    n = t.stickyWrapper.width()
                };
                if (n != null) {
                    t.stickyElement.css('width', n)
                }
            }
        },
        t = {
            init: function(n) {
                return this.each(function() {
                    var o = e.extend({}, s, n);
                    var r = e(this),
                        a = r.attr('id'),
                        p = a ? a + '-' + s.wrapperClassName : s.wrapperClassName,
                        l = e('<div></div>').attr('id', p).addClass(o.wrapperClassName);
                    r.wrapAll(function() {
                        if (e(this).parent('#' + p).length == 0) {
                            return l
                        }
                    });
                    var c = r.parent();
                    if (o.center) {
                        c.css({
                            width: r.outerWidth(),
                            marginLeft: 'auto',
                            marginRight: 'auto'
                        })
                    };
                    if (r.css('float') === 'right') {
                        r.css({
                            'float': 'none'
                        }).parent().css({
                            'float': 'right'
                        })
                    };
                    o.stickyElement = r;
                    o.stickyWrapper = c;
                    o.currentTop = null;
                    i.push(o);
                    t.setWrapperHeight(this);
                    t.setupChangeListeners(this)
                })
            },
            setWrapperHeight: function(t) {
                var i = e(t),
                    n = i.parent();
                if (n) {
                    n.css('height', i.outerHeight())
                }
            },
            setupChangeListeners: function(e) {
                if (window.MutationObserver) {
                    var i = new window.MutationObserver(function(i) {
                        if (i[0].addedNodes.length || i[0].removedNodes.length) {
                            t.setWrapperHeight(e)
                        }
                    });
                    i.observe(e, {
                        subtree: !0,
                        childList: !0
                    })
                } else {
                    if (window.addEventListener) {
                        e.addEventListener('DOMNodeInserted', function() {
                            t.setWrapperHeight(e)
                        }, !1);
                        e.addEventListener('DOMNodeRemoved', function() {
                            t.setWrapperHeight(e)
                        }, !1)
                    } else if (window.attachEvent) {
                        e.attachEvent('onDOMNodeInserted', function() {
                            t.setWrapperHeight(e)
                        });
                        e.attachEvent('onDOMNodeRemoved', function() {
                            t.setWrapperHeight(e)
                        })
                    }
                }
            },
            update: n,
            unstick: function(t) {
                return this.each(function() {
                    var n = this,
                        s = e(n),
                        r = -1,
                        t = i.length;
                    while (t-- > 0) {
                        if (i[t].stickyElement.get(0) === n) {
                            p.call(i, t, 1);
                            r = t
                        }
                    };
                    if (r !== -1) {
                        s.unwrap();
                        s.css({
                            'width': '',
                            'position': '',
                            'top': '',
                            'float': '',
                            'z-index': ''
                        })
                    }
                })
            }
        };
    if (window.addEventListener) {
        window.addEventListener('scroll', n, !1);
        window.addEventListener('resize', a, !1)
    } else if (window.attachEvent) {
        window.attachEvent('onscroll', n);
        window.attachEvent('onresize', a)
    };
    e.fn.sticky = function(i) {
        if (t[i]) {
            return t[i].apply(this, o.call(arguments, 1))
        } else if (typeof i === 'object' || !i) {
            return t.init.apply(this, arguments)
        } else {
            e.error('Method ' + i + ' does not exist on jQuery.sticky')
        }
    };
    e.fn.unstick = function(i) {
        if (t[i]) {
            return t[i].apply(this, o.call(arguments, 1))
        } else if (typeof i === 'object' || !i) {
            return t.unstick.apply(this, arguments)
        } else {
            e.error('Method ' + i + ' does not exist on jQuery.sticky')
        }
    };
    e(function() {
        setTimeout(n, 0)
    })
}));;
(function(e) {
    if (e.hasInitialised) return;
    var t = {
        escapeRegExp: function(e) {
            return e.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')
        },
        hasClass: function(e, t) {
            var i = ' ';
            return (e.nodeType === 1 && (i + e.className + i).replace(/[\n\t]/g, i).indexOf(i + t + i) >= 0)
        },
        addClass: function(e, t) {
            e.className += ' ' + t
        },
        removeClass: function(e, t) {
            var i = new RegExp('\\b' + this.escapeRegExp(t) + '\\b');
            e.className = e.className.replace(i, '')
        },
        interpolateString: function(e, t) {
            var i = /{{([a-z][a-z0-9\-_]*)}}/gi;
            return e.replace(i, function(e) {
                return t(arguments[1]) || ''
            })
        },
        getCookie: function(e) {
            var i = '; ' + document.cookie,
                t = i.split('; ' + e + '=');
            return t.length < 2 ? undefined : t.pop().split(';').shift()
        },
        setCookie: function(e, t, i, n, r, a) {
            var s = new Date();
            s.setDate(s.getDate() + (i || 365));
            var o = [e + '=' + t, 'expires=' + s.toUTCString(), 'path=' + (r || '/')];
            if (n) {
                o.push('domain=' + n)
            };
            if (a) {
                o.push('secure')
            };
            document.cookie = o.join(';')
        },
        deepExtend: function(e, t) {
            for (var i in t) {
                if (t.hasOwnProperty(i)) {
                    if (i in e && this.isPlainObject(e[i]) && this.isPlainObject(t[i])) {
                        this.deepExtend(e[i], t[i])
                    } else {
                        e[i] = t[i]
                    }
                }
            };
            return e
        },
        throttle: function(e, t) {
            var i = !1;
            return function() {
                if (!i) {
                    e.apply(this, arguments);
                    i = !0;
                    setTimeout(function() {
                        i = !1
                    }, t)
                }
            }
        },
        hash: function(e) {
            var t = 0,
                i, n, o;
            if (e.length === 0) return t;
            for (i = 0, o = e.length; i < o; ++i) {
                n = e.charCodeAt(i);
                t = (t << 5) - t + n;
                t |= 0
            };
            return t
        },
        normaliseHex: function(e) {
            if (e[0] == '#') {
                e = e.substr(1)
            };
            if (e.length == 3) {
                e = e[0] + e[0] + e[1] + e[1] + e[2] + e[2]
            };
            return e
        },
        getContrast: function(e) {
            e = this.normaliseHex(e);
            var t = parseInt(e.substr(0, 2), 16),
                i = parseInt(e.substr(2, 2), 16),
                n = parseInt(e.substr(4, 2), 16),
                o = (t * 299 + i * 587 + n * 114) / 1000;
            return o >= 128 ? '#000' : '#fff'
        },
        getLuminance: function(e) {
            var t = parseInt(this.normaliseHex(e), 16),
                i = 38,
                n = (t >> 16) + i,
                o = ((t >> 8) & 0x00ff) + i,
                s = (t & 0x0000ff) + i,
                r = (0x1000000 + (n < 255 ? (n < 1 ? 0 : n) : 255) * 0x10000 + (o < 255 ? (o < 1 ? 0 : o) : 255) * 0x100 + (s < 255 ? (s < 1 ? 0 : s) : 255)).toString(16).slice(1);
            return '#' + r
        },
        isMobile: function() {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        },
        isPlainObject: function(e) {
            return (typeof e === 'object' && e !== null && e.constructor == Object)
        },
        traverseDOMPath: function(e, i) {
            if (!e || !e.parentNode) return null;
            if (t.hasClass(e, i)) return e;
            return this.traverseDOMPath(e.parentNode, i)
        }
    };
    e.status = {
        deny: 'deny',
        allow: 'allow',
        dismiss: 'dismiss'
    };
    e.transitionEnd = (function() {
        var i = document.createElement('div'),
            t = {
                t: 'transitionend',
                OT: 'oTransitionEnd',
                msT: 'MSTransitionEnd',
                MozT: 'transitionend',
                WebkitT: 'webkitTransitionEnd'
            };
        for (var e in t) {
            if (t.hasOwnProperty(e) && typeof i.style[e + 'ransition'] != 'undefined') {
                return t[e]
            }
        };
        return ''
    })();
    e.hasTransition = !!e.transitionEnd;
    var i = Object.keys(e.status).map(t.escapeRegExp);
    e.customStyles = {};
    e.Popup = (function() {
        var d = {
            enabled: !0,
            container: null,
            cookie: {
                name: 'cookieconsent_status',
                path: '/',
                domain: '',
                expiryDays: 365,
                secure: !1
            },
            onPopupOpen: function() {},
            onPopupClose: function() {},
            onInitialise: function(e) {},
            onStatusChange: function(e, t) {},
            onRevokeChoice: function() {},
            onNoCookieLaw: function(e, t) {},
            content: {
                header: 'Cookies used on the website!',
                message: 'This website uses cookies to ensure you get the best experience on our website.',
                dismiss: 'Got it!',
                allow: 'Allow cookies',
                deny: 'Decline',
                link: 'Learn more',
                href: 'https://cookiesandyou.com',
                close: '&#x274c;',
                target: '_blank',
                policy: 'Cookie Policy'
            },
            elements: {
                header: '<span class="cc-header">{{header}}</span>&nbsp;',
                message: '<span id="cookieconsent:desc" class="cc-message">{{message}}</span>',
                messagelink: '<span id="cookieconsent:desc" class="cc-message">{{message}} <a aria-label="learn more about cookies" role=button tabindex="0" class="cc-link" href="{{href}}" rel="noopener noreferrer nofollow" target="{{target}}">{{link}}</a></span>',
                dismiss: '<a aria-label="dismiss cookie message" role=button tabindex="0" class="cc-btn cc-dismiss">{{dismiss}}</a>',
                allow: '<a aria-label="allow cookies" role=button tabindex="0"  class="cc-btn cc-allow">{{allow}}</a>',
                deny: '<a aria-label="deny cookies" role=button tabindex="0" class="cc-btn cc-deny">{{deny}}</a>',
                link: '<a aria-label="learn more about cookies" role=button tabindex="0" class="cc-link" href="{{href}}" rel="noopener noreferrer nofollow" target="{{target}}">{{link}}</a>',
                close: '<span aria-label="dismiss cookie message" role=button tabindex="0" class="cc-close">{{close}}</span>'
            },
            window: '<div role="dialog" aria-live="polite" aria-label="cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window {{classes}}"><!--googleoff: all-->{{children}}<!--googleon: all--></div>',
            revokeBtn: '<div class="cc-revoke {{classes}}">{{policy}}</div>',
            compliance: {
                info: '<div class="cc-compliance">{{dismiss}}</div>',
                'opt-in': '<div class="cc-compliance cc-highlight">{{deny}}{{allow}}</div>',
                'opt-out': '<div class="cc-compliance cc-highlight">{{deny}}{{allow}}</div>'
            },
            type: 'info',
            layouts: {
                basic: '{{messagelink}}{{compliance}}',
                'basic-close': '{{messagelink}}{{compliance}}{{close}}',
                'basic-header': '{{header}}{{message}}{{link}}{{compliance}}'
            },
            layout: 'basic',
            position: 'bottom',
            theme: 'block',
            static: !1,
            palette: null,
            revokable: !1,
            animateRevokable: !0,
            showLink: !0,
            dismissOnScroll: !1,
            dismissOnTimeout: !1,
            dismissOnWindowClick: !1,
            ignoreClicksFrom: ['cc-revoke', 'cc-btn'],
            autoOpen: !0,
            autoAttach: !0,
            whitelistPage: [],
            blacklistPage: [],
            overrideHTML: null
        };

        function n() {
            this.initialise.apply(this, arguments)
        };
        n.prototype.initialise = function(e) {
            if (this.options) {
                this.destroy()
            };
            t.deepExtend((this.options = {}), d);
            if (t.isPlainObject(e)) {
                t.deepExtend(this.options, e)
            };
            if (g.call(this)) {
                this.options.enabled = !1
            };
            if (c(this.options.blacklistPage, location.pathname)) {
                this.options.enabled = !1
            };
            if (c(this.options.whitelistPage, location.pathname)) {
                this.options.enabled = !0
            };
            var i = this.options.window.replace('{{classes}}', b.call(this).join(' ')).replace('{{children}}', m.call(this)),
                n = this.options.overrideHTML;
            if (typeof n == 'string' && n.length) {
                i = n
            };
            if (this.options.static) {
                var s = o.call(this, '<div class="cc-grower">' + i + '</div>');
                s.style.display = '';
                this.element = s.firstChild;
                this.element.style.display = 'none';
                t.addClass(this.element, 'cc-invisible')
            } else {
                this.element = o.call(this, i)
            };
            u.call(this);
            y.call(this);
            if (this.options.autoOpen) {
                this.autoOpen()
            }
        };
        n.prototype.destroy = function() {
            if (this.onButtonClick && this.element) {
                this.element.removeEventListener('click', this.onButtonClick);
                this.onButtonClick = null
            };
            if (this.dismissTimeout) {
                clearTimeout(this.dismissTimeout);
                this.dismissTimeout = null
            };
            if (this.onWindowScroll) {
                window.removeEventListener('scroll', this.onWindowScroll);
                this.onWindowScroll = null
            };
            if (this.onWindowClick) {
                window.removeEventListener('click', this.onWindowClick);
                this.onWindowClick = null
            };
            if (this.onMouseMove) {
                window.removeEventListener('mousemove', this.onMouseMove);
                this.onMouseMove = null
            };
            if (this.element && this.element.parentNode) {
                this.element.parentNode.removeChild(this.element)
            };
            this.element = null;
            if (this.revokeBtn && this.revokeBtn.parentNode) {
                this.revokeBtn.parentNode.removeChild(this.revokeBtn)
            };
            this.revokeBtn = null;
            h(this.options.palette);
            this.options = null
        };
        n.prototype.open = function(t) {
            if (!this.element) return;
            if (!this.isOpen()) {
                if (e.hasTransition) {
                    this.fadeIn()
                } else {
                    this.element.style.display = ''
                };
                if (this.options.revokable) {
                    this.toggleRevokeButton()
                };
                this.options.onPopupOpen.call(this)
            };
            return this
        };
        n.prototype.close = function(t) {
            if (!this.element) return;
            if (this.isOpen()) {
                if (e.hasTransition) {
                    this.fadeOut()
                } else {
                    this.element.style.display = 'none'
                };
                if (t && this.options.revokable) {
                    this.toggleRevokeButton(!0)
                };
                this.options.onPopupClose.call(this)
            };
            return this
        };
        n.prototype.fadeIn = function() {
            var i = this.element;
            if (!e.hasTransition || !i) return;
            if (this.afterTransition) {
                r.call(this, i)
            };
            if (t.hasClass(i, 'cc-invisible')) {
                i.style.display = '';
                if (this.options.static) {
                    var o = this.element.clientHeight;
                    this.element.parentNode.style.maxHeight = o + 'px'
                };
                var n = 20;
                this.openingTimeout = setTimeout(s.bind(this, i), n)
            }
        };
        n.prototype.fadeOut = function() {
            var i = this.element;
            if (!e.hasTransition || !i) return;
            if (this.openingTimeout) {
                clearTimeout(this.openingTimeout);
                s.bind(this, i)
            };
            if (!t.hasClass(i, 'cc-invisible')) {
                if (this.options.static) {
                    this.element.parentNode.style.maxHeight = ''
                };
                this.afterTransition = r.bind(this, i);
                i.addEventListener(e.transitionEnd, this.afterTransition);
                t.addClass(i, 'cc-invisible')
            }
        };
        n.prototype.isOpen = function() {
            return (this.element && this.element.style.display == '' && (e.hasTransition ? !t.hasClass(this.element, 'cc-invisible') : !0))
        };
        n.prototype.toggleRevokeButton = function(e) {
            if (this.revokeBtn) this.revokeBtn.style.display = e ? '' : 'none'
        };
        n.prototype.revokeChoice = function(e) {
            this.options.enabled = !0;
            this.clearStatus();
            this.options.onRevokeChoice.call(this);
            if (!e) {
                this.autoOpen()
            }
        };
        n.prototype.hasAnswered = function(t) {
            return Object.keys(e.status).indexOf(this.getStatus()) >= 0
        };
        n.prototype.hasConsented = function(t) {
            var i = this.getStatus();
            return i == e.status.allow || i == e.status.dismiss
        };
        n.prototype.autoOpen = function(e) {
            if (!this.hasAnswered() && this.options.enabled) {
                this.open()
            } else if (this.hasAnswered() && this.options.revokable) {
                this.toggleRevokeButton(!0)
            }
        };
        n.prototype.setStatus = function(i) {
            var n = this.options.cookie,
                o = t.getCookie(n.name),
                s = Object.keys(e.status).indexOf(o) >= 0;
            if (Object.keys(e.status).indexOf(i) >= 0) {
                t.setCookie(n.name, i, n.expiryDays, n.domain, n.path, n.secure);
                this.options.onStatusChange.call(this, i, s)
            } else {
                this.clearStatus()
            }
        };
        n.prototype.getStatus = function() {
            return t.getCookie(this.options.cookie.name)
        };
        n.prototype.clearStatus = function() {
            var e = this.options.cookie;
            t.setCookie(e.name, '', -1, e.domain, e.path)
        };

        function s(e) {
            this.openingTimeout = null;
            t.removeClass(e, 'cc-invisible')
        };

        function r(t) {
            t.style.display = 'none';
            t.removeEventListener(e.transitionEnd, this.afterTransition);
            this.afterTransition = null
        };

        function g() {
            var t = this.options.onInitialise.bind(this);
            if (!window.navigator.cookieEnabled) {
                t(e.status.deny);
                return !0
            };
            if (window.CookiesOK || window.navigator.CookiesOK) {
                t(e.status.allow);
                return !0
            };
            var o = Object.keys(e.status),
                n = this.getStatus(),
                i = o.indexOf(n) >= 0;
            if (i) {
                t(n)
            };
            return i
        };

        function a() {
            var t = this.options.position.split('-'),
                e = [];
            t.forEach(function(t) {
                e.push('cc-' + t)
            });
            return e
        };

        function b() {
            var e = this.options,
                n = e.position == 'top' || e.position == 'bottom' ? 'banner' : 'floating';
            if (t.isMobile()) {
                n = 'floating'
            };
            var i = ['cc-' + n, 'cc-type-' + e.type, 'cc-theme-' + e.theme];
            if (e.static) {
                i.push('cc-static')
            };
            i.push.apply(i, a.call(this));
            var o = f.call(this, this.options.palette);
            if (this.customStyleSelector) {
                i.push(this.customStyleSelector)
            };
            return i
        };

        function m() {
            var i = {};
            var e = this.options;
            if (!e.showLink) {
                e.elements.link = '';
                e.elements.messagelink = e.elements.message
            };
            Object.keys(e.elements).forEach(function(n) {
                i[n] = t.interpolateString(e.elements[n], function(t) {
                    var i = e.content[t];
                    return t && typeof i == 'string' && i.length ? i : ''
                })
            });
            var o = e.compliance[e.type];
            if (!o) {
                o = e.compliance.info
            };
            i.compliance = t.interpolateString(o, function(e) {
                return i[e]
            });
            var n = e.layouts[e.layout];
            if (!n) {
                n = e.layouts.basic
            };
            return t.interpolateString(n, function(e) {
                return i[e]
            })
        };

        function o(i) {
            var o = this.options,
                r = document.createElement('div'),
                s = o.container && o.container.nodeType === 1 ? o.container : document.body;
            r.innerHTML = i;
            var n = r.children[0];
            n.style.display = 'none';
            if (t.hasClass(n, 'cc-window') && e.hasTransition) {
                t.addClass(n, 'cc-invisible')
            };
            this.onButtonClick = v.bind(this);
            n.addEventListener('click', this.onButtonClick);
            if (o.autoAttach) {
                if (!s.firstChild) {
                    s.appendChild(n)
                } else {
                    s.insertBefore(n, s.firstChild)
                }
            };
            return n
        };

        function v(n) {
            var o = t.traverseDOMPath(n.target, 'cc-btn') || n.target;
            if (t.hasClass(o, 'cc-btn')) {
                var s = o.className.match(new RegExp('\\bcc-(' + i.join('|') + ')\\b')),
                    r = (s && s[1]) || !1;
                if (r) {
                    this.setStatus(r);
                    this.close(!0)
                }
            };
            if (t.hasClass(o, 'cc-close')) {
                this.setStatus(e.status.dismiss);
                this.close(!0)
            };
            if (t.hasClass(o, 'cc-revoke')) {
                this.revokeChoice()
            }
        };

        function f(e) {
            var n = t.hash(JSON.stringify(e)),
                o = 'cc-color-override-' + n,
                i = t.isPlainObject(e);
            this.customStyleSelector = i ? o : null;
            if (i) {
                l(n, e, '.' + o)
            };
            return i
        };

        function l(i, n, o) {
            if (e.customStyles[i]) {
                ++e.customStyles[i].references;
                return
            };
            var c = {};
            var r = n.popup,
                s = n.button,
                a = n.highlight;
            if (r) {
                r.text = r.text ? r.text : t.getContrast(r.background);
                r.link = r.link ? r.link : r.text;
                c[o + '.cc-window'] = ['color: ' + r.text, 'background-color: ' + r.background];
                c[o + '.cc-revoke'] = ['color: ' + r.text, 'background-color: ' + r.background];
                c[o + ' .cc-link,' + o + ' .cc-link:active,' + o + ' .cc-link:visited'] = ['color: ' + r.link];
                if (s) {
                    s.text = s.text ? s.text : t.getContrast(s.background);
                    s.border = s.border ? s.border : 'transparent';
                    c[o + ' .cc-btn'] = ['color: ' + s.text, 'border-color: ' + s.border, 'background-color: ' + s.background];
                    if (s.padding) {
                        c[o + ' .cc-btn'].push('padding: ' + s.padding)
                    };
                    if (s.background != 'transparent') {
                        c[o + ' .cc-btn:hover, ' + o + ' .cc-btn:focus'] = ['background-color: ' + (s.hover || p(s.background))]
                    };
                    if (a) {
                        a.text = a.text ? a.text : t.getContrast(a.background);
                        a.border = a.border ? a.border : 'transparent';
                        c[o + ' .cc-highlight .cc-btn:first-child'] = ['color: ' + a.text, 'border-color: ' + a.border, 'background-color: ' + a.background]
                    } else {
                        c[o + ' .cc-highlight .cc-btn:first-child'] = ['color: ' + r.text]
                    }
                }
            };
            var u = document.createElement('style');
            document.head.appendChild(u);
            e.customStyles[i] = {
                references: 1,
                element: u.sheet
            };
            var h = -1;
            for (var l in c) {
                if (c.hasOwnProperty(l)) {
                    u.sheet.insertRule(l + '{' + c[l].join(';') + '}', ++h)
                }
            }
        };

        function p(e) {
            e = t.normaliseHex(e);
            if (e == '000000') {
                return '#222'
            };
            return t.getLuminance(e)
        };

        function h(i) {
            if (t.isPlainObject(i)) {
                var s = t.hash(JSON.stringify(i)),
                    o = e.customStyles[s];
                if (o && !--o.references) {
                    var n = o.element.ownerNode;
                    if (n && n.parentNode) {
                        n.parentNode.removeChild(n)
                    };
                    e.customStyles[s] = null
                }
            }
        };

        function c(e, t) {
            for (var n = 0, o = e.length; n < o; ++n) {
                var i = e[n];
                if ((i instanceof RegExp && i.test(t)) || (typeof i == 'string' && i.length && i === t)) {
                    return !0
                }
            };
            return !1
        };

        function u() {
            var s = this.setStatus.bind(this),
                r = this.close.bind(this),
                a = this.options.dismissOnTimeout;
            if (typeof a == 'number' && a >= 0) {
                this.dismissTimeout = window.setTimeout(function() {
                    s(e.status.dismiss);
                    r(!0)
                }, Math.floor(a))
            };
            var o = this.options.dismissOnScroll;
            if (typeof o == 'number' && o >= 0) {
                var n = function(t) {
                    if (window.pageYOffset > Math.floor(o)) {
                        s(e.status.dismiss);
                        r(!0);
                        window.removeEventListener('scroll', n);
                        this.onWindowScroll = null
                    }
                };
                if (this.options.enabled) {
                    this.onWindowScroll = n;
                    window.addEventListener('scroll', n)
                }
            };
            var l = this.options.dismissOnWindowClick,
                c = this.options.ignoreClicksFrom;
            if (l) {
                var i = function(n) {
                    var o = !1,
                        u = n.path.length,
                        h = c.length;
                    for (var l = 0; l < u; l++) {
                        if (o) continue;
                        for (var a = 0; a < h; a++) {
                            if (o) continue;
                            o = t.hasClass(n.path[l], c[a])
                        }
                    };
                    if (!o) {
                        s(e.status.dismiss);
                        r(!0);
                        window.removeEventListener('click', i);
                        this.onWindowClick = null
                    }
                }.bind(this);
                if (this.options.enabled) {
                    this.onWindowClick = i;
                    window.addEventListener('click', i)
                }
            }
        };

        function y() {
            if (this.options.type != 'info') this.options.revokable = !0;
            if (t.isMobile()) this.options.animateRevokable = !1;
            if (this.options.revokable) {
                var i = a.call(this);
                if (this.options.animateRevokable) {
                    i.push('cc-animate')
                };
                if (this.customStyleSelector) {
                    i.push(this.customStyleSelector)
                };
                var s = this.options.revokeBtn.replace('{{classes}}', i.join(' ')).replace('{{policy}}', this.options.content.policy);
                this.revokeBtn = o.call(this, s);
                var e = this.revokeBtn;
                if (this.options.animateRevokable) {
                    var r = !1,
                        n = t.throttle(function(i) {
                            var n = !1,
                                o = 20,
                                s = window.innerHeight - 20;
                            if (t.hasClass(e, 'cc-top') && i.clientY < o) n = !0;
                            if (t.hasClass(e, 'cc-bottom') && i.clientY > s) n = !0;
                            if (n) {
                                if (!t.hasClass(e, 'cc-active')) {
                                    t.addClass(e, 'cc-active')
                                }
                            } else {
                                if (t.hasClass(e, 'cc-active')) {
                                    t.removeClass(e, 'cc-active')
                                }
                            }
                        }, 200);
                    this.onMouseMove = n;
                    window.addEventListener('mousemove', n)
                }
            }
        };
        return n
    })();
    e.Location = (function() {
        var n = {
            timeout: 5000,
            services: ['ipinfo'],
            serviceDefinitions: {
                ipinfo: function() {
                    return {
                        url: '//ipinfo.io',
                        headers: ['Accept: application/json'],
                        callback: function(e, t) {
                            try {
                                var n = JSON.parse(t);
                                return n.error ? i(n) : {
                                    code: n.country
                                }
                            } catch (o) {
                                return i({
                                    error: 'Invalid response (' + o + ')'
                                })
                            }
                        }
                    }
                },
                ipinfodb: function(e) {
                    return {
                        url: '//api.ipinfodb.com/v3/ip-country/?key={api_key}&format=json&callback={callback}',
                        isScript: !0,
                        callback: function(e, t) {
                            try {
                                var n = JSON.parse(t);
                                return n.statusCode == 'ERROR' ? i({
                                    error: n.statusMessage
                                }) : {
                                    code: n.countryCode
                                }
                            } catch (o) {
                                return i({
                                    error: 'Invalid response (' + o + ')'
                                })
                            }
                        }
                    }
                },
                maxmind: function() {
                    return {
                        // url: '//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js',
                        isScript: !0,
                        callback: function(e) {
                            if (!window.geoip2) {
                                e(new Error('Unexpected response format. The downloaded script should have exported `geoip2` to the global scope'));
                                return
                            };
                            geoip2.country(function(t) {
                                try {
                                    e({
                                        code: t.country.iso_code
                                    })
                                } catch (n) {
                                    e(i(n))
                                }
                            }, function(t) {
                                e(i(t))
                            })
                        }
                    }
                }
            }
        };

        function e(e) {
            t.deepExtend((this.options = {}), n);
            if (t.isPlainObject(e)) {
                t.deepExtend(this.options, e)
            };
            this.currentServiceIndex = -1
        };
        e.prototype.getNextService = function() {
            var e;
            do {
                e = this.getServiceByIdx(++this.currentServiceIndex)
            }
            while (this.currentServiceIndex < this.options.services.length && !e);
            return e
        };
        e.prototype.getServiceByIdx = function(e) {
            var i = this.options.services[e];
            if (typeof i === 'function') {
                var n = i();
                if (n.name) {
                    t.deepExtend(n, this.options.serviceDefinitions[n.name](n))
                };
                return n
            };
            if (typeof i === 'string') {
                return this.options.serviceDefinitions[i]()
            };
            if (t.isPlainObject(i)) {
                return this.options.serviceDefinitions[i.name](i)
            };
            return null
        };
        e.prototype.locate = function(e, t) {
            var i = this.getNextService();
            if (!i) {
                t(new Error('No services to run'));
                return
            };
            this.callbackComplete = e;
            this.callbackError = t;
            this.runService(i, this.runNextServiceOnError.bind(this))
        };
        e.prototype.setupUrl = function(e) {
            var t = this.getCurrentServiceOpts();
            return e.url.replace(/\{(.*?)\}/g, function(i, n) {
                if (n === 'callback') {
                    var o = 'callback' + Date.now();
                    window[o] = function(t) {
                        e.__JSONP_DATA = JSON.stringify(t)
                    };
                    return o
                };
                if (n in t.interpolateUrl) {
                    return t.interpolateUrl[n]
                }
            })
        };
        e.prototype.runService = function(e, t) {
            var i = this;
            if (!e || !e.url || !e.callback) {
                return
            };
            var n = e.isScript ? o : s,
                r = this.setupUrl(e);
            n(r, function(n) {
                var o = n ? n.responseText : '';
                if (e.__JSONP_DATA) {
                    o = e.__JSONP_DATA;
                    delete e.__JSONP_DATA
                };
                i.runServiceCallback.call(i, t, e, o)
            }, this.options.timeout, e.data, e.headers)
        };
        e.prototype.runServiceCallback = function(e, t, i) {
            var o = this,
                s = function(t) {
                    if (!n) {
                        o.onServiceResult.call(o, e, t)
                    }
                },
                n = t.callback(s, i);
            if (n) {
                this.onServiceResult.call(this, e, n)
            }
        };
        e.prototype.onServiceResult = function(e, t) {
            if (t instanceof Error || (t && t.error)) {
                e.call(this, t, null)
            } else {
                e.call(this, null, t)
            }
        };
        e.prototype.runNextServiceOnError = function(e, t) {
            if (e) {
                this.logError(e);
                var i = this.getNextService();
                if (i) {
                    this.runService(i, this.runNextServiceOnError.bind(this))
                } else {
                    this.completeService.call(this, this.callbackError, new Error('All services failed'))
                }
            } else {
                this.completeService.call(this, this.callbackComplete, t)
            }
        };
        e.prototype.getCurrentServiceOpts = function() {
            var e = this.options.services[this.currentServiceIndex];
            if (typeof e == 'string') {
                return {
                    name: e
                }
            };
            if (typeof e == 'function') {
                return e()
            };
            if (t.isPlainObject(e)) {
                return e
            };
            return {}
        };
        e.prototype.completeService = function(e, t) {
            this.currentServiceIndex = -1;
            e && e(t)
        };
        e.prototype.logError = function(e) {
            var t = this.currentServiceIndex,
                i = this.getServiceByIdx(t);
            console.warn('The service[' + t + '] (' + i.url + ') responded with the following error', e)
        };

        function o(e, t, n) {
            var o, i = document.createElement('script');
            i.type = 'text/' + (e.type || 'javascript');
            i.src = e.src || e;
            i.async = !1;
            i.onreadystatechange = i.onload = function() {
                var e = i.readyState;
                clearTimeout(o);
                if (!t.done && (!e || /loaded|complete/.test(e))) {
                    t.done = !0;
                    t();
                    i.onreadystatechange = i.onload = null
                }
            };
            document.body.appendChild(i);
            o = setTimeout(function() {
                t.done = !0;
                t();
                i.onreadystatechange = i.onload = null
            }, n)
        };

        function s(e, t, n, s, o) {
            var i = new(window.XMLHttpRequest || window.ActiveXObject)('MSXML2.XMLHTTP.3.0');
            i.open(s ? 'POST' : 'GET', e, 1);
            i.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            if (Array.isArray(o)) {
                for (var r = 0, c = o.length; r < c; ++r) {
                    var a = o[r].split(':', 2);
                    i.setRequestHeader(a[0].replace(/^\s+|\s+$/g, ''), a[1].replace(/^\s+|\s+$/g, ''))
                }
            };
            if (typeof t == 'function') {
                i.onreadystatechange = function() {
                    if (i.readyState > 3) {
                        t(i)
                    }
                }
            };
            i.send(s)
        };

        function i(e) {
            return new Error('Error [' + (e.code || 'UNKNOWN') + ']: ' + e.error)
        };
        return e
    })();
    e.Law = (function() {
        var i = {
            regionalLaw: !0,
            hasLaw: ['AT', 'BE', 'BG', 'HR', 'CZ', 'CY', 'DK', 'EE', 'FI', 'FR', 'DE', 'EL', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'SK', 'ES', 'SE', 'GB', 'UK', 'GR', 'EU'],
            revokable: ['HR', 'CY', 'DK', 'EE', 'FR', 'DE', 'LV', 'LT', 'NL', 'PT', 'ES'],
            explicitAction: ['HR', 'IT', 'ES']
        };

        function e(e) {
            this.initialise.apply(this, arguments)
        };
        e.prototype.initialise = function(e) {
            t.deepExtend((this.options = {}), i);
            if (t.isPlainObject(e)) {
                t.deepExtend(this.options, e)
            }
        };
        e.prototype.get = function(e) {
            var t = this.options;
            return {
                hasLaw: t.hasLaw.indexOf(e) >= 0,
                revokable: t.revokable.indexOf(e) >= 0,
                explicitAction: t.explicitAction.indexOf(e) >= 0
            }
        };
        e.prototype.applyLaw = function(e, t) {
            var i = this.get(t);
            if (!i.hasLaw) {
                e.enabled = !1;
                if (typeof e.onNoCookieLaw === 'function') {
                    e.onNoCookieLaw(t, i)
                }
            };
            if (this.options.regionalLaw) {
                if (i.revokable) {
                    e.revokable = !0
                };
                if (i.explicitAction) {
                    e.dismissOnScroll = !1;
                    e.dismissOnTimeout = !1
                }
            };
            return e
        };
        return e
    })();
    e.initialise = function(i, n, o) {
        var c = new e.Law(i.law);
        if (!n) n = function() {};
        if (!o) o = function() {};
        var s = Object.keys(e.status),
            r = t.getCookie('cookieconsent_status'),
            a = s.indexOf(r) >= 0;
        if (a) {
            n(new e.Popup(i));
            return
        };
        e.getCountryCode(i, function(t) {
            delete i.law;
            delete i.location;
            if (t.code) {
                i = c.applyLaw(i, t.code)
            };
            n(new e.Popup(i))
        }, function(t) {
            delete i.law;
            delete i.location;
            o(t, new e.Popup(i))
        })
    };
    e.getCountryCode = function(t, i, n) {
        if (t.law && t.law.countryCode) {
            i({
                code: t.law.countryCode
            });
            return
        };
        if (t.location) {
            var o = new e.Location(t.location);
            o.locate(function(e) {
                i(e || {})
            }, n);
            return
        };
        i({})
    };
    e.utils = t;
    e.hasInitialised = !0;
    window.cookieconsent = e
})(window.cookieconsent || {});;
(function(e, a) {
    'use strict';
    a.behaviors.site = {
        attach: function(a, n) {
            e(window).scroll(function() {
                t(e(this))
            });
            e(document).ready(function() {
                t();
                if (e('.heading-sidebar-right').length) {
                    e('body').addClass('heading-sidebar-right-body')
                };
                if (e('.fullwidth-no-sidebar').length && !e('.path-laan-penge-service-finans').length) {
                    e('body').addClass('fullwidth-body')
                };
                if (e('body').hasClass('path-laan-penge-service-finans')) {
                    e('.logo #block-lendme-branding img').attr({
                        src: '/themes/lendme/images/Service-Finans-Logo.png',
                        alt: 'Service Finans'
                    })
                };
                if (e('body').hasClass('path-laan-penge-service-finans')) {
                    e('.logo #block-lendme-branding a').removeAttr('href')
                };
                if (e('.path-ansoeg-om-forbrugslaan-skjult-adresse').length) {
                    e('.header-content').append('<div class="moveit"><div class="moveit-inner"><h1></h1></div></div>');
                    e('#lendme-simple-form-address-form p:first').appendTo('.header-content .moveit-inner h1')
                };
                if (e('.sub-page-fullwidth').length) {
                    e('#block-sliders').prepend(e('#block-breadcrumbs'))
                } else {
                    e('.layout-container').prepend(e('#block-breadcrumbs'))
                };
                e('.remove-min + p').remove();
                if (!e('.path-my .min-b').length) {
                    e('.path-my a.min-a').addClass('only-one')
                };
                if (e('.path-laan-penge-service-finans').length) {
                    e('#page-container .header-bar .page-navigation ul li:nth-of-type(2)').removeAttr('href');
                    e('#page-container .header-bar .page-navigation ul li:nth-of-type(2)').html('<a>TLF: 70 60 62 62</a>')
                };
                if (!e('.path-ansoeg-om-forbrugslaan-sektion-to .bank-accept-text p').length) {
                    var n = e('.path-ansoeg-om-forbrugslaan-sektion-to .bank-accept-text').html();
                    e('.path-ansoeg-om-forbrugslaan-sektion-to .bank-accept-text').html('<p>' + n + '</p>')
                };
                if (!e('.path-ansoeg-om-forbrugslaan-sektion-to .personal-data-accept-text p').length) {
                    var a = e('.path-ansoeg-om-forbrugslaan-sektion-to .personal-data-accept-text').html();
                    e('.path-ansoeg-om-forbrugslaan-sektion-to .personal-data-accept-text').html('<p>' + a + '</p>')
                };
                e('[class$="path-ansoeg-om-forbrugslaan-sektion-to"] #block-sliders, [class*=" path-ansoeg-om-forbrugslaan-sektion-to"] #block-sliders, [class*=" path-ansoeg-om-forbrugslaan-sektion-to"] #block-sliders-2').sticky({
                    topSpacing: 0,
                    bottomSpacing: 1450
                })
            });
            e('#block-styrpasagerne p a, .apply-now-mobile.show-on-mobile, .how-to.apply-to-loan').on('click', function() {
                e('html,body').animate({
                    scrollTop: e('#block-sliders').offset().top - e('.page-header .section').outerHeight()
                }, 'slow');
                if (e('body').hasClass('is-open')) {
                    e('.menu-icon.menu-icon-cross').click()
                };
                return !1
            });
            e('.authenticated a:contains("Ansøg nu")').on('click', function() {
                e('.apply-now-popup').fadeIn('fast');
                return !1
            });
            e('.apply-now-popup-buttons-ok').on('click', function() {
                e('.apply-now-popup').fadeOut('fast')
            });
            e('.how-to.read-more').on('click', function() {
                e('html,body').animate({
                    scrollTop: e('#page-content').offset().top - e('.page-header .section').outerHeight()
                }, 'slow')
            });
            if (e('.path-frontpage').length || e('.path-laan-andelsboliglaan').length || e('.path-laan-billaan').length || e('.path-laan-boliglaan').length || e('.path-laan-ferielaan').length || e('.path-laan-forbrugslaan').length || e('.path-laan-kviklaan').length || e('.path-laan-laaneberegner').length || e('.path-laan-realkreditlaan').length || e('.path-laan-laan-penge').length || e('.path-laan-samlelaan').length) {
                e('#page-header .header-bar .page-navigation a.applynow').on('click', function() {
                    e('html,body').animate({
                        scrollTop: e('#block-sliders').offset().top - e('.page-header .section').outerHeight()
                    }, 'slow');
                    e('.menu-icon.menu-icon-cross').click();
                    return !1
                })
            };
            e('#page-header .header-bar .page-navigation a.applynow').removeAttr('href');
            e('#page-header .header-bar .page-navigation a.applynow').on('click', function() {
                var a = '/';
                if (e('#block-sliders').length) {
                    e('html,body').animate({
                        scrollTop: e('#block-sliders').offset().top - e('.page-header .section').outerHeight()
                    }, 'slow')
                } else {
                    window.location.replace(a)
                }
            });
            var i = e('#youtube-embed-video');
            e('.how-to.campaign-film').on('click', function() {
                e('.youtube-video').fadeIn('fast');
                if (!e('#youtube-embed-video').length) {
                    e('.youtube-video-inner').append(i)
                }
            });
            e('.youtube-video, .youtube-close').on('click', function() {
                e('.youtube-video').fadeOut('fast');
                e('#youtube-embed-video').remove()
            });
            e(':checkbox:not(.initialized)').each(function() {
                e(this).labelauty({
                    label: !1
                });
                e(this).addClass('initialized')
            });

            function t(a) {
                var t = 100,
                    n = e('body');
                a = (a === undefined) ? e(window) : a;
                if (a.scrollTop() >= t) {
                    n.addClass('sticky')
                } else {
                    n.removeClass('sticky')
                }
            };

            function l() {
                var e, n = document.createElement('fakeelement'),
                    a = {
                        'animation': 'animationend',
                        'OAnimation': 'oAnimationEnd',
                        'MozAnimation': 'animationend',
                        'WebkitAnimation': 'webkitAnimationEnd'
                    };
                for (e in a) {
                    if (n.style[e] !== undefined) {
                        return a[e]
                    }
                }
            };
            var o = l();
            e('#block-sikkertenkeltgratis .three-bullets div').one(o, function(a) {
                e('#block-sikkertenkeltgratis .three-bullets>div').matchHeight()
            });
            e('.block-region-din-tryghed').append(e('#block-views-block-blog-block-1'))
        }
    };
    e('.menu-icon').click(function() {
        e('body').toggleClass('is-open')
    });
    e('.collapsible.views-row').click(function() {
        e(this).toggleClass('openfaq')
    });
    e('.Lendme').addClass('catactive');
    e('.faq-categories a').click(function() {
        (e('.faq-categories a').not(e(this))).removeClass('catactive');
        e(this).toggleClass('catactive')
    });
    e('.collapsible.Lendme').addClass('show');
    e('.faq-categories a.Lån').click(function() {
        e('.collapsible.Lån').toggleClass('show')
    });
    e('.faq-categories a.Lendme').click(function() {
        e('.collapsible.Lendme').toggleClass('show')
    });
    e('.faq-categories a.Banker').click(function() {
        e('.collapsible.Banker').toggleClass('show')
    });
    e(function() {
        e('input.lendme-element.form-text:first').focus()
    }, 100);
    e('.moveit h1, .moveit h2').each(function() {
        e('span.moveit').detach().appendTo('.header-content')
    });
    e('body.path-node .article-content-wrapper .image').detach().appendTo('.page-header');
    e('body.path-node').addClass('hasContent');
    e('form#user-login-form a').detach().insertAfter('#user-login-form');
    var n = e('.page-navigation li ul');
    n.parent().addClass('hassubmenu');
    n.prepend('<b class=\'lendme-down-open-big\'></b>');
    e('ul b').click(function() {
        e(this).parent().toggleClass('sub-is-open')
    });
    e('.sys-messages').detach().prependTo('.path-application #fieldset_section_wrapper');
    jQuery('#app-progress-body').on('mousedown', '.arrow-right.last', function() {
        jQuery('body').animate({
            scrollTop: jQuery('#edit-submit').offset().top - 350
        }, 2200)
    });
    e('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var a = e(this.hash);
            a = a.length ? a : e('[name=' + this.hash.slice(1) + ']');
            if (a.length && e('body').hasClass('path-gulgratis')) {
                e('html, body').animate({
                    scrollTop: a.offset().top - 30
                }, 500);
                return !1
            } else if (a.length) {
                e('html, body').animate({
                    scrollTop: a.offset().top - 110
                }, 500);
                return !1
            }
        }
    });
    e('#btn-scrollto-apply').on('click', function() {
        e('#edit-firstname').focus()
    });
    e.fn.delayKeyup = function(a, n) {
        var t = 0;
        e(this).keyup(function() {
            clearTimeout(t);
            t = setTimeout(a, n)
        });
        return e(this)
    };
    a.behaviors.applyNowButtomBehaviour = {
        attach: function(a, t) {
            var o = e('#lendme-simple-form-basic-application, #lendme-simple-form-application, .user-login-form, #lendme-password-reset', a),
                i = e('.slider-container', a),
                n = e('#page-container .apply-now-mobile', a);
            if (n.length) {
                o.find('.js-form-item input').on('focus change', function() {
                    if (n.is(':visible')) {
                        n.hide()
                    }
                });
                o.find('.js-form-item select').on('change', function() {
                    if (n.is(':visible')) {
                        n.hide()
                    }
                });
                setTimeout(function() {
                    i.find('.range-slider .irs').on('click', function() {
                        if (n.is(':visible')) {
                            n.hide()
                        }
                    })
                }, 0)
            }
        }
    }
}(jQuery, Drupal));
wow = new WOW({
    boxClass: 'wow',
    animateClass: 'animated',
    offset: 100,
    mobile: !1,
    live: !0
});
wow.init();;
(function(B) {
    'use strict';
    Drupal.behaviors.masonryblog = {
        attach: function(E, o) {
            var A = ['#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675', '#5BA881', '#4BB7DE', '#3D6675'];
            B('.path-blog .views-row').each(function(D) {
                B(this).css('color', A[D])
            });
            var D = B('.path-blog .views-element-container').masonry({
                itemSelector: '.path-blog .views-row',
                gutter: 5,
                columnWidth: '.views-row'
            });
            D.imagesLoaded().progress(function() {
                D.masonry('layout')
            });
            D.masonry('reloadItems');
            B(window).resize(function() {
                B('#container').masonry('reloadItems')
            })
        }
    }
}(jQuery));;
(function(a) {
    'use strict';
    Drupal.behaviors.owlslider = {
        attach: function(e, s) {
            if (window.innerWidth < 768) {
                a('.frontpage-banks .frontpage-banks-items').owlCarousel({
                    loop: !0,
                    items: 5,
                    margin: 20,
                    autoplay: !0,
                    autoplayTimeout: 2000,
                    autoplayHoverPause: !1,
                    responsiveClass: !0,
                    responsive: {
                        768: {
                            items: 3,
                            nav: !1
                        },
                        0: {
                            items: 1,
                            nav: !1
                        }
                    }
                });
                a('#block-testimonials .testamonial-slide, #testimonials .testamonial-slide').owlCarousel({
                    loop: !0,
                    items: 4,
                    margin: 20,
                    autoplay: !1,
                    autoplayTimeout: 3500,
                    autoplayHoverPause: !1,
                    navText: ['', ''],
                    responsiveClass: !0,
                    responsive: {
                        768: {
                            items: 1,
                            nav: !1
                        },
                        0: {
                            items: 1,
                            nav: !1
                        }
                    }
                })
            }
        }
    }
}(jQuery));;
(function(n) {
    'use strict';
    Drupal.behaviors.blog = {
        attach: function(e, o) {
            n(window).on('load resize scroll', function(e) {
                n('.path-node .image img').centerImage()
            })
        }
    }
}(jQuery));;
(function(n) {
    n(document).ready(function() {
        var e = n(window).width(),
            t = n('#block-lendme-content .frontpage-intro'),
            r = n('#block-lendme-content .block-region-right'),
            c = n('#block-lendme-content .gul-image'),
            i = ('.block-region-right');

        function o() {
            t.insertAfter(c)
        };
        if (e <= 720) {
            o()
        };
        n(window).resize(function() {
            e = n(window).width();
            if (e > 720 && t.parents(i).length) {
                return
            };
            if (e > 720 && !t.parents(i).length) {
                t.prependTo(r)
            };
            if (e <= 720) {
                o()
            }
        })
    })
})(jQuery);;
(function(s, e) {
    e.theme.progressBar = function(s) {
        return '<div id="' + s + '" class="progress" aria-live="polite"><div class="progress__label">&nbsp;</div><div class="progress__track"><div class="progress__bar"></div></div><div class="progress__percentage"></div><div class="progress__description">&nbsp;</div></div>'
    };
    e.ProgressBar = function(r, i, t, a) {
        this.id = r;
        this.method = t || 'GET';
        this.updateCallback = i;
        this.errorCallback = a;
        this.element = s(e.theme('progressBar', r))
    };
    s.extend(e.ProgressBar.prototype, {
        setProgress: function(e, r, i) {
            if (e >= 0 && e <= 100) {
                s(this.element).find('div.progress__bar').css('width', e + '%');
                s(this.element).find('div.progress__percentage').html(e + '%')
            };
            s('div.progress__description', this.element).html(r);
            s('div.progress__label', this.element).html(i);
            if (this.updateCallback) {
                this.updateCallback(e, r, this)
            }
        },
        startMonitoring: function(s, e) {
            this.delay = e;
            this.uri = s;
            this.sendPing()
        },
        stopMonitoring: function() {
            clearTimeout(this.timer);
            this.uri = null
        },
        sendPing: function() {
            if (this.timer) {
                clearTimeout(this.timer)
            };
            if (this.uri) {
                var r = this,
                    i = this.uri;
                if (i.indexOf('?') === -1) {
                    i += '?'
                } else {
                    i += '&'
                };
                i += '_format=json';
                s.ajax({
                    type: this.method,
                    url: i,
                    data: '',
                    dataType: 'json',
                    success: function(s) {
                        if (s.status === 0) {
                            r.displayError(s.data);
                            return
                        };
                        r.setProgress(s.percentage, s.message, s.label);
                        r.timer = setTimeout(function() {
                            r.sendPing()
                        }, r.delay)
                    },
                    error: function(s) {
                        var i = new e.AjaxError(s, r.uri);
                        r.displayError('<pre>' + i.message + '</pre>')
                    }
                })
            }
        },
        displayError: function(e) {
            var r = s('<div class="messages messages--error"></div>').html(e);
            s(this.element).before(r).hide();
            if (this.errorCallback) {
                this.errorCallback(this)
            }
        }
    })
})(jQuery, Drupal);;;

function _toConsumableArray(e) {
    if (Array.isArray(e)) {
        for (var t = 0, r = Array(e.length); t < e.length; t++) {
            r[t] = e[t]
        };
        return r
    } else {
        return Array.from(e)
    }
}(function(e, r, t, s) {
    t.behaviors.AJAX = {
        attach: function(r, s) {
            function a(r) {
                var a = s.ajax[r];
                if (typeof a.selector === 'undefined') {
                    a.selector = '#' + r
                };
                e(a.selector).once('drupal-ajax').each(function() {
                    a.element = this;
                    a.base = r;
                    t.ajax(a)
                })
            };
            Object.keys(s.ajax || {}).forEach(function(e) {
                return a(e)
            });
            t.ajax.bindAjaxLinks(document.body);
            e('.use-ajax-submit').once('ajax').each(function() {
                var r = {};
                r.url = e(this.form).attr('action');
                r.setClick = !0;
                r.event = 'click';
                r.progress = {
                    type: 'throbber'
                };
                r.base = e(this).attr('id');
                r.element = this;
                t.ajax(r)
            })
        },
        detach: function(e, r, s) {
            if (s === 'unload') {
                t.ajax.expired().forEach(function(e) {
                    t.ajax.instances[e.instanceIndex] = null
                })
            }
        }
    };
    t.AjaxError = function(r, s, a) {
        var i = void 0,
            n = void 0,
            o = void 0;
        if (r.status) {
            i = '\n' + t.t('An AJAX HTTP error occurred.') + '\n' + t.t('HTTP Result Code: !status', {
                '!status': r.status
            })
        } else {
            i = '\n' + t.t('An AJAX HTTP request terminated abnormally.')
        };
        i += '\n' + t.t('Debugging information follows.');
        var f = '\n' + t.t('Path: !uri', {
            '!uri': s
        });
        n = '';
        try {
            n = '\n' + t.t('StatusText: !statusText', {
                '!statusText': e.trim(r.statusText)
            })
        } catch (l) {};
        o = '';
        try {
            o = '\n' + t.t('ResponseText: !responseText', {
                '!responseText': e.trim(r.responseText)
            })
        } catch (l) {};
        o = o.replace(/<("[^"]*"|'[^']*'|[^'">])*>/gi, '');
        o = o.replace(/[\n]+\s+/g, '\n');
        var c = r.status === 0 ? '\n' + t.t('ReadyState: !readyState', {
            '!readyState': r.readyState
        }) : '';
        a = a ? '\n' + t.t('CustomMessage: !customMessage', {
            '!customMessage': a
        }) : '';
        this.message = i + f + n + a + o + c;
        this.name = 'AjaxError'
    };
    t.AjaxError.prototype = new Error();
    t.AjaxError.prototype.constructor = t.AjaxError;
    t.ajax = function(e) {
        if (arguments.length !== 1) {
            throw new Error('Drupal.ajax() function must be called with one configuration object only')
        };
        var a = e.base || !1,
            s = e.element || !1;
        delete e.base;
        delete e.element;
        if (!e.progress && !s) {
            e.progress = !1
        };
        var r = new t.Ajax(a, s, e);
        r.instanceIndex = t.ajax.instances.length;
        t.ajax.instances.push(r);
        return r
    };
    t.ajax.instances = [];
    t.ajax.expired = function() {
        return t.ajax.instances.filter(function(e) {
            return e && e.element !== !1 && !document.body.contains(e.element)
        })
    };
    t.ajax.bindAjaxLinks = function(r) {
        e(r).find('.use-ajax').once('ajax').each(function(r, s) {
            var a = e(s),
                o = {
                    progress: {
                        type: 'throbber'
                    },
                    dialogType: a.data('dialog-type'),
                    dialog: a.data('dialog-options'),
                    dialogRenderer: a.data('dialog-renderer'),
                    base: a.attr('id'),
                    element: s
                };
            var i = a.attr('href');
            if (i) {
                o.url = i;
                o.event = 'click'
            };
            t.ajax(o)
        })
    };
    t.Ajax = function(r, a, o) {
        var f = {
            event: a ? 'mousedown' : null,
            keypress: !0,
            selector: r ? '#' + r : null,
            effect: 'none',
            speed: 'none',
            method: 'replaceWith',
            progress: {
                type: 'throbber',
                message: t.t('Please wait...')
            },
            submit: {
                js: !0
            }
        };
        e.extend(this, f, o);
        this.commands = new t.AjaxCommands();
        this.instanceIndex = !1;
        if (this.wrapper) {
            this.wrapper = '#' + this.wrapper
        };
        this.element = a;
        this.element_settings = o;
        this.elementSettings = o;
        if (this.element && this.element.form) {
            this.$form = e(this.element.form)
        };
        if (!this.url) {
            var l = e(this.element);
            if (l.is('a')) {
                this.url = l.attr('href')
            } else if (this.element && a.form) {
                this.url = this.$form.attr('action')
            }
        };
        var c = this.url;
        this.url = this.url.replace(/\/nojs(\/|$|\?|#)/g, '/ajax$1');
        if (s.ajaxTrustedUrl[c]) {
            s.ajaxTrustedUrl[this.url] = !0
        };
        var i = this;
        i.options = {
            url: i.url,
            data: i.submit,
            beforeSerialize: function(e, t) {
                return i.beforeSerialize(e, t)
            },
            beforeSubmit: function(e, t, r) {
                i.ajaxing = !0;
                return i.beforeSubmit(e, t, r)
            },
            beforeSend: function(e, t) {
                i.ajaxing = !0;
                return i.beforeSend(e, t)
            },
            success: function(r, a, o) {
                if (typeof r === 'string') {
                    r = e.parseJSON(r)
                };
                if (r !== null && !s.ajaxTrustedUrl[i.url]) {
                    if (o.getResponseHeader('X-Drupal-Ajax-Token') !== '1') {
                        var n = t.t('The response failed verification so will not be processed.');
                        return i.error(o, i.url, n)
                    }
                };
                return i.success(r, a)
            },
            complete: function(e, t) {
                i.ajaxing = !1;
                if (t === 'error' || t === 'parsererror') {
                    return i.error(e, i.url)
                }
            },
            dataType: 'json',
            type: 'POST'
        };
        if (o.dialog) {
            i.options.data.dialogOptions = o.dialog
        };
        if (i.options.url.indexOf('?') === -1) {
            i.options.url += '?'
        } else {
            i.options.url += '&'
        };
        var n = 'drupal_' + (o.dialogType || 'ajax');
        if (o.dialogRenderer) {
            n += '.' + o.dialogRenderer
        };
        i.options.url += t.ajax.WRAPPER_FORMAT + '=' + n;
        e(i.element).on(o.event, function(e) {
            if (!s.ajaxTrustedUrl[i.url] && !t.url.isLocal(i.url)) {
                throw new Error(t.t('The callback URL is not local and not trusted: !url', {
                    '!url': i.url
                }))
            };
            return i.eventResponse(this, e)
        });
        if (o.keypress) {
            e(i.element).on('keypress', function(e) {
                return i.keypressResponse(this, e)
            })
        };
        if (o.prevent) {
            e(i.element).on(o.prevent, !1)
        }
    };
    t.ajax.WRAPPER_FORMAT = '_wrapper_format';
    t.Ajax.AJAX_REQUEST_PARAMETER = '_drupal_ajax';
    t.Ajax.prototype.execute = function() {
        if (this.ajaxing) {
            return
        };
        try {
            this.beforeSerialize(this.element, this.options);
            return e.ajax(this.options)
        } catch (t) {
            this.ajaxing = !1;
            r.alert('An error occurred while attempting to process ' + this.options.url + ': ' + t.message);
            return e.Deferred().reject()
        }
    };
    t.Ajax.prototype.keypressResponse = function(t, r) {
        var s = this;
        if (r.which === 13 || r.which === 32 && t.type !== 'text' && t.type !== 'textarea' && t.type !== 'tel' && t.type !== 'number') {
            r.preventDefault();
            r.stopPropagation();
            e(t).trigger(s.elementSettings.event)
        }
    };
    t.Ajax.prototype.eventResponse = function(t, s) {
        s.preventDefault();
        s.stopPropagation();
        var a = this;
        if (a.ajaxing) {
            return
        };
        try {
            if (a.$form) {
                if (a.setClick) {
                    t.form.clk = t
                };
                a.$form.ajaxSubmit(a.options)
            } else {
                a.beforeSerialize(a.element, a.options);
                e.ajax(a.options)
            }
        } catch (o) {
            a.ajaxing = !1;
            r.alert('An error occurred while attempting to process ' + a.options.url + ': ' + o.message)
        }
    };
    t.Ajax.prototype.beforeSerialize = function(e, r) {
        if (this.$form) {
            var o = this.settings || s;
            t.detachBehaviors(this.$form.get(0), o, 'serialize')
        };
        r.data[t.Ajax.AJAX_REQUEST_PARAMETER] = 1;
        var a = s.ajaxPageState;
        r.data['ajax_page_state[theme]'] = a.theme;
        r.data['ajax_page_state[theme_token]'] = a.theme_token;
        r.data['ajax_page_state[libraries]'] = a.libraries
    };
    t.Ajax.prototype.beforeSubmit = function(e, t, r) {};
    t.Ajax.prototype.beforeSend = function(t, r) {
        if (this.$form) {
            r.extraData = r.extraData || {};
            r.extraData.ajax_iframe_upload = '1';
            var a = e.fieldValue(this.element);
            if (a !== null) {
                r.extraData[this.element.name] = a
            }
        };
        e(this.element).prop('disabled', !0);
        if (!this.progress || !this.progress.type) {
            return
        };
        var s = 'setProgressIndicator' + this.progress.type.slice(0, 1).toUpperCase() + this.progress.type.slice(1).toLowerCase();
        if (s in this && typeof this[s] === 'function') {
            this[s].call(this)
        }
    };
    t.Ajax.prototype.setProgressIndicatorBar = function() {
        var r = new t.ProgressBar('ajax-progress-' + this.element.id, e.noop, this.progress.method, e.noop);
        if (this.progress.message) {
            r.setProgress(-1, this.progress.message)
        };
        if (this.progress.url) {
            r.startMonitoring(this.progress.url, this.progress.interval || 1500)
        };
        this.progress.element = e(r.element).addClass('ajax-progress ajax-progress-bar');
        this.progress.object = r;
        e(this.element).after(this.progress.element)
    };
    t.Ajax.prototype.setProgressIndicatorThrobber = function() {
        this.progress.element = e('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
        if (this.progress.message) {
            this.progress.element.find('.throbber').after('<div class="message">' + this.progress.message + '</div>')
        };
        e(this.element).after(this.progress.element)
    };
    t.Ajax.prototype.setProgressIndicatorFullscreen = function() {
        this.progress.element = e('<div class="ajax-progress ajax-progress-fullscreen">&nbsp;</div>');
        e('body').after(this.progress.element)
    };
    t.Ajax.prototype.success = function(r, a) {
        var n = this;
        if (this.progress.element) {
            e(this.progress.element).remove()
        };
        if (this.progress.object) {
            this.progress.object.stopMonitoring()
        };
        e(this.element).prop('disabled', !1);
        var l = e(this.element).parents('[data-drupal-selector]').addBack().toArray(),
            c = !1;
        Object.keys(r || {}).forEach(function(e) {
            if (r[e].command && n.commands[r[e].command]) {
                n.commands[r[e].command](n, r[e], a);
                if (r[e].command === 'invoke' && r[e].method === 'focus') {
                    c = !0
                }
            }
        });
        if (!c && this.element && !e(this.element).data('disable-refocus')) {
            var o = !1;
            for (var i = l.length - 1; !o && i > 0; i--) {
                o = document.querySelector('[data-drupal-selector="' + l[i].getAttribute('data-drupal-selector') + '"]')
            };
            if (o) {
                e(o).trigger('focus')
            }
        };
        if (this.$form) {
            var f = this.settings || s;
            t.attachBehaviors(this.$form.get(0), f)
        };
        this.settings = null
    };
    t.Ajax.prototype.getEffect = function(e) {
        var r = e.effect || this.effect,
            s = e.speed || this.speed,
            t = {};
        if (r === 'none') {
            t.showEffect = 'show';
            t.hideEffect = 'hide';
            t.showSpeed = ''
        } else if (r === 'fade') {
            t.showEffect = 'fadeIn';
            t.hideEffect = 'fadeOut';
            t.showSpeed = s
        } else {
            t.showEffect = r + 'Toggle';
            t.hideEffect = r + 'Toggle';
            t.showSpeed = s
        };
        return t
    };
    t.Ajax.prototype.error = function(r, a, o) {
        if (this.progress.element) {
            e(this.progress.element).remove()
        };
        if (this.progress.object) {
            this.progress.object.stopMonitoring()
        };
        e(this.wrapper).show();
        e(this.element).prop('disabled', !1);
        if (this.$form) {
            var i = this.settings || s;
            t.attachBehaviors(this.$form.get(0), i)
        };
        throw new t.AjaxError(r, a, o)
    };
    t.AjaxCommands = function() {};
    t.AjaxCommands.prototype = {
        insert: function(r, a, o) {
            var c = a.selector ? e(a.selector) : e(r.wrapper),
                f = a.method || r.method,
                n = r.getEffect(a),
                l = void 0,
                d = e('<div></div>').html(a.data),
                i = d.contents();
            if (i.length !== 1 || i.get(0).nodeType !== 1) {
                i = d
            };
            switch (f) {
                case 'html':
                case 'replaceWith':
                case 'replaceAll':
                case 'empty':
                case 'remove':
                    l = a.settings || r.settings || s;
                    t.detachBehaviors(c.get(0), l)
            };
            c[f](i);
            if (n.showEffect !== 'show') {
                i.hide()
            };
            if (i.find('.ajax-new-content').length > 0) {
                i.find('.ajax-new-content').hide();
                i.show();
                i.find('.ajax-new-content')[n.showEffect](n.showSpeed)
            } else if (n.showEffect !== 'show') {
                i[n.showEffect](n.showSpeed)
            };
            if (i.parents('html').length > 0) {
                l = a.settings || r.settings || s;
                t.attachBehaviors(i.get(0), l)
            }
        },
        remove: function(r, a, o) {
            var i = a.settings || r.settings || s;
            e(a.selector).each(function() {
                t.detachBehaviors(this, i)
            }).remove()
        },
        changed: function(r, s, a) {
            var o = e(s.selector);
            if (!o.hasClass('ajax-changed')) {
                o.addClass('ajax-changed');
                if (s.asterisk) {
                    o.find(s.asterisk).append(' <abbr class="ajax-changed" title="' + t.t('Changed') + '">*</abbr> ')
                }
            }
        },
        alert: function(e, t, s) {
            r.alert(t.text, t.title)
        },
        redirect: function(e, t, s) {
            r.location = t.url
        },
        css: function(t, r, s) {
            e(r.selector).css(r.argument)
        },
        settings: function(r, a, o) {
            var i = s.ajax;
            if (i) {
                t.ajax.expired().forEach(function(e) {
                    if (e.selector) {
                        var t = e.selector.replace('#', '');
                        if (t in i) {
                            delete i[t]
                        }
                    }
                })
            };
            if (a.merge) {
                e.extend(!0, s, a.settings)
            } else {
                r.settings = a.settings
            }
        },
        data: function(t, r, s) {
            e(r.selector).data(r.name, r.value)
        },
        invoke: function(t, r, s) {
            var a = e(r.selector);
            a[r.method].apply(a, _toConsumableArray(r.args))
        },
        restripe: function(t, r, s) {
            e(r.selector).find('> tbody > tr:visible, > tr:visible').removeClass('odd even').filter(':even').addClass('odd').end().filter(':odd').addClass('even')
        },
        update_build_id: function(t, r, s) {
            e('input[name="form_build_id"][value="' + r.old + '"]').val(r.new)
        },
        add_css: function(t, r, s) {
            e('head').prepend(r.data);
            var a = void 0,
                o = /^@import url\("(.*)"\);$/igm;
            if (document.styleSheets[0].addImport && o.test(r.data)) {
                o.lastIndex = 0;
                do {
                    a = o.exec(r.data);
                    document.styleSheets[0].addImport(a[1])
                }
                while (a);
            }
        }
    }
})(jQuery, window, Drupal, drupalSettings);;
(function(s) {
    Drupal.Ajax.prototype.success = function(t, i) {
        if (this.progress.element) {
            s(this.progress.element).remove()
        };
        if (this.progress.object) {
            this.progress.object.stopMonitoring()
        };
        s(this.element).prop('disabled', !1);
        for (var e in t) {
            if (t.hasOwnProperty(e) && t[e].command && this.commands[t[e].command]) {
                this.commands[t[e].command](this, t[e], i)
            }
        };
        if (this.$form) {
            var o = this.settings || drupalSettings;
            Drupal.attachBehaviors(this.$form.get(0), o)
        };
        this.settings = null
    }
})(jQuery);;
/*!
 * jQuery Form Plugin
 * version: 3.51.0-2014.06.20
 * Requires jQuery v1.5 or later
 * Copyright (c) 2014 M. Alsup
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses.
 * https://github.com/malsup/form#copyright-and-license
 */
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("undefined" != typeof jQuery ? jQuery : window.Zepto)
}(function(e) {
    "use strict";

    function t(t) {
        var r = t.data;
        t.isDefaultPrevented() || (t.preventDefault(), e(t.target).ajaxSubmit(r))
    }

    function r(t) {
        var r = t.target,
            a = e(r);
        if (!a.is("[type=submit],[type=image]")) {
            var n = a.closest("[type=submit]");
            if (0 === n.length) return;
            r = n[0]
        }
        var i = this;
        if (i.clk = r, "image" == r.type)
            if (void 0 !== t.offsetX) i.clk_x = t.offsetX, i.clk_y = t.offsetY;
            else if ("function" == typeof e.fn.offset) {
            var o = a.offset();
            i.clk_x = t.pageX - o.left, i.clk_y = t.pageY - o.top
        } else i.clk_x = t.pageX - r.offsetLeft, i.clk_y = t.pageY - r.offsetTop;
        setTimeout(function() {
            i.clk = i.clk_x = i.clk_y = null
        }, 100)
    }

    function a() {
        if (e.fn.ajaxSubmit.debug) {
            var t = "[jquery.form] " + Array.prototype.join.call(arguments, "");
            window.console && window.console.log ? window.console.log(t) : window.opera && window.opera.postError && window.opera.postError(t)
        }
    }
    var n = {};
    n.fileapi = void 0 !== e("<input type='file'/>").get(0).files, n.formdata = void 0 !== window.FormData;
    var i = !!e.fn.prop;
    e.fn.attr2 = function() {
        if (!i) return this.attr.apply(this, arguments);
        var e = this.prop.apply(this, arguments);
        return e && e.jquery || "string" == typeof e ? e : this.attr.apply(this, arguments)
    }, e.fn.ajaxSubmit = function(t) {
        function r(r) {
            var a, n, i = e.param(r, t.traditional).split("&"),
                o = i.length,
                s = [];
            for (a = 0; o > a; a++) i[a] = i[a].replace(/\+/g, " "), n = i[a].split("="), s.push([decodeURIComponent(n[0]), decodeURIComponent(n[1])]);
            return s
        }

        function o(a) {
            for (var n = new FormData, i = 0; i < a.length; i++) n.append(a[i].name, a[i].value);
            if (t.extraData) {
                var o = r(t.extraData);
                for (i = 0; i < o.length; i++) o[i] && n.append(o[i][0], o[i][1])
            }
            t.data = null;
            var s = e.extend(!0, {}, e.ajaxSettings, t, {
                contentType: !1,
                processData: !1,
                cache: !1,
                type: u || "POST"
            });
            t.uploadProgress && (s.xhr = function() {
                var r = e.ajaxSettings.xhr();
                return r.upload && r.upload.addEventListener("progress", function(e) {
                    var r = 0,
                        a = e.loaded || e.position,
                        n = e.total;
                    e.lengthComputable && (r = Math.ceil(a / n * 100)), t.uploadProgress(e, a, n, r)
                }, !1), r
            }), s.data = null;
            var c = s.beforeSend;
            return s.beforeSend = function(e, r) {
                r.data = t.formData ? t.formData : n, c && c.call(this, e, r)
            }, e.ajax(s)
        }

        function s(r) {
            function n(e) {
                var t = null;
                try {
                    e.contentWindow && (t = e.contentWindow.document)
                } catch (r) {
                    a("cannot get iframe.contentWindow document: " + r)
                }
                if (t) return t;
                try {
                    t = e.contentDocument ? e.contentDocument : e.document
                } catch (r) {
                    a("cannot get iframe.contentDocument: " + r), t = e.document
                }
                return t
            }

            function o() {
                function t() {
                    try {
                        var e = n(g).readyState;
                        a("state = " + e), e && "uninitialized" == e.toLowerCase() && setTimeout(t, 50)
                    } catch (r) {
                        a("Server abort: ", r, " (", r.name, ")"), s(k), j && clearTimeout(j), j = void 0
                    }
                }
                var r = f.attr2("target"),
                    i = f.attr2("action"),
                    o = "multipart/form-data",
                    c = f.attr("enctype") || f.attr("encoding") || o;
                w.setAttribute("target", p), (!u || /post/i.test(u)) && w.setAttribute("method", "POST"), i != m.url && w.setAttribute("action", m.url), m.skipEncodingOverride || u && !/post/i.test(u) || f.attr({
                    encoding: "multipart/form-data",
                    enctype: "multipart/form-data"
                }), m.timeout && (j = setTimeout(function() {
                    T = !0, s(D)
                }, m.timeout));
                var l = [];
                try {
                    if (m.extraData)
                        for (var d in m.extraData) m.extraData.hasOwnProperty(d) && l.push(e.isPlainObject(m.extraData[d]) && m.extraData[d].hasOwnProperty("name") && m.extraData[d].hasOwnProperty("value") ? e('<input type="hidden" name="' + m.extraData[d].name + '">').val(m.extraData[d].value).appendTo(w)[0] : e('<input type="hidden" name="' + d + '">').val(m.extraData[d]).appendTo(w)[0]);
                    m.iframeTarget || v.appendTo("body"), g.attachEvent ? g.attachEvent("onload", s) : g.addEventListener("load", s, !1), setTimeout(t, 15);
                    try {
                        w.submit()
                    } catch (h) {
                        var x = document.createElement("form").submit;
                        x.apply(w)
                    }
                } finally {
                    w.setAttribute("action", i), w.setAttribute("enctype", c), r ? w.setAttribute("target", r) : f.removeAttr("target"), e(l).remove()
                }
            }

            function s(t) {
                if (!x.aborted && !F) {
                    if (M = n(g), M || (a("cannot access response document"), t = k), t === D && x) return x.abort("timeout"), void S.reject(x, "timeout");
                    if (t == k && x) return x.abort("server abort"), void S.reject(x, "error", "server abort");
                    if (M && M.location.href != m.iframeSrc || T) {
                        g.detachEvent ? g.detachEvent("onload", s) : g.removeEventListener("load", s, !1);
                        var r, i = "success";
                        try {
                            if (T) throw "timeout";
                            var o = "xml" == m.dataType || M.XMLDocument || e.isXMLDoc(M);
                            if (a("isXml=" + o), !o && window.opera && (null === M.body || !M.body.innerHTML) && --O) return a("requeing onLoad callback, DOM not available"), void setTimeout(s, 250);
                            var u = M.body ? M.body : M.documentElement;
                            x.responseText = u ? u.innerHTML : null, x.responseXML = M.XMLDocument ? M.XMLDocument : M, o && (m.dataType = "xml"), x.getResponseHeader = function(e) {
                                var t = {
                                    "content-type": m.dataType
                                };
                                return t[e.toLowerCase()]
                            }, u && (x.status = Number(u.getAttribute("status")) || x.status, x.statusText = u.getAttribute("statusText") || x.statusText);
                            var c = (m.dataType || "").toLowerCase(),
                                l = /(json|script|text)/.test(c);
                            if (l || m.textarea) {
                                var f = M.getElementsByTagName("textarea")[0];
                                if (f) x.responseText = f.value, x.status = Number(f.getAttribute("status")) || x.status, x.statusText = f.getAttribute("statusText") || x.statusText;
                                else if (l) {
                                    var p = M.getElementsByTagName("pre")[0],
                                        h = M.getElementsByTagName("body")[0];
                                    p ? x.responseText = p.textContent ? p.textContent : p.innerText : h && (x.responseText = h.textContent ? h.textContent : h.innerText)
                                }
                            } else "xml" == c && !x.responseXML && x.responseText && (x.responseXML = X(x.responseText));
                            try {
                                E = _(x, c, m)
                            } catch (y) {
                                i = "parsererror", x.error = r = y || i
                            }
                        } catch (y) {
                            a("error caught: ", y), i = "error", x.error = r = y || i
                        }
                        x.aborted && (a("upload aborted"), i = null), x.status && (i = x.status >= 200 && x.status < 300 || 304 === x.status ? "success" : "error"), "success" === i ? (m.success && m.success.call(m.context, E, "success", x), S.resolve(x.responseText, "success", x), d && e.event.trigger("ajaxSuccess", [x, m])) : i && (void 0 === r && (r = x.statusText), m.error && m.error.call(m.context, x, i, r), S.reject(x, "error", r), d && e.event.trigger("ajaxError", [x, m, r])), d && e.event.trigger("ajaxComplete", [x, m]), d && !--e.active && e.event.trigger("ajaxStop"), m.complete && m.complete.call(m.context, x, i), F = !0, m.timeout && clearTimeout(j), setTimeout(function() {
                            m.iframeTarget ? v.attr("src", m.iframeSrc) : v.remove(), x.responseXML = null
                        }, 100)
                    }
                }
            }
            var c, l, m, d, p, v, g, x, y, b, T, j, w = f[0],
                S = e.Deferred();
            if (S.abort = function(e) {
                    x.abort(e)
                }, r)
                for (l = 0; l < h.length; l++) c = e(h[l]), i ? c.prop("disabled", !1) : c.removeAttr("disabled");
            if (m = e.extend(!0, {}, e.ajaxSettings, t), m.context = m.context || m, p = "jqFormIO" + (new Date).getTime(), m.iframeTarget ? (v = e(m.iframeTarget), b = v.attr2("name"), b ? p = b : v.attr2("name", p)) : (v = e('<iframe name="' + p + '" src="' + m.iframeSrc + '" />'), v.css({
                    position: "absolute",
                    top: "-1000px",
                    left: "-1000px"
                })), g = v[0], x = {
                    aborted: 0,
                    responseText: null,
                    responseXML: null,
                    status: 0,
                    statusText: "n/a",
                    getAllResponseHeaders: function() {},
                    getResponseHeader: function() {},
                    setRequestHeader: function() {},
                    abort: function(t) {
                        var r = "timeout" === t ? "timeout" : "aborted";
                        a("aborting upload... " + r), this.aborted = 1;
                        try {
                            g.contentWindow.document.execCommand && g.contentWindow.document.execCommand("Stop")
                        } catch (n) {}
                        v.attr("src", m.iframeSrc), x.error = r, m.error && m.error.call(m.context, x, r, t), d && e.event.trigger("ajaxError", [x, m, r]), m.complete && m.complete.call(m.context, x, r)
                    }
                }, d = m.global, d && 0 === e.active++ && e.event.trigger("ajaxStart"), d && e.event.trigger("ajaxSend", [x, m]), m.beforeSend && m.beforeSend.call(m.context, x, m) === !1) return m.global && e.active--, S.reject(), S;
            if (x.aborted) return S.reject(), S;
            y = w.clk, y && (b = y.name, b && !y.disabled && (m.extraData = m.extraData || {}, m.extraData[b] = y.value, "image" == y.type && (m.extraData[b + ".x"] = w.clk_x, m.extraData[b + ".y"] = w.clk_y)));
            var D = 1,
                k = 2,
                A = e("meta[name=csrf-token]").attr("content"),
                L = e("meta[name=csrf-param]").attr("content");
            L && A && (m.extraData = m.extraData || {}, m.extraData[L] = A), m.forceSync ? o() : setTimeout(o, 10);
            var E, M, F, O = 50,
                X = e.parseXML || function(e, t) {
                    return window.ActiveXObject ? (t = new ActiveXObject("Microsoft.XMLDOM"), t.async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && "parsererror" != t.documentElement.nodeName ? t : null
                },
                C = e.parseJSON || function(e) {
                    return window.eval("(" + e + ")")
                },
                _ = function(t, r, a) {
                    var n = t.getResponseHeader("content-type") || "",
                        i = "xml" === r || !r && n.indexOf("xml") >= 0,
                        o = i ? t.responseXML : t.responseText;
                    return i && "parsererror" === o.documentElement.nodeName && e.error && e.error("parsererror"), a && a.dataFilter && (o = a.dataFilter(o, r)), "string" == typeof o && ("json" === r || !r && n.indexOf("json") >= 0 ? o = C(o) : ("script" === r || !r && n.indexOf("javascript") >= 0) && e.globalEval(o)), o
                };
            return S
        }
        if (!this.length) return a("ajaxSubmit: skipping submit process - no element selected"), this;
        var u, c, l, f = this;
        "function" == typeof t ? t = {
            success: t
        } : void 0 === t && (t = {}), u = t.type || this.attr2("method"), c = t.url || this.attr2("action"), l = "string" == typeof c ? e.trim(c) : "", l = l || window.location.href || "", l && (l = (l.match(/^([^#]+)/) || [])[1]), t = e.extend(!0, {
            url: l,
            success: e.ajaxSettings.success,
            type: u || e.ajaxSettings.type,
            iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
        }, t);
        var m = {};
        if (this.trigger("form-pre-serialize", [this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
        if (t.beforeSerialize && t.beforeSerialize(this, t) === !1) return a("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
        var d = t.traditional;
        void 0 === d && (d = e.ajaxSettings.traditional);
        var p, h = [],
            v = this.formToArray(t.semantic, h);
        if (t.data && (t.extraData = t.data, p = e.param(t.data, d)), t.beforeSubmit && t.beforeSubmit(v, this, t) === !1) return a("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
        if (this.trigger("form-submit-validate", [v, this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
        var g = e.param(v, d);
        p && (g = g ? g + "&" + p : p), "GET" == t.type.toUpperCase() ? (t.url += (t.url.indexOf("?") >= 0 ? "&" : "?") + g, t.data = null) : t.data = g;
        var x = [];
        if (t.resetForm && x.push(function() {
                f.resetForm()
            }), t.clearForm && x.push(function() {
                f.clearForm(t.includeHidden)
            }), !t.dataType && t.target) {
            var y = t.success || function() {};
            x.push(function(r) {
                var a = t.replaceTarget ? "replaceWith" : "html";
                e(t.target)[a](r).each(y, arguments)
            })
        } else t.success && x.push(t.success);
        if (t.success = function(e, r, a) {
                for (var n = t.context || this, i = 0, o = x.length; o > i; i++) x[i].apply(n, [e, r, a || f, f])
            }, t.error) {
            var b = t.error;
            t.error = function(e, r, a) {
                var n = t.context || this;
                b.apply(n, [e, r, a, f])
            }
        }
        if (t.complete) {
            var T = t.complete;
            t.complete = function(e, r) {
                var a = t.context || this;
                T.apply(a, [e, r, f])
            }
        }
        var j = e("input[type=file]:enabled", this).filter(function() {
                return "" !== e(this).val()
            }),
            w = j.length > 0,
            S = "multipart/form-data",
            D = f.attr("enctype") == S || f.attr("encoding") == S,
            k = n.fileapi && n.formdata;
        a("fileAPI :" + k);
        var A, L = (w || D) && !k;
        t.iframe !== !1 && (t.iframe || L) ? t.closeKeepAlive ? e.get(t.closeKeepAlive, function() {
            A = s(v)
        }) : A = s(v) : A = (w || D) && k ? o(v) : e.ajax(t), f.removeData("jqxhr").data("jqxhr", A);
        for (var E = 0; E < h.length; E++) h[E] = null;
        return this.trigger("form-submit-notify", [this, t]), this
    }, e.fn.ajaxForm = function(n) {
        if (n = n || {}, n.delegation = n.delegation && e.isFunction(e.fn.on), !n.delegation && 0 === this.length) {
            var i = {
                s: this.selector,
                c: this.context
            };
            return !e.isReady && i.s ? (a("DOM not ready, queuing ajaxForm"), e(function() {
                e(i.s, i.c).ajaxForm(n)
            }), this) : (a("terminating; zero elements found by selector" + (e.isReady ? "" : " (DOM not ready)")), this)
        }
        return n.delegation ? (e(document).off("submit.form-plugin", this.selector, t).off("click.form-plugin", this.selector, r).on("submit.form-plugin", this.selector, n, t).on("click.form-plugin", this.selector, n, r), this) : this.ajaxFormUnbind().bind("submit.form-plugin", n, t).bind("click.form-plugin", n, r)
    }, e.fn.ajaxFormUnbind = function() {
        return this.unbind("submit.form-plugin click.form-plugin")
    }, e.fn.formToArray = function(t, r) {
        var a = [];
        if (0 === this.length) return a;
        var i, o = this[0],
            s = this.attr("id"),
            u = t ? o.getElementsByTagName("*") : o.elements;
        if (u && !/MSIE [678]/.test(navigator.userAgent) && (u = e(u).get()), s && (i = e(':input[form="' + s + '"]').get(), i.length && (u = (u || []).concat(i))), !u || !u.length) return a;
        var c, l, f, m, d, p, h;
        for (c = 0, p = u.length; p > c; c++)
            if (d = u[c], f = d.name, f && !d.disabled)
                if (t && o.clk && "image" == d.type) o.clk == d && (a.push({
                    name: f,
                    value: e(d).val(),
                    type: d.type
                }), a.push({
                    name: f + ".x",
                    value: o.clk_x
                }, {
                    name: f + ".y",
                    value: o.clk_y
                }));
                else if (m = e.fieldValue(d, !0), m && m.constructor == Array)
            for (r && r.push(d), l = 0, h = m.length; h > l; l++) a.push({
                name: f,
                value: m[l]
            });
        else if (n.fileapi && "file" == d.type) {
            r && r.push(d);
            var v = d.files;
            if (v.length)
                for (l = 0; l < v.length; l++) a.push({
                    name: f,
                    value: v[l],
                    type: d.type
                });
            else a.push({
                name: f,
                value: "",
                type: d.type
            })
        } else null !== m && "undefined" != typeof m && (r && r.push(d), a.push({
            name: f,
            value: m,
            type: d.type,
            required: d.required
        }));
        if (!t && o.clk) {
            var g = e(o.clk),
                x = g[0];
            f = x.name, f && !x.disabled && "image" == x.type && (a.push({
                name: f,
                value: g.val()
            }), a.push({
                name: f + ".x",
                value: o.clk_x
            }, {
                name: f + ".y",
                value: o.clk_y
            }))
        }
        return a
    }, e.fn.formSerialize = function(t) {
        return e.param(this.formToArray(t))
    }, e.fn.fieldSerialize = function(t) {
        var r = [];
        return this.each(function() {
            var a = this.name;
            if (a) {
                var n = e.fieldValue(this, t);
                if (n && n.constructor == Array)
                    for (var i = 0, o = n.length; o > i; i++) r.push({
                        name: a,
                        value: n[i]
                    });
                else null !== n && "undefined" != typeof n && r.push({
                    name: this.name,
                    value: n
                })
            }
        }), e.param(r)
    }, e.fn.fieldValue = function(t) {
        for (var r = [], a = 0, n = this.length; n > a; a++) {
            var i = this[a],
                o = e.fieldValue(i, t);
            null === o || "undefined" == typeof o || o.constructor == Array && !o.length || (o.constructor == Array ? e.merge(r, o) : r.push(o))
        }
        return r
    }, e.fieldValue = function(t, r) {
        var a = t.name,
            n = t.type,
            i = t.tagName.toLowerCase();
        if (void 0 === r && (r = !0), r && (!a || t.disabled || "reset" == n || "button" == n || ("checkbox" == n || "radio" == n) && !t.checked || ("submit" == n || "image" == n) && t.form && t.form.clk != t || "select" == i && -1 == t.selectedIndex)) return null;
        if ("select" == i) {
            var o = t.selectedIndex;
            if (0 > o) return null;
            for (var s = [], u = t.options, c = "select-one" == n, l = c ? o + 1 : u.length, f = c ? o : 0; l > f; f++) {
                var m = u[f];
                if (m.selected) {
                    var d = m.value;
                    if (d || (d = m.attributes && m.attributes.value && !m.attributes.value.specified ? m.text : m.value), c) return d;
                    s.push(d)
                }
            }
            return s
        }
        return e(t).val()
    }, e.fn.clearForm = function(t) {
        return this.each(function() {
            e("input,select,textarea", this).clearFields(t)
        })
    }, e.fn.clearFields = e.fn.clearInputs = function(t) {
        var r = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function() {
            var a = this.type,
                n = this.tagName.toLowerCase();
            r.test(a) || "textarea" == n ? this.value = "" : "checkbox" == a || "radio" == a ? this.checked = !1 : "select" == n ? this.selectedIndex = -1 : "file" == a ? /MSIE/.test(navigator.userAgent) ? e(this).replaceWith(e(this).clone(!0)) : e(this).val("") : t && (t === !0 && /hidden/.test(a) || "string" == typeof t && e(this).is(t)) && (this.value = "")
        })
    }, e.fn.resetForm = function() {
        return this.each(function() {
            ("function" == typeof this.reset || "object" == typeof this.reset && !this.reset.nodeType) && this.reset()
        })
    }, e.fn.enable = function(e) {
        return void 0 === e && (e = !0), this.each(function() {
            this.disabled = !e
        })
    }, e.fn.selected = function(t) {
        return void 0 === t && (t = !0), this.each(function() {
            var r = this.type;
            if ("checkbox" == r || "radio" == r) this.checked = t;
            else if ("option" == this.tagName.toLowerCase()) {
                var a = e(this).parent("select");
                t && a[0] && "select-one" == a[0].type && a.find("option").selected(!1), this.selected = t
            }
        })
    }, e.fn.ajaxSubmit.debug = !1
});;
/*! jQuery UI - v1.12.1 - 2017-03-31
 * http://jqueryui.com
 * Copyright jQuery Foundation and other contributors; Licensed  */
! function(a) {
    "function" == typeof define && define.amd ? define(["jquery", "../ie", "../version", "../widget"], a) : a(jQuery)
}(function(a) {
    var b = !1;
    return a(document).on("mouseup", function() {
        b = !1
    }), a.widget("ui.mouse", {
        version: "1.12.1",
        options: {
            cancel: "input, textarea, button, select, option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var b = this;
            this.element.on("mousedown." + this.widgetName, function(a) {
                return b._mouseDown(a)
            }).on("click." + this.widgetName, function(c) {
                if (!0 === a.data(c.target, b.widgetName + ".preventClickEvent")) return a.removeData(c.target, b.widgetName + ".preventClickEvent"), c.stopImmediatePropagation(), !1
            }), this.started = !1
        },
        _mouseDestroy: function() {
            this.element.off("." + this.widgetName), this._mouseMoveDelegate && this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate)
        },
        _mouseDown: function(c) {
            if (!b) {
                this._mouseMoved = !1, this._mouseStarted && this._mouseUp(c), this._mouseDownEvent = c;
                var d = this,
                    e = 1 === c.which,
                    f = !("string" != typeof this.options.cancel || !c.target.nodeName) && a(c.target).closest(this.options.cancel).length;
                return !(e && !f && this._mouseCapture(c)) || (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                    d.mouseDelayMet = !0
                }, this.options.delay)), this._mouseDistanceMet(c) && this._mouseDelayMet(c) && (this._mouseStarted = this._mouseStart(c) !== !1, !this._mouseStarted) ? (c.preventDefault(), !0) : (!0 === a.data(c.target, this.widgetName + ".preventClickEvent") && a.removeData(c.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(a) {
                    return d._mouseMove(a)
                }, this._mouseUpDelegate = function(a) {
                    return d._mouseUp(a)
                }, this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate), c.preventDefault(), b = !0, !0))
            }
        },
        _mouseMove: function(b) {
            if (this._mouseMoved) {
                if (a.ui.ie && (!document.documentMode || document.documentMode < 9) && !b.button) return this._mouseUp(b);
                if (!b.which)
                    if (b.originalEvent.altKey || b.originalEvent.ctrlKey || b.originalEvent.metaKey || b.originalEvent.shiftKey) this.ignoreMissingWhich = !0;
                    else if (!this.ignoreMissingWhich) return this._mouseUp(b)
            }
            return (b.which || b.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(b), b.preventDefault()) : (this._mouseDistanceMet(b) && this._mouseDelayMet(b) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, b) !== !1, this._mouseStarted ? this._mouseDrag(b) : this._mouseUp(b)), !this._mouseStarted)
        },
        _mouseUp: function(c) {
            this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, c.target === this._mouseDownEvent.target && a.data(c.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(c)), this._mouseDelayTimer && (clearTimeout(this._mouseDelayTimer), delete this._mouseDelayTimer), this.ignoreMissingWhich = !1, b = !1, c.preventDefault()
        },
        _mouseDistanceMet: function(a) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function() {
            return this.mouseDelayMet
        },
        _mouseStart: function() {},
        _mouseDrag: function() {},
        _mouseStop: function() {},
        _mouseCapture: function() {
            return !0
        }
    })
});;
/*! jQuery UI - v1.12.1 - 2017-03-31
 * http://jqueryui.com
 * Copyright jQuery Foundation and other contributors; Licensed  */
! function(a) {
    "function" == typeof define && define.amd ? define(["jquery", "./mouse", "../data", "../plugin", "../safe-active-element", "../safe-blur", "../scroll-parent", "../version", "../widget"], a) : a(jQuery)
}(function(a) {
    return a.widget("ui.draggable", a.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "drag",
        options: {
            addClasses: !0,
            appendTo: "parent",
            axis: !1,
            connectToSortable: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            iframeFix: !1,
            opacity: !1,
            refreshPositions: !1,
            revert: !1,
            revertDuration: 500,
            scope: "default",
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: !1,
            snapMode: "both",
            snapTolerance: 20,
            stack: !1,
            zIndex: !1,
            drag: null,
            start: null,
            stop: null
        },
        _create: function() {
            "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this._addClass("ui-draggable"), this._setHandleClassName(), this._mouseInit()
        },
        _setOption: function(a, b) {
            this._super(a, b), "handle" === a && (this._removeHandleClassName(), this._setHandleClassName())
        },
        _destroy: function() {
            return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this._removeHandleClassName(), void this._mouseDestroy())
        },
        _mouseCapture: function(b) {
            var c = this.options;
            return !(this.helper || c.disabled || a(b.target).closest(".ui-resizable-handle").length > 0) && (this.handle = this._getHandle(b), !!this.handle && (this._blurActiveElement(b), this._blockFrames(c.iframeFix === !0 ? "iframe" : c.iframeFix), !0))
        },
        _blockFrames: function(b) {
            this.iframeBlocks = this.document.find(b).map(function() {
                var b = a(this);
                return a("<div>").css("position", "absolute").appendTo(b.parent()).outerWidth(b.outerWidth()).outerHeight(b.outerHeight()).offset(b.offset())[0]
            })
        },
        _unblockFrames: function() {
            this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks)
        },
        _blurActiveElement: function(b) {
            var c = a.ui.safeActiveElement(this.document[0]),
                d = a(b.target);
            d.closest(c).length || a.ui.safeBlur(c)
        },
        _mouseStart: function(b) {
            var c = this.options;
            return this.helper = this._createHelper(b), this._addClass(this.helper, "ui-draggable-dragging"), this._cacheHelperProportions(), a.ui.ddmanager && (a.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = this.helper.parents().filter(function() {
                return "fixed" === a(this).css("position")
            }).length > 0, this.positionAbs = this.element.offset(), this._refreshOffsets(b), this.originalPosition = this.position = this._generatePosition(b, !1), this.originalPageX = b.pageX, this.originalPageY = b.pageY, c.cursorAt && this._adjustOffsetFromHelper(c.cursorAt), this._setContainment(), this._trigger("start", b) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), a.ui.ddmanager && !c.dropBehaviour && a.ui.ddmanager.prepareOffsets(this, b), this._mouseDrag(b, !0), a.ui.ddmanager && a.ui.ddmanager.dragStart(this, b), !0)
        },
        _refreshOffsets: function(a) {
            this.offset = {
                top: this.positionAbs.top - this.margins.top,
                left: this.positionAbs.left - this.margins.left,
                scroll: !1,
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }, this.offset.click = {
                left: a.pageX - this.offset.left,
                top: a.pageY - this.offset.top
            }
        },
        _mouseDrag: function(b, c) {
            if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(b, !0), this.positionAbs = this._convertPositionTo("absolute"), !c) {
                var d = this._uiHash();
                if (this._trigger("drag", b, d) === !1) return this._mouseUp(new a.Event("mouseup", b)), !1;
                this.position = d.position
            }
            return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", a.ui.ddmanager && a.ui.ddmanager.drag(this, b), !1
        },
        _mouseStop: function(b) {
            var c = this,
                d = !1;
            return a.ui.ddmanager && !this.options.dropBehaviour && (d = a.ui.ddmanager.drop(this, b)), this.dropped && (d = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !d || "valid" === this.options.revert && d || this.options.revert === !0 || a.isFunction(this.options.revert) && this.options.revert.call(this.element, d) ? a(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
                c._trigger("stop", b) !== !1 && c._clear()
            }) : this._trigger("stop", b) !== !1 && this._clear(), !1
        },
        _mouseUp: function(b) {
            return this._unblockFrames(), a.ui.ddmanager && a.ui.ddmanager.dragStop(this, b), this.handleElement.is(b.target) && this.element.trigger("focus"), a.ui.mouse.prototype._mouseUp.call(this, b)
        },
        cancel: function() {
            return this.helper.is(".ui-draggable-dragging") ? this._mouseUp(new a.Event("mouseup", {
                target: this.element[0]
            })) : this._clear(), this
        },
        _getHandle: function(b) {
            return !this.options.handle || !!a(b.target).closest(this.element.find(this.options.handle)).length
        },
        _setHandleClassName: function() {
            this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, this._addClass(this.handleElement, "ui-draggable-handle")
        },
        _removeHandleClassName: function() {
            this._removeClass(this.handleElement, "ui-draggable-handle")
        },
        _createHelper: function(b) {
            var c = this.options,
                d = a.isFunction(c.helper),
                e = d ? a(c.helper.apply(this.element[0], [b])) : "clone" === c.helper ? this.element.clone().removeAttr("id") : this.element;
            return e.parents("body").length || e.appendTo("parent" === c.appendTo ? this.element[0].parentNode : c.appendTo), d && e[0] === this.element[0] && this._setPositionRelative(), e[0] === this.element[0] || /(fixed|absolute)/.test(e.css("position")) || e.css("position", "absolute"), e
        },
        _setPositionRelative: function() {
            /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative")
        },
        _adjustOffsetFromHelper: function(b) {
            "string" == typeof b && (b = b.split(" ")), a.isArray(b) && (b = {
                left: +b[0],
                top: +b[1] || 0
            }), "left" in b && (this.offset.click.left = b.left + this.margins.left), "right" in b && (this.offset.click.left = this.helperProportions.width - b.right + this.margins.left), "top" in b && (this.offset.click.top = b.top + this.margins.top), "bottom" in b && (this.offset.click.top = this.helperProportions.height - b.bottom + this.margins.top)
        },
        _isRootNode: function(a) {
            return /(html|body)/i.test(a.tagName) || a === this.document[0]
        },
        _getParentOffset: function() {
            var b = this.offsetParent.offset(),
                c = this.document[0];
            return "absolute" === this.cssPosition && this.scrollParent[0] !== c && a.contains(this.scrollParent[0], this.offsetParent[0]) && (b.left += this.scrollParent.scrollLeft(), b.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (b = {
                top: 0,
                left: 0
            }), {
                top: b.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: b.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" !== this.cssPosition) return {
                top: 0,
                left: 0
            };
            var a = this.element.position(),
                b = this._isRootNode(this.scrollParent[0]);
            return {
                top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + (b ? 0 : this.scrollParent.scrollTop()),
                left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + (b ? 0 : this.scrollParent.scrollLeft())
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var b, c, d, e = this.options,
                f = this.document[0];
            return this.relativeContainer = null, e.containment ? "window" === e.containment ? void(this.containment = [a(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, a(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, a(window).scrollLeft() + a(window).width() - this.helperProportions.width - this.margins.left, a(window).scrollTop() + (a(window).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === e.containment ? void(this.containment = [0, 0, a(f).width() - this.helperProportions.width - this.margins.left, (a(f).height() || f.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : e.containment.constructor === Array ? void(this.containment = e.containment) : ("parent" === e.containment && (e.containment = this.helper[0].parentNode), c = a(e.containment), d = c[0], void(d && (b = /(scroll|auto)/.test(c.css("overflow")), this.containment = [(parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (b ? Math.max(d.scrollWidth, d.offsetWidth) : d.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (b ? Math.max(d.scrollHeight, d.offsetHeight) : d.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relativeContainer = c))) : void(this.containment = null)
        },
        _convertPositionTo: function(a, b) {
            b || (b = this.position);
            var c = "absolute" === a ? 1 : -1,
                d = this._isRootNode(this.scrollParent[0]);
            return {
                top: b.top + this.offset.relative.top * c + this.offset.parent.top * c - ("fixed" === this.cssPosition ? -this.offset.scroll.top : d ? 0 : this.offset.scroll.top) * c,
                left: b.left + this.offset.relative.left * c + this.offset.parent.left * c - ("fixed" === this.cssPosition ? -this.offset.scroll.left : d ? 0 : this.offset.scroll.left) * c
            }
        },
        _generatePosition: function(a, b) {
            var c, d, e, f, g = this.options,
                h = this._isRootNode(this.scrollParent[0]),
                i = a.pageX,
                j = a.pageY;
            return h && this.offset.scroll || (this.offset.scroll = {
                top: this.scrollParent.scrollTop(),
                left: this.scrollParent.scrollLeft()
            }), b && (this.containment && (this.relativeContainer ? (d = this.relativeContainer.offset(), c = [this.containment[0] + d.left, this.containment[1] + d.top, this.containment[2] + d.left, this.containment[3] + d.top]) : c = this.containment, a.pageX - this.offset.click.left < c[0] && (i = c[0] + this.offset.click.left), a.pageY - this.offset.click.top < c[1] && (j = c[1] + this.offset.click.top), a.pageX - this.offset.click.left > c[2] && (i = c[2] + this.offset.click.left), a.pageY - this.offset.click.top > c[3] && (j = c[3] + this.offset.click.top)), g.grid && (e = g.grid[1] ? this.originalPageY + Math.round((j - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY, j = c ? e - this.offset.click.top >= c[1] || e - this.offset.click.top > c[3] ? e : e - this.offset.click.top >= c[1] ? e - g.grid[1] : e + g.grid[1] : e, f = g.grid[0] ? this.originalPageX + Math.round((i - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX, i = c ? f - this.offset.click.left >= c[0] || f - this.offset.click.left > c[2] ? f : f - this.offset.click.left >= c[0] ? f - g.grid[0] : f + g.grid[0] : f), "y" === g.axis && (i = this.originalPageX), "x" === g.axis && (j = this.originalPageY)), {
                top: j - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : h ? 0 : this.offset.scroll.top),
                left: i - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : h ? 0 : this.offset.scroll.left)
            }
        },
        _clear: function() {
            this._removeClass(this.helper, "ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy()
        },
        _trigger: function(b, c, d) {
            return d = d || this._uiHash(), a.ui.plugin.call(this, b, [c, d, this], !0), /^(drag|start|stop)/.test(b) && (this.positionAbs = this._convertPositionTo("absolute"), d.offset = this.positionAbs), a.Widget.prototype._trigger.call(this, b, c, d)
        },
        plugins: {},
        _uiHash: function() {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            }
        }
    }), a.ui.plugin.add("draggable", "connectToSortable", {
        start: function(b, c, d) {
            var e = a.extend({}, c, {
                item: d.element
            });
            d.sortables = [], a(d.options.connectToSortable).each(function() {
                var c = a(this).sortable("instance");
                c && !c.options.disabled && (d.sortables.push(c), c.refreshPositions(), c._trigger("activate", b, e))
            })
        },
        stop: function(b, c, d) {
            var e = a.extend({}, c, {
                item: d.element
            });
            d.cancelHelperRemoval = !1, a.each(d.sortables, function() {
                var a = this;
                a.isOver ? (a.isOver = 0, d.cancelHelperRemoval = !0, a.cancelHelperRemoval = !1, a._storedCSS = {
                    position: a.placeholder.css("position"),
                    top: a.placeholder.css("top"),
                    left: a.placeholder.css("left")
                }, a._mouseStop(b), a.options.helper = a.options._helper) : (a.cancelHelperRemoval = !0, a._trigger("deactivate", b, e))
            })
        },
        drag: function(b, c, d) {
            a.each(d.sortables, function() {
                var e = !1,
                    f = this;
                f.positionAbs = d.positionAbs, f.helperProportions = d.helperProportions, f.offset.click = d.offset.click, f._intersectsWith(f.containerCache) && (e = !0, a.each(d.sortables, function() {
                    return this.positionAbs = d.positionAbs, this.helperProportions = d.helperProportions, this.offset.click = d.offset.click, this !== f && this._intersectsWith(this.containerCache) && a.contains(f.element[0], this.element[0]) && (e = !1), e
                })), e ? (f.isOver || (f.isOver = 1, d._parent = c.helper.parent(), f.currentItem = c.helper.appendTo(f.element).data("ui-sortable-item", !0), f.options._helper = f.options.helper, f.options.helper = function() {
                    return c.helper[0]
                }, b.target = f.currentItem[0], f._mouseCapture(b, !0), f._mouseStart(b, !0, !0), f.offset.click.top = d.offset.click.top, f.offset.click.left = d.offset.click.left, f.offset.parent.left -= d.offset.parent.left - f.offset.parent.left, f.offset.parent.top -= d.offset.parent.top - f.offset.parent.top, d._trigger("toSortable", b), d.dropped = f.element, a.each(d.sortables, function() {
                    this.refreshPositions()
                }), d.currentItem = d.element, f.fromOutside = d), f.currentItem && (f._mouseDrag(b), c.position = f.position)) : f.isOver && (f.isOver = 0, f.cancelHelperRemoval = !0, f.options._revert = f.options.revert, f.options.revert = !1, f._trigger("out", b, f._uiHash(f)), f._mouseStop(b, !0), f.options.revert = f.options._revert, f.options.helper = f.options._helper, f.placeholder && f.placeholder.remove(), c.helper.appendTo(d._parent), d._refreshOffsets(b), c.position = d._generatePosition(b, !0), d._trigger("fromSortable", b), d.dropped = !1, a.each(d.sortables, function() {
                    this.refreshPositions()
                }))
            })
        }
    }), a.ui.plugin.add("draggable", "cursor", {
        start: function(b, c, d) {
            var e = a("body"),
                f = d.options;
            e.css("cursor") && (f._cursor = e.css("cursor")), e.css("cursor", f.cursor)
        },
        stop: function(b, c, d) {
            var e = d.options;
            e._cursor && a("body").css("cursor", e._cursor)
        }
    }), a.ui.plugin.add("draggable", "opacity", {
        start: function(b, c, d) {
            var e = a(c.helper),
                f = d.options;
            e.css("opacity") && (f._opacity = e.css("opacity")), e.css("opacity", f.opacity)
        },
        stop: function(b, c, d) {
            var e = d.options;
            e._opacity && a(c.helper).css("opacity", e._opacity)
        }
    }), a.ui.plugin.add("draggable", "scroll", {
        start: function(a, b, c) {
            c.scrollParentNotHidden || (c.scrollParentNotHidden = c.helper.scrollParent(!1)), c.scrollParentNotHidden[0] !== c.document[0] && "HTML" !== c.scrollParentNotHidden[0].tagName && (c.overflowOffset = c.scrollParentNotHidden.offset())
        },
        drag: function(b, c, d) {
            var e = d.options,
                f = !1,
                g = d.scrollParentNotHidden[0],
                h = d.document[0];
            g !== h && "HTML" !== g.tagName ? (e.axis && "x" === e.axis || (d.overflowOffset.top + g.offsetHeight - b.pageY < e.scrollSensitivity ? g.scrollTop = f = g.scrollTop + e.scrollSpeed : b.pageY - d.overflowOffset.top < e.scrollSensitivity && (g.scrollTop = f = g.scrollTop - e.scrollSpeed)), e.axis && "y" === e.axis || (d.overflowOffset.left + g.offsetWidth - b.pageX < e.scrollSensitivity ? g.scrollLeft = f = g.scrollLeft + e.scrollSpeed : b.pageX - d.overflowOffset.left < e.scrollSensitivity && (g.scrollLeft = f = g.scrollLeft - e.scrollSpeed))) : (e.axis && "x" === e.axis || (b.pageY - a(h).scrollTop() < e.scrollSensitivity ? f = a(h).scrollTop(a(h).scrollTop() - e.scrollSpeed) : a(window).height() - (b.pageY - a(h).scrollTop()) < e.scrollSensitivity && (f = a(h).scrollTop(a(h).scrollTop() + e.scrollSpeed))), e.axis && "y" === e.axis || (b.pageX - a(h).scrollLeft() < e.scrollSensitivity ? f = a(h).scrollLeft(a(h).scrollLeft() - e.scrollSpeed) : a(window).width() - (b.pageX - a(h).scrollLeft()) < e.scrollSensitivity && (f = a(h).scrollLeft(a(h).scrollLeft() + e.scrollSpeed)))), f !== !1 && a.ui.ddmanager && !e.dropBehaviour && a.ui.ddmanager.prepareOffsets(d, b)
        }
    }), a.ui.plugin.add("draggable", "snap", {
        start: function(b, c, d) {
            var e = d.options;
            d.snapElements = [], a(e.snap.constructor !== String ? e.snap.items || ":data(ui-draggable)" : e.snap).each(function() {
                var b = a(this),
                    c = b.offset();
                this !== d.element[0] && d.snapElements.push({
                    item: this,
                    width: b.outerWidth(),
                    height: b.outerHeight(),
                    top: c.top,
                    left: c.left
                })
            })
        },
        drag: function(b, c, d) {
            var e, f, g, h, i, j, k, l, m, n, o = d.options,
                p = o.snapTolerance,
                q = c.offset.left,
                r = q + d.helperProportions.width,
                s = c.offset.top,
                t = s + d.helperProportions.height;
            for (m = d.snapElements.length - 1; m >= 0; m--) i = d.snapElements[m].left - d.margins.left, j = i + d.snapElements[m].width, k = d.snapElements[m].top - d.margins.top, l = k + d.snapElements[m].height, r < i - p || q > j + p || t < k - p || s > l + p || !a.contains(d.snapElements[m].item.ownerDocument, d.snapElements[m].item) ? (d.snapElements[m].snapping && d.options.snap.release && d.options.snap.release.call(d.element, b, a.extend(d._uiHash(), {
                snapItem: d.snapElements[m].item
            })), d.snapElements[m].snapping = !1) : ("inner" !== o.snapMode && (e = Math.abs(k - t) <= p, f = Math.abs(l - s) <= p, g = Math.abs(i - r) <= p, h = Math.abs(j - q) <= p, e && (c.position.top = d._convertPositionTo("relative", {
                top: k - d.helperProportions.height,
                left: 0
            }).top), f && (c.position.top = d._convertPositionTo("relative", {
                top: l,
                left: 0
            }).top), g && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: i - d.helperProportions.width
            }).left), h && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: j
            }).left)), n = e || f || g || h, "outer" !== o.snapMode && (e = Math.abs(k - s) <= p, f = Math.abs(l - t) <= p, g = Math.abs(i - q) <= p, h = Math.abs(j - r) <= p, e && (c.position.top = d._convertPositionTo("relative", {
                top: k,
                left: 0
            }).top), f && (c.position.top = d._convertPositionTo("relative", {
                top: l - d.helperProportions.height,
                left: 0
            }).top), g && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: i
            }).left), h && (c.position.left = d._convertPositionTo("relative", {
                top: 0,
                left: j - d.helperProportions.width
            }).left)), !d.snapElements[m].snapping && (e || f || g || h || n) && d.options.snap.snap && d.options.snap.snap.call(d.element, b, a.extend(d._uiHash(), {
                snapItem: d.snapElements[m].item
            })), d.snapElements[m].snapping = e || f || g || h || n)
        }
    }), a.ui.plugin.add("draggable", "stack", {
        start: function(b, c, d) {
            var e, f = d.options,
                g = a.makeArray(a(f.stack)).sort(function(b, c) {
                    return (parseInt(a(b).css("zIndex"), 10) || 0) - (parseInt(a(c).css("zIndex"), 10) || 0)
                });
            g.length && (e = parseInt(a(g[0]).css("zIndex"), 10) || 0, a(g).each(function(b) {
                a(this).css("zIndex", e + b)
            }), this.css("zIndex", e + g.length))
        }
    }), a.ui.plugin.add("draggable", "zIndex", {
        start: function(b, c, d) {
            var e = a(c.helper),
                f = d.options;
            e.css("zIndex") && (f._zIndex = e.css("zIndex")), e.css("zIndex", f.zIndex)
        },
        stop: function(b, c, d) {
            var e = d.options;
            e._zIndex && a(c.helper).css("zIndex", e._zIndex)
        }
    }), a.ui.draggable
});;
/*! jQuery UI - v1.12.1 - 2017-03-31
 * http://jqueryui.com
 * Copyright jQuery Foundation and other contributors; Licensed  */
! function(a) {
    "function" == typeof define && define.amd ? define(["jquery", "./mouse", "../disable-selection", "../plugin", "../version", "../widget"], a) : a(jQuery)
}(function(a) {
    return a.widget("ui.resizable", a.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: !1,
            animate: !1,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: !1,
            autoHide: !1,
            classes: {
                "ui-resizable-se": "ui-icon ui-icon-gripsmall-diagonal-se"
            },
            containment: !1,
            ghost: !1,
            grid: !1,
            handles: "e,s,se",
            helper: !1,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 90,
            resize: null,
            start: null,
            stop: null
        },
        _num: function(a) {
            return parseFloat(a) || 0
        },
        _isNumber: function(a) {
            return !isNaN(parseFloat(a))
        },
        _hasScroll: function(b, c) {
            if ("hidden" === a(b).css("overflow")) return !1;
            var d = c && "left" === c ? "scrollLeft" : "scrollTop",
                e = !1;
            return b[d] > 0 || (b[d] = 1, e = b[d] > 0, b[d] = 0, e)
        },
        _create: function() {
            var b, c = this.options,
                d = this;
            this._addClass("ui-resizable"), a.extend(this, {
                _aspectRatio: !!c.aspectRatio,
                aspectRatio: c.aspectRatio,
                originalElement: this.element,
                _proportionallyResizeElements: [],
                _helper: c.helper || c.ghost || c.animate ? c.helper || "ui-resizable-helper" : null
            }), this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i) && (this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
                position: this.element.css("position"),
                width: this.element.outerWidth(),
                height: this.element.outerHeight(),
                top: this.element.css("top"),
                left: this.element.css("left")
            })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, b = {
                marginTop: this.originalElement.css("marginTop"),
                marginRight: this.originalElement.css("marginRight"),
                marginBottom: this.originalElement.css("marginBottom"),
                marginLeft: this.originalElement.css("marginLeft")
            }, this.element.css(b), this.originalElement.css("margin", 0), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                position: "static",
                zoom: 1,
                display: "block"
            })), this.originalElement.css(b), this._proportionallyResize()), this._setupHandles(), c.autoHide && a(this.element).on("mouseenter", function() {
                c.disabled || (d._removeClass("ui-resizable-autohide"), d._handles.show())
            }).on("mouseleave", function() {
                c.disabled || d.resizing || (d._addClass("ui-resizable-autohide"), d._handles.hide())
            }), this._mouseInit()
        },
        _destroy: function() {
            this._mouseDestroy();
            var b, c = function(b) {
                a(b).removeData("resizable").removeData("ui-resizable").off(".resizable").find(".ui-resizable-handle").remove()
            };
            return this.elementIsWrapper && (c(this.element), b = this.element, this.originalElement.css({
                position: b.css("position"),
                width: b.outerWidth(),
                height: b.outerHeight(),
                top: b.css("top"),
                left: b.css("left")
            }).insertAfter(b), b.remove()), this.originalElement.css("resize", this.originalResizeStyle), c(this.originalElement), this
        },
        _setOption: function(a, b) {
            switch (this._super(a, b), a) {
                case "handles":
                    this._removeHandles(), this._setupHandles()
            }
        },
        _setupHandles: function() {
            var b, c, d, e, f, g = this.options,
                h = this;
            if (this.handles = g.handles || (a(".ui-resizable-handle", this.element).length ? {
                    n: ".ui-resizable-n",
                    e: ".ui-resizable-e",
                    s: ".ui-resizable-s",
                    w: ".ui-resizable-w",
                    se: ".ui-resizable-se",
                    sw: ".ui-resizable-sw",
                    ne: ".ui-resizable-ne",
                    nw: ".ui-resizable-nw"
                } : "e,s,se"), this._handles = a(), this.handles.constructor === String)
                for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), d = this.handles.split(","), this.handles = {}, c = 0; c < d.length; c++) b = a.trim(d[c]), e = "ui-resizable-" + b, f = a("<div>"), this._addClass(f, "ui-resizable-handle " + e), f.css({
                    zIndex: g.zIndex
                }), this.handles[b] = ".ui-resizable-" + b, this.element.append(f);
            this._renderAxis = function(b) {
                var c, d, e, f;
                b = b || this.element;
                for (c in this.handles) this.handles[c].constructor === String ? this.handles[c] = this.element.children(this.handles[c]).first().show() : (this.handles[c].jquery || this.handles[c].nodeType) && (this.handles[c] = a(this.handles[c]), this._on(this.handles[c], {
                    mousedown: h._mouseDown
                })), this.elementIsWrapper && this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i) && (d = a(this.handles[c], this.element), f = /sw|ne|nw|se|n|s/.test(c) ? d.outerHeight() : d.outerWidth(), e = ["padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left"].join(""), b.css(e, f), this._proportionallyResize()), this._handles = this._handles.add(this.handles[c])
            }, this._renderAxis(this.element), this._handles = this._handles.add(this.element.find(".ui-resizable-handle")), this._handles.disableSelection(), this._handles.on("mouseover", function() {
                h.resizing || (this.className && (f = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), h.axis = f && f[1] ? f[1] : "se")
            }), g.autoHide && (this._handles.hide(), this._addClass("ui-resizable-autohide"))
        },
        _removeHandles: function() {
            this._handles.remove()
        },
        _mouseCapture: function(b) {
            var c, d, e = !1;
            for (c in this.handles) d = a(this.handles[c])[0], (d === b.target || a.contains(d, b.target)) && (e = !0);
            return !this.options.disabled && e
        },
        _mouseStart: function(b) {
            var c, d, e, f = this.options,
                g = this.element;
            return this.resizing = !0, this._renderProxy(), c = this._num(this.helper.css("left")), d = this._num(this.helper.css("top")), f.containment && (c += a(f.containment).scrollLeft() || 0, d += a(f.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                left: c,
                top: d
            }, this.size = this._helper ? {
                width: this.helper.width(),
                height: this.helper.height()
            } : {
                width: g.width(),
                height: g.height()
            }, this.originalSize = this._helper ? {
                width: g.outerWidth(),
                height: g.outerHeight()
            } : {
                width: g.width(),
                height: g.height()
            }, this.sizeDiff = {
                width: g.outerWidth() - g.width(),
                height: g.outerHeight() - g.height()
            }, this.originalPosition = {
                left: c,
                top: d
            }, this.originalMousePosition = {
                left: b.pageX,
                top: b.pageY
            }, this.aspectRatio = "number" == typeof f.aspectRatio ? f.aspectRatio : this.originalSize.width / this.originalSize.height || 1, e = a(".ui-resizable-" + this.axis).css("cursor"), a("body").css("cursor", "auto" === e ? this.axis + "-resize" : e), this._addClass("ui-resizable-resizing"), this._propagate("start", b), !0
        },
        _mouseDrag: function(b) {
            var c, d, e = this.originalMousePosition,
                f = this.axis,
                g = b.pageX - e.left || 0,
                h = b.pageY - e.top || 0,
                i = this._change[f];
            return this._updatePrevProperties(), !!i && (c = i.apply(this, [b, g, h]), this._updateVirtualBoundaries(b.shiftKey), (this._aspectRatio || b.shiftKey) && (c = this._updateRatio(c, b)), c = this._respectSize(c, b), this._updateCache(c), this._propagate("resize", b), d = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), a.isEmptyObject(d) || (this._updatePrevProperties(), this._trigger("resize", b, this.ui()), this._applyChanges()), !1)
        },
        _mouseStop: function(b) {
            this.resizing = !1;
            var c, d, e, f, g, h, i, j = this.options,
                k = this;
            return this._helper && (c = this._proportionallyResizeElements, d = c.length && /textarea/i.test(c[0].nodeName), e = d && this._hasScroll(c[0], "left") ? 0 : k.sizeDiff.height, f = d ? 0 : k.sizeDiff.width, g = {
                width: k.helper.width() - f,
                height: k.helper.height() - e
            }, h = parseFloat(k.element.css("left")) + (k.position.left - k.originalPosition.left) || null, i = parseFloat(k.element.css("top")) + (k.position.top - k.originalPosition.top) || null, j.animate || this.element.css(a.extend(g, {
                top: i,
                left: h
            })), k.helper.height(k.size.height), k.helper.width(k.size.width), this._helper && !j.animate && this._proportionallyResize()), a("body").css("cursor", "auto"), this._removeClass("ui-resizable-resizing"), this._propagate("stop", b), this._helper && this.helper.remove(), !1
        },
        _updatePrevProperties: function() {
            this.prevPosition = {
                top: this.position.top,
                left: this.position.left
            }, this.prevSize = {
                width: this.size.width,
                height: this.size.height
            }
        },
        _applyChanges: function() {
            var a = {};
            return this.position.top !== this.prevPosition.top && (a.top = this.position.top + "px"), this.position.left !== this.prevPosition.left && (a.left = this.position.left + "px"), this.size.width !== this.prevSize.width && (a.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (a.height = this.size.height + "px"), this.helper.css(a), a
        },
        _updateVirtualBoundaries: function(a) {
            var b, c, d, e, f, g = this.options;
            f = {
                minWidth: this._isNumber(g.minWidth) ? g.minWidth : 0,
                maxWidth: this._isNumber(g.maxWidth) ? g.maxWidth : 1 / 0,
                minHeight: this._isNumber(g.minHeight) ? g.minHeight : 0,
                maxHeight: this._isNumber(g.maxHeight) ? g.maxHeight : 1 / 0
            }, (this._aspectRatio || a) && (b = f.minHeight * this.aspectRatio, d = f.minWidth / this.aspectRatio, c = f.maxHeight * this.aspectRatio, e = f.maxWidth / this.aspectRatio, b > f.minWidth && (f.minWidth = b), d > f.minHeight && (f.minHeight = d), c < f.maxWidth && (f.maxWidth = c), e < f.maxHeight && (f.maxHeight = e)), this._vBoundaries = f
        },
        _updateCache: function(a) {
            this.offset = this.helper.offset(), this._isNumber(a.left) && (this.position.left = a.left), this._isNumber(a.top) && (this.position.top = a.top), this._isNumber(a.height) && (this.size.height = a.height), this._isNumber(a.width) && (this.size.width = a.width)
        },
        _updateRatio: function(a) {
            var b = this.position,
                c = this.size,
                d = this.axis;
            return this._isNumber(a.height) ? a.width = a.height * this.aspectRatio : this._isNumber(a.width) && (a.height = a.width / this.aspectRatio), "sw" === d && (a.left = b.left + (c.width - a.width), a.top = null), "nw" === d && (a.top = b.top + (c.height - a.height), a.left = b.left + (c.width - a.width)), a
        },
        _respectSize: function(a) {
            var b = this._vBoundaries,
                c = this.axis,
                d = this._isNumber(a.width) && b.maxWidth && b.maxWidth < a.width,
                e = this._isNumber(a.height) && b.maxHeight && b.maxHeight < a.height,
                f = this._isNumber(a.width) && b.minWidth && b.minWidth > a.width,
                g = this._isNumber(a.height) && b.minHeight && b.minHeight > a.height,
                h = this.originalPosition.left + this.originalSize.width,
                i = this.originalPosition.top + this.originalSize.height,
                j = /sw|nw|w/.test(c),
                k = /nw|ne|n/.test(c);
            return f && (a.width = b.minWidth), g && (a.height = b.minHeight), d && (a.width = b.maxWidth), e && (a.height = b.maxHeight), f && j && (a.left = h - b.minWidth), d && j && (a.left = h - b.maxWidth), g && k && (a.top = i - b.minHeight), e && k && (a.top = i - b.maxHeight), a.width || a.height || a.left || !a.top ? a.width || a.height || a.top || !a.left || (a.left = null) : a.top = null, a
        },
        _getPaddingPlusBorderDimensions: function(a) {
            for (var b = 0, c = [], d = [a.css("borderTopWidth"), a.css("borderRightWidth"), a.css("borderBottomWidth"), a.css("borderLeftWidth")], e = [a.css("paddingTop"), a.css("paddingRight"), a.css("paddingBottom"), a.css("paddingLeft")]; b < 4; b++) c[b] = parseFloat(d[b]) || 0, c[b] += parseFloat(e[b]) || 0;
            return {
                height: c[0] + c[2],
                width: c[1] + c[3]
            }
        },
        _proportionallyResize: function() {
            if (this._proportionallyResizeElements.length)
                for (var a, b = 0, c = this.helper || this.element; b < this._proportionallyResizeElements.length; b++) a = this._proportionallyResizeElements[b], this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(a)), a.css({
                    height: c.height() - this.outerDimensions.height || 0,
                    width: c.width() - this.outerDimensions.width || 0
                })
        },
        _renderProxy: function() {
            var b = this.element,
                c = this.options;
            this.elementOffset = b.offset(), this._helper ? (this.helper = this.helper || a("<div style='overflow:hidden;'></div>"), this._addClass(this.helper, this._helper), this.helper.css({
                width: this.element.outerWidth(),
                height: this.element.outerHeight(),
                position: "absolute",
                left: this.elementOffset.left + "px",
                top: this.elementOffset.top + "px",
                zIndex: ++c.zIndex
            }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
        },
        _change: {
            e: function(a, b) {
                return {
                    width: this.originalSize.width + b
                }
            },
            w: function(a, b) {
                var c = this.originalSize,
                    d = this.originalPosition;
                return {
                    left: d.left + b,
                    width: c.width - b
                }
            },
            n: function(a, b, c) {
                var d = this.originalSize,
                    e = this.originalPosition;
                return {
                    top: e.top + c,
                    height: d.height - c
                }
            },
            s: function(a, b, c) {
                return {
                    height: this.originalSize.height + c
                }
            },
            se: function(b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [b, c, d]))
            },
            sw: function(b, c, d) {
                return a.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [b, c, d]))
            },
            ne: function(b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [b, c, d]))
            },
            nw: function(b, c, d) {
                return a.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [b, c, d]))
            }
        },
        _propagate: function(b, c) {
            a.ui.plugin.call(this, b, [c, this.ui()]), "resize" !== b && this._trigger(b, c, this.ui())
        },
        plugins: {},
        ui: function() {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            }
        }
    }), a.ui.plugin.add("resizable", "animate", {
        stop: function(b) {
            var c = a(this).resizable("instance"),
                d = c.options,
                e = c._proportionallyResizeElements,
                f = e.length && /textarea/i.test(e[0].nodeName),
                g = f && c._hasScroll(e[0], "left") ? 0 : c.sizeDiff.height,
                h = f ? 0 : c.sizeDiff.width,
                i = {
                    width: c.size.width - h,
                    height: c.size.height - g
                },
                j = parseFloat(c.element.css("left")) + (c.position.left - c.originalPosition.left) || null,
                k = parseFloat(c.element.css("top")) + (c.position.top - c.originalPosition.top) || null;
            c.element.animate(a.extend(i, k && j ? {
                top: k,
                left: j
            } : {}), {
                duration: d.animateDuration,
                easing: d.animateEasing,
                step: function() {
                    var d = {
                        width: parseFloat(c.element.css("width")),
                        height: parseFloat(c.element.css("height")),
                        top: parseFloat(c.element.css("top")),
                        left: parseFloat(c.element.css("left"))
                    };
                    e && e.length && a(e[0]).css({
                        width: d.width,
                        height: d.height
                    }), c._updateCache(d), c._propagate("resize", b)
                }
            })
        }
    }), a.ui.plugin.add("resizable", "containment", {
        start: function() {
            var b, c, d, e, f, g, h, i = a(this).resizable("instance"),
                j = i.options,
                k = i.element,
                l = j.containment,
                m = l instanceof a ? l.get(0) : /parent/.test(l) ? k.parent().get(0) : l;
            m && (i.containerElement = a(m), /document/.test(l) || l === document ? (i.containerOffset = {
                left: 0,
                top: 0
            }, i.containerPosition = {
                left: 0,
                top: 0
            }, i.parentData = {
                element: a(document),
                left: 0,
                top: 0,
                width: a(document).width(),
                height: a(document).height() || document.body.parentNode.scrollHeight
            }) : (b = a(m), c = [], a(["Top", "Right", "Left", "Bottom"]).each(function(a, d) {
                c[a] = i._num(b.css("padding" + d))
            }), i.containerOffset = b.offset(), i.containerPosition = b.position(), i.containerSize = {
                height: b.innerHeight() - c[3],
                width: b.innerWidth() - c[1]
            }, d = i.containerOffset, e = i.containerSize.height, f = i.containerSize.width, g = i._hasScroll(m, "left") ? m.scrollWidth : f, h = i._hasScroll(m) ? m.scrollHeight : e, i.parentData = {
                element: m,
                left: d.left,
                top: d.top,
                width: g,
                height: h
            }))
        },
        resize: function(b) {
            var c, d, e, f, g = a(this).resizable("instance"),
                h = g.options,
                i = g.containerOffset,
                j = g.position,
                k = g._aspectRatio || b.shiftKey,
                l = {
                    top: 0,
                    left: 0
                },
                m = g.containerElement,
                n = !0;
            m[0] !== document && /static/.test(m.css("position")) && (l = i), j.left < (g._helper ? i.left : 0) && (g.size.width = g.size.width + (g._helper ? g.position.left - i.left : g.position.left - l.left), k && (g.size.height = g.size.width / g.aspectRatio, n = !1), g.position.left = h.helper ? i.left : 0), j.top < (g._helper ? i.top : 0) && (g.size.height = g.size.height + (g._helper ? g.position.top - i.top : g.position.top), k && (g.size.width = g.size.height * g.aspectRatio, n = !1), g.position.top = g._helper ? i.top : 0), e = g.containerElement.get(0) === g.element.parent().get(0), f = /relative|absolute/.test(g.containerElement.css("position")), e && f ? (g.offset.left = g.parentData.left + g.position.left, g.offset.top = g.parentData.top + g.position.top) : (g.offset.left = g.element.offset().left, g.offset.top = g.element.offset().top), c = Math.abs(g.sizeDiff.width + (g._helper ? g.offset.left - l.left : g.offset.left - i.left)), d = Math.abs(g.sizeDiff.height + (g._helper ? g.offset.top - l.top : g.offset.top - i.top)), c + g.size.width >= g.parentData.width && (g.size.width = g.parentData.width - c, k && (g.size.height = g.size.width / g.aspectRatio, n = !1)), d + g.size.height >= g.parentData.height && (g.size.height = g.parentData.height - d, k && (g.size.width = g.size.height * g.aspectRatio, n = !1)), n || (g.position.left = g.prevPosition.left, g.position.top = g.prevPosition.top, g.size.width = g.prevSize.width, g.size.height = g.prevSize.height)
        },
        stop: function() {
            var b = a(this).resizable("instance"),
                c = b.options,
                d = b.containerOffset,
                e = b.containerPosition,
                f = b.containerElement,
                g = a(b.helper),
                h = g.offset(),
                i = g.outerWidth() - b.sizeDiff.width,
                j = g.outerHeight() - b.sizeDiff.height;
            b._helper && !c.animate && /relative/.test(f.css("position")) && a(this).css({
                left: h.left - e.left - d.left,
                width: i,
                height: j
            }), b._helper && !c.animate && /static/.test(f.css("position")) && a(this).css({
                left: h.left - e.left - d.left,
                width: i,
                height: j
            })
        }
    }), a.ui.plugin.add("resizable", "alsoResize", {
        start: function() {
            var b = a(this).resizable("instance"),
                c = b.options;
            a(c.alsoResize).each(function() {
                var b = a(this);
                b.data("ui-resizable-alsoresize", {
                    width: parseFloat(b.width()),
                    height: parseFloat(b.height()),
                    left: parseFloat(b.css("left")),
                    top: parseFloat(b.css("top"))
                })
            })
        },
        resize: function(b, c) {
            var d = a(this).resizable("instance"),
                e = d.options,
                f = d.originalSize,
                g = d.originalPosition,
                h = {
                    height: d.size.height - f.height || 0,
                    width: d.size.width - f.width || 0,
                    top: d.position.top - g.top || 0,
                    left: d.position.left - g.left || 0
                };
            a(e.alsoResize).each(function() {
                var b = a(this),
                    d = a(this).data("ui-resizable-alsoresize"),
                    e = {},
                    f = b.parents(c.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                a.each(f, function(a, b) {
                    var c = (d[b] || 0) + (h[b] || 0);
                    c && c >= 0 && (e[b] = c || null)
                }), b.css(e)
            })
        },
        stop: function() {
            a(this).removeData("ui-resizable-alsoresize")
        }
    }), a.ui.plugin.add("resizable", "ghost", {
        start: function() {
            var b = a(this).resizable("instance"),
                c = b.size;
            b.ghost = b.originalElement.clone(), b.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: c.height,
                width: c.width,
                margin: 0,
                left: 0,
                top: 0
            }), b._addClass(b.ghost, "ui-resizable-ghost"), a.uiBackCompat !== !1 && "string" == typeof b.options.ghost && b.ghost.addClass(this.options.ghost), b.ghost.appendTo(b.helper)
        },
        resize: function() {
            var b = a(this).resizable("instance");
            b.ghost && b.ghost.css({
                position: "relative",
                height: b.size.height,
                width: b.size.width
            })
        },
        stop: function() {
            var b = a(this).resizable("instance");
            b.ghost && b.helper && b.helper.get(0).removeChild(b.ghost.get(0))
        }
    }), a.ui.plugin.add("resizable", "grid", {
        resize: function() {
            var b, c = a(this).resizable("instance"),
                d = c.options,
                e = c.size,
                f = c.originalSize,
                g = c.originalPosition,
                h = c.axis,
                i = "number" == typeof d.grid ? [d.grid, d.grid] : d.grid,
                j = i[0] || 1,
                k = i[1] || 1,
                l = Math.round((e.width - f.width) / j) * j,
                m = Math.round((e.height - f.height) / k) * k,
                n = f.width + l,
                o = f.height + m,
                p = d.maxWidth && d.maxWidth < n,
                q = d.maxHeight && d.maxHeight < o,
                r = d.minWidth && d.minWidth > n,
                s = d.minHeight && d.minHeight > o;
            d.grid = i, r && (n += j), s && (o += k), p && (n -= j), q && (o -= k), /^(se|s|e)$/.test(h) ? (c.size.width = n, c.size.height = o) : /^(ne)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.top = g.top - m) : /^(sw)$/.test(h) ? (c.size.width = n, c.size.height = o, c.position.left = g.left - l) : ((o - k <= 0 || n - j <= 0) && (b = c._getPaddingPlusBorderDimensions(this)), o - k > 0 ? (c.size.height = o, c.position.top = g.top - m) : (o = k - b.height, c.size.height = o, c.position.top = g.top + f.height - o), n - j > 0 ? (c.size.width = n, c.position.left = g.left - l) : (n = j - b.width, c.size.width = n, c.position.left = g.left + f.width - n))
        }
    }), a.ui.resizable
});;
(function(e) {
    Drupal.behaviors.betterMessages = {
        attach: function(n) {
            var s = drupalSettings.better_messages,
                o = e('#better-messages-default');
            if (s.jquery_ui.draggable == '1') {
                o.draggable()
            };
            if (s.jquery_ui.resizable == '1') {
                o.resizable()
            };
            if (!o.hasClass('better-messeges-processed')) {
                s.open = function() {
                    switch (s.popin.effect) {
                        case 'fadeIn':
                            o.fadeIn(s.popin.duration);
                            break;
                        case 'slideDown':
                            o.slideDown(s.popin.duration);
                            break;
                        default:
                            o.fadeIn(s.popin.duration);
                            break
                    }
                };
                s.close = function() {
                    switch (s.popout.effect) {
                        case 'fadeOut':
                            o.hide();
                            break;
                        case 'slideUp':
                            o.hide();
                            break;
                        default:
                            o.hide();
                            break
                    };
                    o.addClass('better-messeges-processed')
                };
                s.countDownClose = function(o) {
                    if (o > 0) {
                        o--;
                        if (s.show_countdown == '1') {
                            e('.message-timer').text(Drupal.t('Closing in !seconds seconds', {
                                '!seconds': o
                            }))
                        };
                        if (o > 0) {
                            s.countDown = setTimeout(function() {
                                s.countDownClose(o)
                            }, 1000)
                        } else {
                            s.close()
                        }
                    }
                };
                o.css('width', s.width);
                var t = s.vertical,
                    i = s.horizontal;
                switch (s.position) {
                    case 'center':
                        t = (e(window).height() - o.height()) / 2;
                        i = (e(window).width() - o.width()) / 2;
                        o.css({
                            'top': t + 'px',
                            'left': i + 'px'
                        });
                        break;
                    case 'tl':
                        o.css({
                            'top': t + 'px',
                            'left': i + 'px'
                        });
                        break;
                    case 'tr':
                        o.css({
                            'top': t + 'px',
                            'right': i + 'px'
                        });
                        break;
                    case 'bl':
                        o.css({
                            'bottom': t + 'px',
                            'left': i + 'px'
                        });
                        break;
                    case 'br':
                        o.css({
                            'bottom': t + 'px',
                            'right': i + 'px'
                        });
                        break
                };
                if (s.opendelay != 0) {
                    setTimeout(function() {
                        s.open()
                    }, s.opendelay * 1000)
                } else {
                    s.open()
                };
                if (s.autoclose != 0) {
                    s.countDownClose(s.autoclose)
                };
                if (s.hover_autoclose == '1') {
                    o.hover(function() {
                        clearTimeout(s.countDown);
                        e('.message-timer').fadeOut('slow')
                    }, function() {})
                };
                e('a.message-close').click(function() {
                    s.close();
                    return !1
                });
                e(document).keypress(function(e) {
                    if (e.keyCode == 27) {
                        s.close();
                        return !1
                    }
                });
                if (s.fixed == '1') {
                    o.css({
                        'position': 'fixed'
                    })
                } else {
                    o.css({
                        'position': 'absolute'
                    });
                    e(window).scroll(function() {
                        o.stop().css({
                            top: (e(window).scrollTop() + t) + 'px'
                        })
                    })
                }
            }
        }
    }
})(jQuery);;;
var _slice = Array.prototype.slice,
    _slicedToArray = (function() {
        function e(e, t) {
            var n = [],
                r = !0,
                a = !1,
                o = undefined;
            try {
                for (var i = e[Symbol.iterator](), s; !(r = (s = i.next()).done); r = !0) {
                    n.push(s.value);
                    if (t && n.length === t) break
                }
            } catch (l) {
                a = !0;
                o = l
            } finally {
                try {
                    if (!r && i['return']) i['return']()
                } finally {
                    if (a) throw o
                }
            };
            return n
        };
        return function(t, i) {
            if (Array.isArray(t)) {
                return t
            } else if (Symbol.iterator in Object(t)) {
                return e(t, i)
            } else {
                throw new TypeError('Invalid attempt to destructure non-iterable instance')
            }
        }
    })(),
    _extends = Object.assign || function(e) {
        for (var n = 1; n < arguments.length; n++) {
            var i = arguments[n];
            for (var t in i) {
                if (Object.prototype.hasOwnProperty.call(i, t)) {
                    e[t] = i[t]
                }
            }
        };
        return e
    };

function _toConsumableArray(e) {
    if (Array.isArray(e)) {
        for (var t = 0, i = Array(e.length); t < e.length; t++) i[t] = e[t];
        return i
    } else {
        return Array.from(e)
    }
}(function(e, t) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = t(require('jquery')) : typeof define === 'function' && define.amd ? define(['jquery'], t) : e.parsley = t(e.jQuery)
})(this, function(e) {
    'use strict';
    var V = 1,
        h = {};
    var t = {
        attr: function(e, t, i) {
            var n, r, s, a = new RegExp('^' + t, 'i');
            if ('undefined' === typeof i) i = {};
            else {
                for (n in i) {
                    if (i.hasOwnProperty(n)) delete i[n]
                }
            };
            if (!e) return i;
            s = e.attributes;
            for (n = s.length; n--;) {
                r = s[n];
                if (r && r.specified && a.test(r.name)) {
                    i[this.camelize(r.name.slice(t.length))] = this.deserializeValue(r.value)
                }
            };
            return i
        },
        checkAttr: function(e, t, i) {
            return e.hasAttribute(t + i)
        },
        setAttr: function(e, t, i, n) {
            e.setAttribute(this.dasherize(t + i), String(n))
        },
        generateID: function() {
            return '' + V++
        },
        deserializeValue: function(t) {
            var i;
            try {
                return t ? t == 'true' || (t == 'false' ? !1 : t == 'null' ? null : !isNaN(i = Number(t)) ? i : /^[\[\{]/.test(t) ? e.parseJSON(t) : t) : t
            } catch (n) {
                return t
            }
        },
        camelize: function(e) {
            return e.replace(/-+(.)?/g, function(e, t) {
                return t ? t.toUpperCase() : ''
            })
        },
        dasherize: function(e) {
            return e.replace(/::/g, '/').replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2').replace(/([a-z\d])([A-Z])/g, '$1_$2').replace(/_/g, '-').toLowerCase()
        },
        warn: function() {
            var e;
            if (window.console && 'function' === typeof window.console.warn)(e = window.console).warn.apply(e, arguments)
        },
        warnOnce: function(e) {
            if (!h[e]) {
                h[e] = !0;
                this.warn.apply(this, arguments)
            }
        },
        _resetWarnings: function() {
            h = {}
        },
        trimString: function(e) {
            return e.replace(/^\s+|\s+$/g, '')
        },
        parse: {
            date: function u(e) {
                var s = e.match(/^(\d{4,})-(\d\d)-(\d\d)$/);
                if (!s) return null;
                var a = s.map(function(e) {
                        return parseInt(e, 10)
                    }),
                    t = _slicedToArray(a, 4),
                    o = t[0],
                    i = t[1],
                    n = t[2],
                    r = t[3],
                    u = new Date(i, n - 1, r);
                if (u.getFullYear() !== i || u.getMonth() + 1 !== n || u.getDate() !== r) return null;
                return u
            },
            string: function(e) {
                return e
            },
            integer: function(e) {
                if (isNaN(e)) return null;
                return parseInt(e, 10)
            },
            number: function(e) {
                if (isNaN(e)) throw null;
                return parseFloat(e)
            },
            'boolean': function(e) {
                return !/^\s*false\s*$/i.test(e)
            },
            object: function(e) {
                return t.deserializeValue(e)
            },
            regexp: function(e) {
                var t = '';
                if (/^\/.*\/(?:[gimy]*)$/.test(e)) {
                    t = e.replace(/.*\/([gimy]*)$/, '$1');
                    e = e.replace(new RegExp('^/(.*?)/' + t + '$'), '$1')
                } else {
                    e = '^' + e + '$'
                };
                return new RegExp(e, t)
            }
        },
        parseRequirement: function(e, t) {
            var n = this.parse[e || 'string'];
            if (!n) throw 'Unknown requirement specification: "' + e + '"';
            var i = n(t);
            if (i === null) throw 'Requirement is not a ' + e + ': "' + t + '"';
            return i
        },
        namespaceEvents: function(t, i) {
            t = this.trimString(t || '').split(/\s+/);
            if (!t[0]) return '';
            return e.map(t, function(e) {
                return e + '.' + i
            }).join(' ')
        },
        difference: function(t, i) {
            var n = [];
            e.each(t, function(e, t) {
                if (i.indexOf(t) == -1) n.push(t)
            });
            return n
        },
        all: function(t) {
            return e.when.apply(e, _toConsumableArray(t).concat([42, 42]))
        },
        objectCreate: Object.create || (function() {
            var e = function() {};
            return function(t) {
                if (arguments.length > 1) {
                    throw Error('Second argument not supported')
                };
                if (typeof t != 'object') {
                    throw TypeError('Argument must be an object')
                };
                e.prototype = t;
                var i = {};
                e.prototype = null;
                return i
            }
        })(),
        _SubmitSelector: 'input[type="submit"], button:submit'
    };
    var v = {
        namespace: 'data-parsley-',
        inputs: 'input, textarea, select',
        excluded: 'input[type=button], input[type=submit], input[type=reset], input[type=hidden]',
        priorityEnabled: !0,
        multiple: null,
        group: null,
        uiEnabled: !0,
        validationThreshold: 3,
        focus: 'first',
        trigger: !1,
        triggerAfterFailure: 'input',
        errorClass: 'parsley-error',
        successClass: 'parsley-success',
        classHandler: function(e) {},
        errorsContainer: function(e) {},
        errorsWrapper: '<ul class="parsley-errors-list"></ul>',
        errorTemplate: '<li></li>'
    };
    var n = function() {
        this.__id__ = t.generateID()
    };
    n.prototype = {
        asyncSupport: !0,
        _pipeAccordingToValidationResult: function() {
            var i = this,
                t = function() {
                    var t = e.Deferred();
                    if (!0 !== i.validationResult) t.reject();
                    return t.resolve().promise()
                };
            return [t, t]
        },
        actualizeOptions: function() {
            t.attr(this.element, this.options.namespace, this.domOptions);
            if (this.parent && this.parent.actualizeOptions) this.parent.actualizeOptions();
            return this
        },
        _resetOptions: function(e) {
            this.domOptions = t.objectCreate(this.parent.options);
            this.options = t.objectCreate(this.domOptions);
            for (var i in e) {
                if (e.hasOwnProperty(i)) this.options[i] = e[i]
            };
            this.actualizeOptions()
        },
        _listeners: null,
        on: function(e, t) {
            this._listeners = this._listeners || {};
            var i = this._listeners[e] = this._listeners[e] || [];
            i.push(t);
            return this
        },
        subscribe: function(t, i) {
            e.listenTo(this, t.toLowerCase(), i)
        },
        off: function(e, t) {
            var i = this._listeners && this._listeners[e];
            if (i) {
                if (!t) {
                    delete this._listeners[e]
                } else {
                    for (var n = i.length; n--;)
                        if (i[n] === t) i.splice(n, 1)
                }
            };
            return this
        },
        unsubscribe: function(t, i) {
            e.unsubscribeTo(this, t.toLowerCase())
        },
        trigger: function(e, t, i) {
            t = t || this;
            var n = this._listeners && this._listeners[e],
                r, a;
            if (n) {
                for (var s = n.length; s--;) {
                    r = n[s].call(t, t, i);
                    if (r === !1) return r
                }
            };
            if (this.parent) {
                return this.parent.trigger(e, t, i)
            };
            return !0
        },
        asyncIsValid: function(e, i) {
            t.warnOnce('asyncIsValid is deprecated; please use whenValid instead');
            return this.whenValid({
                group: e,
                force: i
            })
        },
        _findRelated: function() {
            return this.options.multiple ? e(this.parent.element.querySelectorAll('[' + this.options.namespace + 'multiple="' + this.options.multiple + '"]')) : this.$element
        }
    };
    var z = function(e, i) {
            var r = e.match(/^\s*\[(.*)\]\s*$/);
            if (!r) throw 'Requirement is not an array: "' + e + '"';
            var n = r[1].split(',').map(t.trimString);
            if (n.length !== i) throw 'Requirement has ' + n.length + ' values when ' + i + ' are needed';
            return n
        },
        T = function(e, i, n) {
            var a = null,
                o = {};
            for (var r in e) {
                if (r) {
                    var s = n(r);
                    if ('string' === typeof s) s = t.parseRequirement(e[r], s);
                    o[r] = s
                } else {
                    a = t.parseRequirement(e[r], i)
                }
            };
            return [a, o]
        },
        c = function(t) {
            e.extend(!0, this, t)
        };
    c.prototype = {
        validate: function(e, i) {
            if (this.fn) {
                if (arguments.length > 3) i = [].slice.call(arguments, 1, -1);
                return this.fn(e, i)
            };
            if (Array.isArray(e)) {
                if (!this.validateMultiple) throw 'Validator `' + this.name + '` does not handle multiple values';
                return this.validateMultiple.apply(this, arguments)
            } else {
                var n = arguments[arguments.length - 1];
                if (this.validateDate && n._isDateInput()) {
                    arguments[0] = t.parse.date(arguments[0]);
                    if (arguments[0] === null) return !1;
                    return this.validateDate.apply(this, arguments)
                };
                if (this.validateNumber) {
                    if (isNaN(e)) return !1;
                    arguments[0] = parseFloat(arguments[0]);
                    return this.validateNumber.apply(this, arguments)
                };
                if (this.validateString) {
                    return this.validateString.apply(this, arguments)
                };
                throw 'Validator `' + this.name + '` only handles multiple values'
            }
        },
        parseRequirements: function(i, n) {
            if ('string' !== typeof i) {
                return Array.isArray(i) ? i : [i]
            };
            var r = this.requirementType;
            if (Array.isArray(r)) {
                var a = z(i, r.length);
                for (var s = 0; s < a.length; s++) a[s] = t.parseRequirement(r[s], a[s]);
                return a
            } else if (e.isPlainObject(r)) {
                return T(r, i, n)
            } else {
                return [t.parseRequirement(r, i)]
            }
        },
        requirementType: 'string',
        priority: 2
    };
    var y = function(e, t) {
            this.__class__ = 'ValidatorRegistry';
            this.locale = 'en';
            this.init(e || {}, t || {})
        },
        p = {
            email: /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,
            number: /^-?(\d*\.)?\d+(e[-+]?\d+)?$/i,
            integer: /^-?\d+$/,
            digits: /^\d+$/,
            alphanum: /^\w+$/i,
            date: {
                test: function(e) {
                    return t.parse.date(e) !== null
                }
            },
            url: new RegExp('^(?:(?:https?|ftp)://)?(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:/\\S*)?$', 'i')
        };
    p.range = p.number;
    var g = function(e) {
            var t = ('' + e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if (!t) {
                return 0
            };
            return Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0))
        },
        D = function(e, i) {
            return i.map(t.parse[e])
        },
        w = function(e, t) {
            return function(i) {
                for (var r = arguments.length, s = Array(r > 1 ? r - 1 : 0), n = 1; n < r; n++) {
                    s[n - 1] = arguments[n]
                };
                s.pop();
                return t.apply(undefined, [i].concat(_toConsumableArray(D(e, s))))
            }
        },
        m = function(e) {
            return {
                validateDate: w('date', e),
                validateNumber: w('number', e),
                requirementType: e.length <= 2 ? 'string' : ['string', 'string'],
                priority: 30
            }
        };
    y.prototype = {
        init: function(e, t) {
            this.catalog = t;
            this.validators = _extends({}, this.validators);
            for (var i in e) this.addValidator(i, e[i].fn, e[i].priority);
            window.Parsley.trigger('parsley:validator:init')
        },
        setLocale: function(e) {
            if ('undefined' === typeof this.catalog[e]) throw new Error(e + ' is not available in the catalog');
            this.locale = e;
            return this
        },
        addCatalog: function(e, t, i) {
            if ('object' === typeof t) this.catalog[e] = t;
            if (!0 === i) return this.setLocale(e);
            return this
        },
        addMessage: function(e, t, i) {
            if ('undefined' === typeof this.catalog[e]) this.catalog[e] = {};
            this.catalog[e][t] = i;
            return this
        },
        addMessages: function(e, t) {
            for (var i in t) this.addMessage(e, i, t[i]);
            return this
        },
        addValidator: function(e, i, n) {
            if (this.validators[e]) t.warn('Validator "' + e + '" is already defined.');
            else if (v.hasOwnProperty(e)) {
                t.warn('"' + e + '" is a restricted keyword and is not a valid validator name.');
                return
            };
            return this._setValidator.apply(this, arguments)
        },
        updateValidator: function(e, i, n) {
            if (!this.validators[e]) {
                t.warn('Validator "' + e + '" is not already defined.');
                return this.addValidator.apply(this, arguments)
            };
            return this._setValidator.apply(this, arguments)
        },
        removeValidator: function(e) {
            if (!this.validators[e]) t.warn('Validator "' + e + '" is not defined.');
            delete this.validators[e];
            return this
        },
        _setValidator: function(e, t, i) {
            if ('object' !== typeof t) {
                t = {
                    fn: t,
                    priority: i
                }
            };
            if (!t.validate) {
                t = new c(t)
            };
            this.validators[e] = t;
            for (var n in t.messages || {}) this.addMessage(n, e, t.messages[n]);
            return this
        },
        getErrorMessage: function(e) {
            var t;
            if ('type' === e.name) {
                var i = this.catalog[this.locale][e.name] || {};
                t = i[e.requirements]
            } else t = this.formatMessage(this.catalog[this.locale][e.name], e.requirements);
            return t || this.catalog[this.locale].defaultMessage || this.catalog.en.defaultMessage
        },
        formatMessage: function(e, t) {
            if ('object' === typeof t) {
                for (var i in t) e = this.formatMessage(e, t[i]);
                return e
            };
            return 'string' === typeof e ? e.replace(/%s/i, t) : ''
        },
        validators: {
            notblank: {
                validateString: function(e) {
                    return (/\S/.test(e))
                },
                priority: 2
            },
            required: {
                validateMultiple: function(e) {
                    return e.length > 0
                },
                validateString: function(e) {
                    return (/\S/.test(e))
                },
                priority: 512
            },
            type: {
                validateString: function(e, t) {
                    var d = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];
                    var a = d.step,
                        n = a === undefined ? 'any' : a,
                        o = d.base,
                        l = o === undefined ? 0 : o,
                        u = p[t];
                    if (!u) {
                        throw new Error('validator type `' + t + '` is not supported')
                    };
                    if (!u.test(e)) return !1;
                    if ('number' === t) {
                        if (!/^any$/i.test(n || '')) {
                            var r = Number(e),
                                s = Math.max(g(n), g(l));
                            if (g(r) > s) return !1;
                            var i = function(e) {
                                return Math.round(e * Math.pow(10, s))
                            };
                            if ((i(r) - i(l)) % i(n) != 0) return !1
                        }
                    };
                    return !0
                },
                requirementType: {
                    '': 'string',
                    step: 'string',
                    base: 'number'
                },
                priority: 256
            },
            pattern: {
                validateString: function(e, t) {
                    return t.test(e)
                },
                requirementType: 'regexp',
                priority: 64
            },
            minlength: {
                validateString: function(e, t) {
                    return e.length >= t
                },
                requirementType: 'integer',
                priority: 30
            },
            maxlength: {
                validateString: function(e, t) {
                    return e.length <= t
                },
                requirementType: 'integer',
                priority: 30
            },
            length: {
                validateString: function(e, t, i) {
                    return e.length >= t && e.length <= i
                },
                requirementType: ['integer', 'integer'],
                priority: 30
            },
            mincheck: {
                validateMultiple: function(e, t) {
                    return e.length >= t
                },
                requirementType: 'integer',
                priority: 30
            },
            maxcheck: {
                validateMultiple: function(e, t) {
                    return e.length <= t
                },
                requirementType: 'integer',
                priority: 30
            },
            check: {
                validateMultiple: function(e, t, i) {
                    return e.length >= t && e.length <= i
                },
                requirementType: ['integer', 'integer'],
                priority: 30
            },
            min: m(function(e, t) {
                return e >= t
            }),
            max: m(function(e, t) {
                return e <= t
            }),
            range: m(function(e, t, i) {
                return e >= t && e <= i
            }),
            equalto: {
                validateString: function(t, i) {
                    var n = e(i);
                    if (n.length) return t === n.val();
                    else return t === i
                },
                priority: 256
            }
        }
    };
    var l = {};
    var I = function N(e, t, i) {
        var a = [],
            o = [];
        for (var n = 0; n < e.length; n++) {
            var s = !1;
            for (var r = 0; r < t.length; r++)
                if (e[n].assert.name === t[r].assert.name) {
                    s = !0;
                    break
                };
            if (s) o.push(e[n]);
            else a.push(e[n])
        };
        return {
            kept: o,
            added: a,
            removed: !i ? N(t, e, !0).added : []
        }
    };
    l.Form = {
        _actualizeTriggers: function() {
            var e = this;
            this.$element.on('submit.Parsley', function(t) {
                e.onSubmitValidate(t)
            });
            this.$element.on('click.Parsley', t._SubmitSelector, function(t) {
                e.onSubmitButton(t)
            });
            if (!1 === this.options.uiEnabled) return;
            this.element.setAttribute('novalidate', '')
        },
        focus: function() {
            this._focusedField = null;
            if (!0 === this.validationResult || 'none' === this.options.focus) return null;
            for (var t = 0; t < this.fields.length; t++) {
                var e = this.fields[t];
                if (!0 !== e.validationResult && e.validationResult.length > 0 && 'undefined' === typeof e.options.noFocus) {
                    this._focusedField = e.$element;
                    if ('first' === this.options.focus) break
                }
            };
            if (null === this._focusedField) return null;
            return this._focusedField.focus()
        },
        _destroyUI: function() {
            this.$element.off('.Parsley')
        }
    };
    l.Field = {
        _reflowUI: function() {
            this._buildUI();
            if (!this._ui) return;
            var e = I(this.validationResult, this._ui.lastValidationResult);
            this._ui.lastValidationResult = this.validationResult;
            this._manageStatusClass();
            this._manageErrorsMessages(e);
            this._actualizeTriggers();
            if ((e.kept.length || e.added.length) && !this._failedOnce) {
                this._failedOnce = !0;
                this._actualizeTriggers()
            }
        },
        getErrorsMessages: function() {
            if (!0 === this.validationResult) return [];
            var t = [];
            for (var e = 0; e < this.validationResult.length; e++) t.push(this.validationResult[e].errorMessage || this._getErrorMessage(this.validationResult[e].assert));
            return t
        },
        addError: function(e) {
            var t = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
            var n = t.message,
                r = t.assert,
                i = t.updateClass,
                s = i === undefined ? !0 : i;
            this._buildUI();
            this._addError(e, {
                message: n,
                assert: r
            });
            if (s) this._errorClass()
        },
        updateError: function(e) {
            var t = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
            var n = t.message,
                r = t.assert,
                i = t.updateClass,
                s = i === undefined ? !0 : i;
            this._buildUI();
            this._updateError(e, {
                message: n,
                assert: r
            });
            if (s) this._errorClass()
        },
        removeError: function(e) {
            var n = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
            var t = n.updateClass,
                i = t === undefined ? !0 : t;
            this._buildUI();
            this._removeError(e);
            if (i) this._manageStatusClass()
        },
        _manageStatusClass: function() {
            if (this.hasConstraints() && this.needsValidation() && !0 === this.validationResult) this._successClass();
            else if (this.validationResult.length > 0) this._errorClass();
            else this._resetClass()
        },
        _manageErrorsMessages: function(t) {
            if ('undefined' !== typeof this.options.errorsMessagesDisabled) return;
            if ('undefined' !== typeof this.options.errorMessage) {
                if (t.added.length || t.kept.length) {
                    this._insertErrorWrapper();
                    if (0 === this._ui.$errorsWrapper.find('.parsley-custom-error-message').length) this._ui.$errorsWrapper.append(e(this.options.errorTemplate).addClass('parsley-custom-error-message'));
                    return this._ui.$errorsWrapper.addClass('filled').find('.parsley-custom-error-message').html(this.options.errorMessage)
                };
                return this._ui.$errorsWrapper.removeClass('filled').find('.parsley-custom-error-message').remove()
            };
            for (var i = 0; i < t.removed.length; i++) this._removeError(t.removed[i].assert.name);
            for (i = 0; i < t.added.length; i++) this._addError(t.added[i].assert.name, {
                message: t.added[i].errorMessage,
                assert: t.added[i].assert
            });
            for (i = 0; i < t.kept.length; i++) this._updateError(t.kept[i].assert.name, {
                message: t.kept[i].errorMessage,
                assert: t.kept[i].assert
            })
        },
        _addError: function(t, i) {
            var n = i.message,
                r = i.assert;
            this._insertErrorWrapper();
            this._ui.$errorsWrapper.addClass('filled').append(e(this.options.errorTemplate).addClass('parsley-' + t).html(n || this._getErrorMessage(r)))
        },
        _updateError: function(e, t) {
            var i = t.message,
                n = t.assert;
            this._ui.$errorsWrapper.addClass('filled').find('.parsley-' + e).html(i || this._getErrorMessage(n))
        },
        _removeError: function(e) {
            this._ui.$errorsWrapper.removeClass('filled').find('.parsley-' + e).remove()
        },
        _getErrorMessage: function(e) {
            var t = e.name + 'Message';
            if ('undefined' !== typeof this.options[t]) return window.Parsley.formatMessage(this.options[t], e.requirements);
            return window.Parsley.getErrorMessage(e)
        },
        _buildUI: function() {
            if (this._ui || !1 === this.options.uiEnabled) return;
            var t = {};
            this.element.setAttribute(this.options.namespace + 'id', this.__id__);
            t.$errorClassHandler = this._manageClassHandler();
            t.errorsWrapperId = 'parsley-id-' + (this.options.multiple ? 'multiple-' + this.options.multiple : this.__id__);
            t.$errorsWrapper = e(this.options.errorsWrapper).attr('id', t.errorsWrapperId);
            t.lastValidationResult = [];
            t.validationInformationVisible = !1;
            this._ui = t
        },
        _manageClassHandler: function() {
            if ('string' === typeof this.options.classHandler) {
                if (e(this.options.classHandler).length === 0) ParsleyUtils.warn('No elements found that match the selector `' + this.options.classHandler + '` set in options.classHandler or data-parsley-class-handler');
                return e(this.options.classHandler)
            };
            if ('function' === typeof this.options.classHandler) var t = this.options.classHandler.call(this, this);
            if ('undefined' !== typeof t && t.length) return t;
            return this._inputHolder()
        },
        _inputHolder: function() {
            if (!this.options.multiple || this.element.nodeName === 'SELECT') return this.$element;
            return this.$element.parent()
        },
        _insertErrorWrapper: function() {
            var i;
            if (0 !== this._ui.$errorsWrapper.parent().length) return this._ui.$errorsWrapper.parent();
            if ('string' === typeof this.options.errorsContainer) {
                if (e(this.options.errorsContainer).length) return e(this.options.errorsContainer).append(this._ui.$errorsWrapper);
                else t.warn('The errors container `' + this.options.errorsContainer + '` does not exist in DOM')
            } else if ('function' === typeof this.options.errorsContainer) i = this.options.errorsContainer.call(this, this);
            if ('undefined' !== typeof i && i.length) return i.append(this._ui.$errorsWrapper);
            return this._inputHolder().after(this._ui.$errorsWrapper)
        },
        _actualizeTriggers: function() {
            var i = this,
                e = this._findRelated(),
                n;
            e.off('.Parsley');
            if (this._failedOnce) e.on(t.namespaceEvents(this.options.triggerAfterFailure, 'Parsley'), function() {
                i._validateIfNeeded()
            });
            else if (n = t.namespaceEvents(this.options.trigger, 'Parsley')) {
                e.on(n, function(e) {
                    i._validateIfNeeded(e)
                })
            }
        },
        _validateIfNeeded: function(e) {
            var t = this;
            if (e && /key|input/.test(e.type))
                if (!(this._ui && this._ui.validationInformationVisible) && this.getValue().length <= this.options.validationThreshold) return;
            if (this.options.debounce) {
                window.clearTimeout(this._debounced);
                this._debounced = window.setTimeout(function() {
                    return t.validate()
                }, this.options.debounce)
            } else this.validate()
        },
        _resetUI: function() {
            this._failedOnce = !1;
            this._actualizeTriggers();
            if ('undefined' === typeof this._ui) return;
            this._ui.$errorsWrapper.removeClass('filled').children().remove();
            this._resetClass();
            this._ui.lastValidationResult = [];
            this._ui.validationInformationVisible = !1
        },
        _destroyUI: function() {
            this._resetUI();
            if ('undefined' !== typeof this._ui) this._ui.$errorsWrapper.remove();
            delete this._ui
        },
        _successClass: function() {
            this._ui.validationInformationVisible = !0;
            this._ui.$errorClassHandler.removeClass(this.options.errorClass).addClass(this.options.successClass)
        },
        _errorClass: function() {
            this._ui.validationInformationVisible = !0;
            this._ui.$errorClassHandler.removeClass(this.options.successClass).addClass(this.options.errorClass)
        },
        _resetClass: function() {
            this._ui.$errorClassHandler.removeClass(this.options.successClass).removeClass(this.options.errorClass)
        }
    };
    var o = function(t, i, n) {
            this.__class__ = 'Form';
            this.element = t;
            this.$element = e(t);
            this.domOptions = i;
            this.options = n;
            this.parent = window.Parsley;
            this.fields = [];
            this.validationResult = null
        },
        F = {
            pending: null,
            resolved: !0,
            rejected: !1
        };
    o.prototype = {
        onSubmitValidate: function(e) {
            var r = this;
            if (!0 === e.parsley) return;
            var n = this._submitSource || this.$element.find(t._SubmitSelector)[0];
            this._submitSource = null;
            this.$element.find('.parsley-synthetic-submit-button').prop('disabled', !0);
            if (n && null !== n.getAttribute('formnovalidate')) return;
            window.Parsley._remoteCache = {};
            var i = this.whenValidate({
                event: e
            });
            if ('resolved' === i.state() && !1 !== this._trigger('submit')) {} else {
                e.stopImmediatePropagation();
                e.preventDefault();
                if ('pending' === i.state()) i.done(function() {
                    r._submit(n)
                })
            }
        },
        onSubmitButton: function(e) {
            this._submitSource = e.currentTarget
        },
        _submit: function(t) {
            if (!1 === this._trigger('submit')) return;
            if (t) {
                var i = this.$element.find('.parsley-synthetic-submit-button').prop('disabled', !1);
                if (0 === i.length) i = e('<input class="parsley-synthetic-submit-button" type="hidden">').appendTo(this.$element);
                i.attr({
                    name: t.getAttribute('name'),
                    value: t.getAttribute('value')
                })
            };
            this.$element.trigger(_extends(e.Event('submit'), {
                parsley: !0
            }))
        },
        validate: function(i) {
            if (arguments.length >= 1 && !e.isPlainObject(i)) {
                t.warnOnce('Calling validate on a parsley form without passing arguments as an object is deprecated.');
                var n = _slice.call(arguments),
                    r = n[0],
                    s = n[1],
                    a = n[2];
                i = {
                    group: r,
                    force: s,
                    event: a
                }
            };
            return F[this.whenValidate(i).state()]
        },
        whenValidate: function() {
            var s, i = this,
                r = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
            var o = r.group,
                l = r.force,
                n = r.event;
            this.submitEvent = n;
            if (n) {
                this.submitEvent = _extends({}, n, {
                    preventDefault: function() {
                        t.warnOnce('Using `this.submitEvent.preventDefault()` is deprecated; instead, call `this.validationResult = false`');
                        i.validationResult = !1
                    }
                })
            };
            this.validationResult = !0;
            this._trigger('validate');
            this._refreshFields();
            var a = this._withoutReactualizingFormOptions(function() {
                return e.map(i.fields, function(e) {
                    return e.whenValidate({
                        force: l,
                        group: o
                    })
                })
            });
            return (s = t.all(a).done(function() {
                i._trigger('success')
            }).fail(function() {
                i.validationResult = !1;
                i.focus();
                i._trigger('error')
            }).always(function() {
                i._trigger('validated')
            })).pipe.apply(s, _toConsumableArray(this._pipeAccordingToValidationResult()))
        },
        isValid: function(i) {
            if (arguments.length >= 1 && !e.isPlainObject(i)) {
                t.warnOnce('Calling isValid on a parsley form without passing arguments as an object is deprecated.');
                var n = _slice.call(arguments),
                    r = n[0],
                    s = n[1];
                i = {
                    group: r,
                    force: s
                }
            };
            return F[this.whenValid(i).state()]
        },
        whenValid: function() {
            var a = this,
                i = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
            var r = i.group,
                s = i.force;
            this._refreshFields();
            var n = this._withoutReactualizingFormOptions(function() {
                return e.map(a.fields, function(e) {
                    return e.whenValid({
                        group: r,
                        force: s
                    })
                })
            });
            return t.all(n)
        },
        reset: function() {
            for (var e = 0; e < this.fields.length; e++) this.fields[e].reset();
            this._trigger('reset')
        },
        destroy: function() {
            this._destroyUI();
            for (var e = 0; e < this.fields.length; e++) this.fields[e].destroy();
            this.$element.removeData('Parsley');
            this._trigger('destroy')
        },
        _refreshFields: function() {
            return this.actualizeOptions()._bindFields()
        },
        _bindFields: function() {
            var i = this,
                n = this.fields;
            this.fields = [];
            this.fieldsMappedById = {};
            this._withoutReactualizingFormOptions(function() {
                i.$element.find(i.options.inputs).not(i.options.excluded).each(function(e, t) {
                    var n = new window.Parsley.Factory(t, {}, i);
                    if (('Field' === n.__class__ || 'FieldMultiple' === n.__class__) && !0 !== n.options.excluded) {
                        var r = n.__class__ + '-' + n.__id__;
                        if ('undefined' === typeof i.fieldsMappedById[r]) {
                            i.fieldsMappedById[r] = n;
                            i.fields.push(n)
                        }
                    }
                });
                e.each(t.difference(n, i.fields), function(e, t) {
                    t.reset()
                })
            });
            return this
        },
        _withoutReactualizingFormOptions: function(e) {
            var i = this.actualizeOptions;
            this.actualizeOptions = function() {
                return this
            };
            var t = e();
            this.actualizeOptions = i;
            return t
        },
        _trigger: function(e) {
            return this.trigger('form:' + e)
        }
    };
    var C = function(e, t, i, n, r) {
            var a = window.Parsley._validatorRegistry.validators[t],
                s = new c(a);
            n = n || e.options[t + 'Priority'] || s.priority;
            r = !0 === r;
            _extends(this, {
                validator: s,
                name: t,
                requirements: i,
                priority: n,
                isDomConstraint: r
            });
            this._parseRequirements(e.options)
        },
        q = function(e) {
            var t = e[0].toUpperCase();
            return t + e.slice(1)
        };
    C.prototype = {
        validate: function(e, t) {
            var i;
            return (i = this.validator).validate.apply(i, [e].concat(_toConsumableArray(this.requirementList), [t]))
        },
        _parseRequirements: function(e) {
            var t = this;
            this.requirementList = this.validator.parseRequirements(this.requirements, function(i) {
                return e[t.name + q(i)]
            })
        }
    };
    var E = function(t, i, n, r) {
            this.__class__ = 'Field';
            this.element = t;
            this.$element = e(t);
            if ('undefined' !== typeof r) {
                this.parent = r
            };
            this.options = n;
            this.domOptions = i;
            this.constraints = [];
            this.constraintsByName = {};
            this.validationResult = !0;
            this._bindConstraints()
        },
        S = {
            pending: null,
            resolved: !0,
            rejected: !1
        };
    E.prototype = {
        validate: function(i) {
            if (arguments.length >= 1 && !e.isPlainObject(i)) {
                t.warnOnce('Calling validate on a parsley field without passing arguments as an object is deprecated.');
                i = {
                    options: i
                }
            };
            var n = this.whenValidate(i);
            if (!n) return !0;
            switch (n.state()) {
                case 'pending':
                    return null;
                case 'resolved':
                    return !0;
                case 'rejected':
                    return this.validationResult
            }
        },
        whenValidate: function() {
            var i, e = this,
                n = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
            var r = n.force,
                t = n.group;
            this.refreshConstraints();
            if (t && !this._isInGroup(t)) return;
            this.value = this.getValue();
            this._trigger('validate');
            return (i = this.whenValid({
                force: r,
                value: this.value,
                _refreshed: !0
            }).always(function() {
                e._reflowUI()
            }).done(function() {
                e._trigger('success')
            }).fail(function() {
                e._trigger('error')
            }).always(function() {
                e._trigger('validated')
            })).pipe.apply(i, _toConsumableArray(this._pipeAccordingToValidationResult()))
        },
        hasConstraints: function() {
            return 0 !== this.constraints.length
        },
        needsValidation: function(e) {
            if ('undefined' === typeof e) e = this.getValue();
            if (!e.length && !this._isRequired() && 'undefined' === typeof this.options.validateIfEmpty) return !1;
            return !0
        },
        _isInGroup: function(t) {
            if (Array.isArray(this.options.group)) return -1 !== e.inArray(t, this.options.group);
            return this.options.group === t
        },
        isValid: function(i) {
            if (arguments.length >= 1 && !e.isPlainObject(i)) {
                t.warnOnce('Calling isValid on a parsley field without passing arguments as an object is deprecated.');
                var r = _slice.call(arguments),
                    s = r[0],
                    a = r[1];
                i = {
                    force: s,
                    value: a
                }
            };
            var n = this.whenValid(i);
            if (!n) return !0;
            return S[n.state()]
        },
        whenValid: function() {
            var d = this,
                n = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
            var s = n.force,
                l = s === undefined ? !1 : s,
                i = n.value,
                a = n.group,
                u = n._refreshed;
            if (!u) this.refreshConstraints();
            if (a && !this._isInGroup(a)) return;
            this.validationResult = !0;
            if (!this.hasConstraints()) return e.when();
            if ('undefined' === typeof i || null === i) i = this.getValue();
            if (!this.needsValidation(i) && !0 !== l) return e.when();
            var o = this._getGroupedConstraints(),
                r = [];
            e.each(o, function(n, s) {
                var a = t.all(e.map(s, function(e) {
                    return d._validateConstraint(i, e)
                }));
                r.push(a);
                if (a.state() === 'rejected') return !1
            });
            return t.all(r)
        },
        _validateConstraint: function(i, n) {
            var r = this,
                s = n.validate(i, this);
            if (!1 === s) s = e.Deferred().reject();
            return t.all([s]).fail(function(e) {
                if (!(r.validationResult instanceof Array)) r.validationResult = [];
                r.validationResult.push({
                    assert: n,
                    errorMessage: 'string' === typeof e && e
                })
            })
        },
        getValue: function() {
            var e;
            if ('function' === typeof this.options.value) e = this.options.value(this);
            else if ('undefined' !== typeof this.options.value) e = this.options.value;
            else e = this.$element.val();
            if ('undefined' === typeof e || null === e) return '';
            return this._handleWhitespace(e)
        },
        reset: function() {
            this._resetUI();
            return this._trigger('reset')
        },
        destroy: function() {
            this._destroyUI();
            this.$element.removeData('Parsley');
            this.$element.removeData('FieldMultiple');
            this._trigger('destroy')
        },
        refreshConstraints: function() {
            return this.actualizeOptions()._bindConstraints()
        },
        addConstraint: function(e, t, i, n) {
            if (window.Parsley._validatorRegistry.validators[e]) {
                var r = new C(this, e, t, i, n);
                if ('undefined' !== this.constraintsByName[r.name]) this.removeConstraint(r.name);
                this.constraints.push(r);
                this.constraintsByName[r.name] = r
            };
            return this
        },
        removeConstraint: function(e) {
            for (var t = 0; t < this.constraints.length; t++)
                if (e === this.constraints[t].name) {
                    this.constraints.splice(t, 1);
                    break
                };
            delete this.constraintsByName[e];
            return this
        },
        updateConstraint: function(e, t, i) {
            return this.removeConstraint(e).addConstraint(e, t, i)
        },
        _bindConstraints: function() {
            var i = [],
                n = {};
            for (var e = 0; e < this.constraints.length; e++)
                if (!1 === this.constraints[e].isDomConstraint) {
                    i.push(this.constraints[e]);
                    n[this.constraints[e].name] = this.constraints[e]
                };
            this.constraints = i;
            this.constraintsByName = n;
            for (var t in this.options) this.addConstraint(t, this.options[t], undefined, !0);
            return this._bindHtml5Constraints()
        },
        _bindHtml5Constraints: function() {
            if (null !== this.element.getAttribute('required')) this.addConstraint('required', !0, undefined, !0);
            if (null !== this.element.getAttribute('pattern')) this.addConstraint('pattern', this.element.getAttribute('pattern'), undefined, !0);
            var e = this.element.getAttribute('min'),
                t = this.element.getAttribute('max');
            if (null !== e && null !== t) this.addConstraint('range', [e, t], undefined, !0);
            else if (null !== e) this.addConstraint('min', e, undefined, !0);
            else if (null !== t) this.addConstraint('max', t, undefined, !0);
            if (null !== this.element.getAttribute('minlength') && null !== this.element.getAttribute('maxlength')) this.addConstraint('length', [this.element.getAttribute('minlength'), this.element.getAttribute('maxlength')], undefined, !0);
            else if (null !== this.element.getAttribute('minlength')) this.addConstraint('minlength', this.element.getAttribute('minlength'), undefined, !0);
            else if (null !== this.element.getAttribute('maxlength')) this.addConstraint('maxlength', this.element.getAttribute('maxlength'), undefined, !0);
            var i = this.element.type;
            if ('number' === i) {
                return this.addConstraint('type', ['number', {
                    step: this.element.getAttribute('step') || '1',
                    base: e || this.element.getAttribute('value')
                }], undefined, !0)
            } else if (/^(email|url|range|date)$/i.test(i)) {
                return this.addConstraint('type', i, undefined, !0)
            };
            return this
        },
        _isRequired: function() {
            if ('undefined' === typeof this.constraintsByName.required) return !1;
            return !1 !== this.constraintsByName.required.requirements
        },
        _trigger: function(e) {
            return this.trigger('field:' + e)
        },
        _handleWhitespace: function(e) {
            if (!0 === this.options.trimValue) t.warnOnce('data-parsley-trim-value="true" is deprecated, please use data-parsley-whitespace="trim"');
            if ('squish' === this.options.whitespace) e = e.replace(/\s{2,}/g, ' ');
            if ('trim' === this.options.whitespace || 'squish' === this.options.whitespace || !0 === this.options.trimValue) e = t.trimString(e);
            return e
        },
        _isDateInput: function() {
            var e = this.constraintsByName.type;
            return e && e.requirements === 'date'
        },
        _getGroupedConstraints: function() {
            if (!1 === this.options.priorityEnabled) return [this.constraints];
            var i = [],
                n = {};
            for (var e = 0; e < this.constraints.length; e++) {
                var t = this.constraints[e].priority;
                if (!n[t]) i.push(n[t] = []);
                n[t].push(this.constraints[e])
            };
            i.sort(function(e, t) {
                return t[0].priority - e[0].priority
            });
            return i
        }
    };
    var s = E,
        A = function() {
            this.__class__ = 'FieldMultiple'
        };
    A.prototype = {
        addElement: function(e) {
            this.$elements.push(e);
            return this
        },
        refreshConstraints: function() {
            var i;
            this.constraints = [];
            if (this.element.nodeName === 'SELECT') {
                this.actualizeOptions()._bindConstraints();
                return this
            };
            for (var n = 0; n < this.$elements.length; n++) {
                if (!e('html').has(this.$elements[n]).length) {
                    this.$elements.splice(n, 1);
                    continue
                };
                i = this.$elements[n].data('FieldMultiple').refreshConstraints().constraints;
                for (var t = 0; t < i.length; t++) this.addConstraint(i[t].name, i[t].requirements, i[t].priority, i[t].isDomConstraint)
            };
            return this
        },
        getValue: function() {
            if ('function' === typeof this.options.value) return this.options.value(this);
            else if ('undefined' !== typeof this.options.value) return this.options.value;
            if (this.element.nodeName === 'INPUT') {
                if (this.element.type === 'radio') return this._findRelated().filter(':checked').val() || '';
                if (this.element.type === 'checkbox') {
                    var t = [];
                    this._findRelated().filter(':checked').each(function() {
                        t.push(e(this).val())
                    });
                    return t
                }
            };
            if (this.element.nodeName === 'SELECT' && null === this.$element.val()) return [];
            return this.$element.val()
        },
        _init: function() {
            this.$elements = [this.$element];
            return this
        }
    };
    var d = function(t, i, n) {
        this.element = t;
        this.$element = e(t);
        var r = this.$element.data('Parsley');
        if (r) {
            if ('undefined' !== typeof n && r.parent === window.Parsley) {
                r.parent = n;
                r._resetOptions(r.options)
            };
            if ('object' === typeof i) {
                _extends(r.options, i)
            };
            return r
        };
        if (!this.$element.length) throw new Error('You must bind Parsley on an existing element.');
        if ('undefined' !== typeof n && 'Form' !== n.__class__) throw new Error('Parent instance must be a Form instance');
        this.parent = n || window.Parsley;
        return this.init(i)
    };
    d.prototype = {
        init: function(e) {
            this.__class__ = 'Parsley';
            this.__version__ = '2.7.2';
            this.__id__ = t.generateID();
            this._resetOptions(e);
            if (this.element.nodeName === 'FORM' || t.checkAttr(this.element, this.options.namespace, 'validate') && !this.$element.is(this.options.inputs)) return this.bind('parsleyForm');
            return this.isMultiple() ? this.handleMultiple() : this.bind('parsleyField')
        },
        isMultiple: function() {
            return this.element.type === 'radio' || this.element.type === 'checkbox' || this.element.nodeName === 'SELECT' && null !== this.element.getAttribute('multiple')
        },
        handleMultiple: function() {
            var a = this,
                r, o, i;
            this.options.multiple = this.options.multiple || (r = this.element.getAttribute('name')) || this.element.getAttribute('id');
            if (this.element.nodeName === 'SELECT' && null !== this.element.getAttribute('multiple')) {
                this.options.multiple = this.options.multiple || this.__id__;
                return this.bind('parsleyFieldMultiple')
            } else if (!this.options.multiple) {
                t.warn('To be bound by Parsley, a radio, a checkbox and a multiple select input must have either a name or a multiple option.', this.$element);
                return this
            };
            this.options.multiple = this.options.multiple.replace(/(:|\.|\[|\]|\{|\}|\$)/g, '');
            if (r) {
                e('input[name="' + r + '"]').each(function(e, t) {
                    if (t.type === 'radio' || t.type === 'checkbox') t.setAttribute(a.options.namespace + 'multiple', a.options.multiple)
                })
            };
            var s = this._findRelated();
            for (var n = 0; n < s.length; n++) {
                i = e(s.get(n)).data('Parsley');
                if ('undefined' !== typeof i) {
                    if (!this.$element.data('FieldMultiple')) {
                        i.addElement(this.$element)
                    };
                    break
                }
            };
            this.bind('parsleyField', !0);
            return i || this.bind('parsleyFieldMultiple')
        },
        bind: function(i, r) {
            var a;
            switch (i) {
                case 'parsleyForm':
                    a = e.extend(new o(this.element, this.domOptions, this.options), new n(), window.ParsleyExtend)._bindFields();
                    break;
                case 'parsleyField':
                    a = e.extend(new s(this.element, this.domOptions, this.options, this.parent), new n(), window.ParsleyExtend);
                    break;
                case 'parsleyFieldMultiple':
                    a = e.extend(new s(this.element, this.domOptions, this.options, this.parent), new A(), new n(), window.ParsleyExtend)._init();
                    break;
                default:
                    throw new Error(i + 'is not a supported Parsley type')
            };
            if (this.options.multiple) t.setAttr(this.element, this.options.namespace, 'multiple', this.options.multiple);
            if ('undefined' !== typeof r) {
                this.$element.data('FieldMultiple', a);
                return a
            };
            this.$element.data('Parsley', a);
            a._actualizeTriggers();
            a._trigger('init');
            return a
        }
    };
    var f = e.fn.jquery.split('.');
    if (parseInt(f[0]) <= 1 && parseInt(f[1]) < 8) {
        throw 'The loaded version of jQuery is too old. Please upgrade to 1.8.x or better.'
    };
    if (!f.forEach) {
        t.warn('Parsley requires ES5 to run properly. Please include https://github.com/es-shims/es5-shim')
    };
    var i = _extends(new n(), {
        element: document,
        $element: e(document),
        actualizeOptions: null,
        _resetOptions: null,
        Factory: d,
        version: '2.7.2'
    });
    _extends(s.prototype, l.Field, n.prototype);
    _extends(o.prototype, l.Form, n.prototype);
    _extends(d.prototype, n.prototype);
    e.fn.parsley = e.fn.psly = function(i) {
        if (this.length > 1) {
            var n = [];
            this.each(function() {
                n.push(e(this).parsley(i))
            });
            return n
        };
        if (!e(this).length) {
            t.warn('You must bind Parsley on an existing element.');
            return
        };
        return new d(this[0], i)
    };
    if ('undefined' === typeof window.ParsleyExtend) window.ParsleyExtend = {};
    i.options = _extends(t.objectCreate(v), window.ParsleyConfig);
    window.ParsleyConfig = i.options;
    window.Parsley = window.psly = i;
    i.Utils = t;
    window.ParsleyUtils = {};
    e.each(t, function(e, i) {
        if ('function' === typeof i) {
            window.ParsleyUtils[e] = function() {
                t.warnOnce('Accessing `window.ParsleyUtils` is deprecated. Use `window.Parsley.Utils` instead.');
                return t[e].apply(t, arguments)
            }
        }
    });
    var x = window.Parsley._validatorRegistry = new y(window.ParsleyConfig.validators, window.ParsleyConfig.i18n);
    window.ParsleyValidator = {};
    e.each('setLocale addCatalog addMessage addMessages getErrorMessage formatMessage addValidator updateValidator removeValidator'.split(' '), function(e, i) {
        window.Parsley[i] = function() {
            return x[i].apply(x, arguments)
        };
        window.ParsleyValidator[i] = function() {
            var e;
            t.warnOnce('Accessing the method \'' + i + '\' through Validator is deprecated. Simply call \'window.Parsley.' + i + '(...)\'');
            return (e = window.Parsley)[i].apply(e, arguments)
        }
    });
    window.Parsley.UI = l;
    window.ParsleyUI = {
        removeError: function(e, i, n) {
            var r = !0 !== n;
            t.warnOnce('Accessing UI is deprecated. Call \'removeError\' on the instance directly. Please comment in issue 1073 as to your need to call this method.');
            return e.removeError(i, {
                updateClass: r
            })
        },
        getErrorsMessages: function(e) {
            t.warnOnce('Accessing UI is deprecated. Call \'getErrorsMessages\' on the instance directly.');
            return e.getErrorsMessages()
        }
    };
    e.each('addError updateError'.split(' '), function(e, i) {
        window.ParsleyUI[i] = function(e, n, r, s, a) {
            var o = !0 !== a;
            t.warnOnce('Accessing UI is deprecated. Call \'' + i + '\' on the instance directly. Please comment in issue 1073 as to your need to call this method.');
            return e[i](n, {
                message: r,
                assert: s,
                updateClass: o
            })
        }
    });
    if (!1 !== window.ParsleyConfig.autoBind) {
        e(function() {
            if (e('[data-parsley-validate]').length) e('[data-parsley-validate]').parsley()
        })
    };
    var k = e({});
    var a = function() {
        t.warnOnce('Parsley\'s pubsub module is deprecated; use the \'on\' and \'off\' methods on parsley instances or window.Parsley')
    };

    function P(e, t) {
        if (!e.parsleyAdaptedCallback) {
            e.parsleyAdaptedCallback = function() {
                var i = Array.prototype.slice.call(arguments, 0);
                i.unshift(this);
                e.apply(t || k, i)
            }
        };
        return e.parsleyAdaptedCallback
    };
    var b = 'parsley:';

    function r(e) {
        if (e.lastIndexOf(b, 0) === 0) return e.substr(b.length);
        return e
    };
    e.listen = function(e, t) {
        var i;
        a();
        if ('object' === typeof arguments[1] && 'function' === typeof arguments[2]) {
            i = arguments[1];
            t = arguments[2]
        };
        if ('function' !== typeof t) throw new Error('Wrong parameters');
        window.Parsley.on(r(e), P(t, i))
    };
    e.listenTo = function(e, t, i) {
        a();
        if (!(e instanceof s) && !(e instanceof o)) throw new Error('Must give Parsley instance');
        if ('string' !== typeof t || 'function' !== typeof i) throw new Error('Wrong parameters');
        e.on(r(t), P(i))
    };
    e.unsubscribe = function(e, t) {
        a();
        if ('string' !== typeof e || 'function' !== typeof t) throw new Error('Wrong arguments');
        window.Parsley.off(r(e), t.parsleyAdaptedCallback)
    };
    e.unsubscribeTo = function(e, t) {
        a();
        if (!(e instanceof s) && !(e instanceof o)) throw new Error('Must give Parsley instance');
        e.off(r(t))
    };
    e.unsubscribeAll = function(t) {
        a();
        window.Parsley.off(r(t));
        e('form,input,textarea,select').each(function() {
            var i = e(this).data('Parsley');
            if (i) {
                i.off(r(t))
            }
        })
    };
    e.emit = function(e, t) {
        var l;
        a();
        var i = t instanceof s || t instanceof o,
            n = Array.prototype.slice.call(arguments, i ? 2 : 1);
        n.unshift(r(e));
        if (!i) {
            t = window.Parsley
        }(l = t).trigger.apply(l, _toConsumableArray(n))
    };
    var j = {};
    e.extend(!0, i, {
        asyncValidators: {
            'default': {
                fn: function(e) {
                    return e.status >= 200 && e.status < 300
                },
                url: !1
            },
            reverse: {
                fn: function(e) {
                    return e.status < 200 || e.status >= 300
                },
                url: !1
            }
        },
        addAsyncValidator: function(e, t, n, r) {
            i.asyncValidators[e] = {
                fn: t,
                url: n || !1,
                options: r || {}
            };
            return this
        }
    });
    i.addValidator('remote', {
        requirementType: {
            '': 'string',
            'validator': 'string',
            'reverse': 'boolean',
            'options': 'object'
        },
        validateString: function(t, n, r, s) {
            var h = {};
            var o, l, a = r.validator || (!0 === r.reverse ? 'reverse' : 'default');
            if ('undefined' === typeof i.asyncValidators[a]) throw new Error('Calling an undefined async validator: `' + a + '`');
            n = i.asyncValidators[a].url || n;
            if (n.indexOf('{value}') > -1) {
                n = n.replace('{value}', encodeURIComponent(t))
            } else {
                h[s.element.getAttribute('name') || s.element.getAttribute('id')] = t
            };
            var f = e.extend(!0, r.options || {}, i.asyncValidators[a].options);
            o = e.extend(!0, {}, {
                url: n,
                data: h,
                type: 'GET'
            }, f);
            s.trigger('field:ajaxoptions', s, o);
            l = e.param(o);
            if ('undefined' === typeof i._remoteCache) i._remoteCache = {};
            var u = i._remoteCache[l] = i._remoteCache[l] || e.ajax(o),
                d = function() {
                    var t = i.asyncValidators[a].fn.call(s, u, n, r);
                    if (!t) t = e.Deferred().reject();
                    return e.when(t)
                };
            return u.then(d, d)
        },
        priority: -1
    });
    i.on('form:submit', function() {
        i._remoteCache = {}
    });
    n.prototype.addAsyncValidator = function() {
        t.warnOnce('Accessing the method `addAsyncValidator` through an instance is deprecated. Simply call `Parsley.addAsyncValidator(...)`');
        return i.addAsyncValidator.apply(i, arguments)
    };
    i.addMessages('en', {
        defaultMessage: 'This value seems to be invalid.',
        type: {
            email: 'This value should be a valid email.',
            url: 'This value should be a valid url.',
            number: 'This value should be a valid number.',
            integer: 'This value should be a valid integer.',
            digits: 'This value should be digits.',
            alphanum: 'This value should be alphanumeric.'
        },
        notblank: 'This value should not be blank.',
        required: 'This value is required.',
        pattern: 'This value seems to be invalid.',
        min: 'This value should be greater than or equal to %s.',
        max: 'This value should be lower than or equal to %s.',
        range: 'This value should be between %s and %s.',
        minlength: 'This value is too short. It should have %s characters or more.',
        maxlength: 'This value is too long. It should have %s characters or fewer.',
        length: 'This value length is invalid. It should be between %s and %s characters long.',
        mincheck: 'You must select at least %s choices.',
        maxcheck: 'You must select %s choices or fewer.',
        check: 'You must select between %s and %s choices.',
        equalto: 'This value should be the same.'
    });
    i.setLocale('en');

    function M() {
        var t = this,
            i = window || global;
        _extends(this, {
            isNativeEvent: function(e) {
                return e.originalEvent && e.originalEvent.isTrusted !== !1
            },
            fakeInputEvent: function(i) {
                if (t.isNativeEvent(i)) {
                    e(i.target).trigger('input')
                }
            },
            misbehaves: function(i) {
                if (t.isNativeEvent(i)) {
                    t.behavesOk(i);
                    e(document).on('change.inputevent', i.data.selector, t.fakeInputEvent);
                    t.fakeInputEvent(i)
                }
            },
            behavesOk: function(i) {
                if (t.isNativeEvent(i)) {
                    e(document).off('input.inputevent', i.data.selector, t.behavesOk).off('change.inputevent', i.data.selector, t.misbehaves)
                }
            },
            install: function() {
                if (i.inputEventPatched) {
                    return
                };
                i.inputEventPatched = '0.0.3';
                var s = ['select', 'input[type="checkbox"]', 'input[type="radio"]', 'input[type="file"]'];
                for (var r = 0; r < s.length; r++) {
                    var n = s[r];
                    e(document).on('input.inputevent', n, {
                        selector: n
                    }, t.behavesOk).on('change.inputevent', n, {
                        selector: n
                    }, t.misbehaves)
                }
            },
            uninstall: function() {
                delete i.inputEventPatched;
                e(document).off('.inputevent')
            }
        })
    };
    var R = new M();
    R.install();
    var O = i;
    return O
});;
(function(e) {
    'use strict';

    function i(e, t) {
        if (this.createTextRange) {
            var i = this.createTextRange();
            i.collapse(!0);
            i.moveStart('character', e);
            i.moveEnd('character', t - e);
            i.select()
        } else if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(e, t)
        }
    };

    function n(e) {
        var l = this.value.length;
        e = (e.toLowerCase() == 'start' ? 'Start' : 'End');
        if (document.selection) {
            var t = document.selection.createRange(),
                i, a, n;
            i = t.duplicate();
            i.expand('textedit');
            i.setEndPoint('EndToEnd', t);
            a = i.text.length - t.text.length;
            n = a + t.text.length;
            return e == 'Start' ? a : n
        } else if (typeof(this['selection' + e]) != 'undefined') {
            l = this['selection' + e]
        };
        return l
    };
    var a = {
        codes: {
            46: 127,
            188: 44,
            109: 45,
            190: 46,
            191: 47,
            192: 96,
            220: 92,
            222: 39,
            221: 93,
            219: 91,
            173: 45,
            187: 61,
            186: 59,
            189: 45,
            110: 46
        },
        shifts: {
            96: '~',
            49: '!',
            50: '@',
            51: '#',
            52: '$',
            53: '%',
            54: '^',
            55: '&',
            56: '*',
            57: '(',
            48: ')',
            45: '_',
            61: '+',
            91: '{',
            93: '}',
            92: '|',
            59: ':',
            39: '"',
            44: '<',
            46: '>',
            47: '?'
        }
    };
    e.fn.number = function(s, t, l, r) {
        r = (typeof r === 'undefined') ? ',' : r;
        l = (typeof l === 'undefined') ? '.' : l;
        t = (typeof t === 'undefined') ? 0 : t;
        var u = ('\\u' + ('0000' + (l.charCodeAt(0).toString(16))).slice(-4)),
            h = new RegExp('[^' + u + '0-9]', 'g'),
            o = new RegExp(u, 'g');
        if (s === !0) {
            if (this.is('input:text')) {
                return this.on({
                    'keydown.format': function(s) {
                        var g = e(this),
                            o = g.data('numFormat'),
                            c = (s.keyCode ? s.keyCode : s.which),
                            f = '',
                            u = n.apply(this, ['start']),
                            d = n.apply(this, ['end']),
                            p = '',
                            v = !1;
                        if (a.codes.hasOwnProperty(c)) {
                            c = a.codes[c]
                        };
                        if (!s.shiftKey && (c >= 65 && c <= 90)) {
                            c += 32
                        } else if (!s.shiftKey && (c >= 69 && c <= 105)) {
                            c -= 48
                        } else if (s.shiftKey && a.shifts.hasOwnProperty(c)) {
                            f = a.shifts[c]
                        };
                        if (f == '') f = String.fromCharCode(c);
                        if (c != 8 && c != 45 && c != 127 && f != l && !f.match(/[0-9]/)) {
                            var h = (s.keyCode ? s.keyCode : s.which);
                            if (h == 46 || h == 8 || h == 127 || h == 9 || h == 27 || h == 13 || ((h == 65 || h == 82 || h == 80 || h == 83 || h == 70 || h == 72 || h == 66 || h == 74 || h == 84 || h == 90 || h == 61 || h == 173 || h == 48) && (s.ctrlKey || s.metaKey) === !0) || ((h == 86 || h == 67 || h == 88) && (s.ctrlKey || s.metaKey) === !0) || ((h >= 35 && h <= 39)) || ((h >= 112 && h <= 123))) {
                                return
                            };
                            s.preventDefault();
                            return !1
                        };
                        if (u == 0 && d == this.value.length) {
                            if (c == 8) {
                                u = d = 1;
                                this.value = '';
                                o.init = (t > 0 ? -1 : 0);
                                o.c = (t > 0 ? -(t + 1) : 0);
                                i.apply(this, [0, 0])
                            } else if (f == l) {
                                u = d = 1;
                                this.value = '0' + l + (new Array(t + 1).join('0'));
                                o.init = (t > 0 ? 1 : 0);
                                o.c = (t > 0 ? -(t + 1) : 0)
                            } else if (c == 45) {
                                u = d = 2;
                                this.value = '-0' + l + (new Array(t + 1).join('0'));
                                o.init = (t > 0 ? 1 : 0);
                                o.c = (t > 0 ? -(t + 1) : 0);
                                i.apply(this, [2, 2])
                            } else {
                                o.init = (t > 0 ? -1 : 0);
                                o.c = (t > 0 ? -(t) : 0)
                            }
                        } else {
                            o.c = d - this.value.length
                        };
                        o.isPartialSelection = u == d ? !1 : !0;
                        if (t > 0 && f == l && u == this.value.length - t - 1) {
                            o.c++;
                            o.init = Math.max(0, o.init);
                            s.preventDefault();
                            v = this.value.length + o.c
                        } else if (c == 45 && (u != 0 || this.value.indexOf('-') == 0)) {
                            s.preventDefault()
                        } else if (f == l) {
                            o.init = Math.max(0, o.init);
                            s.preventDefault()
                        } else if (t > 0 && c == 127 && u == this.value.length - t - 1) {
                            s.preventDefault()
                        } else if (t > 0 && c == 8 && u == this.value.length - t) {
                            s.preventDefault();
                            o.c--;
                            v = this.value.length + o.c
                        } else if (t > 0 && c == 127 && u > this.value.length - t - 1) {
                            if (this.value === '') return;
                            if (this.value.slice(u, u + 1) != '0') {
                                p = this.value.slice(0, u) + '0' + this.value.slice(u + 1);
                                g.val(p)
                            };
                            s.preventDefault();
                            v = this.value.length + o.c
                        } else if (t > 0 && c == 8 && u > this.value.length - t) {
                            if (this.value === '') return;
                            if (this.value.slice(u - 1, u) != '0') {
                                p = this.value.slice(0, u - 1) + '0' + this.value.slice(u);
                                g.val(p)
                            };
                            s.preventDefault();
                            o.c--;
                            v = this.value.length + o.c
                        } else if (c == 127 && this.value.slice(u, u + 1) == r) {
                            s.preventDefault()
                        } else if (c == 8 && this.value.slice(u - 1, u) == r) {
                            s.preventDefault();
                            o.c--;
                            v = this.value.length + o.c
                        } else if (t > 0 && u == d && this.value.length > t + 1 && u > this.value.length - t - 1 && isFinite(+f) && !s.metaKey && !s.ctrlKey && !s.altKey && f.length === 1) {
                            if (d === this.value.length) {
                                p = this.value.slice(0, u - 1)
                            } else {
                                p = this.value.slice(0, u) + this.value.slice(u + 1)
                            };
                            this.value = p;
                            v = u
                        };
                        if (v !== !1) {
                            i.apply(this, [v, v])
                        };
                        g.data('numFormat', o)
                    },
                    'keyup.format': function(l) {
                        var r = e(this),
                            a = r.data('numFormat'),
                            s = (l.keyCode ? l.keyCode : l.which),
                            h = n.apply(this, ['start']),
                            o = n.apply(this, ['end']),
                            u;
                        if (h === 0 && o === 0 && (s === 189 || s === 109)) {
                            r.val('-' + r.val());
                            h = 1;
                            a.c = 1 - this.value.length;
                            a.init = 1;
                            r.data('numFormat', a);
                            u = this.value.length + a.c;
                            i.apply(this, [u, u])
                        };
                        if (this.value === '' || (s < 48 || s > 57) && (s < 96 || s > 105) && s !== 8 && s !== 46 && s !== 110) return;
                        r.val(r.val());
                        if (t > 0) {
                            if (a.init < 1) {
                                h = this.value.length - t - (a.init < 0 ? 1 : 0);
                                a.c = h - this.value.length;
                                a.init = 1;
                                r.data('numFormat', a)
                            } else if (h > this.value.length - t && s != 8) {
                                a.c++;
                                r.data('numFormat', a)
                            }
                        };
                        if (s == 46 && !a.isPartialSelection) {
                            a.c++;
                            r.data('numFormat', a)
                        };
                        u = this.value.length + a.c;
                        i.apply(this, [u, u])
                    },
                    'paste.format': function(t) {
                        var n = e(this),
                            i = t.originalEvent,
                            a = null;
                        if (window.clipboardData && window.clipboardData.getData) {
                            a = window.clipboardData.getData('Text')
                        } else if (i.clipboardData && i.clipboardData.getData) {
                            a = i.clipboardData.getData('text/plain')
                        };
                        n.val(a);
                        t.preventDefault();
                        return !1
                    }
                }).each(function() {
                    var i = e(this).data('numFormat', {
                        c: -(t + 1),
                        decimals: t,
                        thousands_sep: r,
                        dec_point: l,
                        regex_dec_num: h,
                        regex_dec: o,
                        init: this.value.indexOf('.') ? !0 : !1
                    });
                    if (this.value === '') return;
                    i.val(i.val())
                })
            } else {
                return this.each(function() {
                    var i = e(this),
                        a = +i.text().replace(h, '').replace(o, '.');
                    i.number(!isFinite(a) ? 0 : +a, t, l, r)
                })
            }
        };
        return this.text(e.number.apply(window, arguments))
    };
    var l = null,
        t = null;
    if (e.isPlainObject(e.valHooks.text)) {
        if (e.isFunction(e.valHooks.text.get)) l = e.valHooks.text.get;
        if (e.isFunction(e.valHooks.text.set)) t = e.valHooks.text.set
    } else {
        e.valHooks.text = {}
    };
    e.valHooks.text.get = function(t) {
        var n = e(t),
            i, s, a = n.data('numFormat');
        if (!a) {
            if (e.isFunction(l)) {
                return l(t)
            } else {
                return undefined
            }
        } else {
            if (t.value === '') return '';
            i = +(t.value.replace(a.regex_dec_num, '').replace(a.regex_dec, '.'));
            return (t.value.indexOf('-') === 0 ? '-' : '') + (isFinite(i) ? i : 0)
        }
    };
    e.valHooks.text.set = function(i, a) {
        var s = e(i),
            n = s.data('numFormat');
        if (!n) {
            if (e.isFunction(t)) {
                return t(i, a)
            } else {
                return undefined
            }
        } else {
            var l = e.number(a, n.decimals, n.dec_point, n.thousands_sep);
            return e.isFunction(t) ? t(i, l) : i.value = l
        }
    };
    e.number = function(e, t, a, n) {
        n = (typeof n === 'undefined') ? (new Number(1000).toLocaleString() !== '1000' ? new Number(1000).toLocaleString().charAt(1) : '') : n;
        a = (typeof a === 'undefined') ? new Number(0.1).toLocaleString().charAt(1) : a;
        t = !isFinite(+t) ? 0 : Math.abs(t);
        var r = ('\\u' + ('0000' + (a.charCodeAt(0).toString(16))).slice(-4)),
            u = ('\\u' + ('0000' + (n.charCodeAt(0).toString(16))).slice(-4));
        e = (e + '').replace('\.', a).replace(new RegExp(u, 'g'), '').replace(new RegExp(r, 'g'), '.').replace(new RegExp('[^0-9+\-Ee.]', 'g'), '');
        var l = !isFinite(+e) ? 0 : +e,
            i = '',
            s = function(e, t) {
                return '' + (+(Math.round(('' + e).indexOf('e') > 0 ? e : e + 'e+' + t) + 'e-' + t))
            };
        i = (t ? s(l, t) : '' + Math.round(l)).split('.');
        if (i[0].length > 3) {
            i[0] = i[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, n)
        };
        if ((i[1] || '').length < t) {
            i[1] = i[1] || '';
            i[1] += new Array(t - i[1].length + 1).join('0')
        };
        return i.join(a)
    }
})(jQuery);;;
var Mailcheck = {
    domainThreshold: 2,
    secondLevelThreshold: 2,
    topLevelThreshold: 2,
    defaultDomains: ['msn.com', 'bellsouth.net', 'telus.net', 'comcast.net', 'optusnet.com.au', 'earthlink.net', 'qq.com', 'sky.com', 'icloud.com', 'mac.com', 'sympatico.ca', 'googlemail.com', 'att.net', 'xtra.co.nz', 'web.de', 'cox.net', 'gmail.com', 'ymail.com', 'aim.com', 'rogers.com', 'verizon.net', 'rocketmail.com', 'google.com', 'optonline.net', 'sbcglobal.net', 'aol.com', 'me.com', 'btinternet.com', 'charter.net', 'shaw.ca'],
    defaultSecondLevelDomains: ['yahoo', 'hotmail', 'mail', 'live', 'outlook', 'gmx'],
    defaultTopLevelDomains: ['com', 'com.au', 'com.tw', 'ca', 'co.nz', 'co.uk', 'de', 'fr', 'it', 'ru', 'net', 'org', 'edu', 'gov', 'jp', 'nl', 'kr', 'se', 'eu', 'ie', 'co.il', 'us', 'at', 'be', 'dk', 'hk', 'es', 'gr', 'ch', 'no', 'cz', 'in', 'net', 'net.au', 'info', 'biz', 'mil', 'co.jp', 'sg', 'hu', 'uk'],
    run: function(e) {
        e.domains = e.domains || Mailcheck.defaultDomains;
        e.secondLevelDomains = e.secondLevelDomains || Mailcheck.defaultSecondLevelDomains;
        e.topLevelDomains = e.topLevelDomains || Mailcheck.defaultTopLevelDomains;
        e.distanceFunction = e.distanceFunction || Mailcheck.sift4Distance;
        var n = function(e) {
                return e
            },
            o = e.suggested || n,
            t = e.empty || n,
            i = Mailcheck.suggest(Mailcheck.encodeEmail(e.email), e.domains, e.secondLevelDomains, e.topLevelDomains, e.distanceFunction);
        return i ? o(i) : t()
    },
    suggest: function(n, a, o, t, l) {
        n = n.toLowerCase();
        var e = this.splitEmail(n);
        if (o && t) {
            if (o.indexOf(e.secondLevelDomain) !== -1 && t.indexOf(e.topLevelDomain) !== -1) {
                return !1
            }
        };
        var i = this.findClosestDomain(e.domain, a, l, this.domainThreshold);
        if (i) {
            if (i == e.domain) {
                return !1
            } else {
                return {
                    address: e.address,
                    domain: i,
                    full: e.address + '@' + i
                }
            }
        };
        var s = this.findClosestDomain(e.secondLevelDomain, o, l, this.secondLevelThreshold),
            r = this.findClosestDomain(e.topLevelDomain, t, l, this.topLevelThreshold);
        if (e.domain) {
            i = e.domain;
            var c = !1;
            if (s && s != e.secondLevelDomain) {
                i = i.replace(e.secondLevelDomain, s);
                c = !0
            };
            if (r && r != e.topLevelDomain && e.secondLevelDomain !== '') {
                i = i.replace(new RegExp(e.topLevelDomain + '$'), r);
                c = !0
            };
            if (c) {
                return {
                    address: e.address,
                    domain: i,
                    full: e.address + '@' + i
                }
            }
        };
        return !1
    },
    findClosestDomain: function(e, n, i, t) {
        t = t || this.topLevelThreshold;
        var a, l = Infinity,
            c = null;
        if (!e || !n) {
            return !1
        };
        if (!i) {
            i = this.sift4Distance
        };
        for (var o = 0; o < n.length; o++) {
            if (e === n[o]) {
                return e
            };
            a = i(e, n[o]);
            if (a < l) {
                l = a;
                c = n[o]
            }
        };
        if (l <= t && c !== null) {
            return c
        } else {
            return !1
        }
    },
    sift4Distance: function(i, o, c) {
        if (c === undefined) {
            c = 5
        };
        if (!i || !i.length) {
            if (!o) {
                return 0
            };
            return o.length
        };
        if (!o || !o.length) {
            return i.length
        };
        var s = i.length,
            r = o.length,
            n = 0,
            e = 0,
            d = 0,
            l = 0,
            h = 0,
            u = [];
        while ((n < s) && (e < r)) {
            if (i.charAt(n) == o.charAt(e)) {
                l++;
                var f = !1,
                    m = 0;
                while (m < u.length) {
                    var a = u[m];
                    if (n <= a.c1 || e <= a.c2) {
                        f = Math.abs(e - n) >= Math.abs(a.c2 - a.c1);
                        if (f) {
                            h++
                        } else {
                            if (!a.trans) {
                                a.trans = !0;
                                h++
                            }
                        };
                        break
                    } else {
                        if (n > a.c2 && e > a.c1) {
                            u.splice(m, 1)
                        } else {
                            m++
                        }
                    }
                };
                u.push({
                    c1: n,
                    c2: e,
                    trans: f
                })
            } else {
                d += l;
                l = 0;
                if (n != e) {
                    n = e = Math.min(n, e)
                };
                for (var t = 0; t < c && (n + t < s || e + t < r); t++) {
                    if ((n + t < s) && (i.charAt(n + t) == o.charAt(e))) {
                        n += t - 1;
                        e--;
                        break
                    };
                    if ((e + t < r) && (i.charAt(n) == o.charAt(e + t))) {
                        n--;
                        e += t - 1;
                        break
                    }
                }
            };
            n++;
            e++;
            if ((n >= s) || (e >= r)) {
                d += l;
                l = 0;
                n = e = Math.min(n, e)
            }
        };
        d += l;
        return Math.round(Math.max(s, r) - d + h)
    },
    splitEmail: function(e) {
        e = e !== null ? (e.replace(/^\s*/, '').replace(/\s*$/, '')) : null;
        var o = e.split('@');
        if (o.length < 2) {
            return !1
        };
        for (var a = 0; a < o.length; a++) {
            if (o[a] === '') {
                return !1
            }
        };
        var l = o.pop(),
            n = l.split('.'),
            c = '',
            i = '';
        if (n.length === 0) {
            return !1
        } else if (n.length == 1) {
            i = n[0]
        } else {
            c = n[0];
            for (var t = 1; t < n.length; t++) {
                i += n[t] + '.'
            };
            i = i.substring(0, i.length - 1)
        };
        return {
            topLevelDomain: i,
            secondLevelDomain: c,
            domain: l,
            address: o.join('@')
        }
    },
    encodeEmail: function(e) {
        var n = encodeURI(e);
        n = n.replace('%20', ' ').replace('%25', '%').replace('%5E', '^').replace('%60', '`').replace('%7B', '{').replace('%7C', '|').replace('%7D', '}');
        return n
    }
};
if (typeof module !== 'undefined' && module.exports) {
    module.exports = Mailcheck
};
if (typeof define === 'function' && define.amd) {
    define('mailcheck', [], function() {
        return Mailcheck
    })
};
if (typeof window !== 'undefined' && window.jQuery) {
    (function(e) {
        e.fn.mailcheck = function(e) {
            var n = this;
            if (e.suggested) {
                var o = e.suggested;
                e.suggested = function(e) {
                    o(n, e)
                }
            };
            if (e.empty) {
                var i = e.empty;
                e.empty = function() {
                    i.call(null, n)
                }
            };
            e.email = this.val();
            Mailcheck.run(e)
        }
    })(jQuery)
};;
(function(e, t) {
    var a;
    t.behaviors.lendme_application_form = {
        attach: function() {
            a = this;
            var i = e('#lendme-simple-form-basic-application, #lendme-simple-form-application, #lendme-simple-form-full-application, #lendme-simple-form-address-form, #lendme-simple-form-co-application'),
                r;
            a.$body = e('body');
            e('.js--number').number(!0, 0, ',', '.');
            e('#edit-grossincome, #edit-netincome, #edit-disposalincome, #edit-coappgrossincome, #edit-coappnetincome').on('change keyup', function() {
                if (e(this).val() !== '') {
                    e(this).val(a.addThousandSeparator(e(this).val()))
                }
            });
            e('#edit-grossincome').one('click', function() {
                e(this).addClass('error');
                a.hideKeyboard(e(this));
                a.showModal(t.t('Important'), t.t('Remember to input the precise amount on your paycheck.'))
            });
            e('#edit-otherpaymentcards').one('change', function() {
                e(this).addClass('error');
                a.showModal(t.t('Important'), t.t('It is important that you input all debt you have.'));
                e(this).removeClass('error')
            });
            e('#edit-housedebtradio').one('change', function() {
                if (e.inArray(e('#edit-residence').val(), ['2', '3', '4']) !== -1 && e('input[name="houseDebtRadio"]:checked').val() === '0') {
                    e(this).addClass('error');
                    a.showModal(t.t('Important'), t.t('Are you sure you do not have a mortgage loan?'));
                    e(this).removeClass('error')
                }
            });
            e('.debt-field').on('change keyup', function() {
                var t;
                if (e(this).attr('id').indexOf('remainingamount') !== -1) {
                    t = /^[1-9][\d\.]{0,}$/
                } else {
                    t = /^[0-9\.]+$/
                };
                if (e(this).val() !== '') {
                    e(this).val(a.addThousandSeparator(e(this).val()))
                };
                e(this).removeClass('success error');
                if (!t.exec(e(this).val())) {
                    e(this).addClass('error')
                } else {
                    e(this).addClass('success')
                }
            });
            e('.js--debt-type, .js--debt-check, .js--dropdown-check, .js--child-check').on('change keyup', function() {
                e(this).addClass('success');
                if (e(this).val() === '') {
                    e(this).removeClass('success')
                }
            });
            e('#edit-postalno').keydown(function(t) {
                if (e.inArray(t.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (t.keyCode === 65 && (t.ctrlKey === !0 || t.metaKey === !0)) || (t.keyCode >= 35 && t.keyCode <= 40)) {
                    return
                };
                if ((t.shiftKey || (t.keyCode < 48 || t.keyCode > 57)) && (t.keyCode < 96 || t.keyCode > 105)) {
                    t.preventDefault()
                }
            });
            e('#edit-citizenship').on('change', function() {
                e('#edit-permitprimarytype').val('').trigger('change').val('').removeClass('error').removeClass('success');
                e('#edit-residencepermit-0, #edit-residencepermit-1').prop('checked', !1).trigger('change').removeClass('error').removeClass('success')
            });
            e('#edit-maritalstatus').on('change', function() {
                e('#edit-coappiscodebtor-0, #edit-coappiscodebtor-1').prop('checked', !1).trigger('change').removeClass('error').removeClass('success')
            });
            e('#edit-coappiscodebtor-0, #edit-coappiscodebtor-1').on('change', function() {
                e('#edit-partnerinformation-0, #edit-partnerinformation-1').prop('checked', !1).trigger('change').removeClass('error').removeClass('success')
            });
            e('#edit-coappiscodebtor-0, #edit-coappiscodebtor-1').on('change', function() {
                if (e('input[name="coAppIsCoDebtor"]:checked').val() === '0' && (e('select[name="maritalStatus"]').val() === '2' || e('select[name="maritalStatus"]').val() === '3')) {
                    e('#edit-partnerinformation-0, #edit-partnerinformation-1').attr({
                        'required': 'true',
                        'data-parsley-trigger': 'change',
                        'data-parsley-trigger-after-failure': 'change'
                    })
                } else {
                    e('#edit-partnerinformation-0, #edit-partnerinformation-1').removeAttr('required')
                }
            });
            e('#edit-partnerinformation-0, #edit-partnerinformation-1').on('change', function() {
                e('#edit-coappoccupation').prop('selectedIndex', 0).trigger('change').removeClass('error').removeClass('success')
            });
            e('#edit-coappoccupation').on('change', function() {
                e('#edit-coappgrossincome').forceval('').removeClass('error').removeClass('success');
                e('#edit-coappnetincome').forceval('').removeClass('error').removeClass('success')
            });
            e('#edit-residencepermit-0').click(function() {
                e('#edit-permitprimarytype').val('').trigger('change').val('').removeClass('error').removeClass('success')
            });
            e('#edit-permitprimarytype').on('change', function() {
                var o = {};
                var i = '';
                if (e(this).val() === '1') {
                    o = {
                        '': t.t('- Select -'),
                        'A': 'A',
                        'B': 'B',
                        'C': 'C',
                        'E': 'E',
                        'F': 'F',
                        'G': 'G',
                        'H': 'H',
                        'J': 'J',
                        'K': 'K',
                        'R': 'R',
                        'Z': 'Z'
                    };
                    i = t.t('Type of temporary residence permit')
                } else if (e(this).val() === '2') {
                    o = {
                        '': t.t('- Select -'),
                        'B': 'B',
                        'D': 'D',
                        'H': 'H',
                        'L': 'L'
                    };
                    i = t.t('Type of permanent residence permit')
                };
                var a = e('#edit-permitsecondarytype');
                a.empty();
                a.prev().text(i);
                e.each(o, function(t, i) {
                    a.append(e('<option></option>').attr('value', t).text(i))
                });
                a.trigger('change').removeClass('error').removeClass('success');
                e('#edit-permitsequentialnumber, #permitforeignnumber, #permitexpir').val('').trigger('keyup').val('').removeClass('error').removeClass('success')
            });
            e('[type="email"]').on('blur', function() {
                e(this).mailcheck({
                    domains: ['gmail.com', 'hotmail.com', 'live.dk', 'outlook.dk', 'yahoo.com', 'yahoo.dk', 'hotmail.dk', 'icloud.dk', 'msn.com', 'mail.dk', 'jubii.dk', 'outlook.com', 'sol.dk', 'stofanet.dk', 'live.com', 'me.com', 'ofir.dk', 'privat.dk', 'youmail.dk', 'webspeed.dk', 'mail.com', 'ymail.com', 'pc.dk'],
                    secondLevelDomains: ['gmail', 'hotmail', 'live', 'outlook', 'yahoo', 'icloud', 'mail'],
                    topLevelDomains: ['dk', 'com'],
                    suggested: function(a, i) {
                        var o = '<p class="suggestion">' + t.t('Did you mean') + ' <b><i data-email="' + i.full + '" style="cursor: pointer;">' + i.full + '</b></i>?</p>';
                        if (a.next().hasClass('suggestion')) {
                            a.next().html(o)
                        } else {
                            a.after(o)
                        };
                        e('.suggestion').click(function() {
                            a.val(i.full);
                            a.next().remove()
                        })
                    },
                    empty: function(e) {
                        if (e.next().hasClass('suggestion')) {
                            e.next().remove()
                        }
                    }
                })
            });
            var o = e('select');
            if (o.length) {
                o.select2({
                    minimumResultsForSearch: Infinity
                })
            };
            i.once('lendme_simple_form').each(function() {
                e('[name="ssn"], [name="coAppSsn"]').mask('000000-0000');
                var s = e('.js-form-item-occupation select');
                s.on('change', function() {
                    a.translateEmploymentlength(this)
                });
                if (s.val() > 0) {
                    a.translateEmploymentlength(s)
                };
                var n = e('#edit-maritalstatus'),
                    m = e('.co-debtor-text').hide();
                n.on('change', function() {
                    a.translateMaritalStatus(n);
                    a.translatePartnerInformation(n);
                    if (n.val() !== '') {
                        m.show()
                    } else {
                        m.hide()
                    }
                });
                if (n.val() > 0) {
                    a.translateMaritalStatus(n);
                    a.translatePartnerInformation(n)
                };
                e('#edit-coappiscodebtor-1').on('change', function() {
                    e('#edit-coappfirstname, #edit-coapplastname, #edit-coappemail, #edit-coappphone, #edit-coappssn, #edit-coappoccupation, #edit-coappgrossincome, #edit-coappnetincome').val('').trigger('change').val('').removeClass('error').removeClass('success')
                });
                if (e('#edit-employmentlength, #edit-residencelength').length) {
                    e('#edit-employmentlength, #edit-residencelength').datepicker({
                        format: 'mm/yyyy',
                        startView: 'months',
                        minViewMode: 'months',
                        endDate: '+0d',
                        language: 'da-DK',
                        autoclose: !0,
                        disableTouchKeyboard: !0,
                        orientation: 'bottom'
                    }).on('changeDate', function(t) {
                        e(t.currentTarget).trigger('keyup')
                    })
                };
                if (e('#edit-permitexpire').length) {
                    var l = new Date(),
                        T = new Date(l.getFullYear(), l.getMonth(), l.getDate());
                    e('#edit-permitexpire').datepicker({
                        format: 'mm/yyyy',
                        startView: 'months',
                        minViewMode: 'months',
                        language: 'da-DK',
                        autoclose: !0,
                        disableTouchKeyboard: !0,
                        orientation: 'bottom',
                        startDate: T
                    }).on('changeDate', function(t) {
                        e(t.currentTarget).trigger('keyup')
                    })
                };
                i.parsley({
                    successClass: 'success',
                    errorClass: 'error',
                    errorsMessagesDisabled: !0,
                    validationThreshold: 1,
                    exclude: 'input[type=button], input[type=submit], input[type=reset], input[type=hidden], [disabled], :hidden'
                });
                window.Parsley.addValidator('ssn', function(e) {
                    var t = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])(0[1-9]|1[0-2])[0-9]{2}-[0-9]{4}$/;
                    if (!t.exec(e)) {
                        return !1
                    }
                }).addValidator('residencelength', function(t) {
                    var r = e('input[name=ssn]').val(),
                        n = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])(0[1-9]|1[0-2])([0-9]{2})/,
                        a = n.exec(r),
                        s = new Date();
                    if (a[3] < (s.getYear() - 100)) {
                        a[3] = '20' + a[3]
                    } else {
                        a[3] = '19' + a[3]
                    };
                    var i = new Date(a[3], a[2] - 1, a[1]),
                        o = new Date(t.substring(3, 7), parseInt(t.substring(0, 2)) - 1, 1);
                    return i < o
                }).addValidator('netincome', function(t) {
                    t = t.replace('.', '');
                    if (parseInt(e('#edit-grossincome').val().replace('.', '')) <= parseInt(t)) {
                        return !1
                    }
                }).addValidator('disposalincome', function(t) {
                    t = t.replace('.', '');
                    if (parseInt(e('#edit-netincome').val().replace('.', '')) <= parseInt(t)) {
                        return !1
                    }
                }).addValidator('coappnetincome', function(t) {
                    t = t.replace('.', '');
                    if (parseInt(e('#edit-grossincome').val().replace('.', '')) <= parseInt(t)) {
                        return !1
                    }
                });
                window.Parsley.on('form:submit', function(i) {
                    a.$body.addClass('loading');
                    var c = e('#edit-ssn'),
                        o = e('#edit-coappssn').filter('[required]:visible');
                    if (o.length) {
                        if (o.val() !== '' && e('input[name="ssn"]').val() === o.val()) {
                            a.$body.removeClass('loading');
                            a.showModal(t.t('Information'), t.t('The co ssn can not be identical to the applicant ssn.'));
                            return !1
                        } else if (!a.validateSsn(o.val())) {
                            return !1
                        }
                    };
                    var l = e('#edit-coappemail').filter('[required]:visible');
                    if (l.length) {
                        var s = e('#edit-email');
                        if (s.length && l.val().toLowerCase() === s.val().toLowerCase()) {
                            a.$body.removeClass('loading');
                            a.showModal(t.t('Information'), t.t('The e-mail address can not be identical to the mail applicant e-mail.'));
                            return !1
                        }
                    };
                    var n = c.val();
                    if (o.length) {
                        n = o.val()
                    };
                    if (n !== r) {
                        if (!a.validateSsn(n)) {
                            return !1
                        };
                        e.ajax({
                            url: '/lendme/validate/' + n,
                            dataType: 'json',
                            type: 'GET',
                            success: function() {
                                a.$body.removeClass('loading');
                                a.showModal(t.t('Ssn exists'), t.t('The SSN already exist in Lendmes system. Please log in to continue your application.'))
                            },
                            error: function() {
                                r = n;
                                var t = e('#edit-submit')[0];
                                i._submit(t)
                            }
                        });
                        return !1
                    }
                });
                window.Parsley.on('form:error', function() {
                    a.showModal(t.t('Missing fields'), t.t('Missing fields are marked with red. If there is an error, you can read more by clicking the info-icons.'))
                });
                e('#edit-maritalstatus, #edit-residence, #edit-occupation, #edit-accountno, #edit-citizenship').change(function() {
                    i.parsley().isValid()
                });
                e('#edit-maritalstatus').change(function() {
                    if (!e('.divider-coform').length) {
                        e('<div class="divider-coform"></div>').insertAfter('.js-form-item-coappnetincome')
                    }
                });
                var k = e('#clientid'),
                    u = e('#affiliate'),
                    b = e('#userAgent'),
                    S = e('#adwordsid'),
                    w = e('#campaignsource'),
                    x = e('#campaignmedium'),
                    c = e('#campaignterm'),
                    I = e('#campaignname'),
                    D = e('#campaigncontent');
                u.val(utmCookie.readCookie('affiliate'));
                S.val(utmCookie.readCookie('gclid'));
                w.val(utmCookie.readCookie('utm_source'));
                x.val(utmCookie.readCookie('utm_medium'));
                c.val(utmCookie.readCookie('utm_term'));
                I.val(utmCookie.readCookie('utm_campaign'));
                D.val(utmCookie.readCookie('utm_content'));
                var h = utmCookie.readCookie('referrer');
                if (h) {
                    e('#documentreferrer').val(h)
                };
                var v = 10;

                function f() {
                    var t = !1;
                    if (window.dataLayer !== undefined) {
                        e.each(window.dataLayer, function(e, a) {
                            if (a.event === 'gaClientIdReady') {
                                k.val(a.gaClientId);
                                t = !0
                            }
                        })
                    };
                    if (!t && v > 0) {
                        v--;
                        setTimeout(function() {
                            f()
                        }, 800)
                    }
                };
                f();
                b.val(navigator.userAgent);
                var o = a.parseQuery(window.location.search.substring(1));
                if (o.transaction_id) {
                    u.val(o.transaction_id);
                    utmCookie.createCookie('affiliate', o.transaction_id);
                    if (o.id) {
                        c.val(o.id);
                        c.val(o.id);
                        utmCookie.createCookie('utm_term', o.id)
                    }
                };
                var g = e('#edit-firstname');
                if (o.firstName && g) {
                    g.val(o.firstName)
                };
                var y = e('#edit-lastname');
                if (o.lastName && y) {
                    y.val(o.lastName)
                };
                var C = e('#edit-email');
                if (o.email && C) {
                    C.val(o.email)
                };
                var p = e('#edit-phone, #edit-postalno');
                if (o.phone && p) {
                    p.val(o.phone)
                };
                if (typeof form !== 'undefined') {
                    var d = !1;
                    e('input').on('focus', function() {
                        if (!d) {
                            d = !0;
                            if (typeof dataLayer !== 'undefined') {
                                dataLayer.push(form)
                            }
                        }
                    })
                };
                a.setFormElementRadiosTooltip()
            })
        },
        addThousandSeparator: function(e) {
            e += '';
            e = e.replaceAll('\.', '');
            var t = /(\d+)(\d{3})/;
            while (t.test(e)) {
                e = e.replace(t, '$1.$2')
            };
            return e
        },
        hideKeyboard: function(e) {
            e.attr('readonly', 'readonly');
            e.attr('disabled', 'true');
            setTimeout(function() {
                e.blur();
                e.removeAttr('readonly');
                e.removeAttr('disabled')
            }, 100)
        },
        validateSsn: function(e) {
            var s = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])(0[1-9]|1[0-2])([0-9]{2})/,
                i = s.exec(e),
                o = new Date();
            if (i[3] < (o.getYear() - 100)) {
                i[3] = '20' + i[3]
            } else {
                i[3] = '19' + i[3]
            };
            var r = new Date(i[3], i[2] - 1, i[1]),
                n = Math.floor((o.getTime() - r.getTime() + 86400) / 60 / 60 / 24 / 365.25 / 1000);
            if (n < 18) {
                a.$body.removeClass('loading');
                a.showModal(t.t('Error in SSN'), t.t('You must be at least 18 years old to use Lendmes service.'));
                return !1
            };
            return !0
        },
        showModal: function(i, r) {
            var o = e('#better-messages-default');
            if (!o.length) {
                var d = '<div><div id="better-messages-default"><div id="messages-inner"><div class="content"><h2 class="messages-label status">' + i + '</h2> <div class="messages status"><ul>';
                d += '<li class="message-item first">' + r + '</li>';
                d += '</ul></div></div><div class="footer"><span class="message-timer"></span><a class="message-close" href="#">OK</a></div></div></div></div>';
                a.$body.append(d);
                var s = e('.header-bar').height();
                if (s === 0) {
                    s = e('.gulgratis-header-content').height()
                };
                var n = e('.error:first'),
                    l = 0;
                if (n.length) {
                    l = n.offset().top - s * 2
                } else {
                    l = e('form').offset().top - s
                };
                var c = (jQuery(window).width() - o.width()) / 2;
                o = e('#better-messages-default');
                if (e('.path-tjenestetorvet').length) {
                    if (n.length) {
                        o.css({
                            'top': n.offset().top + 100 + 'px',
                            'left': c + 'px',
                            'position': 'fixed',
                            'z-index': 101
                        })
                    } else {
                        o.css({
                            'top': e('form').offset().top + 100 + 'px',
                            'left': c + 'px',
                            'position': 'fixed',
                            'z-index': 101
                        })
                    }
                } else {
                    o.css({
                        'top': l + s + 100 + 'px',
                        'left': c + 'px',
                        'position': 'fixed',
                        'z-index': 101
                    })
                };
                o.fadeIn(0);
                t.behaviors.betterMessages.attach(o);
                if (e('.path-tjenestetorvet').length) {
                    if (n.length) {
                        e('html, body').animate({
                            'scrollTop': n.offset().top
                        }, 300)
                    } else {
                        e('html, body').animate({
                            'scrollTop': e('form').offset().top
                        }, 300)
                    }
                } else {
                    e('html, body').animate({
                        'scrollTop': l
                    }, 300)
                }
            } else {
                o.remove();
                this.showModal(i, r)
            }
        },
        translateEmploymentlength: function(t) {
            var a = e(t).val(),
                i = e('.js-form-item-employmentlength'),
                r = e(t).children('option').length,
                o = e(t).find(':selected').text();
            if (a >= 1 && a < r) {
                i.find('label .js-term-replace').text(o);
                i.find('.description .js-term-replace').text(o.toLowerCase())
            }
        },
        translateMaritalStatus: function(a) {
            var r = e('select[name="coAppOccupation"]');
            if (r.val() !== '' && e('#edit-coappgrossincome').val() === '') {
                r.val('').trigger('change')
            };
            var n = Number(e(a).val()),
                o = e('#edit-coappiscodebtor--wrapper legend'),
                i = '';
            switch (n) {
                case 1:
                case 4:
                case 5:
                case 6:
                    i = t.t('Co-applicant');
                    o.html(t.t('Do you have an co-applicant')).css('padding-left', '3px').css('padding-right', '3px');
                    break;
                case 2:
                    i = t.t('Spouse');
                    o.html(t.t('Is your spouse co-applicant?')).css('padding-left', '3px').css('padding-right', '3px');
                    break;
                case 3:
                    i = t.t('Co-habitant');
                    o.html(t.t('Is your co-habitant co-applicant?')).css('padding-left', '3px').css('padding-right', '3px');
                    break
            };
            if (i !== '') {
                e('#edit-coappfirstname, #edit-coapplastname, #edit-coappemail, #edit-coappphone, #edit-coappssn, #edit-coappoccupation, #edit-coappgrossincome, #edit-coappnetincome').parent().find('.js-term-replace').html(i);
                e('.divider-coform').show()
            };
            if (i == '') {
                e('.form-item-coappfirstname, .form-item-coapplastname, .form-item-coappemail, .form-item-coappphone, .form-item-coappssn, .form-item-coappoccupation, .form-item-coappgrossincome, .form-item-coappnetincome, .divider-coform').hide()
            }
        },
        translatePartnerInformation: function(a) {
            var o = e('select[name="coAppOccupation"]');
            if (o.val() !== '' && e('#edit-coappgrossincome').val() === '') {
                o.val('').trigger('change')
            };
            var r = Number(e(a).val()),
                n = e('#edit-partnerinformation--wrapper legend span span.js-term-replace'),
                i = '';
            switch (r) {
                case 1:
                case 4:
                case 5:
                case 6:
                    i = t.t('Co-applicant');
                    break;
                case 2:
                    i = t.t('Spouse');
                    break;
                case 3:
                    i = t.t('Co-habitant');
                    break
            };
            n.text(i)
        },
        setFormElementRadiosTooltip: function() {
            e('fieldset.fieldgroup.form-item .description').each(function(t) {
                var a = e(this),
                    i = e(this).closest('.form-item'),
                    o = i.find('legend:first span');
                if (!a.hasClass('formtips-processed')) {
                    a.addClass('formtips-processed')
                };
                a.appendTo(i);
                a.hide();
                i.css('position', 'relative');
                o.wrap('<div class="formtips-wrapper clearfix"/>').append('<a class="formtip"></a>');
                i.find('.formtip').hoverIntent({
                    over: function() {
                        a.show()
                    },
                    timeout: 1000,
                    out: function() {
                        a.hide()
                    }
                })
            });
            e('.form-item .description.formtips-processed').css('max-width', 500)
        },
        parseQuery: function(e) {
            var o = e.split('&'),
                a = {};
            for (var i = 0; i < o.length; i++) {
                var t = o[i].split('=');
                if (typeof a[t[0]] === 'undefined') {
                    a[t[0]] = decodeURIComponent(t[1])
                } else if (typeof a[t[0]] === 'string') {
                    var r = [a[t[0]], decodeURIComponent(t[1])];
                    a[t[0]] = r
                } else {
                    a[t[0]].push(decodeURIComponent(t[1]))
                }
            };
            return a
        }
    };
    e.fn.forceval = function(t) {
        var a = e.valHooks.text.set;
        if (a) {
            delete e.valHooks.text.set
        };
        var i = this.each(function() {
            e(this).val(t)
        });
        if (a) {
            e.valHooks.text.set = a
        };
        return i
    }
})(jQuery, Drupal);
String.prototype.replaceAll = function(e, t) {
    var a = this;
    return a.split(e).join(t)
};;
(function(n, e) {
    e.behaviors.minuba_form = {
        attach: function() {
            var i = this.parseQuery(window.location.search.substring(1));
            if (i.minuba_id !== undefined) {
                n('input[name="minubaId"]').val(i.minuba_id);
                n('input[name="campaignContent"]').val(i.orgId);
                document.cookie = 'minubaId=' + i.minuba_id + '; path=/';
                document.cookie = 'campaignContent=' + i.orgId + '; path=/'
            } else {
                var t = document.cookie.split(';');
                for (var a = 0; a < t.length; a++) {
                    var e = t[a];
                    while (e.charAt(0) === ' ') {
                        e = e.substring(1, e.length)
                    };
                    if (e.indexOf('minubaId') === 0) {
                        n('input[name="minubaId"]').val(e.substring('minubaId='.length, e.length))
                    }
                }
            }
        },
        parseQuery: function(a) {
            var t = a.split('&'),
                e = {};
            for (var i = 0; i < t.length; i++) {
                var n = t[i].split('=');
                if (typeof e[n[0]] === 'undefined') {
                    e[n[0]] = decodeURIComponent(n[1])
                } else if (typeof e[n[0]] === 'string') {
                    var o = [e[n[0]], decodeURIComponent(n[1])];
                    e[n[0]] = o
                } else {
                    e[n[0]].push(decodeURIComponent(n[1]))
                }
            };
            return e
        }
    }
})(jQuery, Drupal);;
(function(s, e) {
    e.behaviors.lendme_basic = {
        attach: function() {
            var t = document.cookie.split('; ');
            for (var a = 0; a < t.length; a++) {
                var s = t[a].split('=');
                if (s[0] === 'Drupal.visitor.message') {
                    this.showModal(e.t('Information'), decodeURIComponent(s[1]));
                    document.cookie = s[0] + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;'
                }
            }
        },
        showModal: function(r, o) {
            var a = jQuery('#better-messages-default');
            if (!a.length) {
                var d = '<div><div id="better-messages-default"><div id="messages-inner"><div class="content"><h2 class="messages-label status">' + r + '</h2> <div class="messages status"><ul>';
                d += '<li class="message-item first">' + o + '</li>';
                d += '</ul></div></div><div class="footer"><span class="message-timer"></span><a class="message-close" href="#">OK</a></div></div></div></div>';
                jQuery('body').append(d);
                var t = jQuery('.header-bar').height();
                if (t === 0) {
                    t = jQuery('.gulgratis-header-content').height()
                };
                var l = jQuery('.error:first'),
                    i = 0;
                if (l.length) {
                    i = l.offset().top - t * 2
                } else {
                    i = s('form').offset().top - t
                };
                var n = (jQuery(window).width() - a.width()) / 2;
                a = jQuery('#better-messages-default');
                a.css({
                    'top': i + t + 'px',
                    'left': n + 'px',
                    'position': 'fixed',
                    'z-index': 101
                });
                a.fadeIn(0);
                e.behaviors.betterMessages.attach(a);
                jQuery('html, body').animate({
                    'scrollTop': i
                }, 300)
            } else {
                a.remove();
                this.showModal(r, o)
            }
        }
    }
})(jQuery, Drupal);;
(function(t) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], function(i) {
            t(i, document, window, navigator)
        })
    } else {
        t(jQuery, document, window, navigator)
    }
}(function(t, s, i, r, p) {
    'use strict';
    var c = 0,
        e = (function() {
            var s = r.userAgent,
                e = /msie\s\d+/i,
                i;
            if (s.search(e) > 0) {
                i = e.exec(s).toString();
                i = i.split(' ')[1];
                if (i < 9) {
                    t('html').addClass('lt-ie9');
                    return !0
                }
            };
            return !1
        }());
    if (!Function.prototype.bind) {
        Function.prototype.bind = function(t) {
            var i = this,
                s = [].slice;
            if (typeof i != 'function') {
                throw new TypeError()
            };
            var e = s.call(arguments, 1),
                o = function() {
                    if (this instanceof o) {
                        var n = function() {};
                        n.prototype = i.prototype;
                        var h = new n(),
                            r = i.apply(h, e.concat(s.call(arguments)));
                        if (Object(r) === r) {
                            return r
                        };
                        return h
                    } else {
                        return i.apply(t, e.concat(s.call(arguments)))
                    }
                };
            return o
        }
    };
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(t, i) {
            var e;
            if (this == null) {
                throw new TypeError('"this" is null or not defined')
            };
            var r = Object(this),
                o = r.length >>> 0;
            if (o === 0) {
                return -1
            };
            var s = +i || 0;
            if (Math.abs(s) === Infinity) {
                s = 0
            };
            if (s >= o) {
                return -1
            };
            e = Math.max(s >= 0 ? s : o - Math.abs(s), 0);
            while (e < o) {
                if (e in r && r[e] === t) {
                    return e
                };
                e++
            };
            return -1
        }
    };
    var h = '<span class="irs"><span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span><span class="irs-min">0</span><span class="irs-max">1</span><span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span></span><span class="irs-grid"></span><span class="irs-bar"></span>',
        n = '<span class="irs-bar-edge"></span><span class="irs-shadow shadow-single"></span><span class="irs-slider single"></span>',
        a = '<span class="irs-shadow shadow-from"></span><span class="irs-shadow shadow-to"></span><span class="irs-slider from"></span><span class="irs-slider to"></span>',
        l = '<span class="irs-disable-mask"></span>',
        o = function(o, h, l) {
            this.VERSION = '2.1.4';
            this.input = o;
            this.plugin_count = l;
            this.current_plugin = 0;
            this.calc_count = 0;
            this.update_tm = 0;
            this.old_from = 0;
            this.old_to = 0;
            this.old_min_interval = null;
            this.raf_id = null;
            this.dragging = !1;
            this.force_redraw = !1;
            this.no_diapason = !1;
            this.is_key = !1;
            this.is_update = !1;
            this.is_start = !0;
            this.is_finish = !1;
            this.is_active = !1;
            this.is_resize = !1;
            this.is_click = !1;
            this.$cache = {
                win: t(i),
                body: t(s.body),
                input: t(o),
                cont: null,
                rs: null,
                min: null,
                max: null,
                from: null,
                to: null,
                single: null,
                bar: null,
                line: null,
                s_single: null,
                s_from: null,
                s_to: null,
                shad_single: null,
                shad_from: null,
                shad_to: null,
                edge: null,
                grid: null,
                grid_labels: []
            };
            this.coords = {
                x_gap: 0,
                x_pointer: 0,
                w_rs: 0,
                w_rs_old: 0,
                w_handle: 0,
                p_gap: 0,
                p_gap_left: 0,
                p_gap_right: 0,
                p_step: 0,
                p_pointer: 0,
                p_handle: 0,
                p_single_fake: 0,
                p_single_real: 0,
                p_from_fake: 0,
                p_from_real: 0,
                p_to_fake: 0,
                p_to_real: 0,
                p_bar_x: 0,
                p_bar_w: 0,
                grid_gap: 0,
                big_num: 0,
                big: [],
                big_w: [],
                big_p: [],
                big_x: []
            };
            this.labels = {
                w_min: 0,
                w_max: 0,
                w_from: 0,
                w_to: 0,
                w_single: 0,
                p_min: 0,
                p_max: 0,
                p_from_fake: 0,
                p_from_left: 0,
                p_to_fake: 0,
                p_to_left: 0,
                p_single_fake: 0,
                p_single_left: 0
            };
            var e = this.$cache.input,
                r = e.prop('value'),
                a, n, c;
            a = {
                type: 'single',
                min: 10,
                max: 100,
                from: null,
                to: null,
                step: 1,
                min_interval: 0,
                max_interval: 0,
                drag_interval: !1,
                values: [],
                p_values: [],
                from_fixed: !1,
                from_min: null,
                from_max: null,
                from_shadow: !1,
                to_fixed: !1,
                to_min: null,
                to_max: null,
                to_shadow: !1,
                prettify_enabled: !0,
                prettify_separator: ' ',
                prettify: null,
                force_edges: !1,
                keyboard: !1,
                keyboard_step: 5,
                grid: !1,
                grid_margin: !0,
                grid_num: 4,
                grid_snap: !1,
                hide_min_max: !1,
                hide_from_to: !1,
                prefix: '',
                postfix: '',
                max_postfix: '',
                decorate_both: !0,
                values_separator: ' — ',
                input_values_separator: ';',
                disable: !1,
                onStart: null,
                onChange: null,
                onFinish: null,
                onUpdate: null
            };
            n = {
                type: e.data('type'),
                min: e.data('min'),
                max: e.data('max'),
                from: e.data('from'),
                to: e.data('to'),
                step: e.data('step'),
                min_interval: e.data('minInterval'),
                max_interval: e.data('maxInterval'),
                drag_interval: e.data('dragInterval'),
                values: e.data('values'),
                from_fixed: e.data('fromFixed'),
                from_min: e.data('fromMin'),
                from_max: e.data('fromMax'),
                from_shadow: e.data('fromShadow'),
                to_fixed: e.data('toFixed'),
                to_min: e.data('toMin'),
                to_max: e.data('toMax'),
                to_shadow: e.data('toShadow'),
                prettify_enabled: e.data('prettifyEnabled'),
                prettify_separator: e.data('prettifySeparator'),
                force_edges: e.data('forceEdges'),
                keyboard: e.data('keyboard'),
                keyboard_step: e.data('keyboardStep'),
                grid: e.data('grid'),
                grid_margin: e.data('gridMargin'),
                grid_num: e.data('gridNum'),
                grid_snap: e.data('gridSnap'),
                hide_min_max: e.data('hideMinMax'),
                hide_from_to: e.data('hideFromTo'),
                prefix: e.data('prefix'),
                postfix: e.data('postfix'),
                max_postfix: e.data('maxPostfix'),
                decorate_both: e.data('decorateBoth'),
                values_separator: e.data('valuesSeparator'),
                input_values_separator: e.data('inputValuesSeparator'),
                disable: e.data('disable')
            };
            n.values = n.values && n.values.split(',');
            for (c in n) {
                if (n.hasOwnProperty(c)) {
                    if (!n[c] && n[c] !== 0) {
                        delete n[c]
                    }
                }
            };
            if (r) {
                r = r.split(n.input_values_separator || h.input_values_separator || ';');
                if (r[0] && r[0] == +r[0]) {
                    r[0] = +r[0]
                };
                if (r[1] && r[1] == +r[1]) {
                    r[1] = +r[1]
                };
                if (h && h.values && h.values.length) {
                    a.from = r[0] && h.values.indexOf(r[0]);
                    a.to = r[1] && h.values.indexOf(r[1])
                } else {
                    a.from = r[0] && +r[0];
                    a.to = r[1] && +r[1]
                }
            };
            t.extend(a, h);
            t.extend(a, n);
            this.options = a;
            this.validate();
            this.result = {
                input: this.$cache.input,
                slider: null,
                min: this.options.min,
                max: this.options.max,
                from: this.options.from,
                from_percent: 0,
                from_value: null,
                to: this.options.to,
                to_percent: 0,
                to_value: null
            };
            this.init()
        };
    o.prototype = {
        init: function(t) {
            this.no_diapason = !1;
            this.coords.p_step = this.convertToPercent(this.options.step, !0);
            this.target = 'base';
            this.toggleInput();
            this.append();
            this.setMinMax();
            if (t) {
                this.force_redraw = !0;
                this.calc(!0);
                this.callOnUpdate()
            } else {
                this.force_redraw = !0;
                this.calc(!0);
                this.callOnStart()
            };
            this.updateScene()
        },
        append: function() {
            var t = '<span class="irs js-irs-' + this.plugin_count + '"></span>';
            this.$cache.input.before(t);
            this.$cache.input.prop('readonly', !0);
            this.$cache.cont = this.$cache.input.prev();
            this.result.slider = this.$cache.cont;
            this.$cache.cont.html(h);
            this.$cache.rs = this.$cache.cont.find('.irs');
            this.$cache.min = this.$cache.cont.find('.irs-min');
            this.$cache.max = this.$cache.cont.find('.irs-max');
            this.$cache.from = this.$cache.cont.find('.irs-from');
            this.$cache.to = this.$cache.cont.find('.irs-to');
            this.$cache.single = this.$cache.cont.find('.irs-single');
            this.$cache.bar = this.$cache.cont.find('.irs-bar');
            this.$cache.line = this.$cache.cont.find('.irs-line');
            this.$cache.grid = this.$cache.cont.find('.irs-grid');
            if (this.options.type === 'single') {
                this.$cache.cont.append(n);
                this.$cache.edge = this.$cache.cont.find('.irs-bar-edge');
                this.$cache.s_single = this.$cache.cont.find('.single');
                this.$cache.from[0].style.visibility = 'hidden';
                this.$cache.to[0].style.visibility = 'hidden';
                this.$cache.shad_single = this.$cache.cont.find('.shadow-single')
            } else {
                this.$cache.cont.append(a);
                this.$cache.s_from = this.$cache.cont.find('.from');
                this.$cache.s_to = this.$cache.cont.find('.to');
                this.$cache.shad_from = this.$cache.cont.find('.shadow-from');
                this.$cache.shad_to = this.$cache.cont.find('.shadow-to');
                this.setTopHandler()
            };
            if (this.options.hide_from_to) {
                this.$cache.from[0].style.display = 'none';
                this.$cache.to[0].style.display = 'none';
                this.$cache.single[0].style.display = 'none'
            };
            this.appendGrid();
            if (this.options.disable) {
                this.appendDisableMask();
                this.$cache.input[0].disabled = !0
            } else {
                this.$cache.cont.removeClass('irs-disabled');
                this.$cache.input[0].disabled = !1;
                this.bindEvents()
            };
            if (this.options.drag_interval) {
                this.$cache.bar[0].style.cursor = 'ew-resize'
            }
        },
        setTopHandler: function() {
            var s = this.options.min,
                t = this.options.max,
                e = this.options.from,
                i = this.options.to;
            if (e > s && i === t) {
                this.$cache.s_from.addClass('type_last')
            } else if (i < t) {
                this.$cache.s_to.addClass('type_last')
            }
        },
        changeLevel: function(t) {
            switch (t) {
                case 'single':
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_single_fake);
                    break;
                case 'from':
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
                    this.$cache.s_from.addClass('state_hover');
                    this.$cache.s_from.addClass('type_last');
                    this.$cache.s_to.removeClass('type_last');
                    break;
                case 'to':
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_to_fake);
                    this.$cache.s_to.addClass('state_hover');
                    this.$cache.s_to.addClass('type_last');
                    this.$cache.s_from.removeClass('type_last');
                    break;
                case 'both':
                    this.coords.p_gap_left = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
                    this.coords.p_gap_right = this.toFixed(this.coords.p_to_fake - this.coords.p_pointer);
                    this.$cache.s_to.removeClass('type_last');
                    this.$cache.s_from.removeClass('type_last');
                    break
            }
        },
        appendDisableMask: function() {
            this.$cache.cont.append(l);
            this.$cache.cont.addClass('irs-disabled')
        },
        remove: function() {
            this.$cache.cont.remove();
            this.$cache.cont = null;
            this.$cache.line.off('keydown.irs_' + this.plugin_count);
            this.$cache.body.off('touchmove.irs_' + this.plugin_count);
            this.$cache.body.off('mousemove.irs_' + this.plugin_count);
            this.$cache.win.off('touchend.irs_' + this.plugin_count);
            this.$cache.win.off('mouseup.irs_' + this.plugin_count);
            if (e) {
                this.$cache.body.off('mouseup.irs_' + this.plugin_count);
                this.$cache.body.off('mouseleave.irs_' + this.plugin_count)
            };
            this.$cache.grid_labels = [];
            this.coords.big = [];
            this.coords.big_w = [];
            this.coords.big_p = [];
            this.coords.big_x = [];
            cancelAnimationFrame(this.raf_id)
        },
        bindEvents: function() {
            if (this.no_diapason) {
                return
            };
            this.$cache.body.on('touchmove.irs_' + this.plugin_count, this.pointerMove.bind(this));
            this.$cache.body.on('mousemove.irs_' + this.plugin_count, this.pointerMove.bind(this));
            this.$cache.win.on('touchend.irs_' + this.plugin_count, this.pointerUp.bind(this));
            this.$cache.win.on('mouseup.irs_' + this.plugin_count, this.pointerUp.bind(this));
            this.$cache.line.on('touchstart.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
            this.$cache.line.on('mousedown.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
            if (this.options.drag_interval && this.options.type === 'double') {
                this.$cache.bar.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'both'));
                this.$cache.bar.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'both'))
            } else {
                this.$cache.bar.on('touchstart.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
                this.$cache.bar.on('mousedown.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'))
            };
            if (this.options.type === 'single') {
                this.$cache.single.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'single'));
                this.$cache.s_single.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'single'));
                this.$cache.shad_single.on('touchstart.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
                this.$cache.single.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'single'));
                this.$cache.s_single.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'single'));
                this.$cache.edge.on('mousedown.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
                this.$cache.shad_single.on('mousedown.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'))
            } else {
                this.$cache.single.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, null));
                this.$cache.single.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, null));
                this.$cache.from.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'from'));
                this.$cache.s_from.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'from'));
                this.$cache.to.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'to'));
                this.$cache.s_to.on('touchstart.irs_' + this.plugin_count, this.pointerDown.bind(this, 'to'));
                this.$cache.shad_from.on('touchstart.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
                this.$cache.shad_to.on('touchstart.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
                this.$cache.from.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'from'));
                this.$cache.s_from.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'from'));
                this.$cache.to.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'to'));
                this.$cache.s_to.on('mousedown.irs_' + this.plugin_count, this.pointerDown.bind(this, 'to'));
                this.$cache.shad_from.on('mousedown.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'));
                this.$cache.shad_to.on('mousedown.irs_' + this.plugin_count, this.pointerClick.bind(this, 'click'))
            };
            if (this.options.keyboard) {
                this.$cache.line.on('keydown.irs_' + this.plugin_count, this.key.bind(this, 'keyboard'))
            };
            if (e) {
                this.$cache.body.on('mouseup.irs_' + this.plugin_count, this.pointerUp.bind(this));
                this.$cache.body.on('mouseleave.irs_' + this.plugin_count, this.pointerUp.bind(this))
            }
        },
        pointerMove: function(t) {
            if (!this.dragging) {
                return
            };
            var i = t.pageX || t.originalEvent.touches && t.originalEvent.touches[0].pageX;
            this.coords.x_pointer = i - this.coords.x_gap;
            this.calc()
        },
        pointerUp: function(i) {
            if (this.current_plugin !== this.plugin_count) {
                return
            };
            if (this.is_active) {
                this.is_active = !1
            } else {
                return
            };
            this.$cache.cont.find('.state_hover').removeClass('state_hover');
            this.force_redraw = !0;
            if (e) {
                t('*').prop('unselectable', !1)
            };
            this.updateScene();
            this.restoreOriginalMinInterval();
            if (t.contains(this.$cache.cont[0], i.target) || this.dragging) {
                this.is_finish = !0;
                this.callOnFinish()
            };
            this.dragging = !1
        },
        pointerDown: function(i, s) {
            s.preventDefault();
            var o = s.pageX || s.originalEvent.touches && s.originalEvent.touches[0].pageX;
            if (s.button === 2) {
                return
            };
            if (i === 'both') {
                this.setTempMinInterval()
            };
            if (!i) {
                i = this.target
            };
            this.current_plugin = this.plugin_count;
            this.target = i;
            this.is_active = !0;
            this.dragging = !0;
            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = o - this.coords.x_gap;
            this.calcPointerPercent();
            this.changeLevel(i);
            if (e) {
                t('*').prop('unselectable', !0)
            };
            this.$cache.line.trigger('focus');
            this.updateScene()
        },
        pointerClick: function(t, i) {
            i.preventDefault();
            var s = i.pageX || i.originalEvent.touches && i.originalEvent.touches[0].pageX;
            if (i.button === 2) {
                return
            };
            this.current_plugin = this.plugin_count;
            this.target = t;
            this.is_click = !0;
            this.coords.x_gap = this.$cache.rs.offset().left;
            this.coords.x_pointer = +(s - this.coords.x_gap).toFixed();
            this.force_redraw = !0;
            this.calc();
            this.$cache.line.trigger('focus')
        },
        key: function(t, i) {
            if (this.current_plugin !== this.plugin_count || i.altKey || i.ctrlKey || i.shiftKey || i.metaKey) {
                return
            };
            switch (i.which) {
                case 83:
                case 65:
                case 40:
                case 37:
                    i.preventDefault();
                    this.moveByKey(!1);
                    break;
                case 87:
                case 68:
                case 38:
                case 39:
                    i.preventDefault();
                    this.moveByKey(!0);
                    break
            };
            return !0
        },
        moveByKey: function(t) {
            var i = this.coords.p_pointer;
            if (t) {
                i += this.options.keyboard_step
            } else {
                i -= this.options.keyboard_step
            };
            this.coords.x_pointer = this.toFixed(this.coords.w_rs / 100 * i);
            this.is_key = !0;
            this.calc()
        },
        setMinMax: function() {
            if (!this.options) {
                return
            };
            if (this.options.hide_min_max) {
                this.$cache.min[0].style.display = 'none';
                this.$cache.max[0].style.display = 'none';
                return
            };
            if (this.options.values.length) {
                this.$cache.min.html(this.decorate(this.options.p_values[this.options.min]));
                this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]))
            } else {
                this.$cache.min.html(this.decorate(this._prettify(this.options.min), this.options.min));
                this.$cache.max.html(this.decorate(this._prettify(this.options.max), this.options.max))
            };
            this.labels.w_min = this.$cache.min.outerWidth(!1);
            this.labels.w_max = this.$cache.max.outerWidth(!1)
        },
        setTempMinInterval: function() {
            var t = this.result.to - this.result.from;
            if (this.old_min_interval === null) {
                this.old_min_interval = this.options.min_interval
            };
            this.options.min_interval = t
        },
        restoreOriginalMinInterval: function() {
            if (this.old_min_interval !== null) {
                this.options.min_interval = this.old_min_interval;
                this.old_min_interval = null
            }
        },
        calc: function(t) {
            if (!this.options) {
                return
            };
            this.calc_count++;
            if (this.calc_count === 10 || t) {
                this.calc_count = 0;
                this.coords.w_rs = this.$cache.rs.outerWidth(!1);
                this.calcHandlePercent()
            };
            if (!this.coords.w_rs) {
                return
            };
            this.calcPointerPercent();
            var i = this.getHandleX();
            if (this.target === 'click') {
                this.coords.p_gap = this.coords.p_handle / 2;
                i = this.getHandleX();
                if (this.options.drag_interval) {
                    this.target = 'both_one'
                } else {
                    this.target = this.chooseHandle(i)
                }
            };
            switch (this.target) {
                case 'base':
                    var n = (this.options.max - this.options.min) / 100,
                        a = (this.result.from - this.options.min) / n,
                        p = (this.result.to - this.options.min) / n;
                    this.coords.p_single_real = this.toFixed(a);
                    this.coords.p_from_real = this.toFixed(a);
                    this.coords.p_to_real = this.toFixed(p);
                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
                    this.target = null;
                    break;
                case 'single':
                    if (this.options.from_fixed) {
                        break
                    };
                    this.coords.p_single_real = this.convertToRealPercent(i);
                    this.coords.p_single_real = this.calcWithStep(this.coords.p_single_real);
                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
                    break;
                case 'from':
                    if (this.options.from_fixed) {
                        break
                    };
                    this.coords.p_from_real = this.convertToRealPercent(i);
                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
                    if (this.coords.p_from_real > this.coords.p_to_real) {
                        this.coords.p_from_real = this.coords.p_to_real
                    };
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, 'from');
                    this.coords.p_from_real = this.checkMaxInterval(this.coords.p_from_real, this.coords.p_to_real, 'from');
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                    break;
                case 'to':
                    if (this.options.to_fixed) {
                        break
                    };
                    this.coords.p_to_real = this.convertToRealPercent(i);
                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
                    if (this.coords.p_to_real < this.coords.p_from_real) {
                        this.coords.p_to_real = this.coords.p_from_real
                    };
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, 'to');
                    this.coords.p_to_real = this.checkMaxInterval(this.coords.p_to_real, this.coords.p_from_real, 'to');
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
                    break;
                case 'both':
                    if (this.options.from_fixed || this.options.to_fixed) {
                        break
                    };
                    i = this.toFixed(i + (this.coords.p_handle * 0.1));
                    this.coords.p_from_real = this.convertToRealPercent(i) - this.coords.p_gap_left;
                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, 'from');
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                    this.coords.p_to_real = this.convertToRealPercent(i) + this.coords.p_gap_right;
                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, 'to');
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
                    break;
                case 'both_one':
                    if (this.options.from_fixed || this.options.to_fixed) {
                        break
                    };
                    var r = this.convertToRealPercent(i),
                        l = this.result.from_percent,
                        c = this.result.to_percent,
                        o = c - l,
                        h = o / 2,
                        s = r - h,
                        e = r + h;
                    if (s < 0) {
                        s = 0;
                        e = s + o
                    };
                    if (e > 100) {
                        e = 100;
                        s = e - o
                    };
                    this.coords.p_from_real = this.calcWithStep(s);
                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                    this.coords.p_to_real = this.calcWithStep(e);
                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
                    break
            };
            if (this.options.type === 'single') {
                this.coords.p_bar_x = (this.coords.p_handle / 2);
                this.coords.p_bar_w = this.coords.p_single_fake;
                this.result.from_percent = this.coords.p_single_real;
                this.result.from = this.convertToValue(this.coords.p_single_real);
                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from]
                }
            } else {
                this.coords.p_bar_x = this.toFixed(this.coords.p_from_fake + (this.coords.p_handle / 2));
                this.coords.p_bar_w = this.toFixed(this.coords.p_to_fake - this.coords.p_from_fake);
                this.result.from_percent = this.coords.p_from_real;
                this.result.from = this.convertToValue(this.coords.p_from_real);
                this.result.to_percent = this.coords.p_to_real;
                this.result.to = this.convertToValue(this.coords.p_to_real);
                if (this.options.values.length) {
                    this.result.from_value = this.options.values[this.result.from];
                    this.result.to_value = this.options.values[this.result.to]
                }
            };
            this.calcMinMax();
            this.calcLabels()
        },
        calcPointerPercent: function() {
            if (!this.coords.w_rs) {
                this.coords.p_pointer = 0;
                return
            };
            if (this.coords.x_pointer < 0 || isNaN(this.coords.x_pointer)) {
                this.coords.x_pointer = 0
            } else if (this.coords.x_pointer > this.coords.w_rs) {
                this.coords.x_pointer = this.coords.w_rs
            };
            this.coords.p_pointer = this.toFixed(this.coords.x_pointer / this.coords.w_rs * 100)
        },
        convertToRealPercent: function(t) {
            var i = 100 - this.coords.p_handle;
            return t / i * 100
        },
        convertToFakePercent: function(t) {
            var i = 100 - this.coords.p_handle;
            return t / 100 * i
        },
        getHandleX: function() {
            var i = 100 - this.coords.p_handle,
                t = this.toFixed(this.coords.p_pointer - this.coords.p_gap);
            if (t < 0) {
                t = 0
            } else if (t > i) {
                t = i
            };
            return t
        },
        calcHandlePercent: function() {
            if (this.options.type === 'single') {
                this.coords.w_handle = this.$cache.s_single.outerWidth(!1)
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(!1)
            };
            this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100)
        },
        chooseHandle: function(t) {
            if (this.options.type === 'single') {
                return 'single'
            } else {
                var i = this.coords.p_from_real + ((this.coords.p_to_real - this.coords.p_from_real) / 2);
                if (t >= i) {
                    return this.options.to_fixed ? 'from' : 'to'
                } else {
                    return this.options.from_fixed ? 'to' : 'from'
                }
            }
        },
        calcMinMax: function() {
            if (!this.coords.w_rs) {
                return
            };
            this.labels.p_min = this.labels.w_min / this.coords.w_rs * 100;
            this.labels.p_max = this.labels.w_max / this.coords.w_rs * 100
        },
        calcLabels: function() {
            if (!this.coords.w_rs || this.options.hide_from_to) {
                return
            };
            if (this.options.type === 'single') {
                this.labels.w_single = this.$cache.single.outerWidth(!1);
                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = this.coords.p_single_fake + (this.coords.p_handle / 2) - (this.labels.p_single_fake / 2);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake)
            } else {
                this.labels.w_from = this.$cache.from.outerWidth(!1);
                this.labels.p_from_fake = this.labels.w_from / this.coords.w_rs * 100;
                this.labels.p_from_left = this.coords.p_from_fake + (this.coords.p_handle / 2) - (this.labels.p_from_fake / 2);
                this.labels.p_from_left = this.toFixed(this.labels.p_from_left);
                this.labels.p_from_left = this.checkEdges(this.labels.p_from_left, this.labels.p_from_fake);
                this.labels.w_to = this.$cache.to.outerWidth(!1);
                this.labels.p_to_fake = this.labels.w_to / this.coords.w_rs * 100;
                this.labels.p_to_left = this.coords.p_to_fake + (this.coords.p_handle / 2) - (this.labels.p_to_fake / 2);
                this.labels.p_to_left = this.toFixed(this.labels.p_to_left);
                this.labels.p_to_left = this.checkEdges(this.labels.p_to_left, this.labels.p_to_fake);
                this.labels.w_single = this.$cache.single.outerWidth(!1);
                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
                this.labels.p_single_left = ((this.labels.p_from_left + this.labels.p_to_left + this.labels.p_to_fake) / 2) - (this.labels.p_single_fake / 2);
                this.labels.p_single_left = this.toFixed(this.labels.p_single_left);
                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake)
            }
        },
        updateScene: function() {
            if (this.raf_id) {
                cancelAnimationFrame(this.raf_id);
                this.raf_id = null
            };
            clearTimeout(this.update_tm);
            this.update_tm = null;
            if (!this.options) {
                return
            };
            this.drawHandles();
            if (this.is_active) {
                this.raf_id = requestAnimationFrame(this.updateScene.bind(this))
            } else {
                this.update_tm = setTimeout(this.updateScene.bind(this), 300)
            }
        },
        drawHandles: function() {
            this.coords.w_rs = this.$cache.rs.outerWidth(!1);
            if (!this.coords.w_rs) {
                return
            };
            if (this.coords.w_rs !== this.coords.w_rs_old) {
                this.target = 'base';
                this.is_resize = !0
            };
            if (this.coords.w_rs !== this.coords.w_rs_old || this.force_redraw) {
                this.setMinMax();
                this.calc(!0);
                this.drawLabels();
                if (this.options.grid) {
                    this.calcGridMargin();
                    this.calcGridLabels()
                };
                this.force_redraw = !0;
                this.coords.w_rs_old = this.coords.w_rs;
                this.drawShadow()
            };
            if (!this.coords.w_rs) {
                return
            };
            if (!this.dragging && !this.force_redraw && !this.is_key) {
                return
            };
            if (this.old_from !== this.result.from || this.old_to !== this.result.to || this.force_redraw || this.is_key) {
                this.drawLabels();
                this.$cache.bar[0].style.left = this.coords.p_bar_x + '%';
                this.$cache.bar[0].style.width = this.coords.p_bar_w + '%';
                if (this.options.type === 'single') {
                    this.$cache.s_single[0].style.left = this.coords.p_single_fake + '%';
                    this.$cache.single[0].style.left = this.labels.p_single_left + '%';
                    if (this.options.values.length) {
                        this.$cache.input.prop('value', this.result.from_value)
                    } else {
                        this.$cache.input.prop('value', this.result.from)
                    };
                    this.$cache.input.data('from', this.result.from)
                } else {
                    this.$cache.s_from[0].style.left = this.coords.p_from_fake + '%';
                    this.$cache.s_to[0].style.left = this.coords.p_to_fake + '%';
                    if (this.old_from !== this.result.from || this.force_redraw) {
                        this.$cache.from[0].style.left = this.labels.p_from_left + '%'
                    };
                    if (this.old_to !== this.result.to || this.force_redraw) {
                        this.$cache.to[0].style.left = this.labels.p_to_left + '%'
                    };
                    this.$cache.single[0].style.left = this.labels.p_single_left + '%';
                    if (this.options.values.length) {
                        this.$cache.input.prop('value', this.result.from_value + this.options.input_values_separator + this.result.to_value)
                    } else {
                        this.$cache.input.prop('value', this.result.from + this.options.input_values_separator + this.result.to)
                    };
                    this.$cache.input.data('from', this.result.from);
                    this.$cache.input.data('to', this.result.to)
                };
                if ((this.old_from !== this.result.from || this.old_to !== this.result.to) && !this.is_start) {
                    this.$cache.input.trigger('change')
                };
                this.old_from = this.result.from;
                this.old_to = this.result.to;
                if (!this.is_resize && !this.is_update && !this.is_start && !this.is_finish) {
                    this.callOnChange()
                };
                if (this.is_key || this.is_click) {
                    this.is_key = !1;
                    this.is_click = !1;
                    this.callOnFinish()
                };
                this.is_update = !1;
                this.is_resize = !1;
                this.is_finish = !1
            };
            this.is_start = !1;
            this.is_key = !1;
            this.is_click = !1;
            this.force_redraw = !1
        },
        drawLabels: function() {
            if (!this.options) {
                return
            };
            var n = this.options.values.length,
                i = this.options.p_values,
                t, s, e;
            if (this.options.hide_from_to) {
                return
            };
            if (this.options.type === 'single') {
                if (n) {
                    t = this.decorate(i[this.result.from]);
                    this.$cache.single.html(t)
                } else {
                    t = this.decorate(this._prettify(this.result.from), this.result.from);
                    this.$cache.single.html(t)
                };
                this.calcLabels();
                if (this.labels.p_single_left < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = 'hidden'
                } else {
                    this.$cache.min[0].style.visibility = 'visible'
                };
                if (this.labels.p_single_left + this.labels.p_single_fake > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = 'hidden'
                } else {
                    this.$cache.max[0].style.visibility = 'visible'
                }
            } else {
                if (n) {
                    if (this.options.decorate_both) {
                        t = this.decorate(i[this.result.from]);
                        t += this.options.values_separator;
                        t += this.decorate(i[this.result.to])
                    } else {
                        t = this.decorate(i[this.result.from] + this.options.values_separator + i[this.result.to])
                    };
                    s = this.decorate(i[this.result.from]);
                    e = this.decorate(i[this.result.to]);
                    this.$cache.single.html(t);
                    this.$cache.from.html(s);
                    this.$cache.to.html(e)
                } else {
                    if (this.options.decorate_both) {
                        t = this.decorate(this._prettify(this.result.from), this.result.from);
                        t += this.options.values_separator;
                        t += this.decorate(this._prettify(this.result.to), this.result.to)
                    } else {
                        t = this.decorate(this._prettify(this.result.from) + this.options.values_separator + this._prettify(this.result.to), this.result.to)
                    };
                    s = this.decorate(this._prettify(this.result.from), this.result.from);
                    e = this.decorate(this._prettify(this.result.to), this.result.to);
                    this.$cache.single.html(t);
                    this.$cache.from.html(s);
                    this.$cache.to.html(e)
                };
                this.calcLabels();
                var a = Math.min(this.labels.p_single_left, this.labels.p_from_left),
                    h = this.labels.p_single_left + this.labels.p_single_fake,
                    o = this.labels.p_to_left + this.labels.p_to_fake,
                    r = Math.max(h, o);
                if (this.labels.p_from_left + this.labels.p_from_fake >= this.labels.p_to_left) {
                    this.$cache.from[0].style.visibility = 'hidden';
                    this.$cache.to[0].style.visibility = 'hidden';
                    this.$cache.single[0].style.visibility = 'visible';
                    if (this.result.from === this.result.to) {
                        if (this.target === 'from') {
                            this.$cache.from[0].style.visibility = 'visible'
                        } else if (this.target === 'to') {
                            this.$cache.to[0].style.visibility = 'visible'
                        } else if (!this.target) {
                            this.$cache.from[0].style.visibility = 'visible'
                        };
                        this.$cache.single[0].style.visibility = 'hidden';
                        r = o
                    } else {
                        this.$cache.from[0].style.visibility = 'hidden';
                        this.$cache.to[0].style.visibility = 'hidden';
                        this.$cache.single[0].style.visibility = 'visible';
                        r = Math.max(h, o)
                    }
                } else {
                    this.$cache.from[0].style.visibility = 'visible';
                    this.$cache.to[0].style.visibility = 'visible';
                    this.$cache.single[0].style.visibility = 'hidden'
                };
                if (a < this.labels.p_min + 1) {
                    this.$cache.min[0].style.visibility = 'hidden'
                } else {
                    this.$cache.min[0].style.visibility = 'visible'
                };
                if (r > 100 - this.labels.p_max - 1) {
                    this.$cache.max[0].style.visibility = 'hidden'
                } else {
                    this.$cache.max[0].style.visibility = 'visible'
                }
            }
        },
        drawShadow: function() {
            var t = this.options,
                s = this.$cache,
                h = typeof t.from_min === 'number' && !isNaN(t.from_min),
                n = typeof t.from_max === 'number' && !isNaN(t.from_max),
                a = typeof t.to_min === 'number' && !isNaN(t.to_min),
                l = typeof t.to_max === 'number' && !isNaN(t.to_max),
                i, e, o, r;
            if (t.type === 'single') {
                if (t.from_shadow && (h || n)) {
                    i = this.convertToPercent(h ? t.from_min : t.min);
                    e = this.convertToPercent(n ? t.from_max : t.max) - i;
                    i = this.toFixed(i - (this.coords.p_handle / 100 * i));
                    e = this.toFixed(e - (this.coords.p_handle / 100 * e));
                    i = i + (this.coords.p_handle / 2);
                    s.shad_single[0].style.display = 'block';
                    s.shad_single[0].style.left = i + '%';
                    s.shad_single[0].style.width = e + '%'
                } else {
                    s.shad_single[0].style.display = 'none'
                }
            } else {
                if (t.from_shadow && (h || n)) {
                    i = this.convertToPercent(h ? t.from_min : t.min);
                    e = this.convertToPercent(n ? t.from_max : t.max) - i;
                    i = this.toFixed(i - (this.coords.p_handle / 100 * i));
                    e = this.toFixed(e - (this.coords.p_handle / 100 * e));
                    i = i + (this.coords.p_handle / 2);
                    s.shad_from[0].style.display = 'block';
                    s.shad_from[0].style.left = i + '%';
                    s.shad_from[0].style.width = e + '%'
                } else {
                    s.shad_from[0].style.display = 'none'
                };
                if (t.to_shadow && (a || l)) {
                    o = this.convertToPercent(a ? t.to_min : t.min);
                    r = this.convertToPercent(l ? t.to_max : t.max) - o;
                    o = this.toFixed(o - (this.coords.p_handle / 100 * o));
                    r = this.toFixed(r - (this.coords.p_handle / 100 * r));
                    o = o + (this.coords.p_handle / 2);
                    s.shad_to[0].style.display = 'block';
                    s.shad_to[0].style.left = o + '%';
                    s.shad_to[0].style.width = r + '%'
                } else {
                    s.shad_to[0].style.display = 'none'
                }
            }
        },
        callOnStart: function() {
            if (this.options.onStart && typeof this.options.onStart === 'function') {
                this.options.onStart(this.result)
            }
        },
        callOnChange: function() {
            if (this.options.onChange && typeof this.options.onChange === 'function') {
                this.options.onChange(this.result)
            }
        },
        callOnFinish: function() {
            if (this.options.onFinish && typeof this.options.onFinish === 'function') {
                this.options.onFinish(this.result)
            }
        },
        callOnUpdate: function() {
            if (this.options.onUpdate && typeof this.options.onUpdate === 'function') {
                this.options.onUpdate(this.result)
            }
        },
        toggleInput: function() {
            this.$cache.input.toggleClass('irs-hidden-input')
        },
        convertToPercent: function(t, i) {
            var e = this.options.max - this.options.min,
                r = e / 100,
                s, o;
            if (!e) {
                this.no_diapason = !0;
                return 0
            };
            if (i) {
                s = t
            } else {
                s = t - this.options.min
            };
            o = s / r;
            return this.toFixed(o)
        },
        convertToValue: function(t) {
            var e = this.options.min,
                l = this.options.max,
                c = e.toString().split('.')[1],
                p = l.toString().split('.')[1],
                o, r, h = 0,
                n = 0;
            if (t === 0) {
                return this.options.min
            };
            if (t === 100) {
                return this.options.max
            };
            if (c) {
                o = c.length;
                h = o
            };
            if (p) {
                r = p.length;
                h = r
            };
            if (o && r) {
                h = (o >= r) ? o : r
            };
            if (e < 0) {
                n = Math.abs(e);
                e = +(e + n).toFixed(h);
                l = +(l + n).toFixed(h)
            };
            var i = ((l - e) / 100 * t) + e,
                a = this.options.step.toString().split('.')[1],
                s;
            if (a) {
                i = +i.toFixed(a.length)
            } else {
                i = i / this.options.step;
                i = i * this.options.step;
                i = +i.toFixed(0)
            };
            if (n) {
                i -= n
            };
            if (a) {
                s = +i.toFixed(a.length)
            } else {
                s = this.toFixed(i)
            };
            if (s < this.options.min) {
                s = this.options.min
            } else if (s > this.options.max) {
                s = this.options.max
            };
            return s
        },
        calcWithStep: function(t) {
            var i = Math.round(t / this.coords.p_step) * this.coords.p_step;
            if (i > 100) {
                i = 100
            };
            if (t === 100) {
                i = 100
            };
            return this.toFixed(i)
        },
        checkMinInterval: function(t, i, s) {
            var o = this.options,
                e, r;
            if (!o.min_interval) {
                return t
            };
            e = this.convertToValue(t);
            r = this.convertToValue(i);
            if (s === 'from') {
                if (r - e < o.min_interval) {
                    e = r - o.min_interval
                }
            } else {
                if (e - r < o.min_interval) {
                    e = r + o.min_interval
                }
            };
            return this.convertToPercent(e)
        },
        checkMaxInterval: function(t, i, s) {
            var o = this.options,
                e, r;
            if (!o.max_interval) {
                return t
            };
            e = this.convertToValue(t);
            r = this.convertToValue(i);
            if (s === 'from') {
                if (r - e > o.max_interval) {
                    e = r - o.max_interval
                }
            } else {
                if (e - r > o.max_interval) {
                    e = r + o.max_interval
                }
            };
            return this.convertToPercent(e)
        },
        checkDiapason: function(t, i, s) {
            var e = this.convertToValue(t),
                o = this.options;
            if (typeof i !== 'number') {
                i = o.min
            };
            if (typeof s !== 'number') {
                s = o.max
            };
            if (e < i) {
                e = i
            };
            if (e > s) {
                e = s
            };
            return this.convertToPercent(e)
        },
        toFixed: function(t) {
            t = t.toFixed(9);
            return +t
        },
        _prettify: function(t) {
            if (!this.options.prettify_enabled) {
                return t
            };
            if (this.options.prettify && typeof this.options.prettify === 'function') {
                return this.options.prettify(t)
            } else {
                return this.prettify(t)
            }
        },
        prettify: function(t) {
            var i = t.toString();
            return i.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, '$1' + this.options.prettify_separator)
        },
        checkEdges: function(t, i) {
            if (!this.options.force_edges) {
                return this.toFixed(t)
            };
            if (t < 0) {
                t = 0
            } else if (t > 100 - i) {
                t = 100 - i
            };
            return this.toFixed(t)
        },
        validate: function() {
            var t = this.options,
                i = this.result,
                o = t.values,
                r = o.length,
                s, e;
            if (typeof t.min === 'string') t.min = +t.min;
            if (typeof t.max === 'string') t.max = +t.max;
            if (typeof t.from === 'string') t.from = +t.from;
            if (typeof t.to === 'string') t.to = +t.to;
            if (typeof t.step === 'string') t.step = +t.step;
            if (typeof t.from_min === 'string') t.from_min = +t.from_min;
            if (typeof t.from_max === 'string') t.from_max = +t.from_max;
            if (typeof t.to_min === 'string') t.to_min = +t.to_min;
            if (typeof t.to_max === 'string') t.to_max = +t.to_max;
            if (typeof t.keyboard_step === 'string') t.keyboard_step = +t.keyboard_step;
            if (typeof t.grid_num === 'string') t.grid_num = +t.grid_num;
            if (t.max < t.min) {
                t.max = t.min
            };
            if (r) {
                t.p_values = [];
                t.min = 0;
                t.max = r - 1;
                t.step = 1;
                t.grid_num = t.max;
                t.grid_snap = !0;
                for (e = 0; e < r; e++) {
                    s = +o[e];
                    if (!isNaN(s)) {
                        o[e] = s;
                        s = this._prettify(s)
                    } else {
                        s = o[e]
                    };
                    t.p_values.push(s)
                }
            };
            if (typeof t.from !== 'number' || isNaN(t.from)) {
                t.from = t.min
            };
            if (typeof t.to !== 'number' || isNaN(t.from)) {
                t.to = t.max
            };
            if (t.type === 'single') {
                if (t.from < t.min) {
                    t.from = t.min
                };
                if (t.from > t.max) {
                    t.from = t.max
                }
            } else {
                if (t.from < t.min || t.from > t.max) {
                    t.from = t.min
                };
                if (t.to > t.max || t.to < t.min) {
                    t.to = t.max
                };
                if (t.from > t.to) {
                    t.from = t.to
                }
            };
            if (typeof t.step !== 'number' || isNaN(t.step) || !t.step || t.step < 0) {
                t.step = 1
            };
            if (typeof t.keyboard_step !== 'number' || isNaN(t.keyboard_step) || !t.keyboard_step || t.keyboard_step < 0) {
                t.keyboard_step = 5
            };
            if (typeof t.from_min === 'number' && t.from < t.from_min) {
                t.from = t.from_min
            };
            if (typeof t.from_max === 'number' && t.from > t.from_max) {
                t.from = t.from_max
            };
            if (typeof t.to_min === 'number' && t.to < t.to_min) {
                t.to = t.to_min
            };
            if (typeof t.to_max === 'number' && t.from > t.to_max) {
                t.to = t.to_max
            };
            if (i) {
                if (i.min !== t.min) {
                    i.min = t.min
                };
                if (i.max !== t.max) {
                    i.max = t.max
                };
                if (i.from < i.min || i.from > i.max) {
                    i.from = t.from
                };
                if (i.to < i.min || i.to > i.max) {
                    i.to = t.to
                }
            };
            if (typeof t.min_interval !== 'number' || isNaN(t.min_interval) || !t.min_interval || t.min_interval < 0) {
                t.min_interval = 0
            };
            if (typeof t.max_interval !== 'number' || isNaN(t.max_interval) || !t.max_interval || t.max_interval < 0) {
                t.max_interval = 0
            };
            if (t.min_interval && t.min_interval > t.max - t.min) {
                t.min_interval = t.max - t.min
            };
            if (t.max_interval && t.max_interval > t.max - t.min) {
                t.max_interval = t.max - t.min
            }
        },
        decorate: function(t, i) {
            var e = '',
                s = this.options;
            if (s.prefix) {
                e += s.prefix
            };
            e += t;
            if (s.max_postfix) {
                if (s.values.length && t === s.p_values[s.max]) {
                    e += s.max_postfix;
                    if (s.postfix) {
                        e += ' '
                    }
                } else if (i === s.max) {
                    e += s.max_postfix;
                    if (s.postfix) {
                        e += ' '
                    }
                }
            };
            if (s.postfix) {
                e += s.postfix
            };
            return e
        },
        updateFrom: function() {
            this.result.from = this.options.from;
            this.result.from_percent = this.convertToPercent(this.result.from);
            if (this.options.values) {
                this.result.from_value = this.options.values[this.result.from]
            }
        },
        updateTo: function() {
            this.result.to = this.options.to;
            this.result.to_percent = this.convertToPercent(this.result.to);
            if (this.options.values) {
                this.result.to_value = this.options.values[this.result.to]
            }
        },
        updateResult: function() {
            this.result.min = this.options.min;
            this.result.max = this.options.max;
            this.updateFrom();
            this.updateTo()
        },
        appendGrid: function() {
            if (!this.options.grid) {
                return
            };
            var i = this.options,
                e, n, c = i.max - i.min,
                s = i.grid_num,
                a = 0,
                t = 0,
                h = 4,
                o, p, d = 0,
                r, l = '';
            this.calcGridMargin();
            if (i.grid_snap) {
                s = c / i.step;
                a = this.toFixed(i.step / (c / 100))
            } else {
                a = this.toFixed(100 / s)
            };
            if (s > 4) {
                h = 3
            };
            if (s > 7) {
                h = 2
            };
            if (s > 14) {
                h = 1
            };
            if (s > 28) {
                h = 0
            };
            for (e = 0; e < s + 1; e++) {
                o = h;
                t = this.toFixed(a * e);
                if (t > 100) {
                    t = 100;
                    o -= 2;
                    if (o < 0) {
                        o = 0
                    }
                };
                this.coords.big[e] = t;
                p = (t - (a * (e - 1))) / (o + 1);
                for (n = 1; n <= o; n++) {
                    if (t === 0) {
                        break
                    };
                    d = this.toFixed(t - (p * n));
                    l += '<span class="irs-grid-pol small" style="left: ' + d + '%"></span>'
                };
                l += '<span class="irs-grid-pol" style="left: ' + t + '%"></span>';
                r = this.convertToValue(t);
                if (i.values.length) {
                    r = i.p_values[r]
                } else {
                    r = this._prettify(r)
                };
                l += '<span class="irs-grid-text js-grid-text-' + e + '" style="left: ' + t + '%">' + r + '</span>'
            };
            this.coords.big_num = Math.ceil(s + 1);
            this.$cache.cont.addClass('irs-with-grid');
            this.$cache.grid.html(l);
            this.cacheGridLabels()
        },
        cacheGridLabels: function() {
            var i, t, s = this.coords.big_num;
            for (t = 0; t < s; t++) {
                i = this.$cache.grid.find('.js-grid-text-' + t);
                this.$cache.grid_labels.push(i)
            };
            this.calcGridLabels()
        },
        calcGridLabels: function() {
            var t, o, s = [],
                e = [],
                i = this.coords.big_num;
            for (t = 0; t < i; t++) {
                this.coords.big_w[t] = this.$cache.grid_labels[t].outerWidth(!1);
                this.coords.big_p[t] = this.toFixed(this.coords.big_w[t] / this.coords.w_rs * 100);
                this.coords.big_x[t] = this.toFixed(this.coords.big_p[t] / 2);
                s[t] = this.toFixed(this.coords.big[t] - this.coords.big_x[t]);
                e[t] = this.toFixed(s[t] + this.coords.big_p[t])
            };
            if (this.options.force_edges) {
                if (s[0] < -this.coords.grid_gap) {
                    s[0] = -this.coords.grid_gap;
                    e[0] = this.toFixed(s[0] + this.coords.big_p[0]);
                    this.coords.big_x[0] = this.coords.grid_gap
                };
                if (e[i - 1] > 100 + this.coords.grid_gap) {
                    e[i - 1] = 100 + this.coords.grid_gap;
                    s[i - 1] = this.toFixed(e[i - 1] - this.coords.big_p[i - 1]);
                    this.coords.big_x[i - 1] = this.toFixed(this.coords.big_p[i - 1] - this.coords.grid_gap)
                }
            };
            this.calcGridCollision(2, s, e);
            this.calcGridCollision(4, s, e);
            for (t = 0; t < i; t++) {
                o = this.$cache.grid_labels[t][0];
                o.style.marginLeft = -this.coords.big_x[t] + '%'
            }
        },
        calcGridCollision: function(t, i, s) {
            var e, o, r, h = this.coords.big_num;
            for (e = 0; e < h; e += t) {
                o = e + (t / 2);
                if (o >= h) {
                    break
                };
                r = this.$cache.grid_labels[o][0];
                if (s[e] <= i[o]) {
                    r.style.visibility = 'visible'
                } else {
                    r.style.visibility = 'hidden'
                }
            }
        },
        calcGridMargin: function() {
            if (!this.options.grid_margin) {
                return
            };
            this.coords.w_rs = this.$cache.rs.outerWidth(!1);
            if (!this.coords.w_rs) {
                return
            };
            if (this.options.type === 'single') {
                this.coords.w_handle = this.$cache.s_single.outerWidth(!1)
            } else {
                this.coords.w_handle = this.$cache.s_from.outerWidth(!1)
            };
            this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100);
            this.coords.grid_gap = this.toFixed((this.coords.p_handle / 2) - 0.1);
            this.$cache.grid[0].style.width = this.toFixed(100 - this.coords.p_handle) + '%';
            this.$cache.grid[0].style.left = this.coords.grid_gap + '%'
        },
        update: function(i) {
            if (!this.input) {
                return
            };
            this.is_update = !0;
            this.options.from = this.result.from;
            this.options.to = this.result.to;
            this.options = t.extend(this.options, i);
            this.validate();
            this.updateResult(i);
            this.toggleInput();
            this.remove();
            this.init(!0)
        },
        reset: function() {
            if (!this.input) {
                return
            };
            this.updateResult();
            this.update()
        },
        destroy: function() {
            if (!this.input) {
                return
            };
            this.toggleInput();
            this.$cache.input.prop('readonly', !1);
            t.data(this.input, 'ionRangeSlider', null);
            this.remove();
            this.input = null;
            this.options = null
        }
    };
    t.fn.ionRangeSlider = function(i) {
        return this.each(function() {
            if (!t.data(this, 'ionRangeSlider')) {
                t.data(this, 'ionRangeSlider', new o(this, i, c++))
            }
        })
    };
    (function() {
        var e = 0,
            s = ['ms', 'moz', 'webkit', 'o'];
        for (var t = 0; t < s.length && !i.requestAnimationFrame; ++t) {
            i.requestAnimationFrame = i[s[t] + 'RequestAnimationFrame'];
            i.cancelAnimationFrame = i[s[t] + 'CancelAnimationFrame'] || i[s[t] + 'CancelRequestAnimationFrame']
        };
        if (!i.requestAnimationFrame) i.requestAnimationFrame = function(t, s) {
            var o = new Date().getTime(),
                r = Math.max(0, 16 - (o - e)),
                h = i.setTimeout(function() {
                    t(o + r)
                }, r);
            e = o + r;
            return h
        };
        if (!i.cancelAnimationFrame) i.cancelAnimationFrame = function(t) {
            clearTimeout(t)
        }
    }())
}));;
(function(e) {
    'use strict';
    Drupal.behaviors.rangeSliders = {
        attach: function(n, o) {
            var a = this,
                d = !1,
                t = e('#loanAmount'),
                i = e('#loanTime'),
                s = e('#loanAmountValue');
            if (s.length > 0 && s.val()) {
                t.val(s.val())
            } else {
                t.val(t.data('start'))
            };
            var l = e('#loanTimeValue');
            if (l.length > 0 && l.val()) {
                i.val(l.val())
            } else {
                i.val(i.data('start'))
            };
            var r = a.parseQuery(window.location.search.substring(1));
            if (r.sliderAmount) {
                t.val(r.sliderAmount)
            };
            if (r.sliderLength) {
                i.val(r.sliderLength)
            };
            if (e('body.path-frontpage').length > 0 || e('.use-sliders').length > 0) {
                t.ionRangeSlider({
                    min: t.data('min'),
                    max: t.data('max'),
                    from: t.val(),
                    postfix: ' ksh',
                    grid: !1,
                    hide_min_max: !1,
                    grid_num: 1,
                    prettify_separator: '.',
                    grid_snap: !0,
                    step: 1,
                    onChange: function(e) {
                        a.onUpdateFunction(e, a);
                        d = !0
                    },
                    prettify: function(n) {
                        if (t.data('min') < t.data('step') && d) {
                            n = a.transform(t.data('min'), t.data('step'), n)
                        };
                        return e.number(n, 0, ',', '.')
                    }
                });
                i.ionRangeSlider({
                    min: i.data('min'),
                    max: i.data('max'),
                    from: i.val(),
                    postfix: ' years',
                    grid: !1,
                    hide_min_max: !1,
                    grid_num: 1,
                    prettify_separator: '.',
                    onChange: function(e) {
                        a.onUpdateFunction(e, a)
                    }
                })
            } else {
                t.delayKeyup(function(e) {
                    a.onUpdateFunction(e, a)
                }, 1500);
                i.delayKeyup(function(e) {
                    a.onUpdateFunction(e, a)
                }, 1000)
            };
            a.onUpdateFunction(null, a)
        },
        onUpdateFunction: function(t, r, i) {
            var l = e('#loanAmount'),
                o = e('#loanTime');
            if (l.length && o.length) {
                var a;
                if (t && t.min > 10) {
                    a = r.transform(l.data('min'), l.data('step'), t.from)
                } else {
                    a = parseInt(l.val().toString().replace('\.', ''), 10)
                };
                var u = drupalSettings.lendme.sliders.interestRate / 100,
                    v = parseInt(drupalSettings.lendme.sliders.adminCost, 10),
                    d = drupalSettings.lendme.sliders.establishCost / 100,
                    n = parseInt(o.val(), 10);
                if (n > o.data('max')) {
                    n = o.data('max')
                } else if (n === 0) {
                    n = o.data('min')
                };
                if (i) {
                    var c = i.selectionStart,
                        h = i.selectionEnd;
                    i.setSelectionRange(c, h)
                };
                var g = a + d * a + v * n * 12,
                    s = (g * u) / (1 - (Math.pow(1 + u, -n))),
                    f = s / 12,
                    p = s * n,
                    m = r.RATE(n, -s, a) * 100;
                e('span#yearlyCostInProcent').text(e.number(parseFloat(m.toFixed(2)), 2, ',', '.'));
                e('span#totalCredit').text(e.number(Math.round(a), 0, ',', '.'));
                e('span#totalRepay').text(e.number(Math.round(p), 0, ',', '.'));
                e('span#interestRate').text(e.number(parseFloat(drupalSettings.lendme.sliders.interestRate).toFixed(2), 2, ',', '.'));
                e('span#establishCost').text(e.number(Math.round(d * a), 0, '.', '.'));
                e('span#totalLoadAmount').text(e.number(a, 0, ',', '.'));
                e('div.result-of-slider span.range-slider__value').text(e.number(Math.round(f), 0, ',', '.') + 'ksh');
                e('#loanAmountValue').val(a);
                e('#loanTimeValue').val(n)
            }
        },
        RATE: function(t, a, i, n, r, p) {
            var p = (typeof p === 'undefined') ? 0.01 : p,
                n = (typeof n === 'undefined') ? 0 : n,
                r = (typeof r === 'undefined') ? 0 : r,
                l = eval(t),
                f = 1e-10,
                g = 50,
                d, u, s, h, m = 0,
                o = 0,
                v = 0,
                e = p;
            if (Math.abs(e) < f) {
                d = i * (1 + l * e) + a * (1 + e * r) * l + n
            } else {
                o = Math.exp(l * Math.log(1 + e));
                d = i * o + a * (1 / e + r) * (o - 1) + n
            };
            u = i + a * l + n;
            s = i * o + a * (1 / e + r) * (o - 1) + n;
            v = h = 0;
            m = e;
            while ((Math.abs(u - s) > f) && (v < g)) {
                e = (s * h - u * m) / (s - u);
                h = m;
                m = e;
                if (Math.abs(e) < f) {
                    d = i * (1 + l * e) + a * (1 + e * r) * l + n
                } else {
                    o = Math.exp(l * Math.log(1 + e));
                    d = i * o + a * (1 / e + r) * (o - 1) + n
                };
                u = s;
                s = d;
                ++v
            };
            return e
        },
        transform: function(t, a, e) {
            if (e < a) {
                e = t
            } else if (e > a) {
                e = Math.floor(e / a);
                if (e === t + a) {
                    e = a
                } else {
                    e = e * a
                }
            } else {
                e = a
            };
            return e
        },
        parseQuery: function(e) {
            var i = e.split('&'),
                a = {};
            for (var n = 0; n < i.length; n++) {
                var t = i[n].split('=');
                if (typeof a[t[0]] === 'undefined') {
                    a[t[0]] = decodeURIComponent(t[1])
                } else if (typeof a[t[0]] === 'string') {
                    var r = [a[t[0]], decodeURIComponent(t[1])];
                    a[t[0]] = r
                } else {
                    a[t[0]].push(decodeURIComponent(t[1]))
                }
            };
            return a
        }
    }
}(jQuery));;
