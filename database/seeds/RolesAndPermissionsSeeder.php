<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //Create Permssions
        Permission::create(['name' => 'approve loans']);
        Permission::create(['name' => 'create packages']);
        Permission::create(['name' => 'edit packages']);
        Permission::create(['name' => 'delete packages']);
        Permission::create(['name' => 'send messages']);
        Permission::create(['name' => 'create loans']);
        Permission::create(['name' => 'check agent activity']);
        Permission::create(['name' => 'video chat']);


        //Admin Permissions
        Permission::create(['name' => 'approve bank']);
        Permission::create(['name' => 'disable user accounts']);
        Permission::create(['name' => 'check user activity']);
        Permission::create(['name' => 'check bank activity']);

        //Assigning the permission to the various roles

        $role1 = Role::create(['name' => 'user']);
        $role1->givePermissionTo('create loans', 'send messages', 'video chat');

        $role2 = Role::create(['name' => 'bank']);
        $role2->givePermissionTo(['approve loans', 'create packages', 'edit packages', 'delete packages', 'send messages', 'video chat', 'check agent activity']);

        $role3 = Role::create( ['name' => 'admin']);
        $role3->givePermissionTo(['approve bank', 'disable user accounts', 'check user activity', 'check bank activity']);

        $role4 = Role::create( ['name' => 'agent']);
        $role4->givePermissionTo('approve loans');

        $role5 = Role::create( ['name' => 'advisor']);
        $role5->givePermissionTo(['send messages', 'video chat']);

    }
}
