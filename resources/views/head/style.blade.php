<style>.emaerket-widget-hide {
  display: none;
  visibility: hidden; }

.emaerket-widget-fade-in {
  opacity: 1;
  visibility: visible;
  -webkit-animation: fadeIn .2s ease-in 1 forwards;
  -moz-animation: fadeIn .2s ease-in 1 forwards;
  -o-animation: fadeIn .2s ease-in 1 forwards;
  animation: fadeIn .2s ease-in 1 forwards; }

.emaerket-widget-fade-out {
  opacity: 0;
  visibility: hidden;
  display: none;
  -webkit-animation: fadeOut .2s ease-in-out 1 forwards;
  -moz-animation: fadeOut .2s ease-in-out 1 forwards;
  -o-animation: fadeOut .2s ease-in-out 1 forwards;
  animation: fadeOut .2s ease-in-out 1 forwards; }

.emaerket-widget-slide-in {
  opacity: 0;
  visibility: hidden;
  -webkit-animation: fadeOut .2s ease-in-out 1 forwards;
  -moz-animation: fadeOut .2s ease-in-out 1 forwards;
  -o-animation: fadeOut .2s ease-in-out 1 forwards;
  animation: fadeOut .2s ease-in-out 1 forwards; }

.emaerket-side-widget-container {
  position: fixed;
  transition: all .3s;
  z-index: 9999997; }

.emaerket-side-widget-container-default {
  width: 480px; }

.emaerket-side-widget-container-collapsed {
  width: 40px; }

.emaerket-side-widget-container-no-addons {
  width: 64px; }

/* widget right version ----------------------------------------- */
.emaerket-side-widget-container-right {
  right: 0;
  -webkit-box-shadow: -2px 1px 18px 0px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: -2px 1px 18px 0px rgba(0, 0, 0, 0.2);
  box-shadow: -2px 1px 18px 0px rgba(0, 0, 0, 0.2); }
  .emaerket-side-widget-container-right.emaerket-side-widget-container-default {
    right: -350px; }
    .emaerket-side-widget-container-right.emaerket-side-widget-container-default.emaerket-side-widget-container-open {
      transform: translateX(-350px); }
  .emaerket-side-widget-container-right.emaerket-widget-collapsed.emaerket-side-widget-container-default {
    right: -472px; }
    .emaerket-side-widget-container-right.emaerket-widget-collapsed.emaerket-side-widget-container-default.emaerket-side-widget-container-open {
      transform: translateX(-472px); }

/* widget left version ----------------------------------------- */
.emaerket-side-widget-container-left {
  left: 0;
  -webkit-box-shadow: 2px 1px 18px 0px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 2px 1px 18px 0px rgba(0, 0, 0, 0.2);
  box-shadow: 2px 1px 18px 0px rgba(0, 0, 0, 0.2); }
  .emaerket-side-widget-container-left.emaerket-side-widget-container-default {
    left: -350px; }
    .emaerket-side-widget-container-left.emaerket-side-widget-container-default.emaerket-side-widget-container-open {
      transform: translateX(350px); }
  .emaerket-side-widget-container-left.emaerket-widget-collapsed.emaerket-side-widget-container-default {
    left: -472px; }
    .emaerket-side-widget-container-left.emaerket-widget-collapsed.emaerket-side-widget-container-default.emaerket-side-widget-container-open {
      transform: translateX(472px); }

.emaerket-widget-popup {
  background: #F2F2F2;
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  -ms-border-radius: 10px;
  border-radius: 10px;
  padding: 5px;
  filter: drop-shadow(rgba(0, 0, 0, 0.3) 0 1px 2px); }
  .emaerket-widget-popup.emaerket-widget-popup-top:after {
    border: 10px solid;
    border-color: #F2F2F2 transparent transparent;
    content: "";
    left: 50%;
    margin-left: -10px;
    position: absolute;
    top: 100%; }
  .emaerket-widget-popup.emaerket-widget-popup-bottom:after {
    border: 10px solid;
    border-color: transparent transparent #F2F2F2;
    content: "";
    left: 50%;
    margin-left: -10px;
    position: absolute;
    bottom: 100%; }
  .emaerket-widget-popup.emaerket-widget-popup-right:after {
    border: 10px solid;
    border-color: transparent #F2F2F2 transparent transparent;
    content: "";
    left: -20px;
    position: absolute;
    top: 50%;
    margin-top: -10px; }
  .emaerket-widget-popup.emaerket-widget-popup-left:after {
    border: 10px solid;
    border-color: transparent transparent transparent #F2F2F2;
    content: "";
    right: -20px;
    position: absolute;
    top: 50%;
    margin-top: -10px; }

@media all and (min-width: 1200px) {
  .emaerket-widget-hidden-desktop {
    visibility: hidden;
    display: none; } }

@media all and (max-width: 1200px) {
  .emaerket-widget-hidden-mobile {
    visibility: hidden;
    display: none; } }
    @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300);

@prim: #53e3a6;

*{
	box-sizing: border-box;
	margin: 0;
	padding: 0;

	font-weight: 300;
}

body{
	font-family: 'Source Sans Pro', sans-serif;
	color: white;
	font-weight: 300;

	::-webkit-input-placeholder { /* WebKit browsers */
		font-family: 'Source Sans Pro', sans-serif;
			color:    white;
		font-weight: 300;
	}
	:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
		font-family: 'Source Sans Pro', sans-serif;
		 color:    white;
		 opacity:  1;
		font-weight: 300;
	}
	::-moz-placeholder { /* Mozilla Firefox 19+ */
		font-family: 'Source Sans Pro', sans-serif;
		 color:    white;
		 opacity:  1;
		font-weight: 300;
	}
	:-ms-input-placeholder { /* Internet Explorer 10+ */
		font-family: 'Source Sans Pro', sans-serif;
		 color:    white;
		font-weight: 300;
	}
}

.wrapper{
	background: #50a3a2;
background: -webkit-linear-gradient(top left, #50a3a2 0%, #53e3a6 100%);
background: -moz-linear-gradient(top left, #50a3a2 0%, #53e3a6 100%);
background: -o-linear-gradient(top left, #50a3a2 0%, #53e3a6 100%);
background: linear-gradient(to bottom right, #50a3a2 0%, #53e3a6 100%);

	position: absolute;
	top: 50%;
	left: 0;
	width: 100%;
	height: 400px;
	margin-top: -200px;
	overflow: hidden;

	&.form-success{
		.container{
			h1{
				transform: translateY(85px);
			}
		}
	}
}

.container{
	max-width: 600px;
	margin: 0 auto;
	padding: 80px 0;
	height: 400px;
	text-align: center;

	h1{
		font-size: 40px;
		transition-duration: 1s;
		transition-timing-function: ease-in-put;
		font-weight: 200;
	}
}

form{
	padding: 20px 0;
	position: relative;
	z-index: 2;

	input{
		display: block;
		appearance: none;
		outline: 0;
		border: 1px solid fade(white, 40%);
		background-color: fade(white, 20%);
		width: 250px;

		border-radius: 3px;
		padding: 10px 15px;
		margin: 0 auto 10px auto;
		display: block;
		text-align: center;
		font-size: 18px;

		color: white;

		transition-duration: 0.25s;
		font-weight: 300;

		&:hover{
			background-color: fade(white, 40%);
		}

		&:focus{
			background-color: white;
			width: 300px;

			color: @prim;
		}
	}

	button{
		appearance: none;
		outline: 0;
		background-color: white;
		border: 0;
		padding: 10px 15px;
		color: @prim;
		border-radius: 3px;
		width: 250px;
		cursor: pointer;
		font-size: 18px;
		transition-duration: 0.25s;

		&:hover{
			background-color: rgb(245, 247, 249);
		}
	}
}

.bg-bubbles{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;

	z-index: 1;

	li{
		position: absolute;
		list-style: none;
		display: block;
		width: 40px;
		height: 40px;
		background-color: fade(white, 15%);
		bottom: -160px;

		-webkit-animation: square 25s infinite;
		animation:         square 25s infinite;

		-webkit-transition-timing-function: linear;
		transition-timing-function: linear;

		&:nth-child(1){
			left: 10%;
		}

		&:nth-child(2){
			left: 20%;

			width: 80px;
			height: 80px;

			animation-delay: 2s;
			animation-duration: 17s;
		}

		&:nth-child(3){
			left: 25%;
			animation-delay: 4s;
		}

		&:nth-child(4){
			left: 40%;
			width: 60px;
			height: 60px;

			animation-duration: 22s;

			background-color: fade(white, 25%);
		}

		&:nth-child(5){
			left: 70%;
		}

		&:nth-child(6){
			left: 80%;
			width: 120px;
			height: 120px;

			animation-delay: 3s;
			background-color: fade(white, 20%);
		}

		&:nth-child(7){
			left: 32%;
			width: 160px;
			height: 160px;

			animation-delay: 7s;
		}

		&:nth-child(8){
			left: 55%;
			width: 20px;
			height: 20px;

			animation-delay: 15s;
			animation-duration: 40s;
		}

		&:nth-child(9){
			left: 25%;
			width: 10px;
			height: 10px;

			animation-delay: 2s;
			animation-duration: 40s;
			background-color: fade(white, 30%);
		}

		&:nth-child(10){
			left: 90%;
			width: 160px;
			height: 160px;

			animation-delay: 11s;
		}
	}
}

@-webkit-keyframes square {
  0%   { transform: translateY(0); }
  100% { transform: translateY(-700px) rotate(600deg); }
}
@keyframes square {
  0%   { transform: translateY(0); }
  100% { transform: translateY(-700px) rotate(600deg); }
}
</style>
