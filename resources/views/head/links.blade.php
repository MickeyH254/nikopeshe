<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/themes/lendme/images/favicons/apple-touch-icon-57x57.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/themes/lendme/images/favicons/apple-touch-icon-114x114.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/themes/lendme/images/favicons/apple-touch-icon-72x72.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/themes/lendme/images/favicons/apple-touch-icon-144x144.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/themes/lendme/images/favicons/apple-touch-icon-60x60.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/themes/lendme/images/favicons/apple-touch-icon-120x120.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/themes/lendme/images/favicons/apple-touch-icon-76x76.png?v=75493" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/themes/lendme/images/favicons/apple-touch-icon-152x152.png?v=75493" />
    <link rel="icon" type="image/png" href="/themes/lendme/images/favicons/favicon-196x196.png?v=75493" sizes="196x196" />
    <link rel="icon" type="image/png" href="/themes/lendme/images/favicons/favicon-96x96.png?v=75493" sizes="96x96" />
    <link rel="icon" type="image/png" href="/themes/lendme/images/favicons/favicon-32x32.png?v=75493" sizes="32x32" />
    <link rel="icon" type="image/png" href="/themes/lendme/images/favicons/favicon-16x16.png?v=75493" sizes="16x16" />
    <link rel="icon" type="image/png" href="/themes/lendme/images/favicons/favicon-128.png?v=75493" sizes="128x128" />
    <link href="/themes/lendme/images/favicons/favicon.ico" rel="icon" type="image/x-icon" />
    <!-- <link rel="canonical" href="https://_/frontpage"> -->
    <link rel="shortcut icon" href="/themes/lendme/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="/sites/default/files/css/css_nK4N5nlySStWJxoElT69RYCA9B8h_dJlicG9uZkzbeY.css?pfpqe1" media="all">
<link rel="stylesheet" href="/sites/default/files/css/css_iDZJil-ZaIJYx-QMrQZFdJvZoAAGLStPpR5WqTMt4r4.css?pfpqe1" media="all">
<!-- <link type="text/css" rel="stylesheet" href="https://pushcrew.com/https-v4/css/https-v4.css"><link type="text/css" rel="stylesheet" href="https://pushcrew.com/http-v4/css/httpFront-v4.css?v=2"> -->
<!-- <link type="text/css" rel="stylesheet" charset="UTF-8" href="https://translate.googleapis.com/translate_static/css/translateelement.css"> -->
