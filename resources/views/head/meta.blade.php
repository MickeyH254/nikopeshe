<meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/themes/lendme/images/favicons/mstile-144x144.png?v=75493" />
    <meta name="msapplication-square70x70logo" content="/themes/lendme/images/favicons/mstile-70x70.png?v=75493" />
    <meta name="msapplication-square150x150logo" content="/themes/lendme/images/favicons/mstile-150x150.png?v=75493" />
    <meta name="msapplication-wide310x150logo" content="/themes/lendme/images/favicons/mstile-310x150.png?v=75493" />
    <meta name="msapplication-square310x310logo" content="/themes/lendme/images/favicons/mstile-310x310.png?v=75493" />
    <meta name="format-detection" content="telephone=no">
    <meta name="title" content="Lendme.dk | Lån smart - lad bankerne komme til dig">

<meta name="twitter:card" content="summary">
<meta property="og:site_name" content="Lendme">
<meta name="description" content="✔️ Vores brugere sparer 12.500 kr. i snit ✔️ Brug Lendme, når du skal finde det billigste lån. Det er gratis og uforpligtende. Op til 300.000 kr.">
<meta property="og:type" content="Lån">
<!-- <meta property="og:url" content="https://lendme.dk/"> -->
<meta property="og:title" content="Lendme">
<meta name="keywords" content="lendme">
<meta name="referrer" content="no-referrer">
<meta property="og:description" content="Lån smart - lad bankerne komme til dig">
<meta property="og:image:width" content="2000">
<meta property="og:image:height" content="2000">
<!-- <meta name="Generator" content="Drupal 8 (https://www.drupal.org)"> -->
<meta name="MobileOptimized" content="width">
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Nikopeshe</title>
