<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" charset="utf-8"></script>
<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
<!-- <script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vfl1aVfNF/www-widgetapi.js" async=""></script> -->
<script type="text/javascript" async="" src="https://www.youtube.com/iframe_api"></script>
<!-- <script type="text/javascript" async="" src="https://cdn.pushcrew.com/js/080e749e0cfdaac128aa5a6e86d8b4a7.js"></script> -->
<!-- <script async="" src="https://www.google-analytics.com/analytics.js"></script> -->
<!-- <script src="https://connect.facebook.net/signals/config/338833103198661?v=2.8.30&amp;r=stable" async=""></script> -->
<!-- <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script> -->
<!-- <script type="text/javascript" async="" src="https://cdn.livechatinc.com/tracking.js"></script> -->
<!-- <script src="//www.googletagmanager.com/gtm.js?id=GTM-K43BCC" type="text/javascript" async=""></script> -->
<script type="text/javascript">
      (function(w,d,s,l,i){

        w[l]=w[l]||[];
        w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});
        var f=d.getElementsByTagName(s)[0];
        var j=d.createElement(s);
        var dl=l!='dataLayer'?'&l='+l:'';
        // j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;
        j.type='text/javascript';
        j.async=true;
        f.parentNode.insertBefore(j,f);

      })(window,document,'script','dataLayer','GTM-K43BCC');
    </script><script type="application/ld+json">{
    "@context": "https://schema.org",
    "@graph": [
        {
            "@type": "FinancialService",
            "@id": "https://demo--2.dk/#organization",
            "name": "Lendme",
            "sameAs": [
                "https://www.facebook.com/Lendmedk/",
                "https://www.linkedin.com/company/lendme.dk/",
                "https://www.instagram.com/lendme_dk/",
                "https://twitter.com/lendmedk/"
            ],
            "url": "https://lendme.dk",
            "telephone": "70606262",
            "image": {
                "@type": "ImageObject",
                "url": "https://lendme.dk/themes/lendme/images/Lendme1.png"
            },
            "logo": {
                "@type": "ImageObject",
                "representativeOfPage": "True",
                "url": "https://lendme.dk/themes/lendme/images/Lendme-logo.png"
            },
            "priceRange": "Fra 4,95%",
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": "55.654113",
                "longitude": "12.517725"
            },
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Carl Jacobsens Vej 16",
                "addressRegion": "Valby",
                "postalCode": "2500",
                "addressCountry": "Danmark"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "4.9",
                "ratingCount": "160"
            }
        },
        {
            "@type": "WebSite",
            "name": "Lendme",
            "url": "https://lendme.dk"
        }
    ]
}</script>
<script>var form = {"event":"application_form_step","stepName":"application form","applicationFormStep":"application_start"};</script>

<!-- <script type="text/javascript" async="" src="https://widget.emaerket.dk/js/a30d337df31001886b3bba0017f279bd"></script> -->
