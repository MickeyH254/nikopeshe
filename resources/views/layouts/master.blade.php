<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head.meta')
    @include('head.links')
    @include('head.style')
    @include('head.script')
  </head>
  @yield('body')
</html>
