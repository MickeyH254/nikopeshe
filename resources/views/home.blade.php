<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <title>Nikopeshe</title>

    @include('extension.css')

    @yield('script')

    <style type="text\css">
      body {
        margin:0;
        padding:0;
        display:flex;
        flex-direction:column;
        justify-content:space-between;
        min-height:100vh;
      }

      main {
        flex:1 0 auto;
      }

  </style>
  </head>
  <body class="fixed-sn pink-skin">

    <!--Double navigation-->
    <header>
        @include('extension.side_nav')

        @include('extension.navbar')
    </header>
    <!--/.Double navigation-->


    <main>
        @include('notification')
        @yield('content')

    </main>

    {{-- @include('extension.footer') --}}

    @yield('js')

    @include('extension.js')

    <script>

        // SideNav Initialization
        $(".button-collapse").sideNav();

        new WOW().init();

    </script>

    <script>
      $("#my-alerts").fadeTo(2000, 3000).slideUp(3000, function(){
          $("#my-alerts").slideUp(3000);
        });
    </script>
  </body>
</html>
