@extends('layouts.admin_layout')

@section('content')
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Banks</h4>
          <div class="table-responsive">
            <table class="table">

              <!--Table head-->
              <thead>
                <tr>
                  <th>
                    <a>Bank Name
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Phone Number
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Email
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Registered At
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Action
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                </tr>
              </thead>
              <!--Table head-->

              <!--Table body-->
              <tbody>
                @foreach ($banks as $bank)
                  <tr>
                    <td>{{ $bank->name }}</td>
                    <td>{{ $bank->phone_number }}</td>
                    <td>{{ $bank->email }}</td>
                    <td>{{ $bank->created_at }}</td>
                    @if ($bank->deleted_at == null)
                      <td>
                        <form  method="POST" action="{{ route('admin.bank.disable', ['user' => $bank->id]) }}">
                          {{ csrf_field() }}
                          {{ method_field('PATCH') }}
                          <button type="submit" class="btn btn-warning" name="button">Disable Bank</button>
                        </form>
                      </td>
                    @else
                      <td>
                        <form method="POST" action="{{ route('admin.bank.approve', ['user' => $bank->id]) }}">
                          {{ csrf_field() }}
                          {{ method_field('PATCH') }}
                          <button type="submit" class="btn btn-info">Approve</button>
                        </form>
                      </td>
                    @endif

                @endforeach
              </tbody>
              <!--Table body-->
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
