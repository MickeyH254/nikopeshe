{{-- <div class="container">
    <chat-room :conversation="{{ $conversation }}" :current-user="{{ auth()->user() }}"></chat-room>
</div> --}}


@extends('layouts.app')


@section('content')
  <div class="container">
      <chat-room :conversation="{{ $conversation }}" :current-user="{{ auth()->user() }}"></chat-room>
  </div>
@endsection

@section('scripts')
  <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
@endsection
