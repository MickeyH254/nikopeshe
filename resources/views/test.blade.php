@extends('layouts.master')
@section('body')
<body class="path-frontpage anonymous">
        <!-- <noscript>
            <iframe src="//www.googletagmanager.com/ns.html?id=GTM-K43BCC" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript> -->
        <div id="page-container" class="page-container clearfix">
            <div id="page-header" class="page-header clearfix">
                <div class="header-bar">
                    <div class="cookie-icon">
                        <a></a>
                    </div>
                    <div class="section">
                        <div class="logo floatleft">
                            <div>
                                <div id="block-lendme-branding">
                                    <a href="/" title="Nikopeshe" rel="home">
                                      <p style="text-decoration: none; font-size: 20px; color: white;">
                                        <strong>Nikopeshe</strong>
                                      </p>
                                    </a>
                                </div>
                                <div id="block-lendme-branding-description">
                                    <p>Loans smart - let the banks come to you</p>
                                </div>
                            </div>
                        </div>
                        <div class="menu-icon menu-icon-cross">
                            <span></span>
                            <svg x="0" y="0" width="50px" height="50px" viewBox="0 0 540 540">
                                <rect cx="270" cy="270" r="265"></rect>
                            </svg>
                        </div>
                        <div id="page-navigation" class="page-navigation floatright">
                            <div>
                                <nav role="navigation" aria-labelledby="block-lendme-main-menu-menu" id="block-lendme-main-menu">
                                    <p class="visually-hidden" id="block-lendme-main-menu-menu">Main navigation</p>
                                    <ul>
                                        <li>
                                            <a href="#" data-drupal-link-system-path="laan-penge">Borrow money</a>
                                        </li>
                                        <li>
                                            <a href="#" data-drupal-link-system-path="kundeservice">Customer service</a>
                                        </li>
                                        <li>
                                            <a href="/laan-penge" class="applynow" data-drupal-link-system-path="laan-penge">Apply now</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('login') }}" class="nav-item-login" data-drupal-link-system-path="login">Log in</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-content">
                    <div class="heading-wrapper">
                        <h1>
                            <span class="block-region-title-thin">Nikopeshe receives</span>
                            <span class="block-region-title">more offers</span>
                            <span class="block-region-title-thin">from one application</span>
                        </h1>
                        <h2>Loans smart - let the banks come to you.</h2>
                        <div class="how-to apply-to-loan">
                            <p>Apply for free</p>
                        </div>
                        <div class="how-to get-offers">
                            <p>Receive offers</p>
                        </div>
                        <div class="how-to accept-offer">
                            <p>Choose the best loan</p>
                        </div>
                        <div class="how-to campaign-film">
                            <p>See how easy it is</p>
                        </div>
                    </div>
                </div>
            </div>
            <div></div>
            <div id="page-content-container" class="page-content-container clearfix toptop">
                <div class="offset-container">
                    <div class="section">
                        <div class="row">
                            <div class="grid-1-1 content-100">
                                <a href="#main-content" class="visually-hidden focusable skip-link">Go to main content
                            </a>
                                <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
                                    <div class="layout-container clearfix">
                                        <div id="block-sliders">
                                            <div class="slider-container clearfix use-sliders">
                                                <div class="slider-item grid-1-3">
                                                    <div class="slide-title">loan Amount</div>
                                                    <div class="range-slider">
                                                        <input type="text" id="loanAmount" name="appliedLoanSize" value="" data-start="130000" data-min="10000" data-max="300000" data-step="5000"/>
                                                    </div>
                                                    <small>You can borrow from Ksh 10,000 - 300,000</small>
                                                </div>
                                                <div class="slider-item grid-1-3">
                                                    <div class="slide-title">Loan period</div>
                                                    <div class="range-slider">
                                                        <input type="text" id="loanTime" name="amortizeLength" value="" data-start="7" data-min="1" data-max="10"/>
                                                    </div>
                                                    <small>You have to pay back at 1-10 years</small>
                                                </div>
                                                <div class="slider-item grid-1-3">
    <div class="slide-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Expected performance</font></font></div>
    <div class="result-of-slider">
      <span class="range-slider__value"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2,260 ksh</font></font></span>
    </div>
    <div class="slide-title-down"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">per. </font><font style="vertical-align: inherit;">month</font></font></div>
    <a href="#edit-firstname" class="btn btn-sliders apply-loan" id="btn-scrollto-apply"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">apply now</font></font></a>
  </div>
                                            </div>
                                            <div class="disclaimer">
                                                <p>
                                                    Price example: Total credit amount <span id="totalLoadAmount">40,000</span>
                                                    kr. Variable debt rate <span id="interestRate"></span>
                                                    %. OPP <span id="yearlyCostInProcent"></span>
                                                    %. A b. exp. <span id="establishCost"></span>
                                                    kr. Total amount to be paid back <span id="totalRepay"></span>
                                                    kr.
                                                </p>
                                            </div>
                                        </div>
                                        <div id="block-basicblock">


      <form autocomplete="off" data-drupal-selector="lendme-simple-form-basic-application" action="{{ route('loan_application.store') }}" method="post" id="lendme-simple-form-basic-application" accept-charset="UTF-8" novalidate="">
  <input id="loanAmountValue" autocomplete="off" data-drupal-selector="edit-appliedloansize" type="hidden" name="loan_value" value="130000">
<input id="loanTimeValue" autocomplete="off" data-drupal-selector="edit-amortizelength" type="hidden" name="duration" value="7">
<div class="js-form-item form-item js-form-type-textfield form-item-firstname js-form-item-firstname">
      <label for="edit-firstname" class="js-form-required form-required"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">first name</font></font></label>
        <input tabindex="1" autocomplete="off" data-parsley-pattern="^\D+$" data-parsley-trigger="keyup" data-drupal-selector="edit-firstname" type="text" id="edit-firstname" name="firstName" value="" size="12" maxlength="128" class="form-text required" required="required" aria-required="true">

        </div>
<div class="js-form-item form-item js-form-type-textfield form-item-lastname js-form-item-lastname">
      <label for="edit-lastname" class="js-form-required form-required"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Last name</font></font></label>
        <input tabindex="2" autocomplete="off" data-parsley-pattern="^\D+$" data-parsley-trigger="keyup" data-drupal-selector="edit-lastname" type="text" id="edit-lastname" name="lastName" value="" size="12" maxlength="128" class="form-text required" required="required" aria-required="true">

        </div>
<div class="js-form-item form-item js-form-type-email form-item-email js-form-item-email">
      <label for="edit-email" class="js-form-required form-required"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Email</font></font></label>
        <input tabindex="3" autocomplete="off" data-parsley-trigger="keyup" data-drupal-selector="edit-email" type="email" id="edit-email" name="email" value="" size="6" maxlength="254" class="form-email required" required="required" aria-required="true">

        </div>
<div class="js-form-item form-item js-form-type-number form-item-phone js-form-item-phone">
      <label for="edit-phone" class="js-form-required form-required"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phone number</font></font></label>
        <input tabindex="4" autocomplete="off" data-parsley-pattern="^(\d{2}\s?\d{2}\s?\d{2}\s?\d{2})$" data-parsley-trigger="keyup" data-drupal-selector="edit-phone" type="number" id="edit-phone" name="phone" value="" step="1" size="12" class="form-number required" required="required" aria-required="true">

        </div>
<div class="js-form-item form-item js-form-type-tel form-item-ssn js-form-item-ssn" style="position: relative;">
      <div class="formtips-wrapper"><label for="edit-ssn" class="js-form-required form-required"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Huduma number</font></font><a class="formtip"></a></label></div>
        <input tabindex="5" data-parsley-ssn="" data-parsley-trigger="keyup" data-drupal-selector="edit-ssn" aria-describedby="edit-ssn--description" type="tel" id="edit-ssn" name="huduma" value="" size="11" maxlength="11" class="form-tel required" required="required" aria-required="true">

            <div id="edit-ssn--description" class="description formtips-processed" style="max-width: 500px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
      Enter your CPR number with all 10 digits. </font><font style="vertical-align: inherit;">Your CPR number will always be stored securely without access to anyone other than the banks that will give you an offer.
    </font></font></div>
  </div>
<div class="js-form-item form-item js-form-type-checkbox form-item-acceptmarketing js-form-item-acceptmarketing" style="position: relative;">
        <input tabindex="6" data-drupal-selector="edit-acceptmarketing" aria-describedby="edit-acceptmarketing--description" type="checkbox" id="edit-acceptmarketing" name="acceptMarketing" value="1" class="form-checkbox labelauty initialized" aria-hidden="true" style="display: none;" data-parsley-multiple="acceptMarketing"><label for="edit-acceptmarketing" tabindex="6"><span class="labelauty-unchecked-image"></span><span class="labelauty-checked-image"></span></label>

        <div class="formtips-wrapper"><label for="edit-acceptmarketing" class="option"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nikopeshe must contact me occasionally</font></font><a class="formtip"></a></label></div>
          <div id="edit-acceptmarketing--description" class="description formtips-processed" style="max-width: 500px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
      Yes, Nikopeshe must contact me via electronic mail (email and text) with marketing of financial products and related services as well as newsletters.
    </font></font></div>
  </div>
<div class="btn-wrapper"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><input tabindex="7" data-drupal-selector="edit-submit" type="submit" id="edit-submit" name="op" value="continue" class="button js-form-submit form-submit"></font></font>
<span class="btn-subtext"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Your application will not be sent to banks before you sign up with NemID</font></font></span></div><input id="clientid" data-drupal-selector="edit-clientid" type="hidden" name="clientId" value="317090509.1538075164">
<input id="affiliate" data-drupal-selector="edit-affiliate" type="hidden" name="affiliate" value="">
<input id="userAgent" data-drupal-selector="edit-useragent" type="hidden" name="userAgent" value="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36">
<input id="adwordsid" data-drupal-selector="edit-adwordsid" type="hidden" name="adWordsId" value="">
<input id="campaignsource" data-drupal-selector="edit-campaignsource" type="hidden" name="campaignSource" value="">
<input id="campaignmedium" data-drupal-selector="edit-campaignmedium" type="hidden" name="campaignMedium" value="">
<input id="campaignterm" data-drupal-selector="edit-campaignterm" type="hidden" name="campaignTerm" value="">
<input id="campaignname" data-drupal-selector="edit-campaignname" type="hidden" name="campaignName" value="">
<input id="campaigncontent" data-drupal-selector="edit-campaigncontent" type="hidden" name="campaignContent" value="">
<input id="documentreferrer" data-drupal-selector="edit-documentreferrer" type="hidden" name="documentReferrer" value="">
<input data-drupal-selector="edit-type" type="hidden" name="type" value="1">
<input autocomplete="off" data-drupal-selector="form-tcnbcqlcq0qshpzyx9axiejh3u5kxyhlnuklk-ey5r8" type="hidden" name="form_build_id" value="form-tcnbCQlcQ0qshpzyx9AXIeJH3U5KXyhLNuklK_eY5R8">
<input data-drupal-selector="edit-lendme-simple-form-basic-application" type="hidden" name="form_id" value="lendme_simple_form_basic_application">
<input data-drupal-selector="edit-minubaid" type="hidden" name="minubaId" value="">
<input data-drupal-selector="edit-partnerid" type="hidden" name="partnerId" value="">

</form>

  </div>
                                        <div>
                                            {{-- <div class="views-element-container" id="block-views-block-frontpage-banks-block-1">
                                                <div>
                                                    <div class="frontpage-banks js-view-dom-id-d0042f4cb6ba305f04b6500a60bf76bfc75dd12df743c81403bae1ba345424ef">
                                                        <div class="frontpage-banks-items views-row">
                                                            <img src="/sites/default/files/styles/medium/public/2018-07/BasisBank.png?itok=vhRT2Q3o" width="220" height="50" alt="Basisbank" typeof="foaf:Image"/>
                                                            ,   <img src="/sites/default/files/styles/medium/public/2018-06/Express-Bank-logo.png?itok=nssMS5Vq" width="172" height="52" alt="Express Bank" typeof="foaf:Image"/>
                                                            ,   <img src="/sites/default/files/styles/medium/public/2018-07/Nordea.png?itok=H3bCaHsR" width="190" height="64" alt="Nordea" typeof="foaf:Image"/>
                                                            ,   <img src="/sites/default/files/styles/medium/public/2018-04/Pensam_1.png?itok=qtHNkTTA" width="191" height="46" alt="Pensam Bank" typeof="foaf:Image"/>
                                                            ,   <img src="/sites/default/files/styles/medium/public/2017-11/Resurs-Bank.png?itok=_Q_eCP4f" width="125" height="44" alt="Resurs Bank" typeof="foaf:Image"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div id="block-testimonials">


      <div>
<div>
<div id="block-testamonialtitle">
<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Satisfied users </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">are our business</font></font></span></h2>
</div>

<div class="testamonial-slide">
<div id="block-testamonial1">
<div class="testamonial-bold-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Jens, </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">52 years old</font></font></span></div>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loans </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">150,000 kr.</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Savings </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 55,000 *</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">It has been a very positive and professional experience. </font><font style="vertical-align: inherit;">Borrow money with this method - it is highly recommended. </font></font><br><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5.5</font></font></span></p>
</div>

<div id="block-testamonial2">
<div class="testamonial-bold-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Jette, </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">36 years old.</font></font></span></div>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loans </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">25,000 kr.</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Savings </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4,510 kr. *</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">An all-in-one good experience. </font><font style="vertical-align: inherit;">"Let the banks come to you" is a super slogan. </font><font style="vertical-align: inherit;">Overview and option to choose. </font></font><br><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5.5</font></font></span></p>
</div>

<div id="block-testamonial3">
<div class="testamonial-bold-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ole, </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">31 years old</font></font></span></div>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loans </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">90,000 kr.</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Savings </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 26,100 *</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">There were some problems, but we managed to get the money paid after customer service. </font><font style="vertical-align: inherit;">Honest, Danish company. </font></font><br><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.5</font></font></span></p>
</div>

<div id="block-testamonial4">
<div class="testamonial-bold-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lasse, </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">48 years old</font></font></span></div>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loans </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">60,000 kr.</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Saving </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5,480 kr. *</font></font></span></p>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Canon experience! </font><font style="vertical-align: inherit;">In 3 minutes, I could choose where to take the best loan. </font><font style="vertical-align: inherit;">Highly recommend Nikopeshe. </font></font><br><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5.5</font></font></span></p>
</div>
</div>
</div>

<div class="block-network">
<div><img src="/themes/lendme/images/google-r.png"><span class="network-number"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.9 </font></font></span><a href="https://www.google.dk/search?q=Lendme,+Carl+Jacobsens+Vej+16,+2500+K%C3%B8benhavn&amp;ludocid=11650857541660886471#lrd=0x4652533fc6ec459b:0xa1b029348b60c5c7" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">160 reviews</font></font></a></div>

<div><img src="/themes/lendme/images/facebook-r.png"><span class="network-number"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.8 </font></font></span><a href="https://www.facebook.com/pg/Lendmedk/reviews/" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">36 Reviews</font></font></a></div>
</div>
</div>
<script type="application/ld+json">
<!--//--><![CDATA[// ><!--

{
  // "@context": "http://schema.org/",
  "@type": "Product",
  "name": "Loan",
  "description": "Loans via Nikopeshe. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "brand": "Nikopeshe",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.9",
    "ratingCount": "160"
  }
}

//--><!]]>
</script>
  </div>

                                            <div id="block-besparelsenerforskellen">
                                                <p>* The savings are the difference between the total repayment between the most expensive and cheapest loans offered by the banks.</p>
                                            </div>
                                            <div class="views-element-container" id="block-views-block-frontpage-flow-block-1">


      <div><div class="js-view-dom-id-b2519bee30ff9e5de19a654d56044b4fe00720c3892275f81ee0a4b3eae031c4">




      <header>
      <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fill out one application for a loan </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- get more offers from the banks</font></font></span></h2>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">As the first in Denmark, Nikopeshe gives you quick answers with real loan offers - directly from the banks.</font></font></p>
    </header>




      <div class="frontpage-flow-items wow fadeInUp views-row animated" style="visibility: visible;"><div class="views-field views-field-field-image"><div class="field-content">  <img src="/sites/default/files/2017-11/Ans%C3%B8g_nu.png" width="78" height="100" alt="Apply for free" typeof="foaf:Image">

</div></div><h3 class="views-field views-field-title"><span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Apply for free</font></font></span></h3><div class="views-field views-field-body"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fill in one application to </font></font><a href="/laan-penge"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">borrow money</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> and we will collect an individual offer from the banks</font></font></p></div></div></div>
    <div class="frontpage-flow-items wow fadeInUp views-row animated" style="visibility: visible;"><div class="views-field views-field-field-image"><div class="field-content">  <img src="/sites/default/files/2017-11/Modtag_tilbud.png" width="124" height="100" alt="Offers on loans" typeof="foaf:Image">

</div></div><h3 class="views-field views-field-title"><span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Receive offers</font></font></span></h3><div class="views-field views-field-body"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You receive loan offers from banks, and you can now choose the one that suits you best&nbsp;</font></font></p></div></div></div>
    <div class="frontpage-flow-items wow fadeInUp views-row animated" style="visibility: visible;"><div class="views-field views-field-field-image"><div class="field-content">  <img src="/sites/default/files/2017-12/Vaelg_det_bedste_laan1.png" width="113" height="100" alt="Choose the best loan" typeof="foaf:Image">

</div></div><h3 class="views-field views-field-title"><span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Choose the best loan</font></font></span></h3><div class="views-field views-field-body"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You are not required to send any signed loan documents to the bank&nbsp;</font></font></p></div></div></div>









</div>
</div>

  </div>
  <div id="block-styrpasagerne">



<div><h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOUR </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SAFETY</font></font></span></h2>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nikopeshe aims to help Danish borrowers find the best loans. </font><font style="vertical-align: inherit;">It is safe, easy and free.</font></font></p>

<p><a href="/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">apply now</font></font></a></p>
</div>

</div>
<div class="views-element-container" id="block-views-block-frontpage-flow-block-1">


<div><div class="js-view-dom-id-b2519bee30ff9e5de19a654d56044b4fe00720c3892275f81ee0a4b3eae031c4">




<header>
<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fill out one application for a loan </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- get more offers from the banks</font></font></span></h2>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">As the first in Denmark, Nikopeshe gives you quick answers with real loan offers - directly from the banks.</font></font></p>
</header>




<div class="frontpage-flow-items wow fadeInUp views-row animated" style="visibility: visible;"><div class="views-field views-field-field-image"><div class="field-content">  <img src="/sites/default/files/2017-11/Ans%C3%B8g_nu.png" width="78" height="100" alt="Apply for free" typeof="foaf:Image">

</div></div><h3 class="views-field views-field-title"><span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Apply for free</font></font></span></h3><div class="views-field views-field-body"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fill in one application to </font></font><a href="/laan-penge"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">borrow money</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> and we will collect an individual offer from the banks</font></font></p></div></div></div>
<div class="frontpage-flow-items wow fadeInUp views-row animated" style="visibility: visible;"><div class="views-field views-field-field-image"><div class="field-content">  <img src="/sites/default/files/2017-11/Modtag_tilbud.png" width="124" height="100" alt="Offers on loans" typeof="foaf:Image">

</div></div><h3 class="views-field views-field-title"><span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Receive offers</font></font></span></h3><div class="views-field views-field-body"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You receive loan offers from banks, and you can now choose the one that suits you best&nbsp;</font></font></p></div></div></div>
<div class="frontpage-flow-items wow fadeInUp views-row animated" style="visibility: visible;"><div class="views-field views-field-field-image"><div class="field-content">  <img src="/sites/default/files/2017-12/Vaelg_det_bedste_laan1.png" width="113" height="100" alt="Choose the best loan" typeof="foaf:Image">

</div></div><h3 class="views-field views-field-title"><span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Choose the best loan</font></font></span></h3><div class="views-field views-field-body"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You are not required to send any signed loan documents to the bank&nbsp;</font></font></p></div></div></div>









</div>
</div>

</div>
<div id="block-frontblogger">


<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">We are </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">blogging</font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> about loans and finance</font></font></h2>

<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Read our latest three blog posts about loans and private finance.</font></font></p>

</div>
                                            <div class="views-element-container contextual-region" id="block-views-block-blog-blog-post-front">

    <div>
        <div class="path-blog contextual-region js-view-dom-id-bcf5c6756e54cc4e1eb66ca3ab28d582a9007f049a34742c40ba388d4947fd84">
            <div data-contextual-id="entity.view.edit_form:view=blog:location=block&amp;name=blog&amp;display_id=blog_post_front&amp;langcode=da"></div>

                            <div class="no-video views-row" style="color: rgb(91, 168, 129);">

                    <div class="views-field views-field-title">
                        <span class="field-content">
                            <a href="/laan-penge/hvornaar-kan-jeg-laane-penge-igen" hreflang="da"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                When can I borrow money again
                            </font></font></a>
                        </span>
                    </div>

                    <div class="views-field views-field-field-page-intro-tagline">
                        <div class="field-content">
                            <a href="/laan-penge/hvornaar-kan-jeg-laane-penge-igen" hreflang="da"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Read here to get a clarification of when you can apply for a new loan through Nikopeshe.</font></font></a>
                        </div>
                    </div>

                    <div class="views-field views-field-created">
                        <span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">25-09-2018</font></font></span>
                    </div>

                    <div class="views-field views-field-field-image">
                        <div class="field-content">
                            <a href="/laan-penge/hvornaar-kan-jeg-laane-penge-igen">
                                <img src="/assets/images/articles/3269bbb46fb99fc226be5c25c31d330f.jpg" width="480" height="271" alt="" typeof="foaf:Image">
                            </a>
                        </div>
                    </div>
                </div>
                            <div class="no-video views-row" style="color: rgb(75, 183, 222);">

                    <div class="views-field views-field-title">
                        <span class="field-content">
                            <a href="/laan-penge/hvordan-kan-jeg-forbedre-mine-chancer-for-at-laane-penge" hreflang="da"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                How can I improve my chances of lending ...
                            </font></font></a>
                        </span>
                    </div>

                    <div class="views-field views-field-field-page-intro-tagline">
                        <div class="field-content">
                            <a href="/laan-penge/hvordan-kan-jeg-forbedre-mine-chancer-for-at-laane-penge" hreflang="da"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Are you unsure whether you can be approved for a loan? </font><font style="vertical-align: inherit;">Nikopeshe gives you some suggestions on how to improve your chances of lending money.</font></font></a>
                        </div>
                    </div>

                    <div class="views-field views-field-created">
                        <span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">24-09-2018</font></font></span>
                    </div>

                    <div class="views-field views-field-field-image">
                        <div class="field-content">
                            <a href="/laan-penge/hvordan-kan-jeg-forbedre-mine-chancer-for-at-laane-penge">
                                <img src="/assets/images/articles/2a3148c3794ddd8888dae993dce10fb7.jpg" width="480" height="271" alt="" typeof="foaf:Image">
                            </a>
                        </div>
                    </div>
                </div>
                            <div class="no-video views-row" style="color: rgb(61, 102, 117);">

                    <div class="views-field views-field-title">
                        <span class="field-content">
                            <a href="/laan-penge/faerre-danskere-i-rki" hreflang="da"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                Fewer Danes in RKI
                            </font></font></a>
                        </span>
                    </div>

                    <div class="views-field views-field-field-page-intro-tagline">
                        <div class="field-content">
                            <a href="/laan-penge/faerre-danskere-i-rki" hreflang="da"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">According to a study by Experian in July 2018, the number of Danes registered in the RKI has fallen over the last four years.</font></font></a>
                        </div>
                    </div>

                    <div class="views-field views-field-created">
                        <span class="field-content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">06-09-2018</font></font></span>
                    </div>

                    <div class="views-field views-field-field-image">
                        <div class="field-content">
                            <a href="/laan-penge/faerre-danskere-i-rki">
                                <img src="/assets/images/articles/2bbb1fe81ef8238e55450808eba682fc.jpg" width="480" height="271" alt="" typeof="foaf:Image">
                            </a>
                        </div>
                    </div>
                </div>
                    </div>
    </div>
</div>
                                            </div>
                                            <div class="views-element-container" id="block-views-block-frontpage-faq-block-1">


        <div><div class="js-view-dom-id-d512d19b1c0b5ef774541369892599ce79c14262d3b27fc8a6b91333aeb4c14f">




        <header>
        <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DO YOU WANT TO KNOW MORE?</font></font></h2>
      </header>




        <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How does Nikopeshe work?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p dir="ltr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nikopeshe cooperates with a number of banks, so we can collect and compare offers on loans for you.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loan money by completing one application here on the website, after which Nikopeshe sends the application to the banks that Nikopeshe cooperates with. </font><font style="vertical-align: inherit;">Once they have evaluated your finances, each bank returns an offer.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You can easily and easily follow all offers on Loans on My Page.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Since banks consider your loan application individually, you may want to wait until you have received offers from multiple banks before accepting one of them. </font><font style="vertical-align: inherit;">Thus, you can feel sure that you get the loan that best suits your needs.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">When you accept a loan, complete the process of the bank you have chosen. </font><font style="vertical-align: inherit;">This is because you sign the final loan agreement.</font></font></p></div></div></div>
      <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">What does it cost to use Nikopeshe?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p dir="ltr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">It is completely free and without obligation to use Nikopeshe.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nikopeshe receives a loan offer for you, and if you raise a loan from a bank, Nikopeshe receives a payment directly from the bank to be intermediary between you and the bank - they are happy to get you as a customer.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The offers you receive through Nikopeshe are identical to the offers you may receive if you applied individually to all banks. </font><font style="vertical-align: inherit;">Loan money completely non-binding.</font></font></p></div></div></div>
      <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How long does it take to apply?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p dir="ltr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Typically, it takes 2-3 minutes to complete an application form at Nikopeshe.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nikopeshe asks the same questions for your housing and private economy, as the banks would do if you applied directly to them.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loan money through Nikopeshe, and you save both money by comparing offers on loan and time as you only need to make one application.</font></font></p></div></div></div>
      <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How long does it take before my money is in my account?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">When your application is complete, it typically takes a few hours before we have collected offers for you. </font><font style="vertical-align: inherit;">Sometimes it can go very fast (minutes), sometimes it can take up to 48 hours.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">When you accept an offer and sign the loan agreement with the bank you have chosen, typically 1-2 business days will occur before the money is in your account.</font></font></p></div></div></div>
      <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">What can I borrow money for?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p dir="ltr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Borrow money for exactly what you want; </font><font style="vertical-align: inherit;">renovation of the house, car, boat, travel or something completely different.</font></font></p>

  <p dir="ltr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You also have the opportunity to apply for a collateral loan with a lower interest rate through Nikopeshe.</font></font></p></div></div></div>
      <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">What rate can I get?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p dir="ltr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Your interest rate is determined by each bank based on an assessment of your personal information.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">As each bank assesses your application individually, the offers you receive from banks will have different interest rates - just like if you went down to each bank and asked for a good offer on loans.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">This work makes Nikopeshe for you - borrow money safe and smart through Nikopeshe.</font></font></p></div></div></div>
      <div class="collapsible Lendme views-row catactive show"><div class="views-field views-field-field-question"><div class="field-content wow fadeIn animated" style="visibility: visible;"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">What types of loans can I borrow through Nikopeshe?</font></font></p></div></div><div class="views-field views-field-field-answer"><div class="field-content"><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The banks that Nikopeshe collects loan offers from, offers all </font></font><a href="/laan-penge/forbrugslaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">consumer loans</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> .</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">It can be difficult to keep track of the different types of loans. </font><font style="vertical-align: inherit;">A </font></font><a href="/laan-penge/forbrugslaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">consumer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> loan covers several types of loans. </font><font style="vertical-align: inherit;">We have written more about the different loan types under our </font></font><a href="/laan-penge"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">loan money</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> section.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">If you are looking for a </font></font><a href="/laan-penge/kviklaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">credit loan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> , be aware that it can quickly become very expensive. </font><font style="vertical-align: inherit;">A </font></font><a href="/laan-penge/forbrugslaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mortgage lending</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> is much less about a consumer loan, but often the banks' credit rating is less and the loan is therefore at higher risk. </font><font style="vertical-align: inherit;">At the same time, </font><font style="vertical-align: inherit;">short-term </font></font><a href="/laan-penge/kviklaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">loans</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> have short-term </font><a href="/laan-penge/kviklaan"><font style="vertical-align: inherit;">loans</font></a><font style="vertical-align: inherit;"> . </font><font style="vertical-align: inherit;">This means high interest rates and often pay off to apply for a </font></font><a href="/laan-penge/forbrugslaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">consumer loan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> instead.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">If you are financing a car with a </font></font><a href="/laan-penge/billaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">car loan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> , you can search through Nikopeshe. </font><font style="vertical-align: inherit;">But the smaller the car is brand new and the bank can thus take safety in the car, you must apply for an unsecured </font></font><a href="/laan-penge/billaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">car loan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> . </font><font style="vertical-align: inherit;">The banks you get loan offer from via Nikopeshe, offers all this category of </font></font><a href="/laan-penge/billaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">car loans</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> .</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">A </font></font><a href="/laan-penge/samlelaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">collective loan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> is a term for a loan that you raise to collect more smaller loans in one. </font><font style="vertical-align: inherit;">By </font></font><a href="/laan-penge/samlelaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">collecting loans</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> you can often achieve big savings on the interest rate (OPP). </font><font style="vertical-align: inherit;">This often gives you a lower monthly rate or, alternatively, you can become debt free faster. </font><font style="vertical-align: inherit;">You can also apply for a </font></font><a href="/laan-penge/samlelaan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">collective loan</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> via Nikopeshe.</font></font></p>

  <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">When you apply to </font></font><a href="/laan-penge"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">borrow money</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> via Nikopeshe you will get a rest in your stomach as you know you will find the best loan.</font></font></p></div></div></div>







        <footer>

      </footer>


  </div>
  </div>

    </div>
                                            <div id="block-nyhedsbrev">
                                                <div>
                                                    <h2>
                                                        RECEIVE OUR <span>NEWSLETTER</span>
                                                    </h2>
                                                    <p>We regularly issue newsletters with news and information about economic developments, interest rates, loans, etc. This is relevant both for you who have applied for a loan through Nikopeshe and everyone else who is only interested in private finance.</p>
                                                </div>
                                            </div>
                                            <div id="block-newslettersignup">
                                                <form class="lendme-newsletter-form" data-drupal-selector="lendme-newsletter-form" action="/" method="post" id="lendme-newsletter-form" accept-charset="UTF-8">
                                                    <div id="lendme-newsletter-form-wrapper"></div>
                                                    <div class="js-form-item form-item js-form-type-textfield form-item-email js-form-item-email form-no-label">
                                                        <input placeholder="Enter your e-mail address" data-drupal-selector="edit-email" type="text" id="edit-email--2" name="email" value="" size="60" maxlength="128" class="form-text"/>
                                                    </div>
                                                    <input data-drupal-selector="edit-submit" type="submit" id="edit-submit--2" name="op" value="Send" class="button js-form-submit form-submit"/>
                                                    <input autocomplete="off" data-drupal-selector="form-6eakvpp8rnj82fptiq9s6rbx-1physl91qsmfrxj8" type="hidden" name="form_build_id" value="form-6EAkVPP8rnJ82FpTIq9s6RbX_--1phySl91QSmfRXJ8"/>
                                                    <input data-drupal-selector="edit-lendme-newsletter-form" type="hidden" name="form_id" value="lendme_newsletter_form"/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <footer>
                                        <div>
                                            <div id="block-hardusporgsmal">
                                                <h2>CONTACT US</h2>
                                                <div>
                                                    <div class="contact-us-phone">
                                                        <a href="tel:70606262">70 60 62 62</a>
                                                    </div>
                                                    <p class="pbolder">All weekdays from 10:00 to 16:00</p>
                                                    <p>It must be easy and safe to borrow money through Nikopeshe. Therefore, you are always welcome to call or write to us at customerservice@lendme.dk if you have any questions. We usually respond immediately, but always within one business day.</p>
                                                </div>
                                            </div>
                                            <div class="footermap footermap--footermap_block" id="block-footermap">
                                                <nav class="footermap-col footermap-col--1 footermap-col--footer-menu-1">
                                                    <ul class="footermap-header footermap-header--footer-menu-1">
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="share Mortgages">share Mortgages</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Car loans">Car loans</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Home loans">Home loans</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Ferielån">Ferielån</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Consumer">Consumer</a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                                <nav class="footermap-col footermap-col--2 footermap-col--footer-menu-2">
                                                    <ul class="footermap-header footermap-header--footer-menu-2">
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="payday loans">payday loans</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Borrow money">Borrow money</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="loan Calculator">loan Calculator</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="personal loans">personal loans</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Samlelån">Samlelån</a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                                <nav class="footermap-col footermap-col--3 footermap-col--footer-menu-3">
                                                    <ul class="footermap-header footermap-header--footer-menu-3">
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Customer service">Customer service</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Log in">Log in</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="About Nikopeshe">About Nikopeshe</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Press">Press</a>
                                                        </li>
                                                        <li class="footermap-item footermap-item--depth-1">
                                                            <a href="#" title="Questions and answers">Questions and answers</a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                            <div id="block-footerinformation">
                                                <div>
                                                    <p>
                                                        <img alt="Lendme-logo" src="/themes/lendme/Demo2.png"/>
                                                    </p>
                                                    <address>CARL JACOBSENS VEJ 16, ST, 8 - 2500 VALBY - 70 60 62 62 - INFO@LENDME.DK</address>
                                                </div>
                                            </div>
                                            <div id="block-copyrightfooter">
                                                <p>Copyright © 2018 Nikopeshe. All rights reserved.</p>
                                                <p>
                                                    <a href="/kundeservice/Privacy Policy">Privacy Policy</a>
                                                    <a href="/kundeservice/vilkaar">Terms</a>
                                                </p>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/" class="apply-now-mobile show-on-mobile">Ansøg nu</a>
            </div>
        </div>
        <div class="youtube-video">
            <div class="youtube-video-inner">
                <div class="youtube-close">X</div>
                <!-- <iframe id="youtube-embed-video" width="100%" height="100%" src="https://www.youtube.com/embed/570QvxxIp4k?ecver=1" frameborder="0" allowfullscreen></iframe> -->
            </div>
        </div>
        <div class="spinner">
            <img src="/themes/lendme/images/spinner.svg" alt="Loading">
        </div>
        <!-- <script type="application/json" data-drupal-selector="drupal-settings-json">
            {"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"frontpage","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"da"},"pluralDelimiter":"\u0003","ajaxPageState":{"libraries":"better_messages\/better_messages,core\/html5shiv,core\/jquery.form,footermap\/footermap,formtips\/formtips,formtips\/hoverintent,lendme\/global-scripts,lendme\/global-styling,lendme_basic\/frontpage,lendme_livechat\/livechat,lendme_newsletter\/ajax,lendme_simple_form\/basic_form,lendme_sliders\/sliders,minuba_form\/validation,system\/base,views\/views.module","theme":"lendme","theme_token":null},"ajaxTrustedUrl":{"form_action_p_pvdeGsVG5zNF_XLGPTvYSKCf43t8qZYSwcfZl2uzM":true,"\/frontpage?ajax_form=1":true},"formtips":{"selectors":["formtip"],"interval":500,"sensitivity":3,"timeout":1000,"max_width":"500px","trigger_action":"hover"},"ajax":{"edit-submit--2":{"method":"append","callback":"::respondToAjax","event":"click","progress":{"type":"throbber","message":" "},"url":"\/frontpage?ajax_form=1","dialogType":"ajax","submit":{"_triggering_element_name":"op","_triggering_element_value":"Send"}}},"better_messages":{"jquery_ui":{"draggable":0,"resizable":0},"popin":{"effect":"fadeIn","duration":"slow"},"popout":{"duration":"slow"}},"lendme":{"sliders":{"interestRate":"9","adminCost":"0","establishCost":"5"}},"minuba":{"sliders":{"interestRate":9,"adminCost":0,"establishCost":0}},"user":{"uid":0,"permissionsHash":"c733510859102802bdb7dc87ef0573b6d63021530d43bc15db75a0aa828ccf88"}}
        </script> -->
        <script src="/sites/default/files/js/js_WSy--gIiuKZU4UqPebkUqE2_qfQ6vJX_FO_l18tjz_g.js"></script>
    </body>
@endsection
