<script type="application/ld+json">{
    "@context": "https://schema.org",
    "@graph": [
        {
            "@type": "FinancialService",
            "@id": "https://lendme.dk/#organization",
            "sameAs": [
                "https://www.facebook.com/Lendmedk/",
                "https://www.linkedin.com/company/lendme.dk/",
                "https://www.instagram.com/lendme_dk/",
                "https://twitter.com/lendmedk/"
            ],
            "url": "https://lendme.dk",
            "name": "Lendme",
            "telephone": "70606262",
            "logo": {
                "@type": "ImageObject",
                "representativeOfPage": "True",
                "url": "https://lendme.dk/themes/lendme/images/Lendme-logo.png"
            },
            "image": {
                "@type": "ImageObject",
                "url": "https://lendme.dk/themes/lendme/images/Lendme1.png"
            },
            "priceRange": "Fra 3,49%",
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": "55.654113",
                "longitude": "12.517725"
            },
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Carl Jacobsens Vej 16",
                "addressRegion": "Valby",
                "postalCode": "2500",
                "addressCountry": "Danmark"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "9.6",
                "ratingCount": "720",
                "bestRating": "10"
            }
        },
        {
            "@type": "WebSite",
            "name": "Lendme",
            "url": "https://lendme.dk"
        }
    ]
}</script>

<script type="text/javascript">
  (function(w,d,s,l,i){

    w[l]=w[l]||[];
    w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});
    var f=d.getElementsByTagName(s)[0];
    var j=d.createElement(s);
    var dl=l!='dataLayer'?'&l='+l:'';
    j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;
    j.type='text/javascript';
    j.async=true;
    f.parentNode.insertBefore(j,f);

  })(window,document,'script','dataLayer','GTM-K43BCC');
</script>
<script src="https://wl.lendme.dk/runtime.js?t=1557926820" type="text/javascript" async></script>
<script src="https://wl.lendme.dk/polyfills.js?t=1557926820" type="text/javascript" async></script>
<script src="https://wl.lendme.dk/styles.js?t=1557926820" type="text/javascript" async></script>
<script src="https://wl.lendme.dk/main.js?t=1557926820" type="text/javascript" async></script>


<script>
  window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "position": "bottom-left",
      "content": {
        "message": '<h3>Lendme uses their own cookies and cookies from third parties. These are used to remember your settings, measure traffic and marketing.
</h3><p>Med dit OK giver du samtykke til dette. Læs vores cookie-politik <a href="#" target="blank">her</a></p>',
        "dismiss": "OK"
      },
      "showLink": false,
      cookie: {
        domain: '{{ url('/') }}'
      }
    })
  });
</script>
