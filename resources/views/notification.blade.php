@if (Session::has('loan_success'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('loan_success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('bank_approve'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('bank_approve') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('bank_disable'))
  <div class="alert alert-danger alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('bank_disable') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('agent_enable'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('agent_enable') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('agent_disable'))
  <div class="alert alert-danger alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('agent_disable') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('success'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('success-agent'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('success-agent') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('loan_destroy'))
  <div class="alert alert-warning alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('loan_destroy') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('loan_accept'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('loan_accept') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('package_created'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('package_created') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('package_destroy'))
  <div class="alert alert-warning alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('package_destroy') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('package_update'))
  <div class="alert alert-info alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('package_update') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('application_destroy'))
  <div class="alert alert-danger alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('application_destroy') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(Session::has('application_approve'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
    {{ session('application_approve') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
