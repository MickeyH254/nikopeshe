@extends('layouts.bank_layout')

@section('content')
  @can ('send messages')
    @include('chat.chat')
  @endcan

@endsection
