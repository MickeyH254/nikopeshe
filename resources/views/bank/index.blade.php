@extends('layouts.bank_layout')

@section('content')
  <div class="row">
    <div class="col-md-7 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="clearfix">
            <h4 class="card-title float-left">Visit And Sales Statistics</h4>
            <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div>
          </div>
          <canvas id="line-canvas" class="mt-4"></canvas>
        </div>
      </div>
    </div>
    <div class="col-md-5 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Traffic Sources</h4>
          <canvas id="traffic-chart"></canvas>
          <div id="traffic-chart-legend" class="rounded-legend legend-vertical legend-bottom-left pt-4"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Recently Applied Loans</h4>
          {{-- <div class="table-responsive">
            <table class="table">

              <!--Table head-->
              <thead>
                <tr>
                  <th>
                    <a>Name
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>ID Number

                    </a>
                  </th>
                  <th>
                    <a>Phone Number
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Amount To Be Loaned
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Duration
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                  <th>
                    <a>Logged at
                      <i class="fas fa-sort ml-1"></i>
                    </a>
                  </th>
                </tr>
              </thead>
              <!--Table head-->

              <!--Table body-->
              <tbody>
                <tr>
                  @foreach($loans as $loan)


                  <tr>
                    <td>{{ $loan->users->name }}</td>
                    <td>{{ $loan->users->id_number }}</td>
                    <td>{{ $loan->users->phone_number }}</td>
                    <td>Ksh {{ $loan->loan_value }}</td>
                    <td>{{ $loan->duration }} months</td>
                    {{-- @if($loan->accepted())
                      <td><label class="badge badge-gradient-success">ACCEPTED</label></td>
                    @elseif ($loan->rejected())
                      <td><label class="badge badge-gradient-danger">REJECTED</label></td>
                    @else
                      <td><label class="badge badge-gradient-warning">WATING</label></td>
                    @endif 
                    <td>{{ $loan->created_at }}</td>
                  </tr>


                  @endforeach
                </tr>
              </tbody>
              <!--Table body-->
            </table>

          </div> --}}
        </div>
      </div>
    </div>
  </div>

@endsection

{{-- @extends('layouts.bank_layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as a Bank!
                </div>
            </div>
          </div>
        </div>
      </div>

      @foreach ($user_loans as $user_loan)
        <div class="">
          <li>{{ $user_loan->loan_value }}</li>
          <a href="{{ route('loan_application.show' , ['loan_application' => $user_loan->id ]) }}" class="btn btn-primary">Accept</a>
          <form action="{{ route('loan_application.destroy', [ 'loan_application' => $user_loan->id ]) }}" method="POST">
              {{ method_field('DELETE') }}
              {{ csrf_field() }}
              <button type="submit" class="btn btn-danger">Decline</button>
          </form>

        </div>

      @endforeach



@endsection --}}
