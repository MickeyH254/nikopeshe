@extends('layouts.bank_layout')

@section('content')
  @foreach ($packages as $package)
    {{-- @can('approve loans') --}}
      <ul>
        <a href="#"><li>{{ $package->title }}</li></a>
        @can ('edit packages')
          <a type="button" class="btn btn-info" href="{{ route('packages.edit', ['packages' => $package->id]) }}">Edit</a>
        @endcan
        <form action="{{ route('bank.accept', [ 'loan_application' => $loan_application->id , 'package' => $package->id ]) }}" method="POST">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-success">Accept</button>
        </form>
      </ul>
    {{-- @endcan --}}
  @endforeach
@endsection
