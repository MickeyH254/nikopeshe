@extends('layouts.bank_layout')

@section('content')
  @role('bank')
    <div class="container">
    <form method="POST" action="{{ route('agent.store') }}">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Name</label>
        <input type="text" name="title" class="form-control" id="exampleInputEmail1" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter Title">
      </div>
      <div class="form-group">
        <label for="interest">Interest</label>
        <input type="number" name="interest" class="form-control" value="{{ $my_package->interest }}" placeholder="Enter Interest">
      </div>
      <div class="duration">
        <label for="duration">Duration</label>
        <input type="number" name="duration" value="{{ $my_package->duration }}" class="form-control {{ $errors->has('duration') ? 'is-danger' : '' }}" placeholder="Enter Duration">
      </div>

      <button type="submit" class="btn btn-primary">Create Agent</button>
    </form>

    @include('errors')

    <form action="{{ route('packages.destroy', ['packages' => $my_package->id]) }}" method="post">
      {{ method_field('DELETE') }}
      <button type="submit" class="btn btn-warning">Delete Package</button>
    </form>
  </div>

  @else
    <div class="col-md-8">
        <div class="panel panel-default">
            {{-- <div class="panel-heading">Dashboard</div> --}}

            <div class="panel-body">
                <div class="alert alert-warning">
                    Warning
                </div>
                You do not have the appropriate permissions to access this page
            </div>
        </div>
    </div>
  @endrole
@endsection
