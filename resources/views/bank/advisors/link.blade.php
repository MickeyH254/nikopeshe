@extends('layouts.bank_layout')

@section('content')
  @role('bank')
    <div class="container">
    <form method="POST" action="{{ route('storeAdvisorInvitation') }}">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Email</label>
        <input type="text" name="email" class="form-control" id="exampleInputEmail1" value="{{ old('email') }}" placeholder="Enter Email">
      </div>

      <button type="submit" class="btn btn-primary">Create Advisor</button>
    </form>

    @include('errors')
  @else
    <div class="col-md-8">
        <div class="panel panel-default">
            {{-- <div class="panel-heading">Dashboard</div> --}}

            <div class="panel-body">
                <div class="alert alert-warning">
                    Warning
                </div>
                You do not have the appropriate permissions to access this page
            </div>
        </div>
    </div>
  @endrole
@endsection
