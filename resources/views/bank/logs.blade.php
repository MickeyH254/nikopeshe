@extends('layouts.bank_layout')

@section('content')

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Recently Approved Loans</h4>
                <div class="table-responsive">
                <table class="table" id="example">

                    <!--Table head-->
                    <thead>
                    <tr>
                        <th>
                            Borrower
                        </a>
                        </th>
                        <th>
                            ID Number
                        </th>
                        <th>
                            Phone Number
                        </th>
                        <th>
                            Amount To Be Loaned
                        </th>
                        <th>
                            Duration
                        </th>
                        <th>
                            Offer Status
                        </th>
                        <th>
                            Logged at
                        </th>
                    </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody>
                    <tr>
                        @foreach($logs as $log)


                        <tr>
                          <td>{{ $log->users->name }}</td>
                          <td>{{ $log->users->id_number }}</td>
                          <td>{{ $log->users->phone_number }}</td>
                          <td>Ksh {{ $log->loan_value }}</td>
                          <td>{{ $log->duration }} months</td>
                          @if($log->bank_verdict->status = "Accepted")
                              <td><label class="badge badge-gradient-success">ACCEPTED</label></td>
                          @elseif ($log->bank_verdict->status = "Rejected")
                              <td><label class="badge badge-gradient-danger">REJECTED</label></td>
                          @else
                              <td><label class="badge badge-gradient-warning">WATING</label></td>
                          @endif
                          <td>{{ $log->created_at }}</td>
                        </tr>


                        @endforeach
                    </tr>
                    </tbody>
                    <!--Table body-->
                </table>

                </div>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script> -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                   'excel', 'pdf', 'print'
                ]
            } );
        } );
    </script>
@endsection
