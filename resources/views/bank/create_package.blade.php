@extends('layouts.bank_layout')

@section('content')
  @role('bank')
  <center><title>Create {{ Auth::user()->name }} Package</title></center>
    <div class="container">
      <form method="POST" action="{{ route('packages.store') }}">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" name="title" class="form-control" id="exampleInputEmail1" value="{{ old('title') }}" aria-describedby="emailHelp" placeholder="Enter Title">
        </div>
        <div class="form-group">
          <label for="interest">Interest</label>
          <input type="number" name="interest" class="form-control" value="{{ old('interest') }}" placeholder="Enter Interest">
        </div>
        <div class="duration">
          <label for="duration">Duration</label>
          <input type="number" name="duration" value="{{ old('duration') }}" class="form-control {{ $errors->has('duration') ? 'is-danger' : '' }}" placeholder="Enter Duration">
        </div>
        <div class="form-group">
          <label for="terms_and_conditions">Terms and conditions</label>
          <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>

      @include('errors')
    </div>
  @else
    <div class="col-md-8">
        <div class="panel panel-default">
            {{-- <div class="panel-heading">Dashboard</div> --}}

            <div class="panel-body">
                <div class="alert alert-warning">
                    Warning
                </div>
                You do not have the appropriate permissions to access this page
            </div>
        </div>
    </div>
  @endrole
@endsection
