@extends('layouts.bank_layout')

@section('content')

@if ($user_loans->count() == 0)
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
          Loan Applications
        </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">

          </ol>
        </nav>
      </div>
      <div class="row">
          <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title text-warning">No new Loan Applications</h4>
                <p class="card-description text-info">
                  Currently, there are no new loan applications.
                </p>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endif

@foreach ($user_loans as $user_loan)
  @can ('approve loans')
    <div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
          Loan Applications
        </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">


          </ol>
        </nav>
      </div>
      <div class="row">
          <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Loan Application by {{ $user_loan->users->name }}</h4>
                <p class="card-description">

                </p>
                <div class="template-demo">
                  <h4 class="h4 mb-4">Posted on: {{ $user_loan->created_at }}</h4>

                  <p class="font-weight-normal">{{ $user_loan->users->name }} would like to apply for a loan of Ksh {{ $user_loan->loan_value }} which will be paid in a period of {{ $user_loan->duration }} months</p>
                  <p class="font-weight-normal">ID Number: {{ $user_loan->users->id_number }} Phone Number: {{ $user_loan->users->phone_number }}</p>

                  <a href ="{{ route('loan_application.show', ['loan_application' => $user_loan->id]) }}" class="btn btn-success">Make Offer</a>

                  <form action="{{ route('loan_application.destroy', ['loan_application' => $user_loan->id]) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-warning">Reject</button>
                  </form>

                   {{-- <a href="/bank/reject/{{ $loan->id }}" class="btn btn-success">Reject</a> --}}

                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  @endcan

@endforeach

@endsection

@section('scripts')
  <script>
    $("#my-alerts").fadeTo(2000, 3000).slideUp(3000, function(){
        $("#my-alerts").slideUp(3000);
      });
  </script>

@endsection
