
@extends('home')

@section('css')
  <style>
  body {
  margin:0;
  padding:0;
  display:flex;
  flex-direction:column;
  justify-content:space-between;
  min-height:100vh;
}

main {
  flex:1 0 auto;
}

  </style>
@endsection

@section('content')
  <div class="container">
  <div class="row justify-content-center mt--300">
    <div class="col-lg-8 col-md-12"><br>
      <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div>
          <input type="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} mb-4" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        </div>

        <div>
          <input type="password" id="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} mb-4" placeholder="Password" required>
          @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>

        <div class="d-flex justify-content-between">
            <div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="remember" >
                    <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                </div>
            </div>
            <div>
              @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}">Forgot password?</a>
              @endif
            </div>
        </div>

        <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

          <div class="text-center">
              <p>Not a member?
                  <a href="{{ route('register') }}">Register</a>
              </p>
          </div>

          <div class="text-center">
              <p>Are You a bank?
                  <a href="{{ route('register.bank') }}">Register here</a>
              </p>
          </div>
        </form>
          </div>
        </div>
      </div>


@endsection




{{-- @extends('layouts.users_layout')

@section('content')

<div class="container">
  <div class="row justify-content-center mt--300">
    <div class="col-lg-8 col-md-12"><br>
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <p class="h4 mb-4 text-center">Sign in</p>

        <div>
          <input type="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mb-4" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        </div>

        <div class="">
          <input type="password" id="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} mb-4" placeholder="Password" autocomplete="on" required>
          @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>

        <div class="d-flex justify-content-between">
            <div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="defaultLoginFormRemember">{{ __('Remember Me') }}</label>
                </div>
            </div>
            <div>
              @if (Route::has('password.request'))
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                      {{ __('Forgot Your Password?') }}
                  </a>
              @endif
            </div>
        </div>

        <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

          <div class="text-center">
              <p>Not a member?
                  <a href="{{ route('register') }}">Register</a>
              </p>
          </div>
        </form>
          </div>
        </div>
      </div>

@endsection --}}
