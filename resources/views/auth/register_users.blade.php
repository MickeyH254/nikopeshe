@extends('home')


@section('content')

<div class="container">
  <div class="row justify-content-center mt--300">
    <div class="col-lg-8 col-md-12"><br>
<form method="POST" action="{{ route('register.user') }}">
    @csrf
    <p class="h4 mb-4 text-center">Sign up</p>

    <input type="text" class="form-control disabled{{ $errors->has('name') ? ' is-invalid' : '' }} mb-4" value="{{ $name }}" required placeholder="Name" disabled>
    <input type="text" id="name" type="hidden" class="form-control disabled" name="name" value="{{ $name }}">
    @if ($errors->has('name'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
    <input type="email" class="form-control disabled{{ $errors->has('email') ? ' is-invalid' : '' }} mb-4" value="{{ $email }}" required placeholder="Email" disabled>
    <input id="email" type="hidden" class="form-control disabled" name="email" value="{{ $email }}">
    @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    <input type="number" class="form-control disabled{{ $errors->has('phone_number') ? ' is-invalid' : '' }} mb-4" value="{{ $phone_number }}" required placeholder="Phone Number" disabled>
    <input id="phone_number" type="hidden" class="form-control disabled" name="phone_number" value="{{ $phone_number }}">
    @if ($errors->has('phone_number'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('phone_number') }}</strong>
        </span>
    @endif

    <input type="number" class="form-control disabled{{ $errors->has('id_number') ? ' is-invalid' : '' }} mb-4" value="{{ $id_number }}" required placeholder="Huduma Number" disabled>
    <input id="id_number" type="hidden" class="form-control disabled" name="id_number" value="{{ $id_number }}">
    @if ($errors->has('id_number'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('id_number') }}</strong>
        </span>
    @endif


    <input type="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} mb-4" placeholder="Password" name="password" required>
    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif

    <input type="password" id="password_confirm" class="form-control mb-4" name="password_confirmation" required placeholder="Repeat Password">



    <div class="d-flex justify-content-between">
        <div>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
            </div>
        </div>
    </div>

    <button class="btn btn-info btn-block my-4" type="submit">Register</button>

    <div class="text-center">
        <p>Already a member?
            <a href="{{ route('login')}}">Login</a>
        </p>


    </div>
  </div>
</div>
</div>
</form>


@endsection
