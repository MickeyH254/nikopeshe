@extends('home')

@section('content')


  @if(count($loans) === 0)
  <div class="container">
      <div class="row justify-content-center mt--300">
        <div class="col-lg-8 col-md-12">
          <div class="card border-secondary mb-3" >
            <div class="card-header">You have no Loan applications</div>
            <div class="card-body">
              <h5 class="card-title">Want to Apply for a Loan?</h5>
              <p class="card-text"><a href="{{ route('home')}}">Click Here </a>to apply for a loan</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  @endif
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as a USER!
                </div>
            </div>
          </div>
        </div>
      </div>

      @foreach ($loans as $loan)
        <li>Ksh {{ $loan->loan_value }}</li>
        <a href="{{ route('show_loan_offers', ['loan_application' => $loan->id]) }}" class="btn btn-success" type="submit" name="button">Check Offers</a>

      @endforeach --}}

@foreach($loans as $loan)
<div class="container">
    <div class="row justify-content-center mt--300">
      <div class="col-lg-8 col-md-12">
        <div class="card border-secondary mb-3" >
          <div class="card-header">Loan Application for Ksh {{ $loan->loan_value }}</div>
          <div class="card-body">
            <h5 class="card-title">Application posted on: {{ $loan->created_at }}</h5>
            <p class="card-text">You applied for a loan of Ksh {{$loan->loan_value }} with a repayment period of {{ $loan->duration }} months. Click the link below to see the available offers.</p>
            <a href = "{{ route('show_loan_offers', ['loan_application' =>  $loan->id]) }}" class="btn btn-success" type="submit" name="button">Check Offers</a>
            <!-- <a class="card-link">Another link</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
@endforeach

@endsection

{{--
@section('content')
<br>
<center><h3 class="mb-1">My Loan Applications</h3></center>

@if(Session::has('loan_success'))
  <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
  {{ session('loan_success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if(count($applications) === 0)
<div class="container">
    <div class="row justify-content-center mt--300">
      <div class="col-lg-8 col-md-12">
        <div class="card border-secondary mb-3" >
          <div class="card-header">You have no Loan applications</div>
          <div class="card-body">
            <h5 class="card-title">Want to Apply for a Loan?</h5>
            <p class="card-text"><a href="{{ route('home')}}">Click Here </a>to apply for a loan</p>
          </div>
        </div>
      </div>
    </div>
  </div>

@endif
@foreach($applications as $application)
<div class="container">
    <div class="row justify-content-center mt--300">
      <div class="col-lg-8 col-md-12">
        <div class="card border-secondary mb-3" >
          <div class="card-header">Loan Application</div>
          <div class="card-body">
            <h5 class="card-title">Application posted on: {{ $application->created_at }}</h5>
            <p class="card-text">You applied for a loan of Ksh {{$application->loan_value }} with a repayment period of {{ $application->duration }} months. Click the link below to see offers.</p>
            <a href = "{{ route('loan_offers', ['id' =>  $application->id]) }}" class="card-link">View offers ({{$application->status_count}})</a>
            <!-- <a class="card-link">Another link</a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
@endforeach

@endsection
 --}}
