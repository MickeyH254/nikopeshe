@extends('home')

@section('content')

  @if (Session::has('loan_success'))
    <div class="alert alert-success alert-dismissible fade show" id="my-alerts" role="alert">
      {{ session('loan_success') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif


  <div class="container-fluid text-center">

      <!--Card-->
      <div class="card card-cascade wider reverse my-4 pb-5">

          <!--Card image-->
          <div class="view view-cascade overlay wow fadeIn">
              <img src="/images/intro.jpg" class="img-fluid">
              <a href="#!">
                  <div class="mask rgba-white-slight"></div>
              </a>
          </div>
          <!--/Card image-->

          <!--Card content-->
          <div class="card-body card-body-cascade text-center wow fadeIn" data-wow-delay="0.2s">
              <!--Title-->
              <h4 class="card-title"><strong>Are you tired of all the loan application hustle?</strong></h4>


              <p class="card-text">Here at Nikopeshe we help you get loans faster and efficiently from various banks at the palm of your hands.
              </p>

          </div>
          <!--/.Card content-->

      </div>
      <!--/.Card-->

  </div>

  <!-- Card deck -->
  <div class="card-deck">

    <!-- Card -->
    <div class="card mb-4">

      <!--Card image-->
      <div class="view overlay">
        <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7IGJlkgGRIyf3nLubdT85_fDAr9d5-9kyt0_9MfhdRwHVB5-r" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body">

        <!--Title-->
        <h4 class="card-title">My Loan Applications</h4>
        <!--Text-->
        <p class="card-text">Check up on the status and the offers by the bank of the loans you applied.</p>
        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <a href="{{route('loan_applications')}}">
          <button type="button" class="btn btn-light-blue btn-md">Check out</button>
        </a>
      </div>

    </div>
    <!-- Card -->

    <!-- Card -->
    <div class="card mb-4">

      <!--Card image-->
      <div class="view overlay">
        <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpYKsF0mOfn4pEEi_LD_Wn1aV69Z9nshdTb1Be6Qqp91MlByTd" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body">

        <!--Title-->
        <h4 class="card-title">Financial Advisor</h4>
        <!--Text-->
        <p class="card-text">Check up on the status and the offers by the bank of the loans you applied.</p>
        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <button type="button" class="btn btn-light-blue btn-md">Book Time</button>

      </div>

    </div>
    <!-- Card -->

    <!-- Card -->
    <div class="card mb-4">

      <!--Card image-->
      <div class="view overlay">
        <img class="card-img-top" src="https://digitalsynopsis.com/wp-content/uploads/2017/02/beautiful-color-gradients-backgrounds-006-lady-lips.png" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body">

        <!--Title-->
        <h4 class="card-title">Loan History</h4>
        <!--Text-->
        <p class="card-text">Check up on some of the loans you applied</p>
        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <button type="button" class="btn btn-light-blue btn-md">Read more</button>

      </div>

    </div>
    <!-- Card -->

  </div>
  <!-- Card deck -->

  <div class="card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
        <div class="text-white text-center py-5 px-4 my-5">
          <div>
            <h2 class="display-3 text-white">Want a Loan?</h2>
            <p class="lead text-white">Apply for a loan now to the various banks in Kenya with ease.</p>
            <div class="container" style="padding: auto">
              <div class="row justify-content-center mt--300">

              <div class="col-lg-6 col-md-12">
              <center>
  <div class="card" >


      <div class="card-body" id="loan_apply">

          <form action="{{ route('loan_application.store') }}" method="post">
             {{ csrf_field() }}
              <!-- Material input email -->
              <!-- <div class="md-form">

                  <input class="form-control" required placeholder="Loan Amount" name="loan_amount" type="number">
                  <label for="materialFormCardEmailEx" class="font-weight-light">Amount </label>
              </div> -->

              <!-- Material input email -->

              <div class="slidecontainer">
                <!-- <input type="range" min="10000" max="1000000" value="10000" class="slider" id="myRange"> -->
                <label for="customRange1" class="font-weight-light" style="color: black">Loan Amount</label>
                <input type="range" min="10000" max="1000000" value="10000" step="10000" name="loan_value" class="custom-range" id="customRange1">
                <p style="color: black">Value: Ksh <span id="demo"></span></p>
              </div>

              <div class="slidecontainer">
                <!-- <input type="range" min="10000" max="1000000" value="10000" class="slider" id="myRange"> -->
                <label for="customRange2" style="color: black">Duration to be Paid</label>
                <input type="range" min="6" max="24" value="6" name="duration" class="custom-range" id="customRange2">
                <p style="color: black">Value: <span id="demo2"></span> months</p>
              </div>

              <div class="">
                <p style="color: red">Loan Estimate: Ksh <span id="loan_estimate"></span></p>
              </div>
              @can ('create loans')
                <div class="text-center py-4 mt-3">
                    <button type="submit" class="btn btn-cyan">Apply for loan</button>
                </div>
              @endcan

          </form>


      </div>
      <!-- Card body -->

  </div>
  <!-- Card -->
  </center>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>

  <hr class="my-5">

  <section>

                  <!-- Heading -->
                  <h2 class="mb-5 font-weight-bold text-center">Better Way to get Loans</h2>

                  <!--Grid row-->
                  <div class="row">

                      <!--Grid column-->
                      <div class="col-md-6 mb-4">

                          <!--Carousel Wrapper-->
                          <div id="carousel-example-1z" class="carousel slide carousel-fade carousel-fade" data-ride="carousel">
                              <!--Indicators-->
                              <ol class="carousel-indicators">
                                  <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                                  <li data-target="#carousel-example-1z" data-slide-to="1" class=""></li>
                                  <li data-target="#carousel-example-1z" data-slide-to="2" class=""></li>
                              </ol>
                              <!--/.Indicators-->
                              <!--Slides-->
                              <div class="carousel-inner z-depth-1-half" role="listbox">
                                  <!--First slide-->
                                  <div class="carousel-item active">
                                      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(130).jpg" alt="First slide">
                                  </div>
                                  <!--/First slide-->
                                  <!--Second slide-->
                                  <div class="carousel-item">
                                      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(129).jpg" alt="Second slide">
                                  </div>
                                  <!--/Second slide-->
                                  <!--Third slide-->
                                  <div class="carousel-item">
                                      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Third slide">
                                  </div>
                                  <!--/Third slide-->
                              </div>
                              <!--/.Slides-->
                              <!--Controls-->
                              <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                              </a>
                              <!--/.Controls-->
                          </div>
                          <!--/.Carousel Wrapper-->

                      </div>
                      <!--Grid column-->

                      <!--Grid column-->
                      <div class="col-md-6">

                          <!--Excerpt-->
                          <a href="" class="teal-text">
                              <h6 class="pb-1"><i class="fa fa-heart"></i><strong></strong></h6>
                          </a>
                          <h4 class="mb-3"><strong>We are Here to Help</strong></h4>
                          {{-- <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id
                              quod maxime
                              placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus et aut
                              officiis
                              debitis aut rerum.</p>

                          <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id
                              quod maxime
                              placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus et aut
                              officiis
                              debitis aut rerum.</p> --}}


                      </div>
                      <!--Grid column-->

                  </div>
                  <!--Grid row-->

              </section>

            <hr class="my-5">
  <section id="contact">

      <!-- Heading -->
      <h2 class="mb-5 font-weight-bold text-center">Contact us</h2>

      <!--Grid row-->
      <div class="row">

          <!--Grid column-->
          <div class="col-lg-5 col-md-12">
              <!-- Form contact -->
              <form class="p-5 grey-text">
                  <div class="md-form form-sm"> <i class="fa fa-user prefix"></i>
                      <input type="text" id="form3" value="@auth {{ Auth::user()->name }} @endauth " class="form-control form-control-sm">
                      <label for="form3">Your name</label>
                  </div>
                  <div class="md-form form-sm"> <i class="fa fa-envelope prefix"></i>
                      <input type="text" id="form2" value="@auth {{ Auth::user()->email }} @endauth" class="form-control form-control-sm">
                      <label for="form2">Your email</label>
                  </div>
                  <div class="md-form form-sm"> <i class="fa fa-tag prefix"></i>
                      <input type="text" id="form32" class="form-control form-control-sm">
                      <label for="form34">Subject</label>
                  </div>
                  <div class="md-form form-sm"> <i class="fa fa-pencil prefix"></i>
                      <textarea type="text" id="form8" class="md-textarea form-control form-control-sm" rows="4"></textarea>
                      <label for="form8">Your message</label>
                  </div>
                  <div class="text-center mt-4">
                      <button class="btn btn-primary">Send <i class="fa fa-paper-plane-o ml-1"></i></button>
                  </div>
              </form>
              <!-- Form contact -->
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-7 col-md-12">

              <!--Grid row-->
              <div class="row text-center">

                  <!--Grid column-->
                  <div class="col-lg-4 col-md-12 mb-3">

                      <p><i class="fa fa-map fa-1x mr-2 grey-text"></i>Nairobi, Madaraka</p>

                  </div>
                  <!--Grid column-->

                  <!--Grid column-->
                  <div class="col-lg-4 col-md-6 mb-3">

                      <p><i class="fa fa-building fa-1x mr-2 grey-text"></i>Mon - Fri, 8:00-17:00</p>

                  </div>
                  <!--Grid column-->

                  <!--Grid column-->
                  <div class="col-lg-4 col-md-6 mb-3">

                      <p><i class="fa fa-phone fa-1x mr-2 grey-text"></i>+2547123456789</p>

                  </div>
                  <!--Grid column-->

              </div>
              <!--Grid row-->

              <!--Google map-->
              <div id="map-container" class="z-depth-1-half map-container mb-5" style="height: 400px"></div>

          </div>
          <!--Grid column-->

      </div>
      <!--Grid row-->

  </section>

@endsection

@section('js')
  <script>
    var slider = document.getElementById("customRange1");
    var slider2 = document.getElementById("customRange2");
    var output = document.getElementById("demo");
    var output2 = document.getElementById("demo2");
    var loan_estimate = document.getElementById("loan_estimate");

    output.innerHTML = slider.value;
    output2.innerHTML = slider2.value;

    slider.oninput = function() {
      output.innerHTML = this.value;
    }

    slider2.oninput = function() {
      output2.innerHTML = this.value;
    }


  </script>
@endsection
