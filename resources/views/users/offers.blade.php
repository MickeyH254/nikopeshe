@extends('home')

{{-- @section('content')
  @foreach ($offers as $offer)
    <li>{{ $offer->package_id }}</li>
  @endforeach

@endsection --}}

@section('content')
<br>
<center><h3 class="mb-1">Bank Offers</h3></center>
<div class="row" style="margin-top:40px;">
@if(count($offers) == 0)
  <div class="col-lg-2"></div>
    <div class="col-lg-8">
      <div class="list-group">
        <a  class="list-group-item list-group-item-action flex-column align-items-start active">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">No Offers Yet</h5>
            <small></small>
          </div>
          <p class="mb-1">No Bank has made an offer yet. Please be patient as the banks review your application. All applications expire in a period in 21 days.</p>
          <small></small>
        </a>
      </div>
    </div>
    <div class="col-lg-2"></div><br>
@endif

@foreach($offers as $offer)
  {{-- @if($offer->statusy->user_status == null) --}}
@if($offer->user_status == null)
  <div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
      <!--Content-->
      <div class="modal-content text-center">
        <!--Header-->
        <div class="modal-header d-flex justify-content-center">
          <p class="heading">Are you sure you want to reject the offer?</p>
        </div>

        <!--Body-->
        <div class="modal-body">

          <i class="fas fa-times fa-4x animated rotateIn"></i>

        </div>

        <!--Footer-->
        <div class="modal-footer flex-center">
          <form action="{{ route('offer_rejected', ['bank_verdict' => $offer->id]) }}" method="post">
            {{ csrf_field() }}
            <button type="submit" class="btn  btn-outline-danger">Yes</button>
          </form>
          <a type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</a>
        </div>
      </div>
      <!--/.Content-->
    </div>
  </div>
  <!--Modal: modalConfirmDelete-->

  <div class="col-lg-2"></div>
    <div class="col-lg-8">
      <div class="list-group">
        <a  class="list-group-item list-group-item-action flex-column align-items-start active">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Loan Offer</h5>
            <small>Offer posted on: {{ $offer->created_at }}</small>
          </div>
          <p class="mb-1">{{ $offer->users->name }} Bank has {{ $offer->status }} you a package with a {{ $offer->package->title }} interest rate {{ $offer->package->interest_rate }}</p>
          <small></small>

          <div>
            <a class="btn btn-primary" data-toggle="collapse" href="#details_collapse" aria-expanded="false" aria-controls="collapseExample">
              More details
            </a>
          </div>

          <div class="collapse" id="details_collapse">
            <div class="mt-3">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalConfirmDelete">Reject</button>

          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-2"></div><br>
  @endif
  {{-- @endif --}}
  @endforeach

    {{-- <div class="col-md-4">
    <div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
<div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
<div class="modal-content bg-gradient-danger" id="smth">


</div>
</div>
</div>
</div>
</div>

<script>
         function getMessage(id) {
            $.ajax({
               type:'get',
               url:'/getoffer/'+id,
               success:function(data) {
                 console.log(data);
                 $("#smth").empty();
                  $("#smth").append(data);
               }
            });
         }
      </script> --}}
@endsection
