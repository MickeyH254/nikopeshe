@extends('home')

@section('content')
  <hr class="my-5">
<section id="contact">

<!-- Heading -->
<h2 class="mb-5 font-weight-bold text-center">My Profile</h2>

<!--Grid row-->
<div class="row">

<!--Grid column-->
<div class="col-lg-6 col-md-12">
    <!-- Form contact -->
    <form class="p-5 grey-text" method="POST" action="{{ route('user.update') }}">
      {{ csrf_field() }}
      {{ method_field('PATCH') }}
        <div class="md-form form-sm"> <i class="fa fa-user prefix"></i>
            <input type="text" id="form3" name="name" value="@auth {{ Auth::user()->name }} @endauth " class="form-control form-control-sm {{ $errors->has('name') ? 'is-danger' : '' }}" required>
            <label for="form3">Your name</label>
        </div>
        <div class="md-form form-sm"> <i class="fa fa-envelope prefix"></i>
            <input type="text" id="form2" name="email" value="@auth {{ Auth::user()->email }} @endauth" class="form-control form-control-sm {{ $errors->has('email') ? 'is-danger' : '' }}" required>
            <label for="form2">Your email</label>
        </div>
        <div class="md-form form-sm"> <i class="fa fa-tag prefix"></i>
            <input type="text" id="form32" name="phone_number" value="{{ Auth::user()->phone_number }}" class="form-control form-control-sm {{ $errors->has('phone_number') ? 'is-danger' : '' }}" required>
            <label for="form34">Your Phone Number</label>
        </div>
        <div class="md-form form-sm"> <i class="fa fa-tag prefix"></i>
            <input type="text" id="form32" name="id_number" value="{{ Auth::user()->id_number }}" class="form-control form-control-sm {{ $errors->has('id_number') ? 'is-danger' : '' }}" required>
            <label for="form34">Your ID Number</label>
        </div>
        <div class="text-center mt-4">
            <button class="btn btn-primary">Update Profile <i class="fa fa-paper-plane-o ml-1"></i></button>
        </div>
    </form>
    <!-- Form contact -->

    @include('errors')
</div>
<!--Grid column-->


</div>
<!--Grid row-->

</section>

@endsection
