@component('mail::message')
# Registration Link

Click the button below to complete your registration.

@component('mail::button', $invitation->getLink())
Register
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
