<!-- Navbar -->
<nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
    <!-- SideNav slide-out button -->
    <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
    </div>
    <!-- Breadcrumb-->
    <div class="breadcrumb-dn mr-auto">
      <a href="{{ route('home') }}">
        <p>Nikopeshe</p>
      </a>
    </div>
    @auth
      <ul class="nav navbar-nav nav-flex-icons ml-auto">
        <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ Auth::user()->name }}
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="{{ route('user.profile', ['user' => Auth::user()->id]) }}">Profile</a>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Logout</a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      </ul>

    @else
      <ul class="nav navbar-nav nav-flex-icons ml-auto">
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-user-lock"></i> <span class="clearfix d-none d-sm-inline-block">Login</span></a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}"><i class="far fa-user-plus"></i> <span class="clearfix d-none d-sm-inline-block">Register</span></a>
          </li>
      </ul>
    @endauth

</nav>
<!-- /.Navbar -->
