<!-- Sidebar navigation -->
<div id="slide-out" class="side-nav sn-bg-4 fixed">
    <ul class="custom-scrollbar">
        <!-- Logo -->
        <li>
            <div class="logo-wrapper waves-light">
                <a href="#"><img src="#" class="img-fluid flex-center"></a>
            </div>
        </li>
        <!--/. Logo -->
        <!--Social-->
        <li>
            <ul class="social">
                <li><a href="#" class="icons-sm fb-ic"><i class="fab fa-facebook-f"> </i></a></li>
                <li><a href="#" class="icons-sm pin-ic"><i class="fab fa-pinterest"> </i></a></li>
                <li><a href="#" class="icons-sm gplus-ic"><i class="fab fa-google-plus-g"> </i></a></li>
                <li><a href="#" class="icons-sm tw-ic"><i class="fab fa-twitter"> </i></a></li>
            </ul>
        </li>
        <!--/Social-->
        <!-- Side navigation links -->

        @role('user')
          <li>
            <ul class="collapsible collapsible-accordion">
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fas fa-chevron-right"></i>My Loan Applications<i class="fas fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul class="list-unstyled">
                            <li><a href="{{ route('loan_applications') }}" class="waves-effect">Check Offers</a>
                            </li>
                            <li><a href="#" class="waves-effect">My History</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r"><i class="far fa-hand-pointer"></i>Financial Advisors<i class="fas fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul class="list-unstyled">
                            <li><a href="{{ route('chat.home') }}" class="waves-effect">Chat With</a>
                            </li>
                            <li><a href="#" class="waves-effect">Video chat</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li><a class="collapsible-header waves-effect arrow-r"><i class="far fa-envelope"></i> Contact me<i class="fas fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul class="list-unstyled">
                            <li><a href="#" class="waves-effect">FAQ</a>
                            </li>
                            <li><a href="#" class="waves-effect">Write a message</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        @else
          <li>
            <ul class="collapsible collapsible-accordion">
                <li style="margin-left: 40px">
                  <a href="{{ route('login') }}">
                    <button class="btn btn-primary btn-md waves-effect waves-light">Login</button>
                  </a>
                </li>

                <li style="margin-top: 20px; margin-left: 40px">
                  <a href="{{ route('register') }}">
                    <button class="btn btn-primary btn-md waves-effect waves-light">Register</button>
                  </a>
                </li>
              </ul>
            </li>
        @endrole

        <!--/. Side navigation links -->
    </ul>
    <div class="sidenav-bg mask-strong"></div>
</div>
<!--/. Sidebar navigation -->
