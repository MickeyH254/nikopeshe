<!--Footer-->
<footer class="page-footer text-center text-md-left pt-4">

    <!--Social buttons-->
    <div class="social-section text-center">
        <ul class="list-unstyled list-inline">
            <li class="list-inline-item"><a class="btn-floating btn-fb"><i class="fab fa-facebook-f"> </i></a></li>
            <li class="list-inline-item"><a class="btn-floating btn-tw"><i class="fab fa-twitter"> </i></a></li>
            <li class="list-inline-item"><a class="btn-floating btn-gplus"><i class="fab fa-google-plus-g"> </i></a></li>
            <li class="list-inline-item"><a class="btn-floating btn-li"><i class="fab fa-linkedin-in"> </i></a></li>
            <li class="list-inline-item"><a class="btn-floating btn-git"><i class="fab fa-github"> </i></a></li>
        </ul>
    </div>
    <!--/.Social buttons-->

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
            © {{ Date('Y') }} Copyright: <a href="#"> Nikopeshe </a>

        </div>
    </div>
    <!--/.Copyright-->

</footer>
