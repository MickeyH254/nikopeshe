/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');


import VueChatScroll from 'vue-chat-scroll'
import VueTimeago from 'vue-timeago';
import jQuery from 'jquery';
var $ = jQuery;
window.$ = $;

import GroupChatRoom from './components/laravel-video-chat/GroupChatRoom.vue';
import ChatRoom from './components/laravel-video-chat/ChatRoom.vue';
import VideoSection from './components/laravel-video-chat/VideoSection.vue';


window.Vue = require('vue');



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.use(VueChatScroll);

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('chat-room' , ChatRoom);
Vue.component('group-chat-room', GroupChatRoom);
Vue.component('video-section' , VideoSection);
Vue.use(VueTimeago, {
    name: 'timeago', // component name, `timeago` by default
    locale: 'en',
    locales: {
        // you will need json-loader in webpack 1
        // 'en-US': require('vue-timeago/locales/en-US.json')
        ja: require('date-fns/locale/ja')
    }
})


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
